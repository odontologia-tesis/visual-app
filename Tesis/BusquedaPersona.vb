﻿Imports System.Data.SQLite
Module Conexiones
    Dim con1 As SQLiteConnection = New SQLiteConnection("Data Source=C:\Program Files (x86)\Tesis\Tesis\na-na-na-na-na-na-na-na-na-liderrr.db; Version=3;")
    Dim DGTabla As DataGridView
    Dim TxtEscribiendo As TextBox
    'Creamos el procedimiento para cargar las bases de datos



    Sub Cargar_SQLite(ByVal Sql As String, ByVal Tabla As DataGridView)
        ' Creamos nuestro objeto SQLiteDataAdapte, el cual recibe dos parametros
        'La conexion y la consulta SQL
        con1.Open()
        Dim Da = New SQLiteDataAdapter(Sql, con1)
        ':::Creamos nuestro DataTable para almacenar el resultado
        Dim Dt = New DataTable
        ':::Llenamos nuestro DataTable con el resultado de la consulta
        Da.Fill(Dt)
        ':::Asignamos a nuestro DataGridView el DataTable para mostrar los registros
        Tabla.DataSource = Dt
        ':::Damos color a las filas y formato a la columna hora
        Tabla.RowsDefaultCellStyle.BackColor = Color.LightBlue
        Tabla.AlternatingRowsDefaultCellStyle.BackColor = Color.White
        DGTabla.DataSource = Dt
        Tabla.Columns("Nombre").DefaultCellStyle.Format = "hh:mm:ss tt"
        con1.Dispose()
    End Sub
    Sub Filtro_Campo_Escribiendo_SQLite()
        con1.Open()
        Dim sql As String = "Select * from Persona where NumDocumento Like '%@" & TxtEscribiendo.Text & "%'"
        Cargar_SQLite(sql, DGTabla)
        con1.Dispose()
    End Sub
End Module
Public Class BusquedaPersona
    Private Sub TxtEscribiendo_TextChanged(sender As Object, e As EventArgs) Handles txtEscribiendo.TextChanged
        ':::Validamos si se ha seleccionado la base de datos SQLite
        Filtro_Campo_Escribiendo_SQLite()
    End Sub
End Class

