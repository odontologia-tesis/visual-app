﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SeleccionCuadradito
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SeleccionCuadradito))
        Me.ImagenAhora = New System.Windows.Forms.Panel()
        Me.ListBoxIzquierda = New System.Windows.Forms.ListBox()
        Me.ListBoxArriba = New System.Windows.Forms.ListBox()
        Me.ListBoxDerecha = New System.Windows.Forms.ListBox()
        Me.ListBoxAbajo = New System.Windows.Forms.ListBox()
        Me.ListBoxCentro = New System.Windows.Forms.ListBox()
        Me.ListBoxEspecial = New System.Windows.Forms.ListBox()
        Me.ImagenAntes = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BotonGuardar = New System.Windows.Forms.Button()
        Me.BotonCancelar = New System.Windows.Forms.Button()
        Me.PanelIzquierda = New System.Windows.Forms.Panel()
        Me.PanelArriba = New System.Windows.Forms.Panel()
        Me.PanelAbajo = New System.Windows.Forms.Panel()
        Me.PanelDerecha = New System.Windows.Forms.Panel()
        Me.PanelEspecial = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lbl_elemento = New System.Windows.Forms.Label()
        Me.PanelIzquierda.SuspendLayout()
        Me.PanelArriba.SuspendLayout()
        Me.PanelAbajo.SuspendLayout()
        Me.PanelDerecha.SuspendLayout()
        Me.PanelEspecial.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ImagenAhora
        '
        Me.ImagenAhora.BackColor = System.Drawing.Color.Black
        resources.ApplyResources(Me.ImagenAhora, "ImagenAhora")
        Me.ImagenAhora.Name = "ImagenAhora"
        '
        'ListBoxIzquierda
        '
        Me.ListBoxIzquierda.FormattingEnabled = True
        resources.ApplyResources(Me.ListBoxIzquierda, "ListBoxIzquierda")
        Me.ListBoxIzquierda.Name = "ListBoxIzquierda"
        '
        'ListBoxArriba
        '
        Me.ListBoxArriba.FormattingEnabled = True
        resources.ApplyResources(Me.ListBoxArriba, "ListBoxArriba")
        Me.ListBoxArriba.Name = "ListBoxArriba"
        '
        'ListBoxDerecha
        '
        Me.ListBoxDerecha.FormattingEnabled = True
        resources.ApplyResources(Me.ListBoxDerecha, "ListBoxDerecha")
        Me.ListBoxDerecha.Name = "ListBoxDerecha"
        '
        'ListBoxAbajo
        '
        Me.ListBoxAbajo.FormattingEnabled = True
        resources.ApplyResources(Me.ListBoxAbajo, "ListBoxAbajo")
        Me.ListBoxAbajo.Name = "ListBoxAbajo"
        '
        'ListBoxCentro
        '
        Me.ListBoxCentro.FormattingEnabled = True
        resources.ApplyResources(Me.ListBoxCentro, "ListBoxCentro")
        Me.ListBoxCentro.Name = "ListBoxCentro"
        '
        'ListBoxEspecial
        '
        Me.ListBoxEspecial.FormattingEnabled = True
        resources.ApplyResources(Me.ListBoxEspecial, "ListBoxEspecial")
        Me.ListBoxEspecial.Name = "ListBoxEspecial"
        '
        'ImagenAntes
        '
        Me.ImagenAntes.BackColor = System.Drawing.Color.Black
        resources.ApplyResources(Me.ImagenAntes, "ImagenAntes")
        Me.ImagenAntes.Name = "ImagenAntes"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'BotonGuardar
        '
        resources.ApplyResources(Me.BotonGuardar, "BotonGuardar")
        Me.BotonGuardar.Name = "BotonGuardar"
        Me.BotonGuardar.UseVisualStyleBackColor = True
        '
        'BotonCancelar
        '
        resources.ApplyResources(Me.BotonCancelar, "BotonCancelar")
        Me.BotonCancelar.Name = "BotonCancelar"
        Me.BotonCancelar.UseVisualStyleBackColor = True
        '
        'PanelIzquierda
        '
        Me.PanelIzquierda.BackColor = System.Drawing.Color.Fuchsia
        resources.ApplyResources(Me.PanelIzquierda, "PanelIzquierda")
        Me.PanelIzquierda.Controls.Add(Me.ListBoxIzquierda)
        Me.PanelIzquierda.Name = "PanelIzquierda"
        '
        'PanelArriba
        '
        Me.PanelArriba.BackColor = System.Drawing.Color.Fuchsia
        resources.ApplyResources(Me.PanelArriba, "PanelArriba")
        Me.PanelArriba.Controls.Add(Me.ListBoxArriba)
        Me.PanelArriba.Name = "PanelArriba"
        '
        'PanelAbajo
        '
        Me.PanelAbajo.BackColor = System.Drawing.Color.Fuchsia
        resources.ApplyResources(Me.PanelAbajo, "PanelAbajo")
        Me.PanelAbajo.Controls.Add(Me.ListBoxAbajo)
        Me.PanelAbajo.Name = "PanelAbajo"
        '
        'PanelDerecha
        '
        Me.PanelDerecha.BackColor = System.Drawing.Color.Fuchsia
        resources.ApplyResources(Me.PanelDerecha, "PanelDerecha")
        Me.PanelDerecha.Controls.Add(Me.ListBoxDerecha)
        Me.PanelDerecha.Name = "PanelDerecha"
        '
        'PanelEspecial
        '
        Me.PanelEspecial.BackColor = System.Drawing.Color.Fuchsia
        Me.PanelEspecial.Controls.Add(Me.ListBoxEspecial)
        resources.ApplyResources(Me.PanelEspecial, "PanelEspecial")
        Me.PanelEspecial.Name = "PanelEspecial"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lbl_elemento)
        Me.Panel1.Controls.Add(Me.PanelIzquierda)
        Me.Panel1.Controls.Add(Me.PanelArriba)
        Me.Panel1.Controls.Add(Me.PanelDerecha)
        Me.Panel1.Controls.Add(Me.PanelAbajo)
        Me.Panel1.Controls.Add(Me.ListBoxCentro)
        Me.Panel1.Controls.Add(Me.PanelEspecial)
        Me.Panel1.Controls.Add(Me.BotonCancelar)
        Me.Panel1.Controls.Add(Me.BotonGuardar)
        Me.Panel1.Controls.Add(Me.ImagenAntes)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.ImagenAhora)
        resources.ApplyResources(Me.Panel1, "Panel1")
        Me.Panel1.Name = "Panel1"
        '
        'lbl_elemento
        '
        resources.ApplyResources(Me.lbl_elemento, "lbl_elemento")
        Me.lbl_elemento.Name = "lbl_elemento"
        '
        'SeleccionCuadradito
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Panel1)
        Me.Name = "SeleccionCuadradito"
        Me.PanelIzquierda.ResumeLayout(False)
        Me.PanelArriba.ResumeLayout(False)
        Me.PanelAbajo.ResumeLayout(False)
        Me.PanelDerecha.ResumeLayout(False)
        Me.PanelEspecial.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ImagenAhora As Panel
    Friend WithEvents ListBoxIzquierda As ListBox
    Friend WithEvents ListBoxArriba As ListBox
    Friend WithEvents ListBoxDerecha As ListBox
    Friend WithEvents ListBoxAbajo As ListBox
    Friend WithEvents ListBoxCentro As ListBox
    Friend WithEvents ListBoxEspecial As ListBox
    Friend WithEvents ImagenAntes As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents BotonGuardar As Button
    Friend WithEvents BotonCancelar As Button
    Friend WithEvents PanelIzquierda As Panel
    Friend WithEvents PanelArriba As Panel
    Friend WithEvents PanelAbajo As Panel
    Friend WithEvents PanelEspecial As Panel
    Friend WithEvents Panel1 As Panel
    Public WithEvents PanelDerecha As Panel
    Friend WithEvents lbl_elemento As Label
End Class
