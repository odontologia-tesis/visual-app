﻿Imports System.Data.SQLite

Public Class Paciente
    Private _IdConsulta As Integer
    Private _IdPersona As Integer
    Private _IdObraSocial As Integer
    Private _IdEstadoCivil As Integer
    Private _IdDomicilio As Integer
    Private _IdCuestionario As Integer
    Private _IdHCO As Integer
    Private _IdIntensidadDolor As Integer
    Private _IdDuracionDolor As Integer
    Private _IdEstimuloDolor As Integer
    Private _IdDificultad As Integer
    Private _IdAnormalidad As Integer
    Private _IdLesiones As Integer
    Private _IdCuadraditos As Integer
    Private _IdOdontologo As Integer
    Private _IdConsultorio As Integer
    Private _IdEstadoBucal As Integer


    Public Property IdConsulta() As Integer
        Get
            Return _IdConsulta
        End Get
        Set(ByVal value As Integer)
            _IdConsulta = value
        End Set
    End Property
    Public Property IdPersona() As Integer
        Get
            Return _IdPersona
        End Get
        Set(ByVal value As Integer)
            _IdPersona = value
        End Set
    End Property
    Public Property IdObraSocial() As Integer
        Get
            Return _IdObraSocial
        End Get
        Set(ByVal value As Integer)
            _IdObraSocial = value
        End Set
    End Property
    Property IdEstadoCivil() As Integer
        Get
            Return _IdEstadoCivil
        End Get
        Set(ByVal value As Integer)
            _IdEstadoCivil = value
        End Set
    End Property
    Public Property IdDomicilio() As Integer
        Get
            Return _IdDomicilio
        End Get
        Set(ByVal value As Integer)
            _IdDomicilio = value
        End Set
    End Property
    Public Property IdCuestionario() As Integer
        Get
            Return _IdCuestionario
        End Get
        Set(ByVal value As Integer)
            _IdCuestionario = value
        End Set
    End Property
    Public Property IdHCO() As Integer
        Get
            Return _IdHCO
        End Get
        Set(ByVal value As Integer)
            _IdHCO = value
        End Set
    End Property
    Public Property IdIntensidadDolor() As Integer
        Get
            Return _IdIntensidadDolor
        End Get
        Set(ByVal value As Integer)
            _IdIntensidadDolor = value
        End Set
    End Property
    Public Property IdDuracionDolor() As Integer
        Get
            Return _IdDuracionDolor
        End Get
        Set(ByVal value As Integer)
            _IdDuracionDolor = value
        End Set
    End Property
    Public Property IdEstimuloDolor() As Integer
        Get
            Return _IdEstimuloDolor
        End Get
        Set(ByVal value As Integer)
            _IdEstimuloDolor = value
        End Set
    End Property
    Public Property IdDificultad() As Integer
        Get
            Return _IdDificultad
        End Get
        Set(ByVal value As Integer)
            _IdDificultad = value
        End Set
    End Property
    Public Property IdAnormalidad() As Integer
        Get
            Return _IdAnormalidad
        End Get
        Set(ByVal value As Integer)
            _IdAnormalidad = value
        End Set
    End Property
    Public Property IdLesiones() As Integer
        Get
            Return _IdLesiones
        End Get
        Set(ByVal value As Integer)
            _IdLesiones = value
        End Set
    End Property
    Public Property IdCuadraditos() As Integer
        Get
            Return _IdCuadraditos
        End Get
        Set(ByVal value As Integer)
            _IdCuadraditos = value
        End Set
    End Property
    Public Property IdOdontologo() As Integer
        Get
            Return _IdOdontologo
        End Get
        Set(ByVal value As Integer)
            _IdOdontologo = value
        End Set
    End Property
    Public Property IdConsultorio() As Integer
        Get
            Return _IdConsultorio
        End Get
        Set(ByVal value As Integer)
            _IdConsultorio = value
        End Set
    End Property
    Public Property IdEstadoBucal() As Integer
        Get
            Return _IdEstadoBucal
        End Get
        Set(ByVal value As Integer)
            _IdEstadoBucal = value
        End Set
    End Property

    '----------------------------------------Consulta

    '       _IdConsulta
    '       _IdPersona
    '       _IdOdontologo
    Private _Fecha As Date
    '       _IdCuestionario
    '       _IdConsultorio
    '       _IdEstadoBucal

    Public Property Fecha() As Date
        Get
            Return _Fecha
        End Get
        Set(ByVal value As Date)
            _Fecha = value
        End Set
    End Property



    '----------------------------------------Persona

    '       _IdPersona
    Private _Nombre As String
    Private _Apellido As String
    Private _NumAfiliado As Integer
    '       _IdObraSocial
    Private _FechaNacimiento As Date
    Private _Telefono As String
    Private _Celular As String
    Private _Edad As Integer
    '       _IdEstadoCivil
    Private _Nacionalidad As String
    Private _NumDocumento As Integer
    '       _IdEstadoCivil
    Private _ProfesionActividad As String
    Private _Titular As String
    Private _LugarTrabajo As String

    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property
    Public Property Apellido() As String
        Get
            Return _Apellido
        End Get
        Set(ByVal value As String)
            _Apellido = value
        End Set
    End Property
    Public Property NumAfiliado() As Integer
        Get
            Return _NumAfiliado
        End Get
        Set(ByVal value As Integer)
            _NumAfiliado = value
        End Set
    End Property
    Public Property FechaNacimiento() As Date
        Get
            Return _FechaNacimiento
        End Get
        Set(ByVal value As Date)
            _FechaNacimiento = value
        End Set
    End Property
    Public Property Telefono() As String
        Get
            Return _Telefono
        End Get
        Set(ByVal value As String)
            _Telefono = value
        End Set
    End Property
    Public Property Celular() As String
        Get
            Return _Celular
        End Get
        Set(ByVal value As String)
            _Celular = value
        End Set
    End Property
    Public Property Edad() As Integer
        Get
            Return _Edad
        End Get
        Set(ByVal value As Integer)
            _Edad = value
        End Set
    End Property
    Public Property Nacionalidad() As String
        Get
            Return _Nacionalidad
        End Get
        Set(ByVal value As String)
            _Nacionalidad = value
        End Set
    End Property
    Public Property NumDocumento() As Integer
        Get
            Return _NumDocumento
        End Get
        Set(ByVal value As Integer)
            _NumDocumento = value
        End Set
    End Property
    Public Property ProfesionActividad() As String
        Get
            Return _ProfesionActividad
        End Get
        Set(ByVal value As String)
            _ProfesionActividad = value
        End Set
    End Property
    Public Property Titular() As String
        Get
            Return _Titular
        End Get
        Set(ByVal value As String)
            _Titular = value
        End Set
    End Property
    Public Property LugarTrabajo() As String
        Get
            Return _LugarTrabajo
        End Get
        Set(ByVal value As String)
            _LugarTrabajo = value
        End Set
    End Property

    '----------------------------------------CuestionarioFamiliar

    '       _IdCuestionario
    Private _PadreConVida As Boolean
    Private _EnfermedadPasadaActualPadre As String
    Private _MadreConVida As Boolean
    Private _EnfermedadPasadaActualMadre As String
    Private _Hermanos As Boolean
    Private _Sanos As Boolean
    Private _EnfermoHermano As Boolean
    Private _EnfermedadHermano As String

    Public Property PadreConVida() As Boolean
        Get
            Return _PadreConVida
        End Get
        Set(ByVal value As Boolean)
            _PadreConVida = value
        End Set
    End Property
    Public Property MadreConVida() As Boolean
        Get
            Return _MadreConVida
        End Get
        Set(ByVal value As Boolean)
            _MadreConVida = value
        End Set
    End Property
    Public Property EnfermedadPasadaActualPadre() As String
        Get
            Return _EnfermedadPasadaActualPadre
        End Get
        Set(ByVal value As String)
            _EnfermedadPasadaActualPadre = value
        End Set
    End Property
    Public Property EnfermedadPasadaActualMadre() As String
        Get
            Return _EnfermedadPasadaActualMadre
        End Get
        Set(ByVal value As String)
            _EnfermedadPasadaActualMadre = value
        End Set
    End Property
    Public Property Hermanos() As Boolean
        Get
            Return _Hermanos
        End Get
        Set(ByVal value As Boolean)
            _Hermanos = value
        End Set
    End Property
    Public Property Sanos() As Boolean
        Get
            Return _Sanos
        End Get
        Set(ByVal value As Boolean)
            _Sanos = value
        End Set
    End Property
    Public Property EnfermoHermano() As Boolean
        Get
            Return _EnfermoHermano
        End Get
        Set(ByVal value As Boolean)
            _EnfermoHermano = value
        End Set
    End Property
    Public Property EnfermedadHermano() As String
        Get
            Return _EnfermedadHermano
        End Get
        Set(ByVal value As String)
            _EnfermedadHermano = value
        End Set
    End Property

    '----------------------------------------CuestionarioBasico

    '       _IdCuestionario
    Private _HaceTratamientoMedico As Boolean
    Private _CualTratamientoMedico As String
    Private _MedicamentosActuales As String
    Private _MedicamentosUltimos5Anos As String
    Private _OtraEnfermedadInfectocontagiosa As String
    Private _Transfusiones As Boolean
    Private _FueOperadoAntes As Boolean
    Private _DeQueOperacion As String
    Private _CuandoOperacion As Date
    Private _ProblemasRespiratorios As Boolean
    Private _CualProblemasRespiratorios As String
    Private _Fuma As Boolean
    Private _Embarazada As Boolean
    Private _MesesEmbarazo As String
    Private _OtraEnfermedadRecomendacion As Boolean
    Private _CualEnfermedadRecomendacion As String
    Private _TratamientoAlternativo As Boolean
    Private _CualTratamientoAlternativo As String

    Public Property HaceTratamientoMedico() As Boolean
        Get
            Return _HaceTratamientoMedico
        End Get
        Set(ByVal value As Boolean)
            _HaceTratamientoMedico = value
        End Set
    End Property
    Public Property CualTratamientoMedico() As String
        Get
            Return _CualTratamientoMedico
        End Get
        Set(ByVal value As String)
            _CualTratamientoMedico = value
        End Set
    End Property
    Public Property MedicamentosActuales() As String
        Get
            Return _MedicamentosActuales
        End Get
        Set(ByVal value As String)
            _MedicamentosActuales = value
        End Set
    End Property
    Public Property MedicamentosUltimos5anos() As String
        Get
            Return _MedicamentosUltimos5Anos
        End Get
        Set(ByVal value As String)
            _MedicamentosUltimos5Anos = value
        End Set
    End Property
    Public Property OtraEnfermedadInfectocontagiosa() As String
        Get
            Return _OtraEnfermedadInfectocontagiosa
        End Get
        Set(ByVal value As String)
            _OtraEnfermedadInfectocontagiosa = value
        End Set
    End Property
    Public Property Transfusiones() As Boolean
        Get
            Return _Transfusiones
        End Get
        Set(ByVal value As Boolean)
            _Transfusiones = value
        End Set
    End Property
    Public Property FueOperadoAntes() As Boolean
        Get
            Return _FueOperadoAntes
        End Get
        Set(ByVal value As Boolean)
            _FueOperadoAntes = value
        End Set
    End Property
    Public Property DeQueOperacion() As String
        Get
            Return _DeQueOperacion
        End Get
        Set(ByVal value As String)
            _DeQueOperacion = value
        End Set
    End Property
    Public Property CuandoOperacion() As Date
        Get
            Return _CuandoOperacion
        End Get
        Set(ByVal value As Date)
            _CuandoOperacion = value
        End Set
    End Property
    Public Property ProblemasRespiratorios() As Boolean
        Get
            Return _ProblemasRespiratorios
        End Get
        Set(ByVal value As Boolean)
            _ProblemasRespiratorios = value
        End Set
    End Property
    Public Property CualProblemasRespiratorios() As String
        Get
            Return _CualProblemasRespiratorios
        End Get
        Set(ByVal value As String)
            _CualProblemasRespiratorios = value
        End Set
    End Property
    Public Property Fuma() As Boolean
        Get
            Return _Fuma
        End Get
        Set(ByVal value As Boolean)
            _Fuma = value
        End Set
    End Property
    Public Property Embarazada() As Boolean
        Get
            Return _Embarazada
        End Get
        Set(ByVal value As Boolean)
            _Embarazada = value
        End Set
    End Property
    Public Property MesesEmbarazo() As String
        Get
            Return _MesesEmbarazo
        End Get
        Set(ByVal value As String)
            _MesesEmbarazo = value
        End Set
    End Property
    Public Property OtraEnfermedadRecomendacion() As Boolean
        Get
            Return _OtraEnfermedadRecomendacion
        End Get
        Set(ByVal value As Boolean)
            _OtraEnfermedadRecomendacion = value
        End Set
    End Property
    Public Property CualEnfermedadRecomendacion() As String
        Get
            Return _CualEnfermedadRecomendacion
        End Get
        Set(ByVal value As String)
            _CualEnfermedadRecomendacion = value
        End Set
    End Property
    Public Property TratamientoAlternativo() As Boolean
        Get
            Return _TratamientoAlternativo
        End Get
        Set(ByVal value As Boolean)
            _TratamientoAlternativo = value
        End Set
    End Property
    Public Property CualTratamientoAlternativo() As String
        Get
            Return _CualTratamientoAlternativo
        End Get
        Set(ByVal value As String)
            _CualTratamientoAlternativo = value
        End Set
    End Property

    '----------------------------------------CuestionarioProfundo

    '       IdCuestionario
    Private _MedicoClinico As String
    Private _ClinicaHospital As String
    Private _HaceDeporte As Boolean
    Private _MalestarAlRealizarlo As Boolean
    Private _AlergicoDrogas As Boolean
    Private _AlergicoAnestesia As Boolean
    Private _AlergicoPenisilina As Boolean
    Private _AlergicoOtros As String
    Private _ComoCicatrizaSangra As String
    Private _Hiperlaxitud As Boolean
    Private _AntecedentesFiebreReumatica As Boolean
    Private _MedicacionFiebreReumatica As String
    Private _Diabetico As Boolean
    Private _DiabeticoControlado As Boolean
    Private _MedicacionDiabetico As String
    Private _ProblemaCardiaco As Boolean
    Private _CualProblemaCardiaco As String
    Private _TomaAnticoagulante As Boolean
    Private _FrecuenciaAnticoagulante As String
    Private _PresionAlta As Boolean
    Private _Chagas As Boolean
    Private _TratamientoPresionChagas As String
    Private _ProblemasRenales As Boolean
    Private _UlceraGastrica As Boolean
    Private _TuvoHepatitis As Boolean
    Private _HepatitisA As Boolean
    Private _HepatitisB As Boolean
    Private _HepatitisC As Boolean
    Private _ProblemaHepatico As Boolean
    Private _CualProblemaHepatico As String
    Private _Convulsiones As Boolean
    Private _Epilepsia As Boolean
    Private _MedicacionEpilepsia As String
    Private _SifilisGonorrea As Boolean

    Public Property MedicoClinico() As String
        Get
            Return _MedicoClinico
        End Get
        Set(ByVal value As String)
            _MedicoClinico = value
        End Set
    End Property
    Public Property ClinicaHospital() As String
        Get
            Return _ClinicaHospital
        End Get
        Set(ByVal value As String)
            _ClinicaHospital = value
        End Set
    End Property
    Public Property HaceDeporte() As Boolean
        Get
            Return _HaceDeporte
        End Get
        Set(ByVal value As Boolean)
            _HaceDeporte = value
        End Set
    End Property
    Public Property MalestarAlRealizarlo() As Boolean
        Get
            Return _MalestarAlRealizarlo
        End Get
        Set(ByVal value As Boolean)
            _MalestarAlRealizarlo = value
        End Set
    End Property
    Public Property AlergicoDrogas() As Boolean
        Get
            Return _AlergicoDrogas
        End Get
        Set(ByVal value As Boolean)
            _AlergicoDrogas = value
        End Set
    End Property
    Public Property AlergicoAnestesia() As Boolean
        Get
            Return _AlergicoAnestesia
        End Get
        Set(ByVal value As Boolean)
            _AlergicoAnestesia = value
        End Set
    End Property
    Public Property AlergicoPenisilina() As Boolean
        Get
            Return _AlergicoPenisilina
        End Get
        Set(ByVal value As Boolean)
            _AlergicoPenisilina = value
        End Set
    End Property
    Public Property AlergicoOtros() As String
        Get
            Return _AlergicoOtros
        End Get
        Set(ByVal value As String)
            _AlergicoOtros = value
        End Set
    End Property
    Public Property ComoCicatrizaSangra() As String
        Get
            Return _ComoCicatrizaSangra
        End Get
        Set(ByVal value As String)
            _ComoCicatrizaSangra = value
        End Set
    End Property
    Public Property Hiperlaxitud() As Boolean
        Get
            Return _Hiperlaxitud
        End Get
        Set(ByVal value As Boolean)
            _Hiperlaxitud = value
        End Set
    End Property
    Public Property AntecedentesFiebreReumatica() As Boolean
        Get
            Return _AntecedentesFiebreReumatica
        End Get
        Set(ByVal value As Boolean)
            _AntecedentesFiebreReumatica = value
        End Set
    End Property
    Public Property MedicacionFiebreReumatica() As String
        Get
            Return _MedicacionFiebreReumatica
        End Get
        Set(ByVal value As String)
            _MedicacionFiebreReumatica = value
        End Set
    End Property
    Public Property Diabetico() As Boolean
        Get
            Return _Diabetico
        End Get
        Set(ByVal value As Boolean)
            _Diabetico = value
        End Set
    End Property
    Public Property DiabeticoControlado() As Boolean
        Get
            Return _DiabeticoControlado
        End Get
        Set(ByVal value As Boolean)
            _DiabeticoControlado = value
        End Set
    End Property
    Public Property MedicacionDiabetico() As String
        Get
            Return _MedicacionDiabetico
        End Get
        Set(ByVal value As String)
            _MedicacionDiabetico = value
        End Set
    End Property
    Public Property ProblemaCardiaco() As Boolean
        Get
            Return _ProblemaCardiaco
        End Get
        Set(ByVal value As Boolean)
            _ProblemaCardiaco = value
        End Set
    End Property
    Public Property CualProblemaCardiaco() As String
        Get
            Return _CualProblemaCardiaco
        End Get
        Set(ByVal value As String)
            _CualProblemaCardiaco = value
        End Set
    End Property
    Public Property TomaAnticoagulante() As Boolean
        Get
            Return _TomaAnticoagulante
        End Get
        Set(ByVal value As Boolean)
            _TomaAnticoagulante = value
        End Set
    End Property
    Public Property FrecuenciaAnticoagulante() As String
        Get
            Return _FrecuenciaAnticoagulante
        End Get
        Set(ByVal value As String)
            _FrecuenciaAnticoagulante = value
        End Set
    End Property
    Public Property PresionAlta() As Boolean
        Get
            Return _PresionAlta
        End Get
        Set(ByVal value As Boolean)
            _PresionAlta = value
        End Set
    End Property
    Public Property Chagas() As Boolean
        Get
            Return _Chagas
        End Get
        Set(ByVal value As Boolean)
            _Chagas = value
        End Set
    End Property
    Public Property TratamientoPresionchagas() As String
        Get
            Return _TratamientoPresionChagas
        End Get
        Set(ByVal value As String)
            _TratamientoPresionChagas = value
        End Set
    End Property
    Public Property ProblemasRenales() As Boolean
        Get
            Return _ProblemasRenales
        End Get
        Set(ByVal value As Boolean)
            _ProblemasRenales = value
        End Set
    End Property
    Public Property UlceraGastrica() As Boolean
        Get
            Return _UlceraGastrica
        End Get
        Set(ByVal value As Boolean)
            _UlceraGastrica = value
        End Set
    End Property
    Public Property TuvoHepatitis() As Boolean
        Get
            Return _TuvoHepatitis
        End Get
        Set(ByVal value As Boolean)
            _TuvoHepatitis = value
        End Set
    End Property
    Public Property HepatitisA() As Boolean
        Get
            Return _HepatitisA
        End Get
        Set(ByVal value As Boolean)
            _HepatitisA = value
        End Set
    End Property
    Public Property HepatitisB() As Boolean
        Get
            Return _HepatitisB
        End Get
        Set(ByVal value As Boolean)
            _HepatitisB = value
        End Set
    End Property
    Public Property HepatitisC() As Boolean
        Get
            Return _HepatitisC
        End Get
        Set(ByVal value As Boolean)
            _HepatitisC = value
        End Set
    End Property
    Public Property ProblemaHepatico() As Boolean
        Get
            Return _ProblemaHepatico
        End Get
        Set(ByVal value As Boolean)
            _ProblemaHepatico = value
        End Set
    End Property
    Public Property CualProblemaHepatico() As String
        Get
            Return _CualProblemaHepatico
        End Get
        Set(ByVal value As String)
            _CualProblemaHepatico = value
        End Set
    End Property
    Public Property Convulsiones() As Boolean
        Get
            Return _Convulsiones
        End Get
        Set(ByVal value As Boolean)
            _Convulsiones = value
        End Set
    End Property
    Public Property Epilepsia() As Boolean
        Get
            Return _Epilepsia
        End Get
        Set(ByVal value As Boolean)
            _Epilepsia = value
        End Set
    End Property
    Public Property MedicacionEpilepsia() As String
        Get
            Return _MedicacionEpilepsia
        End Get
        Set(ByVal value As String)
            _MedicacionEpilepsia = value
        End Set
    End Property
    Public Property SifilisGonorrea() As Boolean
        Get
            Return _SifilisGonorrea
        End Get
        Set(ByVal value As Boolean)
            _SifilisGonorrea = value
        End Set
    End Property

    '----------------------------------------HistoriaClinicaOdontologica

    '       _IdHCO
    '       _IdPersona
    '       _IdConsulta
    Private _MotivoConsulta As String
    Private _ConsultoAntes As Boolean
    Private _TomoAlgoHCO As Boolean
    Private _MedicacionHCO As String
    Private _DesdeCuandoMedicacionHCO As Date
    Private _ObtuvoResultadoHCO As Boolean
    Private _HaTenidoDolor As Boolean
    '       _IdIntensidadDolor
    '       _IdDuracionDolor
    '       _IdEstimuloDolor
    Private _DolorLocalizado As Boolean
    Private _DondeDolor As String
    Private _Irradiado As Boolean
    Private _HaciaDondeIrradiado As String
    Private _PudoCalmarIrradiacion As String
    Private _GolpeEnDientes As Boolean
    Private _CuandoGolpeEnDientes As Date
    Private _ComoGolpeEnDientes As String
    Private _FracturaDeDiente As Boolean
    Private _CualFracturaDeDiente As String
    Private _RecibioTratamientoHCO As String
    '       _IdDificultad
    '       _IdAnormalidad
    '       _IdLesiones

    Public Property MotivoConsulta() As String
        Get
            Return _MotivoConsulta
        End Get
        Set(ByVal value As String)
            _MotivoConsulta = value
        End Set
    End Property
    Public Property ConsultoAntes() As Boolean
        Get
            Return _ConsultoAntes
        End Get
        Set(ByVal value As Boolean)
            _ConsultoAntes = value
        End Set
    End Property
    Public Property TomoAlgoHCO() As Boolean
        Get
            Return _TomoAlgoHCO
        End Get
        Set(ByVal value As Boolean)
            _TomoAlgoHCO = value
        End Set
    End Property
    Public Property MedicacionHCO() As String
        Get
            Return _MedicacionHCO
        End Get
        Set(ByVal value As String)
            _MedicacionHCO = value
        End Set
    End Property
    Public Property DesdeCuandoMedicacionHCO() As Date
        Get
            Return _DesdeCuandoMedicacionHCO
        End Get
        Set(ByVal value As Date)
            _DesdeCuandoMedicacionHCO = value
        End Set
    End Property
    Public Property ObtuvoResultadoHCO() As Boolean
        Get
            Return _ObtuvoResultadoHCO
        End Get
        Set(ByVal value As Boolean)
            _ObtuvoResultadoHCO = value
        End Set
    End Property
    Public Property HaTenidoDolor() As Boolean
        Get
            Return _HaTenidoDolor
        End Get
        Set(ByVal value As Boolean)
            _HaTenidoDolor = value
        End Set
    End Property
    Public Property DolorLocalizado() As Boolean
        Get
            Return _DolorLocalizado
        End Get
        Set(ByVal value As Boolean)
            _DolorLocalizado = value
        End Set
    End Property
    Public Property DondeDolor() As String
        Get
            Return _DondeDolor
        End Get
        Set(ByVal value As String)
            _DondeDolor = value
        End Set

    End Property
    Public Property Irradiado() As Boolean
        Get
            Return _Irradiado
        End Get
        Set(ByVal value As Boolean)
            _Irradiado = value
        End Set
    End Property
    Public Property HaciaDondeIrradiado() As String
        Get
            Return _HaciaDondeIrradiado
        End Get
        Set(ByVal value As String)
            _HaciaDondeIrradiado = value
        End Set
    End Property
    Public Property PudoCalmarIrradiacion() As String
        Get
            Return _PudoCalmarIrradiacion
        End Get
        Set(ByVal value As String)
            _PudoCalmarIrradiacion = value
        End Set
    End Property
    Public Property GolpeEnDientes() As Boolean
        Get
            Return _GolpeEnDientes
        End Get
        Set(ByVal value As Boolean)
            _GolpeEnDientes = value
        End Set
    End Property
    Public Property CuandoGolpeEnDientes() As Date
        Get
            Return _CuandoGolpeEnDientes
        End Get
        Set(ByVal value As Date)
            _CuandoGolpeEnDientes = value
        End Set
    End Property
    Public Property ComoGolpeEnDientes() As String
        Get
            Return _ComoGolpeEnDientes
        End Get
        Set(ByVal value As String)
            _ComoGolpeEnDientes = value
        End Set
    End Property
    Public Property FracturaDeDiente() As Boolean
        Get
            Return _FracturaDeDiente
        End Get
        Set(ByVal value As Boolean)
            _FracturaDeDiente = value
        End Set
    End Property
    Public Property CualFracturaDeDiente() As String
        Get
            Return _CualFracturaDeDiente
        End Get
        Set(ByVal value As String)
            _CualFracturaDeDiente = value
        End Set
    End Property
    Public Property RecibioTratamientoHCO() As String
        Get
            Return _RecibioTratamientoHCO
        End Get
        Set(ByVal value As String)
            _RecibioTratamientoHCO = value
        End Set
    End Property

    '----------------------------------------Domicilio

    '       _IdDomicilio
    Private _Calle As String
    Private _Numero As Integer
    Private _Piso As Integer
    Private _Departamento As String
    Private _Barrio As String
    Private _Localidad As String
    Private _Provincia As String
    Private _CodigoPostal As String

    Public Property Calle() As String
        Get
            Return _Calle
        End Get
        Set(ByVal value As String)
            _Calle = value
        End Set
    End Property
    Public Property Numero() As Integer
        Get
            Return _Numero
        End Get
        Set(ByVal value As Integer)
            _Numero = value
        End Set
    End Property
    Public Property Piso() As Integer
        Get
            Return _Piso
        End Get
        Set(ByVal value As Integer)
            _Piso = value
        End Set
    End Property
    Public Property Departamento() As String
        Get
            Return _Departamento
        End Get
        Set(ByVal value As String)
            _Departamento = value
        End Set
    End Property
    Public Property Barrio() As String
        Get
            Return _Barrio
        End Get
        Set(ByVal value As String)
            _Barrio = value
        End Set
    End Property
    Public Property Localidad() As String
        Get
            Return _Localidad
        End Get
        Set(ByVal value As String)
            _Localidad = value
        End Set
    End Property
    Public Property Provincia() As String
        Get
            Return _Provincia
        End Get
        Set(ByVal value As String)
            _Provincia = value
        End Set
    End Property
    Public Property CodigoPostal() As String
        Get
            Return _CodigoPostal
        End Get
        Set(ByVal value As String)
            _CodigoPostal = value
        End Set
    End Property

    '----------------------------------------EstimuloDolor

    '       _IdEstimuloDolor
    Private _DolorEspontaneo As Boolean
    Private _DolorProvocado As Boolean
    Private _DolorAlFrio As Boolean
    Private _DolorAlCalor As Boolean

    Public Property DolorEspontaneo() As Boolean
        Get
            Return _DolorEspontaneo
        End Get
        Set(ByVal value As Boolean)
            _DolorEspontaneo = value
        End Set
    End Property
    Public Property DolorProvocado() As Boolean
        Get
            Return _DolorProvocado
        End Get
        Set(ByVal value As Boolean)
            _DolorProvocado = value
        End Set
    End Property
    Public Property DolorAlFrio() As Boolean
        Get
            Return _DolorAlFrio
        End Get
        Set(ByVal value As Boolean)
            _DolorAlFrio = value
        End Set
    End Property
    Public Property DolorAlCalor() As Boolean
        Get
            Return _DolorAlCalor
        End Get
        Set(ByVal value As Boolean)
            _DolorAlCalor = value
        End Set
    End Property

    '----------------------------------------Dificultad

    '       _IdDificultad
    '       _IdPersona
    Private _Hablar As String
    Private _Masticar As String
    Private _AbrirBoca As String
    Private _Tragar As String

    Public Property Tragar() As String
        Get
            Return _Tragar
        End Get
        Set(ByVal value As String)
            _Tragar = value
        End Set
    End Property
    Public Property Hablar() As String
        Get
            Return _Hablar
        End Get
        Set(ByVal value As String)
            _Hablar = value
        End Set
    End Property
    Public Property Masticar() As String
        Get
            Return _Masticar
        End Get
        Set(ByVal value As String)
            _Masticar = value
        End Set
    End Property
    Public Property AbrirBoca() As String
        Get
            Return _AbrirBoca
        End Get
        Set(ByVal value As String)
            _AbrirBoca = value
        End Set
    End Property

    '----------------------------------------Anormalidad

    '       _IdAnormalidad
    '       _IdPersona
    Private _Labios As String
    Private _Lengua As String
    Private _Paladar As String
    Private _PisoDeBoca As String
    Private _Carrillos As String
    Private _Rebordes As String
    Private _Trigono As String
    Private _Retromolar As String

    Public Property Labios() As String
        Get
            Return _Labios
        End Get
        Set(ByVal value As String)
            _Labios = value
        End Set
    End Property
    Public Property Lengua() As String
        Get
            Return _Lengua
        End Get
        Set(ByVal value As String)
            _Lengua = value
        End Set
    End Property
    Public Property Paladar() As String
        Get
            Return _Paladar
        End Get
        Set(ByVal value As String)
            _Paladar = value
        End Set
    End Property
    Public Property PisoDeBoca() As String
        Get
            Return _PisoDeBoca
        End Get
        Set(ByVal value As String)
            _PisoDeBoca = value
        End Set
    End Property
    Public Property Carrillos() As String
        Get
            Return _Carrillos
        End Get
        Set(ByVal value As String)
            _Carrillos = value
        End Set
    End Property
    Public Property Rebordes() As String
        Get
            Return _Rebordes
        End Get
        Set(ByVal value As String)
            _Rebordes = value
        End Set
    End Property
    Public Property Trigono() As String
        Get
            Return _Trigono
        End Get
        Set(ByVal value As String)
            _Trigono = value
        End Set
    End Property
    Public Property Retromolar() As String
        Get
            Return _Retromolar
        End Get
        Set(ByVal value As String)
            _Retromolar = value
        End Set
    End Property

    '----------------------------------------Lesiones

    '       _IdLesiones
    '       _IdPersona
    Private _Manchas As Boolean
    Private _AbultamientoTejidos As Boolean
    Private _Ulceraciones As Boolean
    Private _Ampollas As Boolean
    Private _Otros As String
    Private _SangranEncias As Boolean
    Private _CuandoSangranEncias As String
    Private _SalePusEnBoca As Boolean
    Private _DondeSalePusEnBoca As String
    Private _MovilidadEnDientes As Boolean
    Private _SienteAltosDientesAlMorder As String
    Private _CaraHinchada As Boolean
    Private _SePusoHielo As Boolean
    Private _SePusoCalor As Boolean
    Private _SePusoOtros As String

    Public Property SePusoOtros() As String
        Get
            Return _SePusoOtros
        End Get
        Set(ByVal value As String)
            _SePusoOtros = value
        End Set
    End Property
    Public Property Manchas() As Boolean
        Get
            Return _Manchas
        End Get
        Set(ByVal value As Boolean)
            _Manchas = value
        End Set
    End Property
    Public Property AbultamientoTejidos() As Boolean
        Get
            Return _AbultamientoTejidos
        End Get
        Set(ByVal value As Boolean)
            _AbultamientoTejidos = value
        End Set
    End Property
    Public Property Ulceraciones() As Boolean
        Get
            Return _Ulceraciones
        End Get
        Set(ByVal value As Boolean)
            _Ulceraciones = value
        End Set
    End Property
    Public Property Ampollas() As Boolean
        Get
            Return _Ampollas
        End Get
        Set(ByVal value As Boolean)
            _Ampollas = value
        End Set
    End Property
    Public Property Otros() As String
        Get
            Return _Otros
        End Get
        Set(ByVal value As String)
            _Otros = value
        End Set
    End Property
    Public Property SangranEncias() As Boolean
        Get
            Return _SangranEncias
        End Get
        Set(ByVal value As Boolean)
            _SangranEncias = value
        End Set
    End Property
    Public Property CuandoSangranEncias() As String
        Get
            Return _CuandoSangranEncias
        End Get
        Set(ByVal value As String)
            _CuandoSangranEncias = value
        End Set
    End Property
    Public Property SalePusEnBoca() As Boolean
        Get
            Return _SalePusEnBoca
        End Get
        Set(ByVal value As Boolean)
            _SalePusEnBoca = value
        End Set
    End Property
    Public Property DondeSalePusEnBoca() As String
        Get
            Return _DondeSalePusEnBoca
        End Get
        Set(ByVal value As String)
            _DondeSalePusEnBoca = value
        End Set
    End Property
    Public Property MovilidadEnDientes() As Boolean
        Get
            Return _MovilidadEnDientes
        End Get
        Set(ByVal value As Boolean)
            _MovilidadEnDientes = value
        End Set
    End Property
    Public Property SienteAltosDientesAlMorder() As String
        Get
            Return _SienteAltosDientesAlMorder
        End Get
        Set(ByVal value As String)
            _SienteAltosDientesAlMorder = value
        End Set
    End Property
    Public Property CaraHinchada() As Boolean
        Get
            Return _CaraHinchada
        End Get
        Set(ByVal value As Boolean)
            _CaraHinchada = value
        End Set
    End Property
    Public Property SePusoHielo() As Boolean
        Get
            Return _SePusoHielo
        End Get
        Set(ByVal value As Boolean)
            _SePusoHielo = value
        End Set
    End Property
    Public Property SePusoCalor() As Boolean
        Get
            Return _SePusoCalor
        End Get
        Set(ByVal value As Boolean)
            _SePusoCalor = value
        End Set
    End Property

    '----------------------------------------EstadoBucal

    '       _IdEstadoBucal
    '       _IdPersona
    '       _IdCuadraditos
    Private _Sarro As Boolean
    Private _EnfermedadPeriodontal As Boolean
    Private _DiagnosticoPresuntivo As String
    Private _PlanTratamiento As String
    Private _CuandoPlanTratamiento As Date
    Private _CantidadDientes As Integer
    Private _Observaciones As String

    Public Property Sarro() As Boolean
        Get
            Return _Sarro
        End Get
        Set(ByVal value As Boolean)
            _Sarro = value
        End Set
    End Property
    Public Property EnfermedadPeriodontal() As Boolean
        Get
            Return _EnfermedadPeriodontal
        End Get
        Set(ByVal value As Boolean)
            _EnfermedadPeriodontal = value
        End Set
    End Property
    Public Property DiagnosticoPresuntivo() As String
        Get
            Return _DiagnosticoPresuntivo
        End Get
        Set(ByVal value As String)
            _DiagnosticoPresuntivo = value
        End Set
    End Property
    Public Property PlanTratamiento() As String
        Get
            Return _PlanTratamiento
        End Get
        Set(ByVal value As String)
            _PlanTratamiento = value
        End Set
    End Property
    Public Property CuandoPlanTratamiento() As Date
        Get
            Return _CuandoPlanTratamiento
        End Get
        Set(ByVal value As Date)
            _CuandoPlanTratamiento = value
        End Set
    End Property
    Public Property CantidadDientes() As Integer
        Get
            Return _CantidadDientes
        End Get
        Set(ByVal value As Integer)
            _CantidadDientes = value
        End Set
    End Property
    Public Property Observaciones() As String
        Get
            Return _Observaciones
        End Get
        Set(ByVal value As String)
            _Observaciones = value
        End Set
    End Property

    '----------------------------------------Cuadraditos

    '       _IdCuadraditos
    Private _i11 As Integer
    Private _i12 As Integer
    Private _i13 As Integer
    Private _i14 As Integer
    Private _i15 As Integer
    Private _i16 As Integer
    Private _i17 As Integer
    Private _i18 As Integer
    Private _i21 As Integer
    Private _i22 As Integer
    Private _i23 As Integer
    Private _i24 As Integer
    Private _i25 As Integer
    Private _i26 As Integer
    Private _i27 As Integer
    Private _i28 As Integer
    Private _i31 As Integer
    Private _i32 As Integer
    Private _i33 As Integer
    Private _i34 As Integer
    Private _i35 As Integer
    Private _i36 As Integer
    Private _i37 As Integer
    Private _i38 As Integer
    Private _i41 As Integer
    Private _i42 As Integer
    Private _i43 As Integer
    Private _i44 As Integer
    Private _i45 As Integer
    Private _i46 As Integer
    Private _i47 As Integer
    Private _i48 As Integer
    Private _i51 As Integer
    Private _i52 As Integer
    Private _i53 As Integer
    Private _i54 As Integer
    Private _i55 As Integer
    Private _i56 As Integer
    Private _i61 As Integer
    Private _i62 As Integer
    Private _i63 As Integer
    Private _i64 As Integer
    Private _i65 As Integer
    Private _i66 As Integer
    Private _i71 As Integer
    Private _i72 As Integer
    Private _i73 As Integer
    Private _i74 As Integer
    Private _i75 As Integer
    Private _i76 As Integer
    Private _i81 As Integer
    Private _i82 As Integer
    Private _i83 As Integer
    Private _i84 As Integer
    Private _i85 As Integer
    Private _i86 As Integer

    Public Property i11() As Integer
        Get
            Return _i11
        End Get
        Set(ByVal value As Integer)
            _i11 = value
        End Set
    End Property
    Public Property i12() As Integer
        Get
            Return _i12
        End Get
        Set(ByVal value As Integer)
            _i12 = value
        End Set
    End Property
    Public Property i13() As Integer
        Get
            Return _i13
        End Get
        Set(ByVal value As Integer)
            _i13 = value
        End Set
    End Property
    Public Property i14() As Integer
        Get
            Return _i14
        End Get
        Set(ByVal value As Integer)
            _i14 = value
        End Set
    End Property
    Public Property i15() As Integer
        Get
            Return _i15
        End Get
        Set(ByVal value As Integer)
            _i15 = value
        End Set
    End Property
    Public Property i16() As Integer
        Get
            Return _i16
        End Get
        Set(ByVal value As Integer)
            _i16 = value
        End Set
    End Property
    Public Property i17() As Integer
        Get
            Return _i17
        End Get
        Set(ByVal value As Integer)
            _i17 = value
        End Set
    End Property
    Public Property i18() As Integer
        Get
            Return _i18
        End Get
        Set(ByVal value As Integer)
            _i18 = value
        End Set
    End Property
    Public Property i21() As Integer
        Get
            Return _i21
        End Get
        Set(ByVal value As Integer)
            _i21 = value
        End Set
    End Property
    Public Property i22() As Integer
        Get
            Return _i22
        End Get
        Set(ByVal value As Integer)
            _i22 = value
        End Set
    End Property
    Public Property i23() As Integer
        Get
            Return _i23
        End Get
        Set(ByVal value As Integer)
            _i23 = value
        End Set
    End Property
    Public Property i24() As Integer
        Get
            Return _i24
        End Get
        Set(ByVal value As Integer)
            _i24 = value
        End Set
    End Property
    Public Property i25() As Integer
        Get
            Return _i25
        End Get
        Set(ByVal value As Integer)
            _i25 = value
        End Set
    End Property
    Public Property i26() As Integer
        Get
            Return _i26
        End Get
        Set(ByVal value As Integer)
            _i26 = value
        End Set
    End Property
    Public Property i27() As Integer
        Get
            Return _i27
        End Get
        Set(ByVal value As Integer)
            _i27 = value
        End Set
    End Property
    Public Property i28() As Integer
        Get
            Return _i28
        End Get
        Set(ByVal value As Integer)
            _i28 = value
        End Set
    End Property
    Public Property i31() As Integer
        Get
            Return _i31
        End Get
        Set(ByVal value As Integer)
            _i31 = value
        End Set
    End Property
    Public Property i32() As Integer
        Get
            Return _i32
        End Get
        Set(ByVal value As Integer)
            _i32 = value
        End Set
    End Property
    Public Property i33() As Integer
        Get
            Return _i33
        End Get
        Set(ByVal value As Integer)
            _i33 = value
        End Set
    End Property
    Public Property i34() As Integer
        Get
            Return _i34
        End Get
        Set(ByVal value As Integer)
            _i34 = value
        End Set
    End Property
    Public Property i35() As Integer
        Get
            Return _i35
        End Get
        Set(ByVal value As Integer)
            _i35 = value
        End Set
    End Property
    Public Property i36() As Integer
        Get
            Return _i36
        End Get
        Set(ByVal value As Integer)
            _i36 = value
        End Set
    End Property
    Public Property i37() As Integer
        Get
            Return _i37
        End Get
        Set(ByVal value As Integer)
            _i37 = value
        End Set
    End Property
    Public Property i38() As Integer
        Get
            Return _i38
        End Get
        Set(ByVal value As Integer)
            _i38 = value
        End Set
    End Property
    Public Property i41() As Integer
        Get
            Return _i41
        End Get
        Set(ByVal value As Integer)
            _i41 = value
        End Set
    End Property
    Public Property i42() As Integer
        Get
            Return _i42
        End Get
        Set(ByVal value As Integer)
            _i42 = value
        End Set
    End Property
    Public Property i43() As Integer
        Get
            Return _i43
        End Get
        Set(ByVal value As Integer)
            _i43 = value
        End Set
    End Property
    Public Property i44() As Integer
        Get
            Return _i44
        End Get
        Set(ByVal value As Integer)
            _i44 = value
        End Set
    End Property
    Public Property i45() As Integer
        Get
            Return _i45
        End Get
        Set(ByVal value As Integer)
            _i45 = value
        End Set
    End Property
    Public Property i46() As Integer
        Get
            Return _i46
        End Get
        Set(ByVal value As Integer)
            _i46 = value
        End Set
    End Property
    Public Property i47() As Integer
        Get
            Return _i47
        End Get
        Set(ByVal value As Integer)
            _i47 = value
        End Set
    End Property
    Public Property i48() As Integer
        Get
            Return _i48
        End Get
        Set(ByVal value As Integer)
            _i48 = value
        End Set
    End Property
    Public Property i51() As Integer
        Get
            Return _i51
        End Get
        Set(ByVal value As Integer)
            _i51 = value
        End Set
    End Property
    Public Property i52() As Integer
        Get
            Return _i52
        End Get
        Set(ByVal value As Integer)
            _i52 = value
        End Set
    End Property
    Public Property i53() As Integer
        Get
            Return _i53
        End Get
        Set(ByVal value As Integer)
            _i53 = value
        End Set
    End Property
    Public Property i54() As Integer
        Get
            Return _i54
        End Get
        Set(ByVal value As Integer)
            _i54 = value
        End Set
    End Property
    Public Property i55() As Integer
        Get
            Return _i55
        End Get
        Set(ByVal value As Integer)
            _i55 = value
        End Set
    End Property
    Public Property i56() As Integer
        Get
            Return _i56
        End Get
        Set(ByVal value As Integer)
            _i56 = value
        End Set
    End Property
    Public Property i61() As Integer
        Get
            Return _i61
        End Get
        Set(ByVal value As Integer)
            _i61 = value
        End Set
    End Property
    Public Property i62() As Integer
        Get
            Return _i62
        End Get
        Set(ByVal value As Integer)
            _i62 = value
        End Set
    End Property
    Public Property i63() As Integer
        Get
            Return _i63
        End Get
        Set(ByVal value As Integer)
            _i63 = value
        End Set
    End Property
    Public Property i64() As Integer
        Get
            Return _i64
        End Get
        Set(ByVal value As Integer)
            _i64 = value
        End Set
    End Property
    Public Property i65() As Integer
        Get
            Return _i65
        End Get
        Set(ByVal value As Integer)
            _i65 = value
        End Set
    End Property
    Public Property i66() As Integer
        Get
            Return _i66
        End Get
        Set(ByVal value As Integer)
            _i66 = value
        End Set
    End Property
    Public Property i71() As Integer
        Get
            Return _i71
        End Get
        Set(ByVal value As Integer)
            _i71 = value
        End Set
    End Property
    Public Property i72() As Integer
        Get
            Return _i72
        End Get
        Set(ByVal value As Integer)
            _i72 = value
        End Set
    End Property
    Public Property i73() As Integer
        Get
            Return _i73
        End Get
        Set(ByVal value As Integer)
            _i73 = value
        End Set
    End Property
    Public Property i74() As Integer
        Get
            Return _i74
        End Get
        Set(ByVal value As Integer)
            _i74 = value
        End Set
    End Property
    Public Property i75() As Integer
        Get
            Return _i75
        End Get
        Set(ByVal value As Integer)
            _i75 = value
        End Set
    End Property
    Public Property i76() As Integer
        Get
            Return _i76
        End Get
        Set(ByVal value As Integer)
            _i76 = value
        End Set
    End Property
    Public Property i81() As Integer
        Get
            Return _i81
        End Get
        Set(ByVal value As Integer)
            _i81 = value
        End Set
    End Property
    Public Property i82() As Integer
        Get
            Return _i82
        End Get
        Set(ByVal value As Integer)
            _i82 = value
        End Set
    End Property
    Public Property i83() As Integer
        Get
            Return _i83
        End Get
        Set(ByVal value As Integer)
            _i83 = value
        End Set
    End Property
    Public Property i84() As Integer
        Get
            Return _i84
        End Get
        Set(ByVal value As Integer)
            _i84 = value
        End Set
    End Property
    Public Property i85() As Integer
        Get
            Return _i85
        End Get
        Set(ByVal value As Integer)
            _i85 = value
        End Set
    End Property
    Public Property i86() As Integer
        Get
            Return _i86
        End Get
        Set(ByVal value As Integer)
            _i86 = value
        End Set
    End Property

    '----------------------------------------
    '----------------------------------------
    '----------------------------------------
    '----------------------------------------
    '----------------------------------------

    Public Function Insertar() As Object

        Dim cmdInsertar_Consulta = "INSERT INTO Consulta(	
IdConsulta,
IdPersona,
IdOdontologo,
Fecha,
IdCuestionario,
IdConsultorio,
IdEstadoBucal	
)VALUES(
IdConsulta,
IdPersona,
IdOdontologo,
Fecha,
IdCuestionario,
IdConsultorio,
IdEstadoBucal);"
        Dim cmdInsertar_Persona = "INSERT INTO Persona(	
IdPersona,
Nombre,
Apellido,
NumAfiliado,
IdObraSocial,
FechaNacimiento,
Telefono,
Celular,
Edad,
IdEstadoCivil,
Nacionalidad,
NumDocumento,
IdEstadoCivil,
ProfesionActividad,
Titular,
LugarTrabajo	
)VALUES(
'IdPersona',
'Nombre',
'Apellido',
'NumAfiliado',
'IdObraSocial',
'FechaNacimiento',
'Telefono',
'Celular',
'Edad',
'IdEstadoCivil',
'Nacionalidad',
'NumDocumento',
'IdEstadoCivil',
'ProfesionActividad',
'Titular',
'LugarTrabajo');"
        Dim cmdInsertar_CuestionarioFamiliar = "INSERT INTO CuestionarioFamiliar(	
IdCuestionario,
PadreConVida,
EnfermedadPasadaActualPadre,
MadreConVida,
EnfermedadPasadaActualMadre,
Hermanos,
Sanos,
EnfermoHermano,
EnfermedadHermano	
)VALUES(
'IdCuestionario',
'PadreConVida',
'EnfermedadPasadaActualPadre',
'MadreConVida',
'EnfermedadPasadaActualMadre',
'Hermanos',
'Sanos',
'EnfermoHermano',
'EnfermedadHermano');"
        Dim cmdInsertar_CuestionarioBasico = "INSERT INTO CuestionarioBasico(	
IdCuestionario,
HaceTratamientoMedico,
CualTratamientoMedico,
MedicamentosActuales,
MedicamentosUltimos5Anos,
OtraEnfermedadInfectocontagiosa,
Transfusiones,
FueOperadoAntes,
DeQueOperacion,
CuandoOperacion,
ProblemasRespiratorios,
CualProblemasRespiratorios,
Fuma,
Embarazada,
MesesEmbarazo,
OtraEnfermedadRecomendacion,
CualEnfermedadRecomendacion,
TratamientoAlternativo,
CualTratamientoAlternativo
)VALUES(
'IdCuestionario',
'HaceTratamientoMedico',
'CualTratamientoMedico',
'MedicamentosActuales',
'MedicamentosUltimos5Anos',
'OtraEnfermedadInfectocontagiosa',
'Transfusiones',
'FueOperadoAntes',
'DeQueOperacion',
'CuandoOperacion',
'ProblemasRespiratorios',
'CualProblemasRespiratorios',
'Fuma',
'Embarazada',
'MesesEmbarazo',
'OtraEnfermedadRecomendacion',
'CualEnfermedadRecomendacion',
'TratamientoAlternativo',
'CualTratamientoAlternativo');"
        Dim cmdInsertar_CuestionarioProfundo = "INSERT INTO CuestionarioProfundo(	
IdCuestionario,
MedicoClinico,
ClinicaHospital,
HaceDeporte,
MalestarAlRealizarlo,
AlergicoDrogas,
AlergicoAnestesia,
AlergicoPenisilina,
AlergicoOtros,
ComoCicatrizaSangra,
Hiperlaxitud,
AntecedentesFiebreReumatica,
MedicacionFiebreReumatica,
Diabetico,
DiabeticoControlado,
MedicacionDiabetico,
ProblemaCardiaco,
CualProblemaCardiaco,
TomaAnticoagulante,
FrecuenciaAnticoagulante,
PresionAlta,
Chagas,
TratamientoPresionChagas,
ProblemasRenales,
UlceraGastrica,
TuvoHepatitis,
HepatitisA,
HepatitisB,
HepatitisC,
ProblemaHepatico,
CualProblemaHepatico,
Convulsiones,
Epilepsia,
MedicacionEpilepsia,
SifilisGonorrea	
)VALUES(
'IdCuestionario',
'MedicoClinico',
'ClinicaHospital',
'HaceDeporte',
'MalestarAlRealizarlo',
'AlergicoDrogas',
'AlergicoAnestesia',
'AlergicoPenisilina',
'AlergicoOtros',
'ComoCicatrizaSangra',
'Hiperlaxitud',
'AntecedentesFiebreReumatica',
'MedicacionFiebreReumatica',
'Diabetico',
'DiabeticoControlado',
'MedicacionDiabetico',
'ProblemaCardiaco',
'CualProblemaCardiaco',
'TomaAnticoagulante',
'FrecuenciaAnticoagulante',
'PresionAlta',
'Chagas',
'TratamientoPresionChagas',
'ProblemasRenales',
'UlceraGastrica',
'TuvoHepatitis',
'HepatitisA',
'HepatitisB',
'HepatitisC',
'ProblemaHepatico',
'CualProblemaHepatico',
'Convulsiones',
'Epilepsia',
'MedicacionEpilepsia',
'SifilisGonorrea');"
        Dim cmdInsertar_HistoriaClinicaOdontologica = "INSERT INTO HistoriaClinicaOdontologica(	
IdHCO,
IdPersona,
IdConsulta,
MotivoConsulta,
ConsultoAntes,
TomoAlgoHCO,
MedicacionHCO,
DesdeCuandoMedicacionHCO,
ObtuvoResultadoHCO,
HaTenidoDolor,
IdIntensidadDolor,
IdDuracionDolor,
IdEstimuloDolor,
DolorLocalizado,
DondeDolor,
Irradiado,
HaciaDondeIrradiado,
PudoCalmarIrradiacion,
GolpeEnDientes,
CuandoGolpeEnDientes,
ComoGolpeEnDientes,
FracturaDeDiente,
CualFracturaDeDiente,
RecibioTratamientoHCO,
IdDificultad,
IdAnormalidad,
IdLesiones	
)VALUES(	
'IdHCO',
'IdPersona',
'IdConsulta',
'MotivoConsulta',
'ConsultoAntes',
'TomoAlgoHCO',
'MedicacionHCO',
'DesdeCuandoMedicacionHCO',
'ObtuvoResultadoHCO',
'HaTenidoDolor',
'IdIntensidadDolor',
'IdDuracionDolor',
'IdEstimuloDolor',
'DolorLocalizado',
'DondeDolor',
'Irradiado',
'HaciaDondeIrradiado',
'PudoCalmarIrradiacion',
'GolpeEnDientes',
'CuandoGolpeEnDientes',
'ComoGolpeEnDientes',
'FracturaDeDiente',
'CualFracturaDeDiente',
'RecibioTratamientoHCO',
'IdDificultad',
'IdAnormalidad',
'IdLesiones');"
        Dim cmdInsertar_Domicilio = "INSERT INTO Domicilio(
IdDomicilio,
Calle,
Numero,
Piso,
Departamento,
Barrio,
Localidad,
Provincia,
CodigoPostal	
)VALUES(
'IdDomicilio',
'Calle',
'Numero',
'Piso',
'Departamento',
'Barrio',
'Localidad',
'Provincia',
'CodigoPostal');"
        Dim cmdInsertar_EstimuloDolor = "INSERT INTO EstimuloDolor(
IdEstimuloDolor,
DolorEspontaneo,
DolorProvocado,
DolorAlFrio,
DolorAlCalor	
)VALUES(
'IdEstimuloDolor',
'DolorEspontaneo',
'DolorProvocado',
'DolorAlFrio',
'DolorAlCalor');"
        Dim cmdInsertar_Dificultad = "INSERT INTO Dificultad(
IdDificultad,
IdPersona,
Hablar,
Masticar,
AbrirBoca,
Tragar	
)VALUES(
'IdDificultad',
'IdPersona',
'Hablar',
'Masticar',
'AbrirBoca',
'Tragar');"
        Dim cmdInsertar_Anormalidad = "INSERT INTO Anormalidad(	
IdAnormalidad,
IdPersona,
Labios,
Lengua,
Paladar,
PisoDeBoca,
Carrillos,
Rebordes,
Trigono,
Retromolar	
)VALUES(
'IdAnormalidad',
'IdPersona',
'Labios',
'Lengua',
'Paladar',
'PisoDeBoca',
'Carrillos',
'Rebordes',
'Trigono',
'Retromolar');"
        Dim cmdInsertar_Lesiones = "INSERT INTO Lesiones(
IdLesiones,
IdPersona,
Manchas,
AbultamientoTejidos,
Ulceraciones,
Ampollas,
Otros,
SangranEncias,
CuandoSangranEncias,
SalePusEnBoca,
DondeSalePusEnBoca,
MovilidadEnDientes,
SienteAltosDientesAlMorder,
CaraHinchada,
SePusoHielo,
SePusoCalor,
SePusoOtros	
)VALUES(
'IdLesiones',
'IdPersona',
'Manchas',
'AbultamientoTejidos',
'Ulceraciones',
'Ampollas',
'Otros',
'SangranEncias',
'CuandoSangranEncias',
'SalePusEnBoca',
'DondeSalePusEnBoca',
'MovilidadEnDientes',
'SienteAltosDientesAlMorder',
'CaraHinchada',
'SePusoHielo',
'SePusoCalor',
'SePusoOtros');"
        Dim cmdInsertar_EstadoBucal = "INSERT INTO EstadoBucal(IdEstadoBucal,
IdPersona,
IdCuadraditos,
Sarro,
EnfermedadPeriodontal,
DiagnosticoPresuntivo,
PlanTratamiento,
CuandoPlanTratamiento,
CantidadDientes,
Observaciones
)VALUES(
'IdEstadoBucal',
'IdPersona',
'IdCuadraditos',
'Sarro',
'EnfermedadPeriodontal',
'DiagnosticoPresuntivo',
'PlanTratamiento',
'CuandoPlanTratamiento',
'CantidadDientes',
'Observaciones');"
        Dim cmdInsertar_Cuadraditos = "INSERT INTO Cuadraditos(IdCuadraditos,
i11,
i12,
i13,
i14,
i15,
i16,
i17,
i18,
i21,
i22,
i23,
i24,
i25,
i26,
i27,
i28,
i31,
i32,
i33,
i34,
i35,
i36,
i37,
i38,
i41,
i42,
i43,
i44,
i45,
i46,
i47,
i48,
i51,
i52,
i53,
i54,
i55,
i56,
i61,
i62,
i63,
i64,
i65,
i66,
i71,
i72,
i73,
i74,
i75,
i76,
i81,
i82,
i83,
i84,
i85,
i86
)VALUES(
'IdCuadraditos',
'i11',
'i12',
'i13',
'i14',
'i15',
'i16',
'i17',
'i18',
'i21',
'i22',
'i23',
'i24',
'i25',
'i26',
'i27',
'i28',
'i31',
'i32',
'i33',
'i34',
'i35',
'i36',
'i37',
'i38',
'i41',
'i42',
'i43',
'i44',
'i45',
'i46',
'i47',
'i48',
'i51',
'i52',
'i53',
'i54',
'i55',
'i56',
'i61',
'i62',
'i63',
'i64',
'i65',
'i66',
'i71',
'i72',
'i73',
'i74',
'i75',
'i76',
'i81',
'i82',
'i83',
'i84',
'i85',
'i86');"


        'Declaro una variable tipo OleDbConnetion y la contruyo enviando la cadena de conexión.
        'En este caso, la cadena de conexión está configurada en las "settings" de la aplicación
        ' Dim conexion = New OleDbConnection(My.Settings.My_Database)

        Dim conexion = New SQLiteConnection("Data Source=C:\Program Files (x86)\Tesis\cosa.db; Version=3;password=12345;")

        'Abro la conexión a la base de datos
        conexion.Open()

        Dim comando1 As New SQLiteCommand(cmdInsertar_Consulta, conexion) 'cmdUpdate_Consulta
        Dim comando2 As New SQLiteCommand(cmdInsertar_Persona, conexion) 'cmdUpdate_Persona
        Dim comando3 As New SQLiteCommand(cmdInsertar_CuestionarioFamiliar, conexion) 'cmdUpdate_CuestionarioFamiliar
        Dim comando4 As New SQLiteCommand(cmdInsertar_CuestionarioBasico, conexion) 'cmdUpdate_CuestionarioBasico
        Dim comando5 As New SQLiteCommand(cmdInsertar_CuestionarioProfundo, conexion) 'cmdUpdate_CuestionarioProfundo
        Dim comando6 As New SQLiteCommand(cmdInsertar_HistoriaClinicaOdontologica, conexion) 'cmdUpdate_HistoriaClinicaOdontologica
        Dim comando7 As New SQLiteCommand(cmdInsertar_Domicilio, conexion) 'cmdUpdate_Domicilio
        Dim comando8 As New SQLiteCommand(cmdInsertar_EstimuloDolor, conexion) 'cmdUpdate_EstimuloDolor
        Dim comando9 As New SQLiteCommand(cmdInsertar_Dificultad, conexion) 'cmdUpdate_Dificultad
        Dim comando10 As New SQLiteCommand(cmdInsertar_Anormalidad, conexion) 'cmdUpdate_Anormalidad
        Dim comando11 As New SQLiteCommand(cmdInsertar_Lesiones, conexion) 'cmdUpdate_Lesiones
        Dim comando12 As New SQLiteCommand(cmdInsertar_EstadoBucal, conexion) 'cmdUpdate_EstadoBucal
        Dim comando13 As New SQLiteCommand(cmdInsertar_Cuadraditos, conexion) 'cmdUpdate_Cuadraditos


        'Por medio de los siguientes pasos, se le especifica a cada parámetro de la sentencia SQL que debe ser reemplazado por
        'los valores que se especifican en cada caso.

        'comando.Parameters.Add(New SQLiteParameter("cosa", Me.cosa))

        comando1.Parameters.Add(New SQLiteParameter("IdConsulta", Me.IdConsulta))
        comando1.Parameters.Add(New SQLiteParameter("IdPersona", Me.IdPersona))
        comando1.Parameters.Add(New SQLiteParameter("IdOdontologo", Me.IdOdontologo))
        comando1.Parameters.Add(New SQLiteParameter("Fecha", Me.Fecha))
        comando1.Parameters.Add(New SQLiteParameter("IdCuestionario", Me.IdCuestionario))
        comando1.Parameters.Add(New SQLiteParameter("IdConsultorio", Me.IdConsultorio))
        comando1.Parameters.Add(New SQLiteParameter("IdEstadoBucal", Me.IdEstadoBucal))
        '----------------------------------------
        comando2.Parameters.Add(New SQLiteParameter("IdPersona", Me.IdPersona))
        comando2.Parameters.Add(New SQLiteParameter("Nombre", Me.Nombre))
        comando2.Parameters.Add(New SQLiteParameter("Apellido", Me.Apellido))
        comando2.Parameters.Add(New SQLiteParameter("NumAfiliado", Me.NumAfiliado))
        comando2.Parameters.Add(New SQLiteParameter("IdObraSocial", Me.IdObraSocial))
        comando2.Parameters.Add(New SQLiteParameter("FechaNacimiento", Me.FechaNacimiento))
        comando2.Parameters.Add(New SQLiteParameter("Telefono", Me.Telefono))
        comando2.Parameters.Add(New SQLiteParameter("Celular", Me.Celular))
        comando2.Parameters.Add(New SQLiteParameter("Edad", Me.Edad))
        comando2.Parameters.Add(New SQLiteParameter("IdEstadoCivil", Me.IdEstadoCivil))
        comando2.Parameters.Add(New SQLiteParameter("Nacionalidad", Me.Nacionalidad))
        comando2.Parameters.Add(New SQLiteParameter("NumDocumento", Me.NumDocumento))
        comando2.Parameters.Add(New SQLiteParameter("IdEstadoCivil", Me.IdEstadoCivil))
        comando2.Parameters.Add(New SQLiteParameter("ProfesionActividad", Me.ProfesionActividad))
        comando2.Parameters.Add(New SQLiteParameter("Titular", Me.Titular))
        comando2.Parameters.Add(New SQLiteParameter("LugarTrabajo", Me.LugarTrabajo))
        '----------------------------------------
        comando3.Parameters.Add(New SQLiteParameter("IdCuestionario", Me.IdCuestionario))
        comando3.Parameters.Add(New SQLiteParameter("PadreConVida", Me.PadreConVida))
        comando3.Parameters.Add(New SQLiteParameter("EnfermedadPasadaActualPadre", Me.EnfermedadPasadaActualPadre))
        comando3.Parameters.Add(New SQLiteParameter("MadreConVida", Me.MadreConVida))
        comando3.Parameters.Add(New SQLiteParameter("EnfermedadPasadaActualMadre", Me.EnfermedadPasadaActualMadre))
        comando3.Parameters.Add(New SQLiteParameter("Hermanos", Me.Hermanos))
        comando3.Parameters.Add(New SQLiteParameter("Sanos", Me.Sanos))
        comando3.Parameters.Add(New SQLiteParameter("EnfermoHermano", Me.EnfermoHermano))
        comando3.Parameters.Add(New SQLiteParameter("EnfermedadHermano", Me.EnfermedadHermano))
        '----------------------------------------
        comando4.Parameters.Add(New SQLiteParameter("IdCuestionario", Me.IdCuestionario))
        comando4.Parameters.Add(New SQLiteParameter("HaceTratamientoMedico", Me.HaceTratamientoMedico))
        comando4.Parameters.Add(New SQLiteParameter("CualTratamientoMedico", Me.CualTratamientoMedico))
        comando4.Parameters.Add(New SQLiteParameter("MedicamentosActuales", Me.MedicamentosActuales))
        comando4.Parameters.Add(New SQLiteParameter("MedicamentosUltimos5Anos", Me.MedicamentosUltimos5anos))
        comando4.Parameters.Add(New SQLiteParameter("OtraEnfermedadInfectocontagiosa", Me.OtraEnfermedadInfectocontagiosa))
        comando4.Parameters.Add(New SQLiteParameter("Transfusiones", Me.Transfusiones))
        comando4.Parameters.Add(New SQLiteParameter("FueOperadoAntes", Me.FueOperadoAntes))
        comando4.Parameters.Add(New SQLiteParameter("DeQueOperacion", Me.DeQueOperacion))
        comando4.Parameters.Add(New SQLiteParameter("CuandoOperacion", Me.CuandoOperacion))
        comando4.Parameters.Add(New SQLiteParameter("ProblemasRespiratorios", Me.ProblemasRespiratorios))
        comando4.Parameters.Add(New SQLiteParameter("CualProblemasRespiratorios", Me.CualProblemasRespiratorios))
        comando4.Parameters.Add(New SQLiteParameter("Fuma", Me.Fuma))
        comando4.Parameters.Add(New SQLiteParameter("Embarazada", Me.Embarazada))
        comando4.Parameters.Add(New SQLiteParameter("MesesEmbarazo", Me.MesesEmbarazo))
        comando4.Parameters.Add(New SQLiteParameter("OtraEnfermedadRecomendacion", Me.OtraEnfermedadRecomendacion))
        comando4.Parameters.Add(New SQLiteParameter("CualEnfermedadRecomendacion", Me.CualEnfermedadRecomendacion))
        comando4.Parameters.Add(New SQLiteParameter("TratamientoAlternativo", Me.TratamientoAlternativo))
        comando4.Parameters.Add(New SQLiteParameter("CualTratamientoAlternativo", Me.CualTratamientoAlternativo))
        '----------------------------------------
        comando5.Parameters.Add(New SQLiteParameter("IdCuestionario", Me.IdCuestionario))
        comando5.Parameters.Add(New SQLiteParameter("MedicoClinico", Me.MedicoClinico))
        comando5.Parameters.Add(New SQLiteParameter("ClinicaHospital", Me.ClinicaHospital))
        comando5.Parameters.Add(New SQLiteParameter("HaceDeporte", Me.HaceDeporte))
        comando5.Parameters.Add(New SQLiteParameter("MalestarAlRealizarlo", Me.MalestarAlRealizarlo))
        comando5.Parameters.Add(New SQLiteParameter("AlergicoDrogas", Me.AlergicoDrogas))
        comando5.Parameters.Add(New SQLiteParameter("AlergicoAnestesia", Me.AlergicoAnestesia))
        comando5.Parameters.Add(New SQLiteParameter("AlergicoPenisilina", Me.AlergicoPenisilina))
        comando5.Parameters.Add(New SQLiteParameter("AlergicoOtros", Me.AlergicoOtros))
        comando5.Parameters.Add(New SQLiteParameter("ComoCicatrizaSangra", Me.ComoCicatrizaSangra))
        comando5.Parameters.Add(New SQLiteParameter("Hiperlaxitud", Me.Hiperlaxitud))
        comando5.Parameters.Add(New SQLiteParameter("AntecedentesFiebreReumatica", Me.AntecedentesFiebreReumatica))
        comando5.Parameters.Add(New SQLiteParameter("MedicacionFiebreReumatica", Me.MedicacionFiebreReumatica))
        comando5.Parameters.Add(New SQLiteParameter("Diabetico", Me.Diabetico))
        comando5.Parameters.Add(New SQLiteParameter("DiabeticoControlado", Me.DiabeticoControlado))
        comando5.Parameters.Add(New SQLiteParameter("MedicacionDiabetico", Me.MedicacionDiabetico))
        comando5.Parameters.Add(New SQLiteParameter("ProblemaCardiaco", Me.ProblemaCardiaco))
        comando5.Parameters.Add(New SQLiteParameter("CualProblemaCardiaco", Me.CualProblemaCardiaco))
        comando5.Parameters.Add(New SQLiteParameter("TomaAnticoagulante", Me.TomaAnticoagulante))
        comando5.Parameters.Add(New SQLiteParameter("FrecuenciaAnticoagulante", Me.FrecuenciaAnticoagulante))
        comando5.Parameters.Add(New SQLiteParameter("PresionAlta", Me.PresionAlta))
        comando5.Parameters.Add(New SQLiteParameter("Chagas", Me.Chagas))
        comando5.Parameters.Add(New SQLiteParameter("TratamientoPresionChagas", Me.TratamientoPresionchagas))
        comando5.Parameters.Add(New SQLiteParameter("ProblemasRenales", Me.ProblemasRenales))
        comando5.Parameters.Add(New SQLiteParameter("UlceraGastrica", Me.UlceraGastrica))
        comando5.Parameters.Add(New SQLiteParameter("TuvoHepatitis", Me.TuvoHepatitis))
        comando5.Parameters.Add(New SQLiteParameter("HepatitisA", Me.HepatitisA))
        comando5.Parameters.Add(New SQLiteParameter("HepatitisB", Me.HepatitisB))
        comando5.Parameters.Add(New SQLiteParameter("HepatitisC", Me.HepatitisC))
        comando5.Parameters.Add(New SQLiteParameter("ProblemaHepatico", Me.ProblemaHepatico))
        comando5.Parameters.Add(New SQLiteParameter("CualProblemaHepatico", Me.CualProblemaHepatico))
        comando5.Parameters.Add(New SQLiteParameter("Convulsiones", Me.Convulsiones))
        comando5.Parameters.Add(New SQLiteParameter("Epilepsia", Me.Epilepsia))
        comando5.Parameters.Add(New SQLiteParameter("MedicacionEpilepsia", Me.MedicacionEpilepsia))
        comando5.Parameters.Add(New SQLiteParameter("SifilisGonorrea", Me.SifilisGonorrea))
        '----------------------------------------
        comando6.Parameters.Add(New SQLiteParameter("IdHCO", Me.IdHCO))
        comando6.Parameters.Add(New SQLiteParameter("IdPersona", Me.IdPersona))
        comando6.Parameters.Add(New SQLiteParameter("IdConsulta", Me.IdConsulta))
        comando6.Parameters.Add(New SQLiteParameter("MotivoConsulta", Me.MotivoConsulta))
        comando6.Parameters.Add(New SQLiteParameter("ConsultoAntes", Me.ConsultoAntes))
        comando6.Parameters.Add(New SQLiteParameter("TomoAlgoHCO", Me.TomoAlgoHCO))
        comando6.Parameters.Add(New SQLiteParameter("MedicacionHCO", Me.MedicacionHCO))
        comando6.Parameters.Add(New SQLiteParameter("DesdeCuandoMedicacionHCO", Me.DesdeCuandoMedicacionHCO))
        comando6.Parameters.Add(New SQLiteParameter("ObtuvoResultadoHCO", Me.ObtuvoResultadoHCO))
        comando6.Parameters.Add(New SQLiteParameter("HaTenidoDolor", Me.HaTenidoDolor))
        comando6.Parameters.Add(New SQLiteParameter("IdIntensidadDolor", Me.IdIntensidadDolor))
        comando6.Parameters.Add(New SQLiteParameter("IdDuracionDolor", Me.IdDuracionDolor))
        comando6.Parameters.Add(New SQLiteParameter("IdEstimuloDolor", Me.IdEstimuloDolor))
        comando6.Parameters.Add(New SQLiteParameter("DolorLocalizado", Me.DolorLocalizado))
        comando6.Parameters.Add(New SQLiteParameter("DondeDolor", Me.DondeDolor))
        comando6.Parameters.Add(New SQLiteParameter("Irradiado", Me.Irradiado))
        comando6.Parameters.Add(New SQLiteParameter("HaciaDondeIrradiado", Me.HaciaDondeIrradiado))
        comando6.Parameters.Add(New SQLiteParameter("PudoCalmarIrradiacion", Me.PudoCalmarIrradiacion))
        comando6.Parameters.Add(New SQLiteParameter("GolpeEnDientes", Me.GolpeEnDientes))
        comando6.Parameters.Add(New SQLiteParameter("CuandoGolpeEnDientes", Me.CuandoGolpeEnDientes))
        comando6.Parameters.Add(New SQLiteParameter("ComoGolpeEnDientes", Me.ComoGolpeEnDientes))
        comando6.Parameters.Add(New SQLiteParameter("FracturaDeDiente", Me.FracturaDeDiente))
        comando6.Parameters.Add(New SQLiteParameter("CualFracturaDeDiente", Me.CualFracturaDeDiente))
        comando6.Parameters.Add(New SQLiteParameter("RecibioTratamientoHCO", Me.RecibioTratamientoHCO))
        comando6.Parameters.Add(New SQLiteParameter("IdDificultad", Me.IdDificultad))
        comando6.Parameters.Add(New SQLiteParameter("IdAnormalidad", Me.IdAnormalidad))
        comando6.Parameters.Add(New SQLiteParameter("IdLesiones", Me.IdLesiones))
        '----------------------------------------
        comando7.Parameters.Add(New SQLiteParameter("IdDomicilio", Me.IdDomicilio))
        comando7.Parameters.Add(New SQLiteParameter("Calle", Me.Calle))
        comando7.Parameters.Add(New SQLiteParameter("Numero", Me.Numero))
        comando7.Parameters.Add(New SQLiteParameter("Piso", Me.Piso))
        comando7.Parameters.Add(New SQLiteParameter("Departamento", Me.Departamento))
        comando7.Parameters.Add(New SQLiteParameter("Barrio", Me.Barrio))
        comando7.Parameters.Add(New SQLiteParameter("Localidad", Me.Localidad))
        comando7.Parameters.Add(New SQLiteParameter("Provincia", Me.Provincia))
        comando7.Parameters.Add(New SQLiteParameter("CodigoPostal", Me.CodigoPostal))
        '----------------------------------------
        comando8.Parameters.Add(New SQLiteParameter("IdEstimuloDolor", Me.IdEstimuloDolor))
        comando8.Parameters.Add(New SQLiteParameter("DolorEspontaneo", Me.DolorEspontaneo))
        comando8.Parameters.Add(New SQLiteParameter("DolorProvocado", Me.DolorProvocado))
        comando8.Parameters.Add(New SQLiteParameter("DolorAlFrio", Me.DolorAlFrio))
        comando8.Parameters.Add(New SQLiteParameter("DolorAlCalor", Me.DolorAlCalor))
        '----------------------------------------
        comando9.Parameters.Add(New SQLiteParameter("IdDificultad", Me.IdDificultad))
        comando9.Parameters.Add(New SQLiteParameter("IdPersona", Me.IdPersona))
        comando9.Parameters.Add(New SQLiteParameter("Hablar", Me.Hablar))
        comando9.Parameters.Add(New SQLiteParameter("Masticar", Me.Masticar))
        comando9.Parameters.Add(New SQLiteParameter("AbrirBoca", Me.AbrirBoca))
        comando9.Parameters.Add(New SQLiteParameter("Tragar", Me.Tragar))
        '----------------------------------------
        comando10.Parameters.Add(New SQLiteParameter("IdAnormalidad", Me.IdAnormalidad))
        comando10.Parameters.Add(New SQLiteParameter("IdPersona", Me.IdPersona))
        comando10.Parameters.Add(New SQLiteParameter("Labios", Me.Labios))
        comando10.Parameters.Add(New SQLiteParameter("Lengua", Me.Lengua))
        comando10.Parameters.Add(New SQLiteParameter("Paladar", Me.Paladar))
        comando10.Parameters.Add(New SQLiteParameter("PisoDeBoca", Me.PisoDeBoca))
        comando10.Parameters.Add(New SQLiteParameter("Carrillos", Me.Carrillos))
        comando10.Parameters.Add(New SQLiteParameter("Rebordes", Me.Rebordes))
        comando10.Parameters.Add(New SQLiteParameter("Trigono", Me.Trigono))
        comando10.Parameters.Add(New SQLiteParameter("Retromolar", Me.Retromolar))
        '----------------------------------------
        comando11.Parameters.Add(New SQLiteParameter("IdLesiones", Me.IdLesiones))
        comando11.Parameters.Add(New SQLiteParameter("IdPersona", Me.IdPersona))
        comando11.Parameters.Add(New SQLiteParameter("Manchas", Me.Manchas))
        comando11.Parameters.Add(New SQLiteParameter("AbultamientoTejidos", Me.AbultamientoTejidos))
        comando11.Parameters.Add(New SQLiteParameter("Ulceraciones", Me.Ulceraciones))
        comando11.Parameters.Add(New SQLiteParameter("Ampollas", Me.Ampollas))
        comando11.Parameters.Add(New SQLiteParameter("Otros", Me.Otros))
        comando11.Parameters.Add(New SQLiteParameter("SangranEncias", Me.SangranEncias))
        comando11.Parameters.Add(New SQLiteParameter("CuandoSangranEncias", Me.CuandoSangranEncias))
        comando11.Parameters.Add(New SQLiteParameter("SalePusEnBoca", Me.SalePusEnBoca))
        comando11.Parameters.Add(New SQLiteParameter("DondeSalePusEnBoca", Me.DondeSalePusEnBoca))
        comando11.Parameters.Add(New SQLiteParameter("MovilidadEnDientes", Me.MovilidadEnDientes))
        comando11.Parameters.Add(New SQLiteParameter("SienteAltosDientesAlMorder", Me.SienteAltosDientesAlMorder))
        comando11.Parameters.Add(New SQLiteParameter("CaraHinchada", Me.CaraHinchada))
        comando11.Parameters.Add(New SQLiteParameter("SePusoHielo", Me.SePusoHielo))
        comando11.Parameters.Add(New SQLiteParameter("SePudoCalor", Me.SePusoCalor))
        comando11.Parameters.Add(New SQLiteParameter("SePusoOtros", Me.SePusoOtros))
        '----------------------------------------
        comando12.Parameters.Add(New SQLiteParameter("IdPersona", Me.IdPersona))
        comando12.Parameters.Add(New SQLiteParameter("IdCuadraditos", Me.IdCuadraditos))
        comando12.Parameters.Add(New SQLiteParameter("Sarro", Me.Sarro))
        comando12.Parameters.Add(New SQLiteParameter("EnfermedadPeriodontal", Me.EnfermedadPeriodontal))
        comando12.Parameters.Add(New SQLiteParameter("DiagnosticoPresuntivo", Me.DiagnosticoPresuntivo))
        comando12.Parameters.Add(New SQLiteParameter("PlanTratamiento", Me.PlanTratamiento))
        comando12.Parameters.Add(New SQLiteParameter("CuandoPlanTratamiento", Me.CuandoPlanTratamiento))
        comando12.Parameters.Add(New SQLiteParameter("CantidadDientes", Me.CantidadDientes))
        comando12.Parameters.Add(New SQLiteParameter("Observaciones", Me.Observaciones))
        '----------------------------------------
        comando13.Parameters.Add(New SQLiteParameter("IdCuadraditos", Me.IdCuadraditos))
        comando13.Parameters.Add(New SQLiteParameter("i11", Me.i11))
        comando13.Parameters.Add(New SQLiteParameter("i12", Me.i12))
        comando13.Parameters.Add(New SQLiteParameter("i13", Me.i13))
        comando13.Parameters.Add(New SQLiteParameter("i14", Me.i14))
        comando13.Parameters.Add(New SQLiteParameter("i15", Me.i15))
        comando13.Parameters.Add(New SQLiteParameter("i16", Me.i16))
        comando13.Parameters.Add(New SQLiteParameter("i17", Me.i17))
        comando13.Parameters.Add(New SQLiteParameter("i18", Me.i18))
        comando13.Parameters.Add(New SQLiteParameter("i21", Me.i21))
        comando13.Parameters.Add(New SQLiteParameter("i22", Me.i22))
        comando13.Parameters.Add(New SQLiteParameter("i23", Me.i23))
        comando13.Parameters.Add(New SQLiteParameter("i24", Me.i24))
        comando13.Parameters.Add(New SQLiteParameter("i25", Me.i25))
        comando13.Parameters.Add(New SQLiteParameter("i26", Me.i26))
        comando13.Parameters.Add(New SQLiteParameter("i27", Me.i27))
        comando13.Parameters.Add(New SQLiteParameter("i28", Me.i28))
        comando13.Parameters.Add(New SQLiteParameter("i31", Me.i31))
        comando13.Parameters.Add(New SQLiteParameter("i32", Me.i32))
        comando13.Parameters.Add(New SQLiteParameter("i33", Me.i33))
        comando13.Parameters.Add(New SQLiteParameter("i34", Me.i34))
        comando13.Parameters.Add(New SQLiteParameter("i35", Me.i35))
        comando13.Parameters.Add(New SQLiteParameter("i36", Me.i36))
        comando13.Parameters.Add(New SQLiteParameter("i37", Me.i37))
        comando13.Parameters.Add(New SQLiteParameter("i38", Me.i38))
        comando13.Parameters.Add(New SQLiteParameter("i41", Me.i41))
        comando13.Parameters.Add(New SQLiteParameter("i42", Me.i42))
        comando13.Parameters.Add(New SQLiteParameter("i43", Me.i43))
        comando13.Parameters.Add(New SQLiteParameter("i44", Me.i44))
        comando13.Parameters.Add(New SQLiteParameter("i45", Me.i45))
        comando13.Parameters.Add(New SQLiteParameter("i46", Me.i46))
        comando13.Parameters.Add(New SQLiteParameter("i47", Me.i47))
        comando13.Parameters.Add(New SQLiteParameter("i48", Me.i48))
        comando13.Parameters.Add(New SQLiteParameter("i51", Me.i51))
        comando13.Parameters.Add(New SQLiteParameter("i52", Me.i52))
        comando13.Parameters.Add(New SQLiteParameter("i53", Me.i53))
        comando13.Parameters.Add(New SQLiteParameter("i54", Me.i54))
        comando13.Parameters.Add(New SQLiteParameter("i55", Me.i55))
        comando13.Parameters.Add(New SQLiteParameter("i56", Me.i56))
        comando13.Parameters.Add(New SQLiteParameter("i61", Me.i61))
        comando13.Parameters.Add(New SQLiteParameter("i62", Me.i62))
        comando13.Parameters.Add(New SQLiteParameter("i63", Me.i63))
        comando13.Parameters.Add(New SQLiteParameter("i64", Me.i64))
        comando13.Parameters.Add(New SQLiteParameter("i65", Me.i65))
        comando13.Parameters.Add(New SQLiteParameter("i66", Me.i66))
        comando13.Parameters.Add(New SQLiteParameter("i71", Me.i71))
        comando13.Parameters.Add(New SQLiteParameter("i72", Me.i72))
        comando13.Parameters.Add(New SQLiteParameter("i73", Me.i73))
        comando13.Parameters.Add(New SQLiteParameter("i74", Me.i74))
        comando13.Parameters.Add(New SQLiteParameter("i75", Me.i75))
        comando13.Parameters.Add(New SQLiteParameter("i76", Me.i76))
        comando13.Parameters.Add(New SQLiteParameter("i81", Me.i81))
        comando13.Parameters.Add(New SQLiteParameter("i82", Me.i82))
        comando13.Parameters.Add(New SQLiteParameter("i83", Me.i83))
        comando13.Parameters.Add(New SQLiteParameter("i84", Me.i84))
        comando13.Parameters.Add(New SQLiteParameter("i85", Me.i85))
        comando13.Parameters.Add(New SQLiteParameter("i86", Me.i86))
        '----------------------------------------

        'Se ejecuta la sentencia SQL en la base de datos. Este método devuelve un número entero que significa cuantos registros
        'han sido afectados por la sentencia SQL.
        comando1.ExecuteNonQuery()
        comando2.ExecuteNonQuery()
        comando3.ExecuteNonQuery()
        comando4.ExecuteNonQuery()
        comando5.ExecuteNonQuery()
        comando6.ExecuteNonQuery()
        comando7.ExecuteNonQuery()
        comando8.ExecuteNonQuery()
        comando9.ExecuteNonQuery()
        comando10.ExecuteNonQuery()
        comando11.ExecuteNonQuery()
        comando12.ExecuteNonQuery()
        comando13.ExecuteNonQuery()


        'Obtengo el ultimo ID generado automáticamente en la base de datos
        Dim cmd_select = "select @@identity"
        comando1 = New SQLiteCommand(cmd_select, conexion)
        comando2 = New SQLiteCommand(cmd_select, conexion)
        comando3 = New SQLiteCommand(cmd_select, conexion)
        comando4 = New SQLiteCommand(cmd_select, conexion)
        comando5 = New SQLiteCommand(cmd_select, conexion)
        comando6 = New SQLiteCommand(cmd_select, conexion)
        comando7 = New SQLiteCommand(cmd_select, conexion)
        comando8 = New SQLiteCommand(cmd_select, conexion)
        comando9 = New SQLiteCommand(cmd_select, conexion)
        comando10 = New SQLiteCommand(cmd_select, conexion)
        comando11 = New SQLiteCommand(cmd_select, conexion)
        comando12 = New SQLiteCommand(cmd_select, conexion)
        comando13 = New SQLiteCommand(cmd_select, conexion)

        Dim dt = New DataTable()

        Dim da1 = New SQLiteDataAdapter(comando1)
        Dim da2 = New SQLiteDataAdapter(comando2)
        Dim da3 = New SQLiteDataAdapter(comando3)
        Dim da4 = New SQLiteDataAdapter(comando4)
        Dim da5 = New SQLiteDataAdapter(comando5)
        Dim da6 = New SQLiteDataAdapter(comando6)
        Dim da7 = New SQLiteDataAdapter(comando7)
        Dim da8 = New SQLiteDataAdapter(comando8)
        Dim da9 = New SQLiteDataAdapter(comando9)
        Dim da10 = New SQLiteDataAdapter(comando10)
        Dim da11 = New SQLiteDataAdapter(comando11)
        Dim da12 = New SQLiteDataAdapter(comando12)
        Dim da13 = New SQLiteDataAdapter(comando13)

        da1.Fill(dt)
        da2.Fill(dt)
        da3.Fill(dt)
        da4.Fill(dt)
        da5.Fill(dt)
        da6.Fill(dt)
        da7.Fill(dt)
        da8.Fill(dt)
        da9.Fill(dt)
        da10.Fill(dt)
        da11.Fill(dt)
        da12.Fill(dt)
        da13.Fill(dt)

        'Obtengo el valor y se lo asigno al atributo Codigo.
        Me.IdConsulta = Convert.ToInt32(dt.Rows(0)(0))
        Me.IdPersona = Convert.ToInt32(dt.Rows(0)(0))
        Me.IdCuestionario = Convert.ToInt32(dt.Rows(0)(0))
        Me.IdHCO = Convert.ToInt32(dt.Rows(0)(0))
        Me.IdDomicilio = Convert.ToInt32(dt.Rows(0)(0))
        Me.IdEstimuloDolor = Convert.ToInt32(dt.Rows(0)(0))
        Me.IdDificultad = Convert.ToInt32(dt.Rows(0)(0))
        Me.IdAnormalidad = Convert.ToInt32(dt.Rows(0)(0))
        Me.IdLesiones = Convert.ToInt32(dt.Rows(0)(0))
        Me.IdEstadoBucal = Convert.ToInt32(dt.Rows(0)(0))
        Me.IdCuadraditos = Convert.ToInt32(dt.Rows(0)(0))

        'Cierro la conexión a la base de datos
        conexion.Dispose()
        'Devuelvo el código generado automáticamente tras la inserción en la tabla
        Return Me.IdConsulta
        Return Me.IdPersona
        Return Me.IdCuestionario
        Return Me.IdHCO
        Return Me.IdDomicilio
        Return Me.IdEstimuloDolor
        Return Me.IdDificultad
        Return Me.IdAnormalidad
        Return Me.IdLesiones
        Return Me.IdEstadoBucal
        Return Me.IdCuadraditos

    End Function
    '===========================================================================================
    Public Sub Actualizar()

        Dim cmdUpdate_Consulta = "UPDATE Consulta SET
        IdPersona='IdPersona',
        IdOdontologo='IdOdontologo',
        Fecha='Fecha',
        IdCuestionario='IdCuestionario',
        IdConsultorio='IdConsultorio',
        IdEstadoBucal='IdEstadoBucal'
        WHERE IdConsulta='IdConsulta';"
        Dim cmdUpdate_Persona = "UPDATE Persona SET
        Nombre='Nombre',
        Apellido='Apellido',
        NumAfiliado='NumAfiliado',
        IdObraSocial='IdObraSocial',
        FechaNacimiento='FechaNacimiento',
        Telefono='Telefono',
        Celular='Celular',
        Edad='Edad',
        IdEstadoCivil='IdEstadoCivil',
        Nacionalidad='Nacionalidad',
        NumDocumento='NumDocumento',
        IdEstadoCivil='IdEstadoCivil',
        ProfesionActividad='ProfesionActividad',
        Titular='Titular',
        LugarTrabajo='LugarTrabajo'
        WHERE IdPersona='IdPersona';"
        Dim cmdUpdate_CuestionarioFamiliar = "UPDATE CuestionarioFamiliar SET
        PadreConVida='PadreConVida',
        EnfermedadPasadaActualPadre='EnfermedadPasadaActualPadre',
        MadreConVida='MadreConVida',
        EnfermedadPasadaActualMadre='EnfermedadPasadaActualMadre',
        Hermanos='Hermanos',
        Sanos='Sanos',
        EnfermoHermano='EnfermoHermano',
        EnfermedadHermano='EnfermedadHermano'
        WHERE IdCuestionario='IdCuestionario';"
        Dim cmdUpdate_CuestionarioBasico = "UPDATE CuestionarioBasico SET
        HaceTratamientoMedico='HaceTratamientoMedico'
        CualTratamientoMedico='CualTratamientoMedico',
        MedicamentosActuales='MedicamentosActuales',
        MedicamentosUltimos5Anos='MedicamentosUltimos5Anos',
        OtraEnfermedadInfectocontagiosa='OtraEnfermedadInfectocontagiosa',
        Transfusiones='Transfusiones',
        FueOperadoAntes='FueOperadoAntes',
        DeQueOperacion='DeQueOperacion',
        CuandoOperacion='CuandoOperacion',
        ProblemasRespiratorios='ProblemasRespiratorios',
        CualProblemasRespiratorios='CualProblemasRespiratorios',
        Fuma='Fuma',
        Embarazada='Embarazada',
        MesesEmbarazo='MesesEmbarazo',
        OtraEnfermedadRecomendacion='OtraEnfermedadRecomendacion',
        CualEnfermedadRecomendacion='CualEnfermedadRecomendacion',
        TratamientoAlternativo='TratamientoAlternativo',
        CualTratamientoAlternativo='CualTratamientoAlternativo',
        WHERE IdCuestionario= 'IdCuestionario';"
        Dim cmdUpdate_CuestionarioProfundo = "UPDATE CuestionarioProfundo SET
        MedicoClinico='MedicoClinico',
        ClinicaHospital='ClinicaHospital',
        HaceDeporte='HaceDeporte',
        MalestarAlRealizarlo='MalestarAlRealizarlo',
        AlergicoDrogas='AlergicoDrogas',
        AlergicoAnestesia='AlergicoAnestesia',
        AlergicoPenisilina='AlergicoPenisilina',
        AlergicoOtros='AlergicoOtros',
        ComoCicatrizaSangra='ComoCicatrizaSangra',
        Hiperlaxitud='Hiperlaxitud',
        AntecedentesFiebreReumatica='AntecedentesFiebreReumatica',
        MedicacionFiebreReumatica='MedicacionFiebreReumatica',
        Diabetico='Diabetico',
        DiabeticoControlado='DiabeticoControlado',
        MedicacionDiabetico='MedicacionDiabetico',
        ProblemaCardiaco='ProblemaCardiaco',
        CualProblemaCardiaco='CualProblemaCardiaco',
        TomaAnticoagulante='TomaAnticoagulante',
        FrecuenciaAnticoagulante='FrecuenciaAnticoagulante',
        PresionAlta='PresionAlta',
        Chagas='Chagas',
        TratamientoPresionChagas='TratamientoPresionChagas',
        ProblemasRenales='ProblemasRenales',
        UlceraGastrica='UlceraGastrica',
        TuvoHepatitis='TuvoHepatitis',
        HepatitisA='HepatitisA',
        HepatitisB='HepatitisB',
        HepatitisC='HepatitisC',
        ProblemaHepatico='ProblemaHepatico',
        CualProblemaHepatico='CualProblemaHepatico',
        Convulsiones='Convulsiones',
        Epilepsia='Epilepsia',
        MedicacionEpilepsia='MedicacionEpilepsia',
        SifilisGonorrea='SifilisGonorrea'
        WHERE IdCuestionario= 'IdCuestionario';"
        Dim cmdUpdate_HistoriaClinicaOdontologica = "UPDATE HistoriaClinicaOdontologica SET
        IdPersona,='IdPersona,',
        IdConsulta,='IdConsulta,',
        MotivoConsulta,='MotivoConsulta,',
        ConsultoAntes,='ConsultoAntes,',
        TomoAlgoHCO,='TomoAlgoHCO,',
        MedicacionHCO,='MedicacionHCO,',
        DesdeCuandoMedicacionHCO,='DesdeCuandoMedicacionHCO,',
        ObtuvoResultadoHCO,='ObtuvoResultadoHCO,',
        HaTenidoDolor,='HaTenidoDolor,',
        IdIntensidadDolor,='IdIntensidadDolor,',
        IdDuracionDolor,='IdDuracionDolor,',
        IdEstimuloDolor,='IdEstimuloDolor,',
        DolorLocalizado,='DolorLocalizado,',
        DondeDolor,='DondeDolor,',
        Irradiado,='Irradiado,',
        HaciaDondeIrradiado,='HaciaDondeIrradiado,',
        PudoCalmarIrradiacion,='PudoCalmarIrradiacion,',
        GolpeEnDientes,='GolpeEnDientes,',
        CuandoGolpeEnDientes,='CuandoGolpeEnDientes,',
        ComoGolpeEnDientes,='ComoGolpeEnDientes,',
        FracturaDeDiente,='FracturaDeDiente,',
        CualFracturaDeDiente,='CualFracturaDeDiente,',
        RecibioTratamientoHCO,='RecibioTratamientoHCO,',
        IdDificultad,='IdDificultad,',
        IdAnormalidad,='IdAnormalidad,',
        IdLesiones='IdLesiones'
        WHERE IdHCO='IdHCO';"
        Dim cmdUpdate_Domicilio = "UPDATE Domicilio SET
        Calle='Calle',
        Numero='Numero',
        Piso='Piso',
        Departamento='Departamento',
        Barrio='Barrio',
        Localidad='Localidad',
        Provincia='Provincia',
        CodigoPostal='CodigoPostal'
        WHERE IdDomicilio='IdDomicilio';"
        Dim cmdUpdate_EstimuloDolor = "UPDATE EstimuloDolor SET
        DolorEspontaneo='DolorEspontaneo',
        DolorProvocado='DolorProvocado',
        DolorAlFrio='DolorAlFrio',
        DolorAlCalor='DolorAlCalor'
        WHERE IdEstimuloDolor='IdEstimuloDolor';"
        Dim cmdUpdate_Dificultad = "UPDATE Dificultad SET
        IdPersona='IdPersona',
        Hablar='Hablar',
        Masticar='Masticar',
        AbrirBoca='AbrirBoca',
        Tragar='Tragar'
        WHERE IdDificultad='IdDificultad';"
        Dim cmdUpdate_Anormalidad = "UPDATE Anormalidad SET
        IdPersona='IdPersona',
        Labios='Labios',
        Lengua='Lengua',
        Paladar='Paladar',
        PisoDeBoca='PisoDeBoca',
        Carrillos='Carrillos',
        Rebordes='Rebordes',
        Trigono='Trigono',
        Retromolar='Retromolar'
        WHERE IdAnormalidad='IdAnormalidad';"
        Dim cmdUpdate_Lesiones = "UPDATE Lesiones SET
        IdPersona='IdPersona',
        Manchas='Manchas',
        AbultamientoTejidos='AbultamientoTejidos',
        Ulceraciones='Ulceraciones',
        Ampollas='Ampollas',
        Otros='Otros',
        SangranEncias='SangranEncias',
        CuandoSangranEncias='CuandoSangranEncias',
        SalePusEnBoca='SalePusEnBoca',
        DondeSalePusEnBoca='DondeSalePusEnBoca',
        MovilidadEnDientes='MovilidadEnDientes',
        SienteAltosDientesAlMorder='SienteAltosDientesAlMorder',
        CaraHinchada='CaraHinchada',
        SePusoHielo='SePusoHielo',
        SePusoCalor='SePusoCalor',
        SePusoOtros='SePusoOtros'
        WHERE IdLesiones='IdLesiones';"
        Dim cmdUpdate_EstadoBucal = "UPDATE EstadoBucal SET
        IdPersona='IdPersona',
        IdCuadraditos='IdCuadraditos',
        Sarro='Sarro',
        EnfermedadPeriodontal='EnfermedadPeriodontal',
        DiagnosticoPresuntivo='DiagnosticoPresuntivo',
        PlanTratamiento='PlanTratamiento',
        CuandoPlanTratamiento='CuandoPlanTratamiento',
        CantidadDientes='CantidadDientes',
        Observaciones='Observaciones'
        WHERE IdEstadoBucal='IdEstadoBucal';"
        Dim cmdUpdate_Cuadraditos = "UPDATE Cuadraditos SET
        i11	='	i11	',
        i12	='	i12	',
        i13	='	i13	',
        i14	='	i14	',
        i15	='	i15	',
        i16	='	i16	',
        i17	='	i17	',
        i18	='	i18	',
        i21	='	i21	',
        i22	='	i22	',
        i23	='	i23	',
        i24	='	i24	',
        i25	='	i25	',
        i26	='	i26	',
        i27	='	i27	',
        i28	='	i28	',
        i31	='	i31	',
        i32	='	i32	',
        i33	='	i33	',
        i34	='	i34	',
        i35	='	i35	',
        i36	='	i36	',
        i37	='	i37	',
        i38	='	i38	',
        i41	='	i41	',
        i42	='	i42	',
        i43	='	i43	',
        i44	='	i44	',
        i45	='	i45	',
        i46	='	i46	',
        i47	='	i47	',
        i48	='	i48	',
        i51	='	i51	',
        i52	='	i52	',
        i53	='	i53	',
        i54	='	i54	',
        i55	='	i55	',
        i56	='	i56	',
        i61	='	i61	',
        i62	='	i62	',
        i63	='	i63	',
        i64	='	i64	',
        i65	='	i65	',
        i66	='	i66	',
        i71	='	i71	',
        i72	='	i72	',
        i73	='	i73	',
        i74	='	i74	',
        i75	='	i75	',
        i76,	='	i76,	',
        i81	='	i81	',
        i82	='	i82	',
        i83	='	i83	',
        i84	='	i84	',
        i85	='	i85	',
        i86	='	i86	'
        WHERE IdCuadraditos='IdCuadraditos';"


        Dim conexion = New SQLiteConnection("Data Source=C:\Program Files (x86)\Tesis\cosa.db; Version=3;password=12345;")


        conexion.Open()


        Dim comando1 As New SQLiteCommand(cmdUpdate_Consulta, conexion)
        Dim comando2 As New SQLiteCommand(cmdUpdate_Persona, conexion)
        Dim comando3 As New SQLiteCommand(cmdUpdate_CuestionarioFamiliar, conexion)
        Dim comando4 As New SQLiteCommand(cmdUpdate_CuestionarioBasico, conexion)
        Dim comando5 As New SQLiteCommand(cmdUpdate_CuestionarioProfundo, conexion)
        Dim comando6 As New SQLiteCommand(cmdUpdate_HistoriaClinicaOdontologica, conexion)
        Dim comando7 As New SQLiteCommand(cmdUpdate_Domicilio, conexion)
        Dim comando8 As New SQLiteCommand(cmdUpdate_EstimuloDolor, conexion)
        Dim comando9 As New SQLiteCommand(cmdUpdate_Dificultad, conexion)
        Dim comando10 As New SQLiteCommand(cmdUpdate_Anormalidad, conexion)
        Dim comando11 As New SQLiteCommand(cmdUpdate_Lesiones, conexion)
        Dim comando12 As New SQLiteCommand(cmdUpdate_EstadoBucal, conexion)
        Dim comando13 As New SQLiteCommand(cmdUpdate_Cuadraditos, conexion)


        '----------------------------------------
        comando1.Parameters.Add(New SQLiteParameter("IdConsulta", Me.IdConsulta))
        comando1.Parameters.Add(New SQLiteParameter("IdPersona", Me.IdPersona))
        comando1.Parameters.Add(New SQLiteParameter("IdOdontologo", Me.IdOdontologo))
        comando1.Parameters.Add(New SQLiteParameter("Fecha", Me.Fecha))
        comando1.Parameters.Add(New SQLiteParameter("IdCuestionario", Me.IdCuestionario))
        comando1.Parameters.Add(New SQLiteParameter("IdConsultorio", Me.IdConsultorio))
        comando1.Parameters.Add(New SQLiteParameter("IdEstadoBucal", Me.IdEstadoBucal))
        '----------------------------------------
        comando2.Parameters.Add(New SQLiteParameter("IdPersona", Me.IdPersona))
        comando2.Parameters.Add(New SQLiteParameter("Nombre", Me.Nombre))
        comando2.Parameters.Add(New SQLiteParameter("Apellido", Me.Apellido))
        comando2.Parameters.Add(New SQLiteParameter("NumAfiliado", Me.NumAfiliado))
        comando2.Parameters.Add(New SQLiteParameter("IdObraSocial", Me.IdObraSocial))
        comando2.Parameters.Add(New SQLiteParameter("FechaNacimiento", Me.FechaNacimiento))
        comando2.Parameters.Add(New SQLiteParameter("Telefono", Me.Telefono))
        comando2.Parameters.Add(New SQLiteParameter("Celular", Me.Celular))
        comando2.Parameters.Add(New SQLiteParameter("Edad", Me.Edad))
        comando2.Parameters.Add(New SQLiteParameter("IdEstadoCivil", Me.IdEstadoCivil))
        comando2.Parameters.Add(New SQLiteParameter("Nacionalidad", Me.Nacionalidad))
        comando2.Parameters.Add(New SQLiteParameter("NumDocumento", Me.NumDocumento))
        comando2.Parameters.Add(New SQLiteParameter("IdEstadoCivil", Me.IdEstadoCivil))
        comando2.Parameters.Add(New SQLiteParameter("ProfesionActividad", Me.ProfesionActividad))
        comando2.Parameters.Add(New SQLiteParameter("Titular", Me.Titular))
        comando2.Parameters.Add(New SQLiteParameter("LugarTrabajo", Me.LugarTrabajo))
        '----------------------------------------
        comando3.Parameters.Add(New SQLiteParameter("IdCuestionario", Me.IdCuestionario))
        comando3.Parameters.Add(New SQLiteParameter("PadreConVida", Me.PadreConVida))
        comando3.Parameters.Add(New SQLiteParameter("EnfermedadPasadaActualPadre", Me.EnfermedadPasadaActualPadre))
        comando3.Parameters.Add(New SQLiteParameter("MadreConVida", Me.MadreConVida))
        comando3.Parameters.Add(New SQLiteParameter("EnfermedadPasadaActualMadre", Me.EnfermedadPasadaActualMadre))
        comando3.Parameters.Add(New SQLiteParameter("Hermanos", Me.Hermanos))
        comando3.Parameters.Add(New SQLiteParameter("Sanos", Me.Sanos))
        comando3.Parameters.Add(New SQLiteParameter("EnfermoHermano", Me.EnfermoHermano))
        comando3.Parameters.Add(New SQLiteParameter("EnfermedadHermano", Me.EnfermedadHermano))
        '----------------------------------------
        comando4.Parameters.Add(New SQLiteParameter("IdCuestionario", Me.IdCuestionario))
        comando4.Parameters.Add(New SQLiteParameter("HaceTratamientoMedico", Me.HaceTratamientoMedico))
        comando4.Parameters.Add(New SQLiteParameter("CualTratamientoMedico", Me.CualTratamientoMedico))
        comando4.Parameters.Add(New SQLiteParameter("MedicamentosActuales", Me.MedicamentosActuales))
        comando4.Parameters.Add(New SQLiteParameter("MedicamentosUltimos5Anos", Me.MedicamentosUltimos5anos))
        comando4.Parameters.Add(New SQLiteParameter("OtraEnfermedadInfectocontagiosa", Me.OtraEnfermedadInfectocontagiosa))
        comando4.Parameters.Add(New SQLiteParameter("Transfusiones", Me.Transfusiones))
        comando4.Parameters.Add(New SQLiteParameter("FueOperadoAntes", Me.FueOperadoAntes))
        comando4.Parameters.Add(New SQLiteParameter("DeQueOperacion", Me.DeQueOperacion))
        comando4.Parameters.Add(New SQLiteParameter("CuandoOperacion", Me.CuandoOperacion))
        comando4.Parameters.Add(New SQLiteParameter("ProblemasRespiratorios", Me.ProblemasRespiratorios))
        comando4.Parameters.Add(New SQLiteParameter("CualProblemasRespiratorios", Me.CualProblemasRespiratorios))
        comando4.Parameters.Add(New SQLiteParameter("Fuma", Me.Fuma))
        comando4.Parameters.Add(New SQLiteParameter("Embarazada", Me.Embarazada))
        comando4.Parameters.Add(New SQLiteParameter("MesesEmbarazo", Me.MesesEmbarazo))
        comando4.Parameters.Add(New SQLiteParameter("OtraEnfermedadRecomendacion", Me.OtraEnfermedadRecomendacion))
        comando4.Parameters.Add(New SQLiteParameter("CualEnfermedadRecomendacion", Me.CualEnfermedadRecomendacion))
        comando4.Parameters.Add(New SQLiteParameter("TratamientoAlternativo", Me.TratamientoAlternativo))
        comando4.Parameters.Add(New SQLiteParameter("CualTratamientoAlternativo", Me.CualTratamientoAlternativo))
        '----------------------------------------
        comando5.Parameters.Add(New SQLiteParameter("IdCuestionario", Me.IdCuestionario))
        comando5.Parameters.Add(New SQLiteParameter("MedicoClinico", Me.MedicoClinico))
        comando5.Parameters.Add(New SQLiteParameter("ClinicaHospital", Me.ClinicaHospital))
        comando5.Parameters.Add(New SQLiteParameter("HaceDeporte", Me.HaceDeporte))
        comando5.Parameters.Add(New SQLiteParameter("MalestarAlRealizarlo", Me.MalestarAlRealizarlo))
        comando5.Parameters.Add(New SQLiteParameter("AlergicoDrogas", Me.AlergicoDrogas))
        comando5.Parameters.Add(New SQLiteParameter("AlergicoAnestesia", Me.AlergicoAnestesia))
        comando5.Parameters.Add(New SQLiteParameter("AlergicoPenisilina", Me.AlergicoPenisilina))
        comando5.Parameters.Add(New SQLiteParameter("AlergicoOtros", Me.AlergicoOtros))
        comando5.Parameters.Add(New SQLiteParameter("ComoCicatrizaSangra", Me.ComoCicatrizaSangra))
        comando5.Parameters.Add(New SQLiteParameter("Hiperlaxitud", Me.Hiperlaxitud))
        comando5.Parameters.Add(New SQLiteParameter("AntecedentesFiebreReumatica", Me.AntecedentesFiebreReumatica))
        comando5.Parameters.Add(New SQLiteParameter("MedicacionFiebreReumatica", Me.MedicacionFiebreReumatica))
        comando5.Parameters.Add(New SQLiteParameter("Diabetico", Me.Diabetico))
        comando5.Parameters.Add(New SQLiteParameter("DiabeticoControlado", Me.DiabeticoControlado))
        comando5.Parameters.Add(New SQLiteParameter("MedicacionDiabetico", Me.MedicacionDiabetico))
        comando5.Parameters.Add(New SQLiteParameter("ProblemaCardiaco", Me.ProblemaCardiaco))
        comando5.Parameters.Add(New SQLiteParameter("CualProblemaCardiaco", Me.CualProblemaCardiaco))
        comando5.Parameters.Add(New SQLiteParameter("TomaAnticoagulante", Me.TomaAnticoagulante))
        comando5.Parameters.Add(New SQLiteParameter("FrecuenciaAnticoagulante", Me.FrecuenciaAnticoagulante))
        comando5.Parameters.Add(New SQLiteParameter("PresionAlta", Me.PresionAlta))
        comando5.Parameters.Add(New SQLiteParameter("Chagas", Me.Chagas))
        comando5.Parameters.Add(New SQLiteParameter("TratamientoPresionChagas", Me.TratamientoPresionchagas))
        comando5.Parameters.Add(New SQLiteParameter("ProblemasRenales", Me.ProblemasRenales))
        comando5.Parameters.Add(New SQLiteParameter("UlceraGastrica", Me.UlceraGastrica))
        comando5.Parameters.Add(New SQLiteParameter("TuvoHepatitis", Me.TuvoHepatitis))
        comando5.Parameters.Add(New SQLiteParameter("HepatitisA", Me.HepatitisA))
        comando5.Parameters.Add(New SQLiteParameter("HepatitisB", Me.HepatitisB))
        comando5.Parameters.Add(New SQLiteParameter("HepatitisC", Me.HepatitisC))
        comando5.Parameters.Add(New SQLiteParameter("ProblemaHepatico", Me.ProblemaHepatico))
        comando5.Parameters.Add(New SQLiteParameter("CualProblemaHepatico", Me.CualProblemaHepatico))
        comando5.Parameters.Add(New SQLiteParameter("Convulsiones", Me.Convulsiones))
        comando5.Parameters.Add(New SQLiteParameter("Epilepsia", Me.Epilepsia))
        comando5.Parameters.Add(New SQLiteParameter("MedicacionEpilepsia", Me.MedicacionEpilepsia))
        comando5.Parameters.Add(New SQLiteParameter("SifilisGonorrea", Me.SifilisGonorrea))
        '----------------------------------------
        comando6.Parameters.Add(New SQLiteParameter("IdHCO", Me.IdHCO))
        comando6.Parameters.Add(New SQLiteParameter("IdPersona", Me.IdPersona))
        comando6.Parameters.Add(New SQLiteParameter("IdConsulta", Me.IdConsulta))
        comando6.Parameters.Add(New SQLiteParameter("MotivoConsulta", Me.MotivoConsulta))
        comando6.Parameters.Add(New SQLiteParameter("ConsultoAntes", Me.ConsultoAntes))
        comando6.Parameters.Add(New SQLiteParameter("TomoAlgoHCO", Me.TomoAlgoHCO))
        comando6.Parameters.Add(New SQLiteParameter("MedicacionHCO", Me.MedicacionHCO))
        comando6.Parameters.Add(New SQLiteParameter("DesdeCuandoMedicacionHCO", Me.DesdeCuandoMedicacionHCO))
        comando6.Parameters.Add(New SQLiteParameter("ObtuvoResultadoHCO", Me.ObtuvoResultadoHCO))
        comando6.Parameters.Add(New SQLiteParameter("HaTenidoDolor", Me.HaTenidoDolor))
        comando6.Parameters.Add(New SQLiteParameter("IdIntensidadDolor", Me.IdIntensidadDolor))
        comando6.Parameters.Add(New SQLiteParameter("IdDuracionDolor", Me.IdDuracionDolor))
        comando6.Parameters.Add(New SQLiteParameter("IdEstimuloDolor", Me.IdEstimuloDolor))
        comando6.Parameters.Add(New SQLiteParameter("DolorLocalizado", Me.DolorLocalizado))
        comando6.Parameters.Add(New SQLiteParameter("DondeDolor", Me.DondeDolor))
        comando6.Parameters.Add(New SQLiteParameter("Irradiado", Me.Irradiado))
        comando6.Parameters.Add(New SQLiteParameter("HaciaDondeIrradiado", Me.HaciaDondeIrradiado))
        comando6.Parameters.Add(New SQLiteParameter("PudoCalmarIrradiacion", Me.PudoCalmarIrradiacion))
        comando6.Parameters.Add(New SQLiteParameter("GolpeEnDientes", Me.GolpeEnDientes))
        comando6.Parameters.Add(New SQLiteParameter("CuandoGolpeEnDientes", Me.CuandoGolpeEnDientes))
        comando6.Parameters.Add(New SQLiteParameter("ComoGolpeEnDientes", Me.ComoGolpeEnDientes))
        comando6.Parameters.Add(New SQLiteParameter("FracturaDeDiente", Me.FracturaDeDiente))
        comando6.Parameters.Add(New SQLiteParameter("CualFracturaDeDiente", Me.CualFracturaDeDiente))
        comando6.Parameters.Add(New SQLiteParameter("RecibioTratamientoHCO", Me.RecibioTratamientoHCO))
        comando6.Parameters.Add(New SQLiteParameter("IdDificultad", Me.IdDificultad))
        comando6.Parameters.Add(New SQLiteParameter("IdAnormalidad", Me.IdAnormalidad))
        comando6.Parameters.Add(New SQLiteParameter("IdLesiones", Me.IdLesiones))
        '----------------------------------------
        comando7.Parameters.Add(New SQLiteParameter("IdDomicilio", Me.IdDomicilio))
        comando7.Parameters.Add(New SQLiteParameter("Calle", Me.Calle))
        comando7.Parameters.Add(New SQLiteParameter("Numero", Me.Numero))
        comando7.Parameters.Add(New SQLiteParameter("Piso", Me.Piso))
        comando7.Parameters.Add(New SQLiteParameter("Departamento", Me.Departamento))
        comando7.Parameters.Add(New SQLiteParameter("Barrio", Me.Barrio))
        comando7.Parameters.Add(New SQLiteParameter("Localidad", Me.Localidad))
        comando7.Parameters.Add(New SQLiteParameter("Provincia", Me.Provincia))
        comando7.Parameters.Add(New SQLiteParameter("CodigoPostal", Me.CodigoPostal))
        '----------------------------------------
        comando8.Parameters.Add(New SQLiteParameter("IdEstimuloDolor", Me.IdEstimuloDolor))
        comando8.Parameters.Add(New SQLiteParameter("DolorEspontaneo", Me.DolorEspontaneo))
        comando8.Parameters.Add(New SQLiteParameter("DolorProvocado", Me.DolorProvocado))
        comando8.Parameters.Add(New SQLiteParameter("DolorAlFrio", Me.DolorAlFrio))
        comando8.Parameters.Add(New SQLiteParameter("DolorAlCalor", Me.DolorAlCalor))
        '----------------------------------------
        comando9.Parameters.Add(New SQLiteParameter("IdDificultad", Me.IdDificultad))
        comando9.Parameters.Add(New SQLiteParameter("IdPersona", Me.IdPersona))
        comando9.Parameters.Add(New SQLiteParameter("Hablar", Me.Hablar))
        comando9.Parameters.Add(New SQLiteParameter("Masticar", Me.Masticar))
        comando9.Parameters.Add(New SQLiteParameter("AbrirBoca", Me.AbrirBoca))
        comando9.Parameters.Add(New SQLiteParameter("Tragar", Me.Tragar))
        '----------------------------------------
        comando10.Parameters.Add(New SQLiteParameter("IdAnormalidad", Me.IdAnormalidad))
        comando10.Parameters.Add(New SQLiteParameter("IdPersona", Me.IdPersona))
        comando10.Parameters.Add(New SQLiteParameter("Labios", Me.Labios))
        comando10.Parameters.Add(New SQLiteParameter("Lengua", Me.Lengua))
        comando10.Parameters.Add(New SQLiteParameter("Paladar", Me.Paladar))
        comando10.Parameters.Add(New SQLiteParameter("PisoDeBoca", Me.PisoDeBoca))
        comando10.Parameters.Add(New SQLiteParameter("Carrillos", Me.Carrillos))
        comando10.Parameters.Add(New SQLiteParameter("Rebordes", Me.Rebordes))
        comando10.Parameters.Add(New SQLiteParameter("Trigono", Me.Trigono))
        comando10.Parameters.Add(New SQLiteParameter("Retromolar", Me.Retromolar))
        '----------------------------------------
        comando11.Parameters.Add(New SQLiteParameter("IdLesiones", Me.IdLesiones))
        comando11.Parameters.Add(New SQLiteParameter("IdPersona", Me.IdPersona))
        comando11.Parameters.Add(New SQLiteParameter("Manchas", Me.Manchas))
        comando11.Parameters.Add(New SQLiteParameter("AbultamientoTejidos", Me.AbultamientoTejidos))
        comando11.Parameters.Add(New SQLiteParameter("Ulceraciones", Me.Ulceraciones))
        comando11.Parameters.Add(New SQLiteParameter("Ampollas", Me.Ampollas))
        comando11.Parameters.Add(New SQLiteParameter("Otros", Me.Otros))
        comando11.Parameters.Add(New SQLiteParameter("SangranEncias", Me.SangranEncias))
        comando11.Parameters.Add(New SQLiteParameter("CuandoSangranEncias", Me.CuandoSangranEncias))
        comando11.Parameters.Add(New SQLiteParameter("SalePusEnBoca", Me.SalePusEnBoca))
        comando11.Parameters.Add(New SQLiteParameter("DondeSalePusEnBoca", Me.DondeSalePusEnBoca))
        comando11.Parameters.Add(New SQLiteParameter("MovilidadEnDientes", Me.MovilidadEnDientes))
        comando11.Parameters.Add(New SQLiteParameter("SienteAltosDientesAlMorder", Me.SienteAltosDientesAlMorder))
        comando11.Parameters.Add(New SQLiteParameter("CaraHinchada", Me.CaraHinchada))
        comando11.Parameters.Add(New SQLiteParameter("SePusoHielo", Me.SePusoHielo))
        comando11.Parameters.Add(New SQLiteParameter("SePudoCalor", Me.SePusoCalor))
        comando11.Parameters.Add(New SQLiteParameter("SePusoOtros", Me.SePusoOtros))
        '----------------------------------------
        comando12.Parameters.Add(New SQLiteParameter("IdPersona", Me.IdPersona))
        comando12.Parameters.Add(New SQLiteParameter("IdCuadraditos", Me.IdCuadraditos))
        comando12.Parameters.Add(New SQLiteParameter("Sarro", Me.Sarro))
        comando12.Parameters.Add(New SQLiteParameter("EnfermedadPeriodontal", Me.EnfermedadPeriodontal))
        comando12.Parameters.Add(New SQLiteParameter("DiagnosticoPresuntivo", Me.DiagnosticoPresuntivo))
        comando12.Parameters.Add(New SQLiteParameter("PlanTratamiento", Me.PlanTratamiento))
        comando12.Parameters.Add(New SQLiteParameter("CuandoPlanTratamiento", Me.CuandoPlanTratamiento))
        comando12.Parameters.Add(New SQLiteParameter("CantidadDientes", Me.CantidadDientes))
        comando12.Parameters.Add(New SQLiteParameter("Observaciones", Me.Observaciones))
        '----------------------------------------
        comando13.Parameters.Add(New SQLiteParameter("IdCuadraditos", Me.IdCuadraditos))
        comando13.Parameters.Add(New SQLiteParameter("i11", Me.i11))
        comando13.Parameters.Add(New SQLiteParameter("i12", Me.i12))
        comando13.Parameters.Add(New SQLiteParameter("i13", Me.i13))
        comando13.Parameters.Add(New SQLiteParameter("i14", Me.i14))
        comando13.Parameters.Add(New SQLiteParameter("i15", Me.i15))
        comando13.Parameters.Add(New SQLiteParameter("i16", Me.i16))
        comando13.Parameters.Add(New SQLiteParameter("i17", Me.i17))
        comando13.Parameters.Add(New SQLiteParameter("i18", Me.i18))
        comando13.Parameters.Add(New SQLiteParameter("i21", Me.i21))
        comando13.Parameters.Add(New SQLiteParameter("i22", Me.i22))
        comando13.Parameters.Add(New SQLiteParameter("i23", Me.i23))
        comando13.Parameters.Add(New SQLiteParameter("i24", Me.i24))
        comando13.Parameters.Add(New SQLiteParameter("i25", Me.i25))
        comando13.Parameters.Add(New SQLiteParameter("i26", Me.i26))
        comando13.Parameters.Add(New SQLiteParameter("i27", Me.i27))
        comando13.Parameters.Add(New SQLiteParameter("i28", Me.i28))
        comando13.Parameters.Add(New SQLiteParameter("i31", Me.i31))
        comando13.Parameters.Add(New SQLiteParameter("i32", Me.i32))
        comando13.Parameters.Add(New SQLiteParameter("i33", Me.i33))
        comando13.Parameters.Add(New SQLiteParameter("i34", Me.i34))
        comando13.Parameters.Add(New SQLiteParameter("i35", Me.i35))
        comando13.Parameters.Add(New SQLiteParameter("i36", Me.i36))
        comando13.Parameters.Add(New SQLiteParameter("i37", Me.i37))
        comando13.Parameters.Add(New SQLiteParameter("i38", Me.i38))
        comando13.Parameters.Add(New SQLiteParameter("i41", Me.i41))
        comando13.Parameters.Add(New SQLiteParameter("i42", Me.i42))
        comando13.Parameters.Add(New SQLiteParameter("i43", Me.i43))
        comando13.Parameters.Add(New SQLiteParameter("i44", Me.i44))
        comando13.Parameters.Add(New SQLiteParameter("i45", Me.i45))
        comando13.Parameters.Add(New SQLiteParameter("i46", Me.i46))
        comando13.Parameters.Add(New SQLiteParameter("i47", Me.i47))
        comando13.Parameters.Add(New SQLiteParameter("i48", Me.i48))
        comando13.Parameters.Add(New SQLiteParameter("i51", Me.i51))
        comando13.Parameters.Add(New SQLiteParameter("i52", Me.i52))
        comando13.Parameters.Add(New SQLiteParameter("i53", Me.i53))
        comando13.Parameters.Add(New SQLiteParameter("i54", Me.i54))
        comando13.Parameters.Add(New SQLiteParameter("i55", Me.i55))
        comando13.Parameters.Add(New SQLiteParameter("i56", Me.i56))
        comando13.Parameters.Add(New SQLiteParameter("i61", Me.i61))
        comando13.Parameters.Add(New SQLiteParameter("i62", Me.i62))
        comando13.Parameters.Add(New SQLiteParameter("i63", Me.i63))
        comando13.Parameters.Add(New SQLiteParameter("i64", Me.i64))
        comando13.Parameters.Add(New SQLiteParameter("i65", Me.i65))
        comando13.Parameters.Add(New SQLiteParameter("i66", Me.i66))
        comando13.Parameters.Add(New SQLiteParameter("i71", Me.i71))
        comando13.Parameters.Add(New SQLiteParameter("i72", Me.i72))
        comando13.Parameters.Add(New SQLiteParameter("i73", Me.i73))
        comando13.Parameters.Add(New SQLiteParameter("i74", Me.i74))
        comando13.Parameters.Add(New SQLiteParameter("i75", Me.i75))
        comando13.Parameters.Add(New SQLiteParameter("i76", Me.i76))
        comando13.Parameters.Add(New SQLiteParameter("i81", Me.i81))
        comando13.Parameters.Add(New SQLiteParameter("i82", Me.i82))
        comando13.Parameters.Add(New SQLiteParameter("i83", Me.i83))
        comando13.Parameters.Add(New SQLiteParameter("i84", Me.i84))
        comando13.Parameters.Add(New SQLiteParameter("i85", Me.i85))
        comando13.Parameters.Add(New SQLiteParameter("i86", Me.i86))
        '----------------------------------------

        'Se ejecuta la sentencia SQL en la base de datos. Este método devuelve un número entero que significa cuantos registros
        'han sido afectados por la sentencia SQL.
        comando1.ExecuteNonQuery()
        comando2.ExecuteNonQuery()
        comando3.ExecuteNonQuery()
        comando4.ExecuteNonQuery()
        comando5.ExecuteNonQuery()
        comando6.ExecuteNonQuery()
        comando7.ExecuteNonQuery()
        comando8.ExecuteNonQuery()
        comando9.ExecuteNonQuery()
        comando10.ExecuteNonQuery()
        comando11.ExecuteNonQuery()
        comando12.ExecuteNonQuery()
        comando13.ExecuteNonQuery()
        conexion.Dispose()

    End Sub
    '===========================================================================================
    Public Shared Function Buscar(ByVal nombre As String, ByVal numdocumento As Integer) As DataTable

        Dim cmd_select = "SELECT * FROM Persona WHERE nombre LIKE @nombre AND  "
        Dim buscador = False

        If numdocumento <> 0 Then
            cmd_select += " AND NumDocumento=@NumDocumento"
            buscador = True
        End If

        Dim conexion = New SQLiteConnection("Data Source=C:\Program Files (x86)\Tesis\cosa.db; Version=3;password=12345;")

        conexion.Open()

        Dim comando As New SQLiteCommand(cmd_select, conexion)

        comando.Parameters.Add(New SQLiteParameter("@nombre", "%" & numdocumento & "%"))


        If buscador Then
            comando.Parameters.Add(New SQLiteParameter("@numdocumento", numdocumento))
        End If

        Dim dt = New DataTable()

        Dim da = New SQLiteDataAdapter(comando)

        da.Fill(dt)

        Return dt

    End Function
    '===========================================================================================
    Public Shared Sub Eliminar()

        'Dim cmd_update = "DELETE FROM Productos WHERE producto_id=@producto_id"

        'Dim conexion = New OleDbConnection(My.Settings.My_Database)
        'conexion.Open()

        'Dim comando As New OleDbCommand(cmd_update, conexion)

        ' comando.Parameters.Add(New OleDbParameter("@producto_id", Me.Codigo))
        '0
        '0
        ' comando.ExecuteNonQuery()
        ' conexion.Close()

    End Sub
    '===========================================================================================
    Public Shared Function Obtener_Odontologos() As DataTable

        Dim cmd_select = "SELECT Odontologo.Id , Persona.Nombre , Persona.Apellido
                            FROM Odontologo, Persona 
                            ON Odontologo.IdPersona = Persona.Id "

        Dim conexion = New SQLiteConnection("Data Source=.\cosa.db; Version=3;password=12345;")
        conexion.Open()

        Dim comando As New SQLiteCommand(cmd_select, conexion)

        Dim dt = New DataTable()

        Dim da = New SQLiteDataAdapter(comando)

        da.Fill(dt)

        Return dt

    End Function
    '===========================================================================================
    Public Sub New()
    End Sub
    '===========================================================================================
    Public Sub New(ByVal codigo_par As Integer)

        Dim cmd_select = "SELECT * FROM Productos WHERE IdPaciente = @IdPaciente "

        Dim conexion = New SQLiteConnection("Data Source=C:\Program Files (x86)\Tesis\cosa.db; Version=3;password=12345;")
        conexion.Open()

        Dim comando As New SQLiteCommand(cmd_select, conexion)

        comando.Parameters.Add(New SQLiteParameter("@IdPaciente", codigo_par))

        Dim dt = New DataTable()

        Dim da = New SQLiteDataAdapter(comando)

        da.Fill(dt)

        Me.Fecha = Convert.ToDateTime(dt.Rows(0)("Fecha"))
        Me.Nombre = Convert.ToString(dt.Rows(0)("Nombre"))
        Me.Apellido = Convert.ToString(dt.Rows(0)("Apellido"))
        Me.NumAfiliado = Convert.ToInt32(dt.Rows(0)("NumAfiliado"))
        Me.FechaNacimiento = Convert.ToDateTime(dt.Rows(0)("FechaNacimiento"))
        Me.Telefono = Convert.ToString(dt.Rows(0)("Telefono"))
        Me.Celular = Convert.ToString(dt.Rows(0)("Celular"))
        Me.Edad = Convert.ToInt32(dt.Rows(0)("Edad"))
        Me.Nacionalidad = Convert.ToString(dt.Rows(0)("Nacionalidad"))
        Me.NumDocumento = Convert.ToInt32(dt.Rows(0)("NumDocumento"))
        Me.ProfesionActividad = Convert.ToString(dt.Rows(0)("ProfesionActividad"))
        Me.Titular = Convert.ToString(dt.Rows(0)("Titular"))
        Me.LugarTrabajo = Convert.ToString(dt.Rows(0)("LugarTrabajo"))
        Me.PadreConVida = Convert.ToBoolean(dt.Rows(0)("PadreConVida"))
        Me.EnfermedadPasadaActualPadre = Convert.ToString(dt.Rows(0)("EnfermedadPasadaActualPadre"))
        Me.MadreConVida = Convert.ToBoolean(dt.Rows(0)("MadreConVida"))
        Me.EnfermedadPasadaActualMadre = Convert.ToString(dt.Rows(0)("EnfermedadPasadaActualMadre"))
        Me.Hermanos = Convert.ToBoolean(dt.Rows(0)("Hermanos"))
        Me.Sanos = Convert.ToBoolean(dt.Rows(0)("Sanos"))
        Me.EnfermoHermano = Convert.ToBoolean(dt.Rows(0)("EnfermoHermano"))
        Me.EnfermedadHermano = Convert.ToString(dt.Rows(0)("EnfermedadHermano"))
        Me.HaceTratamientoMedico = Convert.ToBoolean(dt.Rows(0)("HaceTratamientoMedico"))
        Me.CualTratamientoMedico = Convert.ToString(dt.Rows(0)("CualTratamientoMedico"))
        Me.MedicamentosActuales = Convert.ToString(dt.Rows(0)("MedicamentosActuales"))
        Me.MedicamentosUltimos5anos = Convert.ToString(dt.Rows(0)("MedicamentosUltimos5Anos"))
        Me.OtraEnfermedadInfectocontagiosa = Convert.ToString(dt.Rows(0)("OtraEnfermedadInfectocontagiosa"))
        Me.Transfusiones = Convert.ToBoolean(dt.Rows(0)("Transfusiones"))
        Me.FueOperadoAntes = Convert.ToBoolean(dt.Rows(0)("FueOperadoAntes"))
        Me.DeQueOperacion = Convert.ToString(dt.Rows(0)("DeQueOperacion"))
        Me.CuandoOperacion = Convert.ToDateTime(dt.Rows(0)("CuandoOperacion"))
        Me.ProblemasRespiratorios = Convert.ToBoolean(dt.Rows(0)("ProblemasRespiratorios"))
        Me.CualProblemasRespiratorios = Convert.ToString(dt.Rows(0)("CualProblemasRespiratorios"))
        Me.Fuma = Convert.ToBoolean(dt.Rows(0)("Fuma"))
        Me.Embarazada = Convert.ToBoolean(dt.Rows(0)("Embarazada"))
        Me.MesesEmbarazo = Convert.ToString(dt.Rows(0)("MesesEmbarazo"))
        Me.OtraEnfermedadRecomendacion = Convert.ToBoolean(dt.Rows(0)("OtraEnfermedadRecomendacion"))
        Me.CualEnfermedadRecomendacion = Convert.ToString(dt.Rows(0)("CualEnfermedadRecomendacion"))
        Me.TratamientoAlternativo = Convert.ToBoolean(dt.Rows(0)("TratamientoAlternativo"))
        Me.CualTratamientoAlternativo = Convert.ToString(dt.Rows(0)("CualTratamientoAlternativo"))
        Me.MedicoClinico = Convert.ToString(dt.Rows(0)("MedicoClinico"))
        Me.ClinicaHospital = Convert.ToString(dt.Rows(0)("ClinicaHospital"))
        Me.HaceDeporte = Convert.ToBoolean(dt.Rows(0)("HaceDeporte"))
        Me.MalestarAlRealizarlo = Convert.ToBoolean(dt.Rows(0)("MalestarAlRealizarlo"))
        Me.AlergicoDrogas = Convert.ToBoolean(dt.Rows(0)("AlergicoDrogas"))
        Me.AlergicoAnestesia = Convert.ToBoolean(dt.Rows(0)("AlergicoAnestesia"))
        Me.AlergicoPenisilina = Convert.ToBoolean(dt.Rows(0)("AlergicoPenisilina"))
        Me.AlergicoOtros = Convert.ToString(dt.Rows(0)("AlergicoOtros"))
        Me.ComoCicatrizaSangra = Convert.ToString(dt.Rows(0)("ComoCicatrizaSangra"))
        Me.Hiperlaxitud = Convert.ToBoolean(dt.Rows(0)("Hiperlaxitud"))
        Me.AntecedentesFiebreReumatica = Convert.ToBoolean(dt.Rows(0)("AntecedentesFiebreReumatica"))
        Me.MedicacionFiebreReumatica = Convert.ToString(dt.Rows(0)("MedicacionFiebreReumatica"))
        Me.Diabetico = Convert.ToBoolean(dt.Rows(0)("Diabetico"))
        Me.DiabeticoControlado = Convert.ToBoolean(dt.Rows(0)("DiabeticoControlado"))
        Me.MedicacionDiabetico = Convert.ToString(dt.Rows(0)("MedicacionDiabetico"))
        Me.ProblemaCardiaco = Convert.ToBoolean(dt.Rows(0)("ProblemaCardiaco"))
        Me.CualProblemaCardiaco = Convert.ToString(dt.Rows(0)("CualProblemaCardiaco"))
        Me.TomaAnticoagulante = Convert.ToBoolean(dt.Rows(0)("TomaAnticoagulante"))
        Me.FrecuenciaAnticoagulante = Convert.ToString(dt.Rows(0)("FrecuenciaAnticoagulante"))
        Me.PresionAlta = Convert.ToBoolean(dt.Rows(0)("PresionAlta"))
        Me.Chagas = Convert.ToBoolean(dt.Rows(0)("Chagas"))
        Me.TratamientoPresionchagas = Convert.ToString(dt.Rows(0)("TratamientoPresionChagas"))
        Me.ProblemasRenales = Convert.ToBoolean(dt.Rows(0)("ProblemasRenales"))
        Me.UlceraGastrica = Convert.ToBoolean(dt.Rows(0)("UlceraGastrica"))
        Me.TuvoHepatitis = Convert.ToBoolean(dt.Rows(0)("TuvoHepatitis"))
        Me.HepatitisA = Convert.ToBoolean(dt.Rows(0)("HepatitisA"))
        Me.HepatitisB = Convert.ToBoolean(dt.Rows(0)("HepatitisB"))
        Me.HepatitisC = Convert.ToBoolean(dt.Rows(0)("HepatitisC"))
        Me.ProblemaHepatico = Convert.ToBoolean(dt.Rows(0)("ProblemaHepatico"))
        Me.CualProblemaHepatico = Convert.ToString(dt.Rows(0)("CualProblemaHepatico"))
        Me.Convulsiones = Convert.ToBoolean(dt.Rows(0)("Convulsiones"))
        Me.Epilepsia = Convert.ToBoolean(dt.Rows(0)("Epilepsia"))
        Me.MedicacionEpilepsia = Convert.ToString(dt.Rows(0)("MedicacionEpilepsia"))
        Me.SifilisGonorrea = Convert.ToBoolean(dt.Rows(0)("SifilisGonorrea"))
        Me.MotivoConsulta = Convert.ToString(dt.Rows(0)("MotivoConsulta"))
        Me.ConsultoAntes = Convert.ToBoolean(dt.Rows(0)("ConsultoAntes"))
        Me.TomoAlgoHCO = Convert.ToBoolean(dt.Rows(0)("TomoAlgoHCO"))
        Me.MedicacionHCO = Convert.ToString(dt.Rows(0)("MedicacionHCO"))
        Me.DesdeCuandoMedicacionHCO = Convert.ToDateTime(dt.Rows(0)("DesdeCuandoMedicacionHCO"))
        Me.ObtuvoResultadoHCO = Convert.ToBoolean(dt.Rows(0)("ObtuvoResultadoHCO"))
        Me.HaTenidoDolor = Convert.ToBoolean(dt.Rows(0)("HaTenidoDolor"))
        Me.DolorLocalizado = Convert.ToBoolean(dt.Rows(0)("DolorLocalizado"))
        Me.DondeDolor = Convert.ToString(dt.Rows(0)("DondeDolor"))
        Me.Irradiado = Convert.ToBoolean(dt.Rows(0)("Irradiado"))
        Me.HaciaDondeIrradiado = Convert.ToString(dt.Rows(0)("HaciaDondeIrradiado"))
        Me.PudoCalmarIrradiacion = Convert.ToString(dt.Rows(0)("PudoCalmarIrradiacion"))
        Me.GolpeEnDientes = Convert.ToBoolean(dt.Rows(0)("GolpeEnDientes"))
        Me.CuandoGolpeEnDientes = Convert.ToDateTime(dt.Rows(0)("CuandoGolpeEnDientes"))
        Me.ComoGolpeEnDientes = Convert.ToString(dt.Rows(0)("ComoGolpeEnDientes"))
        Me.FracturaDeDiente = Convert.ToBoolean(dt.Rows(0)("FracturaDeDiente"))
        Me.CualFracturaDeDiente = Convert.ToString(dt.Rows(0)("CualFracturaDeDiente"))
        Me.RecibioTratamientoHCO = Convert.ToString(dt.Rows(0)("RecibioTratamientoHCO"))
        Me.Calle = Convert.ToString(dt.Rows(0)("Calle"))
        Me.Numero = Convert.ToInt32(dt.Rows(0)("Numero"))
        Me.Piso = Convert.ToInt32(dt.Rows(0)("Piso"))
        Me.Departamento = Convert.ToString(dt.Rows(0)("Departamento"))
        Me.Barrio = Convert.ToString(dt.Rows(0)("Barrio"))
        Me.Localidad = Convert.ToString(dt.Rows(0)("Localidad"))
        Me.Provincia = Convert.ToString(dt.Rows(0)("Provincia"))
        Me.CodigoPostal = Convert.ToString(dt.Rows(0)("CodigoPostal"))
        Me.DolorEspontaneo = Convert.ToBoolean(dt.Rows(0)("DolorEspontaneo"))
        Me.DolorProvocado = Convert.ToBoolean(dt.Rows(0)("DolorProvocado"))
        Me.DolorAlFrio = Convert.ToBoolean(dt.Rows(0)("DolorAlFrio"))
        Me.DolorAlCalor = Convert.ToBoolean(dt.Rows(0)("DolorAlCalor"))
        Me.Hablar = Convert.ToString(dt.Rows(0)("Hablar"))
        Me.Masticar = Convert.ToString(dt.Rows(0)("Masticar"))
        Me.AbrirBoca = Convert.ToString(dt.Rows(0)("AbrirBoca"))
        Me.Tragar = Convert.ToString(dt.Rows(0)("Tragar"))
        Me.Labios = Convert.ToString(dt.Rows(0)("Labios"))
        Me.Lengua = Convert.ToString(dt.Rows(0)("Lengua"))
        Me.Paladar = Convert.ToString(dt.Rows(0)("Paladar"))
        Me.PisoDeBoca = Convert.ToString(dt.Rows(0)("PisoDeBoca"))
        Me.Carrillos = Convert.ToString(dt.Rows(0)("Carrillos"))
        Me.Rebordes = Convert.ToString(dt.Rows(0)("Rebordes"))
        Me.Trigono = Convert.ToString(dt.Rows(0)("Trigono"))
        Me.Retromolar = Convert.ToString(dt.Rows(0)("Retromolar"))
        Me.Manchas = Convert.ToBoolean(dt.Rows(0)("Manchas"))
        Me.AbultamientoTejidos = Convert.ToBoolean(dt.Rows(0)("AbultamientoTejidos"))
        Me.Ulceraciones = Convert.ToBoolean(dt.Rows(0)("Ulceraciones"))
        Me.Ampollas = Convert.ToBoolean(dt.Rows(0)("Ampollas"))
        Me.Otros = Convert.ToString(dt.Rows(0)("Otros"))
        Me.SangranEncias = Convert.ToBoolean(dt.Rows(0)("SangranEncias"))
        Me.CuandoSangranEncias = Convert.ToString(dt.Rows(0)("CuandoSangranEncias"))
        Me.SalePusEnBoca = Convert.ToBoolean(dt.Rows(0)("SalePusEnBoca"))
        Me.DondeSalePusEnBoca = Convert.ToString(dt.Rows(0)("DondeSalePusEnBoca"))
        Me.MovilidadEnDientes = Convert.ToBoolean(dt.Rows(0)("MovilidadEnDientes"))
        Me.SienteAltosDientesAlMorder = Convert.ToString(dt.Rows(0)("SienteAltosDientesAlMorder"))
        Me.CaraHinchada = Convert.ToBoolean(dt.Rows(0)("CaraHinchada"))
        Me.SePusoHielo = Convert.ToBoolean(dt.Rows(0)("SePusoHielo"))
        Me.SePusoCalor = Convert.ToBoolean(dt.Rows(0)("SePusoCalor"))
        Me.SePusoOtros = Convert.ToString(dt.Rows(0)("SePusoOtros"))
        Me.Sarro = Convert.ToBoolean(dt.Rows(0)("Sarro"))
        Me.EnfermedadPeriodontal = Convert.ToBoolean(dt.Rows(0)("EnfermedadPeriodontal"))
        Me.DiagnosticoPresuntivo = Convert.ToString(dt.Rows(0)("DiagnosticoPresuntivo"))
        Me.PlanTratamiento = Convert.ToString(dt.Rows(0)("PlanTratamiento"))
        Me.CuandoPlanTratamiento = Convert.ToDateTime(dt.Rows(0)("CuandoPlanTratamiento"))
        Me.CantidadDientes = Convert.ToInt32(dt.Rows(0)("CantidadDientes"))
        Me.Observaciones = Convert.ToString(dt.Rows(0)("Observaciones"))
        Me.i11 = Convert.ToInt32(dt.Rows(0)("i11"))
        Me.i12 = Convert.ToInt32(dt.Rows(0)("i12"))
        Me.i13 = Convert.ToInt32(dt.Rows(0)("i13"))
        Me.i14 = Convert.ToInt32(dt.Rows(0)("i14"))
        Me.i15 = Convert.ToInt32(dt.Rows(0)("i15"))
        Me.i16 = Convert.ToInt32(dt.Rows(0)("i16"))
        Me.i17 = Convert.ToInt32(dt.Rows(0)("i17"))
        Me.i18 = Convert.ToInt32(dt.Rows(0)("i18"))
        Me.i21 = Convert.ToInt32(dt.Rows(0)("i21"))
        Me.i22 = Convert.ToInt32(dt.Rows(0)("i22"))
        Me.i23 = Convert.ToInt32(dt.Rows(0)("i23"))
        Me.i24 = Convert.ToInt32(dt.Rows(0)("i24"))
        Me.i25 = Convert.ToInt32(dt.Rows(0)("i25"))
        Me.i26 = Convert.ToInt32(dt.Rows(0)("i26"))
        Me.i27 = Convert.ToInt32(dt.Rows(0)("i27"))
        Me.i28 = Convert.ToInt32(dt.Rows(0)("i28"))
        Me.i31 = Convert.ToInt32(dt.Rows(0)("i31"))
        Me.i32 = Convert.ToInt32(dt.Rows(0)("i32"))
        Me.i33 = Convert.ToInt32(dt.Rows(0)("i33"))
        Me.i34 = Convert.ToInt32(dt.Rows(0)("i34"))
        Me.i35 = Convert.ToInt32(dt.Rows(0)("i35"))
        Me.i36 = Convert.ToInt32(dt.Rows(0)("i36"))
        Me.i37 = Convert.ToInt32(dt.Rows(0)("i37"))
        Me.i38 = Convert.ToInt32(dt.Rows(0)("i38"))
        Me.i41 = Convert.ToInt32(dt.Rows(0)("i41"))
        Me.i42 = Convert.ToInt32(dt.Rows(0)("i42"))
        Me.i43 = Convert.ToInt32(dt.Rows(0)("i43"))
        Me.i44 = Convert.ToInt32(dt.Rows(0)("i44"))
        Me.i45 = Convert.ToInt32(dt.Rows(0)("i45"))
        Me.i46 = Convert.ToInt32(dt.Rows(0)("i46"))
        Me.i47 = Convert.ToInt32(dt.Rows(0)("i47"))
        Me.i48 = Convert.ToInt32(dt.Rows(0)("i48"))
        Me.i51 = Convert.ToInt32(dt.Rows(0)("i51"))
        Me.i52 = Convert.ToInt32(dt.Rows(0)("i52"))
        Me.i53 = Convert.ToInt32(dt.Rows(0)("i53"))
        Me.i54 = Convert.ToInt32(dt.Rows(0)("i54"))
        Me.i55 = Convert.ToInt32(dt.Rows(0)("i55"))
        Me.i56 = Convert.ToInt32(dt.Rows(0)("i56"))
        Me.i61 = Convert.ToInt32(dt.Rows(0)("i61"))
        Me.i62 = Convert.ToInt32(dt.Rows(0)("i62"))
        Me.i63 = Convert.ToInt32(dt.Rows(0)("i63"))
        Me.i64 = Convert.ToInt32(dt.Rows(0)("i64"))
        Me.i65 = Convert.ToInt32(dt.Rows(0)("i65"))
        Me.i66 = Convert.ToInt32(dt.Rows(0)("i66"))
        Me.i71 = Convert.ToInt32(dt.Rows(0)("i71"))
        Me.i72 = Convert.ToInt32(dt.Rows(0)("i72"))
        Me.i73 = Convert.ToInt32(dt.Rows(0)("i73"))
        Me.i74 = Convert.ToInt32(dt.Rows(0)("i74"))
        Me.i75 = Convert.ToInt32(dt.Rows(0)("i75"))
        Me.i76 = Convert.ToInt32(dt.Rows(0)("i76"))
        Me.i81 = Convert.ToInt32(dt.Rows(0)("i81"))
        Me.i82 = Convert.ToInt32(dt.Rows(0)("i82"))
        Me.i83 = Convert.ToInt32(dt.Rows(0)("i83"))
        Me.i84 = Convert.ToInt32(dt.Rows(0)("i84"))
        Me.i85 = Convert.ToInt32(dt.Rows(0)("i85"))
        Me.i86 = Convert.ToInt32(dt.Rows(0)("i86"))
        Me.IdConsulta = Convert.ToInt32(dt.Rows(0)("IdConsulta"))
        Me.IdPersona = Convert.ToInt32(dt.Rows(0)("IdPersona"))
        Me.IdObraSocial = Convert.ToInt32(dt.Rows(0)("IdObraSocial"))
        Me.IdEstadoCivil = Convert.ToInt32(dt.Rows(0)("IdEstadoCivil"))
        Me.IdDomicilio = Convert.ToInt32(dt.Rows(0)("IdDomicilio"))
        Me.IdCuestionario = Convert.ToInt32(dt.Rows(0)("IdCuestionario"))
        Me.IdHCO = Convert.ToInt32(dt.Rows(0)("IdHCO"))
        Me.IdIntensidadDolor = Convert.ToInt32(dt.Rows(0)("IdIntensidadDolor"))
        Me.IdDuracionDolor = Convert.ToInt32(dt.Rows(0)("IdDuracionDolor"))
        Me.IdEstimuloDolor = Convert.ToInt32(dt.Rows(0)("IdEstimuloDolor"))
        Me.IdDificultad = Convert.ToInt32(dt.Rows(0)("IdDificultad"))
        Me.IdAnormalidad = Convert.ToInt32(dt.Rows(0)("IdAnormalidad"))
        Me.IdLesiones = Convert.ToInt32(dt.Rows(0)("IdLesiones"))
        Me.IdCuadraditos = Convert.ToInt32(dt.Rows(0)("IdCuadraditos"))
        Me.IdOdontologo = Convert.ToInt32(dt.Rows(0)("IdOdontologo"))
        Me.IdConsultorio = Convert.ToInt32(dt.Rows(0)("IdConsultorio"))
        Me.IdEstadoBucal = Convert.ToInt32(dt.Rows(0)("IdEstadoBucal"))


    End Sub
    '===========================================================================================
End Class