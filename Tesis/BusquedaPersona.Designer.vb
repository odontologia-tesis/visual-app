﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BusquedaPersona
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.eqDomicilio = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.varCelular = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.varNumDocumento = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.varNacionalidad = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.varEstadoCivil = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.eqEdad = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.varTelefono = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.varIdFechaNacimiento = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.varNumAfiliado = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.varIdPersona = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.varApellido = New System.Windows.Forms.TextBox()
        Me.varNombre = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtEscribiendo = New System.Windows.Forms.TextBox()
        Me.TextoBuscarDNI = New System.Windows.Forms.Label()
        Me.DGTabla = New System.Windows.Forms.DataGridView()
        Me.Panel14.SuspendLayout()
        Me.Panel13.SuspendLayout()
        Me.Panel12.SuspendLayout()
        Me.Panel11.SuspendLayout()
        Me.Panel10.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.DGTabla, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel14
        '
        Me.Panel14.Controls.Add(Me.eqDomicilio)
        Me.Panel14.Controls.Add(Me.Label15)
        Me.Panel14.Location = New System.Drawing.Point(218, 434)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(443, 27)
        Me.Panel14.TabIndex = 101
        '
        'eqDomicilio
        '
        Me.eqDomicilio.Location = New System.Drawing.Point(61, 3)
        Me.eqDomicilio.Name = "eqDomicilio"
        Me.eqDomicilio.Size = New System.Drawing.Size(379, 20)
        Me.eqDomicilio.TabIndex = 2
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(3, 6)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(52, 13)
        Me.Label15.TabIndex = 0
        Me.Label15.Text = "Domicilio:"
        '
        'Panel13
        '
        Me.Panel13.Controls.Add(Me.varCelular)
        Me.Panel13.Controls.Add(Me.Label14)
        Me.Panel13.Location = New System.Drawing.Point(12, 434)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(200, 27)
        Me.Panel13.TabIndex = 97
        '
        'varCelular
        '
        Me.varCelular.Location = New System.Drawing.Point(51, 3)
        Me.varCelular.Name = "varCelular"
        Me.varCelular.Size = New System.Drawing.Size(145, 20)
        Me.varCelular.TabIndex = 2
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(3, 6)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(42, 13)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Celular:"
        '
        'Panel12
        '
        Me.Panel12.Controls.Add(Me.varNumDocumento)
        Me.Panel12.Controls.Add(Me.Label13)
        Me.Panel12.Location = New System.Drawing.Point(560, 401)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(211, 27)
        Me.Panel12.TabIndex = 105
        '
        'varNumDocumento
        '
        Me.varNumDocumento.Location = New System.Drawing.Point(89, 3)
        Me.varNumDocumento.Name = "varNumDocumento"
        Me.varNumDocumento.Size = New System.Drawing.Size(119, 20)
        Me.varNumDocumento.TabIndex = 2
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(3, 6)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(80, 13)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "Nº Documento:"
        '
        'Panel11
        '
        Me.Panel11.Controls.Add(Me.varNacionalidad)
        Me.Panel11.Controls.Add(Me.Label12)
        Me.Panel11.Location = New System.Drawing.Point(358, 401)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(196, 27)
        Me.Panel11.TabIndex = 104
        '
        'varNacionalidad
        '
        Me.varNacionalidad.Location = New System.Drawing.Point(74, 3)
        Me.varNacionalidad.Name = "varNacionalidad"
        Me.varNacionalidad.Size = New System.Drawing.Size(119, 20)
        Me.varNacionalidad.TabIndex = 2
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(3, 6)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(72, 13)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Nacionalidad:"
        '
        'Panel10
        '
        Me.Panel10.Controls.Add(Me.varEstadoCivil)
        Me.Panel10.Controls.Add(Me.Label11)
        Me.Panel10.Location = New System.Drawing.Point(88, 401)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(264, 27)
        Me.Panel10.TabIndex = 103
        '
        'varEstadoCivil
        '
        Me.varEstadoCivil.Location = New System.Drawing.Point(74, 3)
        Me.varEstadoCivil.Name = "varEstadoCivil"
        Me.varEstadoCivil.Size = New System.Drawing.Size(187, 20)
        Me.varEstadoCivil.TabIndex = 2
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(3, 6)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(65, 13)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Estado Civil:"
        '
        'Panel9
        '
        Me.Panel9.Controls.Add(Me.eqEdad)
        Me.Panel9.Controls.Add(Me.Label9)
        Me.Panel9.Location = New System.Drawing.Point(12, 401)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(68, 27)
        Me.Panel9.TabIndex = 96
        '
        'eqEdad
        '
        Me.eqEdad.Location = New System.Drawing.Point(35, 4)
        Me.eqEdad.Name = "eqEdad"
        Me.eqEdad.Size = New System.Drawing.Size(30, 20)
        Me.eqEdad.TabIndex = 80
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(3, 6)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(35, 13)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Edad:"
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.varTelefono)
        Me.Panel8.Controls.Add(Me.Label8)
        Me.Panel8.Location = New System.Drawing.Point(512, 368)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(209, 27)
        Me.Panel8.TabIndex = 95
        '
        'varTelefono
        '
        Me.varTelefono.Location = New System.Drawing.Point(61, 3)
        Me.varTelefono.Name = "varTelefono"
        Me.varTelefono.Size = New System.Drawing.Size(145, 20)
        Me.varTelefono.TabIndex = 2
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(3, 6)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(52, 13)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Telefono:"
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.varIdFechaNacimiento)
        Me.Panel7.Controls.Add(Me.Label7)
        Me.Panel7.Location = New System.Drawing.Point(282, 368)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(224, 27)
        Me.Panel7.TabIndex = 98
        '
        'varIdFechaNacimiento
        '
        Me.varIdFechaNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.varIdFechaNacimiento.Location = New System.Drawing.Point(126, 3)
        Me.varIdFechaNacimiento.Name = "varIdFechaNacimiento"
        Me.varIdFechaNacimiento.Size = New System.Drawing.Size(95, 20)
        Me.varIdFechaNacimiento.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(3, 6)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(117, 13)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Fecha de Naciemiento:"
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.varNumAfiliado)
        Me.Panel6.Controls.Add(Me.Label6)
        Me.Panel6.Location = New System.Drawing.Point(12, 368)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(264, 27)
        Me.Panel6.TabIndex = 102
        '
        'varNumAfiliado
        '
        Me.varNumAfiliado.Location = New System.Drawing.Point(74, 3)
        Me.varNumAfiliado.Name = "varNumAfiliado"
        Me.varNumAfiliado.Size = New System.Drawing.Size(187, 20)
        Me.varNumAfiliado.TabIndex = 2
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(3, 6)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(65, 13)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Obra Social:"
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.varIdPersona)
        Me.Panel5.Controls.Add(Me.Label5)
        Me.Panel5.Location = New System.Drawing.Point(461, 335)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(287, 27)
        Me.Panel5.TabIndex = 100
        '
        'varIdPersona
        '
        Me.varIdPersona.Location = New System.Drawing.Point(108, 3)
        Me.varIdPersona.Name = "varIdPersona"
        Me.varIdPersona.Size = New System.Drawing.Size(176, 20)
        Me.varIdPersona.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(3, 6)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(99, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Numero de Afiliado:"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.varApellido)
        Me.Panel4.Controls.Add(Me.varNombre)
        Me.Panel4.Controls.Add(Me.Label4)
        Me.Panel4.Location = New System.Drawing.Point(12, 335)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(443, 27)
        Me.Panel4.TabIndex = 99
        '
        'varApellido
        '
        Me.varApellido.Location = New System.Drawing.Point(298, 3)
        Me.varApellido.Name = "varApellido"
        Me.varApellido.Size = New System.Drawing.Size(142, 20)
        Me.varApellido.TabIndex = 3
        '
        'varNombre
        '
        Me.varNombre.Location = New System.Drawing.Point(159, 3)
        Me.varNombre.Name = "varNombre"
        Me.varNombre.Size = New System.Drawing.Size(142, 20)
        Me.varNombre.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(3, 6)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(150, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Nombre/Apellido del paciente:"
        '
        'txtEscribiendo
        '
        Me.txtEscribiendo.Location = New System.Drawing.Point(60, 13)
        Me.txtEscribiendo.Name = "txtEscribiendo"
        Me.txtEscribiendo.Size = New System.Drawing.Size(100, 20)
        Me.txtEscribiendo.TabIndex = 106
        '
        'TextoBuscarDNI
        '
        Me.TextoBuscarDNI.AutoSize = True
        Me.TextoBuscarDNI.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextoBuscarDNI.Location = New System.Drawing.Point(13, 13)
        Me.TextoBuscarDNI.Name = "TextoBuscarDNI"
        Me.TextoBuscarDNI.Size = New System.Drawing.Size(41, 20)
        Me.TextoBuscarDNI.TabIndex = 108
        Me.TextoBuscarDNI.Text = "DNI:"
        '
        'DGTabla
        '
        Me.DGTabla.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGTabla.Location = New System.Drawing.Point(17, 51)
        Me.DGTabla.Name = "DGTabla"
        Me.DGTabla.Size = New System.Drawing.Size(757, 211)
        Me.DGTabla.TabIndex = 112
        '
        'BuscarPersona
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(786, 471)
        Me.Controls.Add(Me.DGTabla)
        Me.Controls.Add(Me.TextoBuscarDNI)
        Me.Controls.Add(Me.txtEscribiendo)
        Me.Controls.Add(Me.Panel14)
        Me.Controls.Add(Me.Panel13)
        Me.Controls.Add(Me.Panel12)
        Me.Controls.Add(Me.Panel11)
        Me.Controls.Add(Me.Panel10)
        Me.Controls.Add(Me.Panel9)
        Me.Controls.Add(Me.Panel8)
        Me.Controls.Add(Me.Panel7)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel4)
        Me.Name = "BuscarPersona"
        Me.Text = "Form2"
        Me.Panel14.ResumeLayout(False)
        Me.Panel14.PerformLayout()
        Me.Panel13.ResumeLayout(False)
        Me.Panel13.PerformLayout()
        Me.Panel12.ResumeLayout(False)
        Me.Panel12.PerformLayout()
        Me.Panel11.ResumeLayout(False)
        Me.Panel11.PerformLayout()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.DGTabla, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Panel14 As Panel
    Friend WithEvents eqDomicilio As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents Panel13 As Panel
    Friend WithEvents varCelular As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents Panel12 As Panel
    Friend WithEvents varNumDocumento As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Panel11 As Panel
    Friend WithEvents varNacionalidad As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Panel10 As Panel
    Friend WithEvents varEstadoCivil As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents Panel9 As Panel
    Friend WithEvents eqEdad As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Panel8 As Panel
    Friend WithEvents varTelefono As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Panel7 As Panel
    Friend WithEvents varIdFechaNacimiento As DateTimePicker
    Friend WithEvents Label7 As Label
    Friend WithEvents Panel6 As Panel
    Friend WithEvents varNumAfiliado As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Panel5 As Panel
    Friend WithEvents varIdPersona As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Panel4 As Panel
    Friend WithEvents varApellido As TextBox
    Friend WithEvents varNombre As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtEscribiendo As TextBox
    Friend WithEvents TextoBuscarDNI As Label
    Friend WithEvents DGTabla As DataGridView
End Class
