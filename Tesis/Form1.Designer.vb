﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim TabPage1 As System.Windows.Forms.TabPage
        Dim Label19 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.GuardarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.Panel18 = New System.Windows.Forms.Panel()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.varCodigoPostal = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Panel90 = New System.Windows.Forms.Panel()
        Me.varLocalidad = New System.Windows.Forms.TextBox()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.Panel91 = New System.Windows.Forms.Panel()
        Me.varProvincia = New System.Windows.Forms.TextBox()
        Me.Label94 = New System.Windows.Forms.Label()
        Me.Panel50 = New System.Windows.Forms.Panel()
        Me.varCalle = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Panel86 = New System.Windows.Forms.Panel()
        Me.varNumero = New System.Windows.Forms.TextBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Panel87 = New System.Windows.Forms.Panel()
        Me.varPiso = New System.Windows.Forms.TextBox()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.Panel88 = New System.Windows.Forms.Panel()
        Me.varDepartamento = New System.Windows.Forms.TextBox()
        Me.Label91 = New System.Windows.Forms.Label()
        Me.Panel89 = New System.Windows.Forms.Panel()
        Me.varBarrio = New System.Windows.Forms.TextBox()
        Me.Label92 = New System.Windows.Forms.Label()
        Me.Panel17 = New System.Windows.Forms.Panel()
        Me.varLugarTrabajo = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Panel16 = New System.Windows.Forms.Panel()
        Me.varTitular = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Panel15 = New System.Windows.Forms.Panel()
        Me.varProfesionActividad = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.varCelular = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.varNumDocumento = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.varNacionalidad = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.varIdEstadoCivil = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.varEdad = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.varTelefono = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.varFechaNacimiento = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.varIdObraSocial = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.varNumAfiliado = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.varApellido = New System.Windows.Forms.TextBox()
        Me.varNombre = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.varFecha = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.varIdConsultorio = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.varIdOdontologo = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Panel33 = New System.Windows.Forms.Panel()
        Me.varOtraEnfermedadInfectocontagiosa = New System.Windows.Forms.CheckBox()
        Me.varTransfusiones = New System.Windows.Forms.CheckBox()
        Me.Panel19 = New System.Windows.Forms.Panel()
        Me.varEnfermedadPasadaActualMadre = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.varMadreConVida = New System.Windows.Forms.CheckBox()
        Me.Panel29 = New System.Windows.Forms.Panel()
        Me.varCualTratamientoAlternativo = New System.Windows.Forms.TextBox()
        Me.varTratamientoAlternativo = New System.Windows.Forms.CheckBox()
        Me.Panel28 = New System.Windows.Forms.Panel()
        Me.varCualEnfermedadRecomendacion = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.varOtraEnfermedadRecomendacion = New System.Windows.Forms.CheckBox()
        Me.Panel27 = New System.Windows.Forms.Panel()
        Me.varCualProblemasRespiratorios = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.varProblemasRespiratorios = New System.Windows.Forms.CheckBox()
        Me.varFuma = New System.Windows.Forms.CheckBox()
        Me.Panel25 = New System.Windows.Forms.Panel()
        Me.varCuandoOperacion = New System.Windows.Forms.DateTimePicker()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.varDeQueOperacion = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.varFueOperadoAntes = New System.Windows.Forms.CheckBox()
        Me.Panel24 = New System.Windows.Forms.Panel()
        Me.varMesesEmbarazo = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.varEmbarazada = New System.Windows.Forms.CheckBox()
        Me.Panel23 = New System.Windows.Forms.Panel()
        Me.varMedicamentosUltimos5Anos = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Panel22 = New System.Windows.Forms.Panel()
        Me.varMedicamentosActuales = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Panel21 = New System.Windows.Forms.Panel()
        Me.varCualTratamientoMedico = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.varHaceTratamientoMedico = New System.Windows.Forms.CheckBox()
        Me.Panel20 = New System.Windows.Forms.Panel()
        Me.varEnfermedadHermano = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.varEnfermoHermano = New System.Windows.Forms.CheckBox()
        Me.varSanos = New System.Windows.Forms.CheckBox()
        Me.varHermanos = New System.Windows.Forms.CheckBox()
        Me.Panel32 = New System.Windows.Forms.Panel()
        Me.varEnfermedadPasadaActualPadre = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.varPadreConVida = New System.Windows.Forms.CheckBox()
        Me.Panel46 = New System.Windows.Forms.Panel()
        Me.varSifilisGonorrea = New System.Windows.Forms.CheckBox()
        Me.Panel30 = New System.Windows.Forms.Panel()
        Me.varMedicoClinico = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Panel38 = New System.Windows.Forms.Panel()
        Me.varMedicacionDiabetico = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.varDiabetico = New System.Windows.Forms.CheckBox()
        Me.varDiabeticoControlado = New System.Windows.Forms.CheckBox()
        Me.Panel45 = New System.Windows.Forms.Panel()
        Me.varMedicacionEpilepsia = New System.Windows.Forms.TextBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.varConvulsiones = New System.Windows.Forms.CheckBox()
        Me.varEpilepsia = New System.Windows.Forms.CheckBox()
        Me.Panel37 = New System.Windows.Forms.Panel()
        Me.varMedicacionFiebreReumatica = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.varHiperlaxitud = New System.Windows.Forms.CheckBox()
        Me.varAntecedentesFiebreReumatica = New System.Windows.Forms.CheckBox()
        Me.Panel39 = New System.Windows.Forms.Panel()
        Me.varCualProblemaCardiaco = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.varProblemaCardiaco = New System.Windows.Forms.CheckBox()
        Me.Panel44 = New System.Windows.Forms.Panel()
        Me.varCualProblemaHepatico = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.varProblemaHepatico = New System.Windows.Forms.CheckBox()
        Me.Panel36 = New System.Windows.Forms.Panel()
        Me.varComoCicatrizaSangra = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Panel31 = New System.Windows.Forms.Panel()
        Me.varClinicaHospital = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Panel40 = New System.Windows.Forms.Panel()
        Me.varFrecuenciaAnticoagulante = New System.Windows.Forms.TextBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.varTomaAnticoagulante = New System.Windows.Forms.CheckBox()
        Me.Panel43 = New System.Windows.Forms.Panel()
        Me.varHepatitisC = New System.Windows.Forms.CheckBox()
        Me.varHepatitisB = New System.Windows.Forms.CheckBox()
        Me.varHepatitisA = New System.Windows.Forms.CheckBox()
        Me.varTuvoHepatitis = New System.Windows.Forms.CheckBox()
        Me.varProblemasRenales = New System.Windows.Forms.CheckBox()
        Me.varUlceraGastrica = New System.Windows.Forms.CheckBox()
        Me.Panel35 = New System.Windows.Forms.Panel()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.varAlergicoOtros = New System.Windows.Forms.TextBox()
        Me.varAlergicoPenisilina = New System.Windows.Forms.CheckBox()
        Me.varAlergicoDrogas = New System.Windows.Forms.CheckBox()
        Me.varAlergicoAnestesia = New System.Windows.Forms.CheckBox()
        Me.Panel34 = New System.Windows.Forms.Panel()
        Me.varHaceDeporte = New System.Windows.Forms.CheckBox()
        Me.varMalestarAlRealizarlo = New System.Windows.Forms.CheckBox()
        Me.Panel41 = New System.Windows.Forms.Panel()
        Me.varTratamientoPresionChagas = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.varPresionAlta = New System.Windows.Forms.CheckBox()
        Me.varChagas = New System.Windows.Forms.CheckBox()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.Panel52 = New System.Windows.Forms.Panel()
        Me.IdDuracionDolor = New System.Windows.Forms.GroupBox()
        Me.varIdDuracionDolor3 = New System.Windows.Forms.RadioButton()
        Me.varIdDuracionDolor2 = New System.Windows.Forms.RadioButton()
        Me.varIdDuracionDolor1 = New System.Windows.Forms.RadioButton()
        Me.IdIntensidadDolor = New System.Windows.Forms.GroupBox()
        Me.varIdIntensidadDolor3 = New System.Windows.Forms.RadioButton()
        Me.varIdIntensidadDolor2 = New System.Windows.Forms.RadioButton()
        Me.varIdIntensidadDolor1 = New System.Windows.Forms.RadioButton()
        Me.varHaTenidoDolor = New System.Windows.Forms.CheckBox()
        Me.Panel61 = New System.Windows.Forms.Panel()
        Me.varTragar = New System.Windows.Forms.TextBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Panel60 = New System.Windows.Forms.Panel()
        Me.varAbrirBoca = New System.Windows.Forms.TextBox()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Panel59 = New System.Windows.Forms.Panel()
        Me.varMasticar = New System.Windows.Forms.TextBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Panel58 = New System.Windows.Forms.Panel()
        Me.varHablar = New System.Windows.Forms.TextBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Panel57 = New System.Windows.Forms.Panel()
        Me.varRecibioTratamientoHCO = New System.Windows.Forms.TextBox()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Panel56 = New System.Windows.Forms.Panel()
        Me.varCualFracturaDeDiente = New System.Windows.Forms.TextBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.varFracturaDeDiente = New System.Windows.Forms.CheckBox()
        Me.Panel55 = New System.Windows.Forms.Panel()
        Me.varCuandoGolpeEnDientes = New System.Windows.Forms.DateTimePicker()
        Me.varComoGolpeEnDientes = New System.Windows.Forms.TextBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.varGolpeEnDientes = New System.Windows.Forms.CheckBox()
        Me.Panel54 = New System.Windows.Forms.Panel()
        Me.varPudoCalmarIrradiacion = New System.Windows.Forms.TextBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.varHaciaDondeIrradiado = New System.Windows.Forms.TextBox()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.varIrradiado = New System.Windows.Forms.CheckBox()
        Me.Panel53 = New System.Windows.Forms.Panel()
        Me.varDondeDolor = New System.Windows.Forms.TextBox()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.varDolorProvocado = New System.Windows.Forms.CheckBox()
        Me.varDolorLocalizado = New System.Windows.Forms.CheckBox()
        Me.varDolorAlFrio = New System.Windows.Forms.CheckBox()
        Me.varDolorAlCalor = New System.Windows.Forms.CheckBox()
        Me.varDolorEspontaneo = New System.Windows.Forms.CheckBox()
        Me.Panel51 = New System.Windows.Forms.Panel()
        Me.varObtuvoResultadoHCO = New System.Windows.Forms.CheckBox()
        Me.varDesdeCuandoMedicacionHCO = New System.Windows.Forms.DateTimePicker()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Panel49 = New System.Windows.Forms.Panel()
        Me.varMedicacionHCO = New System.Windows.Forms.TextBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.varTomoAlgoHCO = New System.Windows.Forms.CheckBox()
        Me.varConsultoAntes = New System.Windows.Forms.CheckBox()
        Me.Panel48 = New System.Windows.Forms.Panel()
        Me.varMotivoConsulta = New System.Windows.Forms.TextBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Panel47 = New System.Windows.Forms.Panel()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.Panel79 = New System.Windows.Forms.Panel()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.EstadoHigieneBucalMalo = New System.Windows.Forms.CheckBox()
        Me.EstadoHigieneBucalDeficiente = New System.Windows.Forms.CheckBox()
        Me.EstadoHigieneBucalBueno = New System.Windows.Forms.CheckBox()
        Me.EstadoHigieneBucalMuyBueno = New System.Windows.Forms.CheckBox()
        Me.Panel78 = New System.Windows.Forms.Panel()
        Me.IndiceDePlaca = New System.Windows.Forms.TextBox()
        Me.MomentosDeAzucarDiario = New System.Windows.Forms.TextBox()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.Panel77 = New System.Windows.Forms.Panel()
        Me.varSePusoOtros = New System.Windows.Forms.TextBox()
        Me.varSePusoCalor = New System.Windows.Forms.CheckBox()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.varSePusoHielo = New System.Windows.Forms.CheckBox()
        Me.varCaraHinchada = New System.Windows.Forms.CheckBox()
        Me.Panel76 = New System.Windows.Forms.Panel()
        Me.varSienteAltosDientesAlMorder = New System.Windows.Forms.TextBox()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.varMovilidadEnDientes = New System.Windows.Forms.CheckBox()
        Me.Panel75 = New System.Windows.Forms.Panel()
        Me.varDondeSalePusEnBoca = New System.Windows.Forms.TextBox()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.varSalePusEnBoca = New System.Windows.Forms.CheckBox()
        Me.Panel74 = New System.Windows.Forms.Panel()
        Me.varCuandoSangranEncias = New System.Windows.Forms.TextBox()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.varSangranEncias = New System.Windows.Forms.CheckBox()
        Me.Panel73 = New System.Windows.Forms.Panel()
        Me.varOtros = New System.Windows.Forms.TextBox()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.Panel71 = New System.Windows.Forms.Panel()
        Me.varAmpollas = New System.Windows.Forms.CheckBox()
        Me.varUlceraciones = New System.Windows.Forms.CheckBox()
        Me.varAbultamientoTejidos = New System.Windows.Forms.CheckBox()
        Me.varManchas = New System.Windows.Forms.CheckBox()
        Me.Panel69 = New System.Windows.Forms.Panel()
        Me.varRetromolar = New System.Windows.Forms.TextBox()
        Me.varTrigono = New System.Windows.Forms.TextBox()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Panel72 = New System.Windows.Forms.Panel()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.Panel67 = New System.Windows.Forms.Panel()
        Me.varRebordes = New System.Windows.Forms.TextBox()
        Me.varCarrillos = New System.Windows.Forms.TextBox()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Panel65 = New System.Windows.Forms.Panel()
        Me.varPisoDeBoca = New System.Windows.Forms.TextBox()
        Me.varPaladar = New System.Windows.Forms.TextBox()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Panel63 = New System.Windows.Forms.Panel()
        Me.varLengua = New System.Windows.Forms.TextBox()
        Me.varLabios = New System.Windows.Forms.TextBox()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Panel62 = New System.Windows.Forms.Panel()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.Panel80 = New System.Windows.Forms.Panel()
        Me.Panel26 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_82 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_86 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_81 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_48 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_62 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_85 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_61 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_46 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_66 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_47 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_65 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_84 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_28 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_45 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_27 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_44 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_26 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_83 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_25 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_42 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_64 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_43 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_63 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_41 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_24 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_22 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_23 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_21 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_75 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_73 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_76 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_71 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_55 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_74 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_56 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_31 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_53 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_72 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_54 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_33 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_51 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_32 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_52 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_35 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_11 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_34 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_12 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_37 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_13 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_36 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_14 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_38 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_15 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_17 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_16 = New System.Windows.Forms.Panel()
        Me.Cuadraditos_18 = New System.Windows.Forms.Panel()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.Panel85 = New System.Windows.Forms.Panel()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.varObservaciones = New System.Windows.Forms.TextBox()
        Me.Panel84 = New System.Windows.Forms.Panel()
        Me.varCuandoPlanTratamiento = New System.Windows.Forms.DateTimePicker()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.varPlanTratamiento = New System.Windows.Forms.TextBox()
        Me.Panel83 = New System.Windows.Forms.Panel()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.varDiagnosticoPresuntivo = New System.Windows.Forms.TextBox()
        Me.Panel82 = New System.Windows.Forms.Panel()
        Me.varEnfermedadPeriodontal = New System.Windows.Forms.CheckBox()
        Me.varSarro = New System.Windows.Forms.CheckBox()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.Panel81 = New System.Windows.Forms.Panel()
        Me.varCantidadDientes = New System.Windows.Forms.TextBox()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.Label77 = New System.Windows.Forms.Label()
        TabPage1 = New System.Windows.Forms.TabPage()
        Label19 = New System.Windows.Forms.Label()
        Me.MenuStrip1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        TabPage1.SuspendLayout()
        Me.Panel18.SuspendLayout()
        Me.Panel14.SuspendLayout()
        Me.Panel90.SuspendLayout()
        Me.Panel91.SuspendLayout()
        Me.Panel50.SuspendLayout()
        Me.Panel86.SuspendLayout()
        Me.Panel87.SuspendLayout()
        Me.Panel88.SuspendLayout()
        Me.Panel89.SuspendLayout()
        Me.Panel17.SuspendLayout()
        Me.Panel16.SuspendLayout()
        Me.Panel15.SuspendLayout()
        Me.Panel13.SuspendLayout()
        Me.Panel12.SuspendLayout()
        Me.Panel11.SuspendLayout()
        Me.Panel10.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.Panel33.SuspendLayout()
        Me.Panel19.SuspendLayout()
        Me.Panel29.SuspendLayout()
        Me.Panel28.SuspendLayout()
        Me.Panel27.SuspendLayout()
        Me.Panel25.SuspendLayout()
        Me.Panel24.SuspendLayout()
        Me.Panel23.SuspendLayout()
        Me.Panel22.SuspendLayout()
        Me.Panel21.SuspendLayout()
        Me.Panel20.SuspendLayout()
        Me.Panel32.SuspendLayout()
        Me.Panel46.SuspendLayout()
        Me.Panel30.SuspendLayout()
        Me.Panel38.SuspendLayout()
        Me.Panel45.SuspendLayout()
        Me.Panel37.SuspendLayout()
        Me.Panel39.SuspendLayout()
        Me.Panel44.SuspendLayout()
        Me.Panel36.SuspendLayout()
        Me.Panel31.SuspendLayout()
        Me.Panel40.SuspendLayout()
        Me.Panel43.SuspendLayout()
        Me.Panel35.SuspendLayout()
        Me.Panel34.SuspendLayout()
        Me.Panel41.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.Panel52.SuspendLayout()
        Me.IdDuracionDolor.SuspendLayout()
        Me.IdIntensidadDolor.SuspendLayout()
        Me.Panel61.SuspendLayout()
        Me.Panel60.SuspendLayout()
        Me.Panel59.SuspendLayout()
        Me.Panel58.SuspendLayout()
        Me.Panel57.SuspendLayout()
        Me.Panel56.SuspendLayout()
        Me.Panel55.SuspendLayout()
        Me.Panel54.SuspendLayout()
        Me.Panel53.SuspendLayout()
        Me.Panel51.SuspendLayout()
        Me.Panel49.SuspendLayout()
        Me.Panel48.SuspendLayout()
        Me.Panel47.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.Panel79.SuspendLayout()
        Me.Panel78.SuspendLayout()
        Me.Panel77.SuspendLayout()
        Me.Panel76.SuspendLayout()
        Me.Panel75.SuspendLayout()
        Me.Panel74.SuspendLayout()
        Me.Panel73.SuspendLayout()
        Me.Panel71.SuspendLayout()
        Me.Panel69.SuspendLayout()
        Me.Panel72.SuspendLayout()
        Me.Panel67.SuspendLayout()
        Me.Panel65.SuspendLayout()
        Me.Panel63.SuspendLayout()
        Me.Panel62.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.Panel80.SuspendLayout()
        Me.TabPage7.SuspendLayout()
        Me.Panel85.SuspendLayout()
        Me.Panel84.SuspendLayout()
        Me.Panel83.SuspendLayout()
        Me.Panel82.SuspendLayout()
        Me.Panel81.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GuardarToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(734, 24)
        Me.MenuStrip1.TabIndex = 1
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'GuardarToolStripMenuItem
        '
        Me.GuardarToolStripMenuItem.Name = "GuardarToolStripMenuItem"
        Me.GuardarToolStripMenuItem.Size = New System.Drawing.Size(61, 20)
        Me.GuardarToolStripMenuItem.Text = "Guardar"
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Controls.Add(Me.TabPage7)
        Me.TabControl1.Location = New System.Drawing.Point(0, 24)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(736, 450)
        Me.TabControl1.TabIndex = 15
        '
        'TabPage1
        '
        TabPage1.AutoScroll = True
        TabPage1.Controls.Add(Me.Panel18)
        TabPage1.Location = New System.Drawing.Point(4, 22)
        TabPage1.Name = "TabPage1"
        TabPage1.Padding = New System.Windows.Forms.Padding(3)
        TabPage1.Size = New System.Drawing.Size(728, 424)
        TabPage1.TabIndex = 0
        TabPage1.Text = "TabPage1"
        TabPage1.UseVisualStyleBackColor = True
        '
        'Panel18
        '
        Me.Panel18.Controls.Add(Me.Button1)
        Me.Panel18.Controls.Add(Me.Panel14)
        Me.Panel18.Controls.Add(Me.Panel90)
        Me.Panel18.Controls.Add(Me.Panel91)
        Me.Panel18.Controls.Add(Me.Panel50)
        Me.Panel18.Controls.Add(Me.Panel86)
        Me.Panel18.Controls.Add(Me.Panel87)
        Me.Panel18.Controls.Add(Me.Panel88)
        Me.Panel18.Controls.Add(Me.Panel89)
        Me.Panel18.Controls.Add(Me.Panel17)
        Me.Panel18.Controls.Add(Me.Panel16)
        Me.Panel18.Controls.Add(Me.Panel15)
        Me.Panel18.Controls.Add(Me.Panel13)
        Me.Panel18.Controls.Add(Me.Panel12)
        Me.Panel18.Controls.Add(Me.Panel11)
        Me.Panel18.Controls.Add(Me.Panel10)
        Me.Panel18.Controls.Add(Me.Panel9)
        Me.Panel18.Controls.Add(Me.Panel8)
        Me.Panel18.Controls.Add(Me.Panel7)
        Me.Panel18.Controls.Add(Me.Panel6)
        Me.Panel18.Controls.Add(Me.Panel5)
        Me.Panel18.Controls.Add(Me.Panel4)
        Me.Panel18.Controls.Add(Me.Panel3)
        Me.Panel18.Controls.Add(Me.Panel2)
        Me.Panel18.Controls.Add(Me.Panel1)
        Me.Panel18.Location = New System.Drawing.Point(0, 0)
        Me.Panel18.Name = "Panel18"
        Me.Panel18.Size = New System.Drawing.Size(726, 386)
        Me.Panel18.TabIndex = 15
        '
        'Panel14
        '
        Me.Panel14.Controls.Add(Me.varCodigoPostal)
        Me.Panel14.Controls.Add(Me.Label15)
        Me.Panel14.Location = New System.Drawing.Point(415, 233)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(136, 27)
        Me.Panel14.TabIndex = 86
        '
        'varCodigoPostal
        '
        Me.varCodigoPostal.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varCodigoPostal.Location = New System.Drawing.Point(39, 3)
        Me.varCodigoPostal.Name = "varCodigoPostal"
        Me.varCodigoPostal.Size = New System.Drawing.Size(93, 20)
        Me.varCodigoPostal.TabIndex = 2
        '
        'Label15
        '
        Me.Label15.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(3, 6)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(30, 13)
        Me.Label15.TabIndex = 0
        Me.Label15.Text = "C.P.:"
        '
        'Panel90
        '
        Me.Panel90.Controls.Add(Me.varLocalidad)
        Me.Panel90.Controls.Add(Me.Label93)
        Me.Panel90.Location = New System.Drawing.Point(7, 233)
        Me.Panel90.Name = "Panel90"
        Me.Panel90.Size = New System.Drawing.Size(199, 27)
        Me.Panel90.TabIndex = 86
        '
        'varLocalidad
        '
        Me.varLocalidad.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varLocalidad.Location = New System.Drawing.Point(62, 3)
        Me.varLocalidad.Name = "varLocalidad"
        Me.varLocalidad.Size = New System.Drawing.Size(134, 20)
        Me.varLocalidad.TabIndex = 2
        '
        'Label93
        '
        Me.Label93.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label93.AutoSize = True
        Me.Label93.Location = New System.Drawing.Point(3, 6)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(56, 13)
        Me.Label93.TabIndex = 0
        Me.Label93.Text = "Localidad:"
        '
        'Panel91
        '
        Me.Panel91.Controls.Add(Me.varProvincia)
        Me.Panel91.Controls.Add(Me.Label94)
        Me.Panel91.Location = New System.Drawing.Point(212, 233)
        Me.Panel91.Name = "Panel91"
        Me.Panel91.Size = New System.Drawing.Size(197, 27)
        Me.Panel91.TabIndex = 85
        '
        'varProvincia
        '
        Me.varProvincia.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varProvincia.Location = New System.Drawing.Point(63, 3)
        Me.varProvincia.Name = "varProvincia"
        Me.varProvincia.Size = New System.Drawing.Size(131, 20)
        Me.varProvincia.TabIndex = 2
        '
        'Label94
        '
        Me.Label94.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label94.AutoSize = True
        Me.Label94.Location = New System.Drawing.Point(3, 6)
        Me.Label94.Name = "Label94"
        Me.Label94.Size = New System.Drawing.Size(54, 13)
        Me.Label94.TabIndex = 0
        Me.Label94.Text = "Provincia:"
        '
        'Panel50
        '
        Me.Panel50.Controls.Add(Me.varCalle)
        Me.Panel50.Controls.Add(Me.Label39)
        Me.Panel50.Location = New System.Drawing.Point(7, 203)
        Me.Panel50.Name = "Panel50"
        Me.Panel50.Size = New System.Drawing.Size(165, 27)
        Me.Panel50.TabIndex = 79
        '
        'varCalle
        '
        Me.varCalle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varCalle.Location = New System.Drawing.Point(39, 4)
        Me.varCalle.Name = "varCalle"
        Me.varCalle.Size = New System.Drawing.Size(123, 20)
        Me.varCalle.TabIndex = 80
        '
        'Label39
        '
        Me.Label39.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(3, 6)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(33, 13)
        Me.Label39.TabIndex = 0
        Me.Label39.Text = "Calle:"
        '
        'Panel86
        '
        Me.Panel86.Controls.Add(Me.varNumero)
        Me.Panel86.Controls.Add(Me.Label45)
        Me.Panel86.Location = New System.Drawing.Point(178, 203)
        Me.Panel86.Name = "Panel86"
        Me.Panel86.Size = New System.Drawing.Size(68, 27)
        Me.Panel86.TabIndex = 80
        '
        'varNumero
        '
        Me.varNumero.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varNumero.Location = New System.Drawing.Point(19, 4)
        Me.varNumero.Name = "varNumero"
        Me.varNumero.Size = New System.Drawing.Size(46, 20)
        Me.varNumero.TabIndex = 80
        '
        'Label45
        '
        Me.Label45.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(3, 6)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(19, 13)
        Me.Label45.TabIndex = 0
        Me.Label45.Text = "Nº"
        '
        'Panel87
        '
        Me.Panel87.Controls.Add(Me.varPiso)
        Me.Panel87.Controls.Add(Me.Label90)
        Me.Panel87.Location = New System.Drawing.Point(252, 203)
        Me.Panel87.Name = "Panel87"
        Me.Panel87.Size = New System.Drawing.Size(59, 27)
        Me.Panel87.TabIndex = 81
        '
        'varPiso
        '
        Me.varPiso.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varPiso.Location = New System.Drawing.Point(26, 4)
        Me.varPiso.Name = "varPiso"
        Me.varPiso.Size = New System.Drawing.Size(30, 20)
        Me.varPiso.TabIndex = 80
        '
        'Label90
        '
        Me.Label90.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label90.AutoSize = True
        Me.Label90.Location = New System.Drawing.Point(3, 6)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(17, 13)
        Me.Label90.TabIndex = 0
        Me.Label90.Text = "P:"
        '
        'Panel88
        '
        Me.Panel88.Controls.Add(Me.varDepartamento)
        Me.Panel88.Controls.Add(Me.Label91)
        Me.Panel88.Location = New System.Drawing.Point(314, 203)
        Me.Panel88.Name = "Panel88"
        Me.Panel88.Size = New System.Drawing.Size(68, 27)
        Me.Panel88.TabIndex = 82
        '
        'varDepartamento
        '
        Me.varDepartamento.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varDepartamento.Location = New System.Drawing.Point(35, 4)
        Me.varDepartamento.Name = "varDepartamento"
        Me.varDepartamento.Size = New System.Drawing.Size(30, 20)
        Me.varDepartamento.TabIndex = 80
        '
        'Label91
        '
        Me.Label91.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label91.AutoSize = True
        Me.Label91.Location = New System.Drawing.Point(3, 6)
        Me.Label91.Name = "Label91"
        Me.Label91.Size = New System.Drawing.Size(33, 13)
        Me.Label91.TabIndex = 0
        Me.Label91.Text = "Dpto:"
        '
        'Panel89
        '
        Me.Panel89.Controls.Add(Me.varBarrio)
        Me.Panel89.Controls.Add(Me.Label92)
        Me.Panel89.Location = New System.Drawing.Point(388, 203)
        Me.Panel89.Name = "Panel89"
        Me.Panel89.Size = New System.Drawing.Size(149, 27)
        Me.Panel89.TabIndex = 83
        '
        'varBarrio
        '
        Me.varBarrio.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varBarrio.Location = New System.Drawing.Point(38, 4)
        Me.varBarrio.Name = "varBarrio"
        Me.varBarrio.Size = New System.Drawing.Size(108, 20)
        Me.varBarrio.TabIndex = 80
        '
        'Label92
        '
        Me.Label92.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label92.AutoSize = True
        Me.Label92.Location = New System.Drawing.Point(3, 6)
        Me.Label92.Name = "Label92"
        Me.Label92.Size = New System.Drawing.Size(37, 13)
        Me.Label92.TabIndex = 0
        Me.Label92.Text = "Barrio:"
        '
        'Panel17
        '
        Me.Panel17.Controls.Add(Me.varLugarTrabajo)
        Me.Panel17.Controls.Add(Me.Label18)
        Me.Panel17.Location = New System.Drawing.Point(7, 299)
        Me.Panel17.Name = "Panel17"
        Me.Panel17.Size = New System.Drawing.Size(286, 27)
        Me.Panel17.TabIndex = 75
        '
        'varLugarTrabajo
        '
        Me.varLugarTrabajo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varLugarTrabajo.Location = New System.Drawing.Point(96, 3)
        Me.varLugarTrabajo.Name = "varLugarTrabajo"
        Me.varLugarTrabajo.Size = New System.Drawing.Size(187, 20)
        Me.varLugarTrabajo.TabIndex = 2
        '
        'Label18
        '
        Me.Label18.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(3, 6)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(87, 13)
        Me.Label18.TabIndex = 0
        Me.Label18.Text = "Lugar de trabajo:"
        '
        'Panel16
        '
        Me.Panel16.Controls.Add(Me.varTitular)
        Me.Panel16.Controls.Add(Me.Label17)
        Me.Panel16.Location = New System.Drawing.Point(299, 266)
        Me.Panel16.Name = "Panel16"
        Me.Panel16.Size = New System.Drawing.Size(202, 27)
        Me.Panel16.TabIndex = 79
        '
        'varTitular
        '
        Me.varTitular.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varTitular.Location = New System.Drawing.Point(48, 3)
        Me.varTitular.Name = "varTitular"
        Me.varTitular.Size = New System.Drawing.Size(151, 20)
        Me.varTitular.TabIndex = 2
        '
        'Label17
        '
        Me.Label17.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(3, 6)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(39, 13)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Titular:"
        '
        'Panel15
        '
        Me.Panel15.Controls.Add(Me.varProfesionActividad)
        Me.Panel15.Controls.Add(Me.Label16)
        Me.Panel15.Location = New System.Drawing.Point(7, 266)
        Me.Panel15.Name = "Panel15"
        Me.Panel15.Size = New System.Drawing.Size(286, 27)
        Me.Panel15.TabIndex = 78
        '
        'varProfesionActividad
        '
        Me.varProfesionActividad.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varProfesionActividad.Location = New System.Drawing.Point(112, 3)
        Me.varProfesionActividad.Name = "varProfesionActividad"
        Me.varProfesionActividad.Size = New System.Drawing.Size(171, 20)
        Me.varProfesionActividad.TabIndex = 2
        '
        'Label16
        '
        Me.Label16.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(3, 6)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(103, 13)
        Me.Label16.TabIndex = 0
        Me.Label16.Text = "Profesion/Actividad:"
        '
        'Panel13
        '
        Me.Panel13.Controls.Add(Me.varCelular)
        Me.Panel13.Controls.Add(Me.Label14)
        Me.Panel13.Location = New System.Drawing.Point(222, 138)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(187, 27)
        Me.Panel13.TabIndex = 67
        '
        'varCelular
        '
        Me.varCelular.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varCelular.Location = New System.Drawing.Point(51, 3)
        Me.varCelular.Name = "varCelular"
        Me.varCelular.Size = New System.Drawing.Size(133, 20)
        Me.varCelular.TabIndex = 2
        '
        'Label14
        '
        Me.Label14.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(3, 6)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(42, 13)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Celular:"
        '
        'Panel12
        '
        Me.Panel12.Controls.Add(Me.varNumDocumento)
        Me.Panel12.Controls.Add(Me.Label13)
        Me.Panel12.Location = New System.Drawing.Point(456, 39)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(188, 27)
        Me.Panel12.TabIndex = 77
        '
        'varNumDocumento
        '
        Me.varNumDocumento.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varNumDocumento.Location = New System.Drawing.Point(89, 3)
        Me.varNumDocumento.Name = "varNumDocumento"
        Me.varNumDocumento.ReadOnly = True
        Me.varNumDocumento.Size = New System.Drawing.Size(96, 20)
        Me.varNumDocumento.TabIndex = 2
        '
        'Label13
        '
        Me.Label13.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(3, 6)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(80, 13)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "Nº Documento:"
        '
        'Panel11
        '
        Me.Panel11.Controls.Add(Me.varNacionalidad)
        Me.Panel11.Controls.Add(Me.Label12)
        Me.Panel11.Location = New System.Drawing.Point(311, 72)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(194, 27)
        Me.Panel11.TabIndex = 76
        '
        'varNacionalidad
        '
        Me.varNacionalidad.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varNacionalidad.Location = New System.Drawing.Point(74, 3)
        Me.varNacionalidad.Name = "varNacionalidad"
        Me.varNacionalidad.ReadOnly = True
        Me.varNacionalidad.Size = New System.Drawing.Size(117, 20)
        Me.varNacionalidad.TabIndex = 2
        '
        'Label12
        '
        Me.Label12.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(3, 6)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(72, 13)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Nacionalidad:"
        '
        'Panel10
        '
        Me.Panel10.Controls.Add(Me.varIdEstadoCivil)
        Me.Panel10.Controls.Add(Me.Label11)
        Me.Panel10.Location = New System.Drawing.Point(511, 72)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(211, 27)
        Me.Panel10.TabIndex = 74
        '
        'varIdEstadoCivil
        '
        Me.varIdEstadoCivil.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varIdEstadoCivil.Location = New System.Drawing.Point(74, 3)
        Me.varIdEstadoCivil.Name = "varIdEstadoCivil"
        Me.varIdEstadoCivil.ReadOnly = True
        Me.varIdEstadoCivil.Size = New System.Drawing.Size(134, 20)
        Me.varIdEstadoCivil.TabIndex = 2
        '
        'Label11
        '
        Me.Label11.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(3, 6)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(65, 13)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Estado Civil:"
        '
        'Panel9
        '
        Me.Panel9.Controls.Add(Me.varEdad)
        Me.Panel9.Controls.Add(Me.Label9)
        Me.Panel9.Location = New System.Drawing.Point(237, 72)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(68, 27)
        Me.Panel9.TabIndex = 66
        '
        'varEdad
        '
        Me.varEdad.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varEdad.Location = New System.Drawing.Point(35, 4)
        Me.varEdad.Name = "varEdad"
        Me.varEdad.ReadOnly = True
        Me.varEdad.Size = New System.Drawing.Size(30, 20)
        Me.varEdad.TabIndex = 80
        '
        'Label9
        '
        Me.Label9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(3, 6)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(35, 13)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Edad:"
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.varTelefono)
        Me.Panel8.Controls.Add(Me.Label8)
        Me.Panel8.Location = New System.Drawing.Point(7, 138)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(209, 27)
        Me.Panel8.TabIndex = 64
        '
        'varTelefono
        '
        Me.varTelefono.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varTelefono.Location = New System.Drawing.Point(61, 3)
        Me.varTelefono.Name = "varTelefono"
        Me.varTelefono.ReadOnly = True
        Me.varTelefono.Size = New System.Drawing.Size(145, 20)
        Me.varTelefono.TabIndex = 2
        '
        'Label8
        '
        Me.Label8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(3, 6)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(52, 13)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Telefono:"
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.varFechaNacimiento)
        Me.Panel7.Controls.Add(Me.Label7)
        Me.Panel7.Location = New System.Drawing.Point(7, 72)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(224, 27)
        Me.Panel7.TabIndex = 69
        '
        'varFechaNacimiento
        '
        Me.varFechaNacimiento.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varFechaNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.varFechaNacimiento.Location = New System.Drawing.Point(126, 3)
        Me.varFechaNacimiento.Name = "varFechaNacimiento"
        Me.varFechaNacimiento.Size = New System.Drawing.Size(95, 20)
        Me.varFechaNacimiento.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(3, 6)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(117, 13)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Fecha de Naciemiento:"
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.varIdObraSocial)
        Me.Panel6.Controls.Add(Me.Label6)
        Me.Panel6.Location = New System.Drawing.Point(7, 105)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(226, 27)
        Me.Panel6.TabIndex = 73
        '
        'varIdObraSocial
        '
        Me.varIdObraSocial.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varIdObraSocial.Location = New System.Drawing.Point(74, 3)
        Me.varIdObraSocial.Name = "varIdObraSocial"
        Me.varIdObraSocial.ReadOnly = True
        Me.varIdObraSocial.Size = New System.Drawing.Size(149, 20)
        Me.varIdObraSocial.TabIndex = 2
        '
        'Label6
        '
        Me.Label6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(3, 6)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(65, 13)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Obra Social:"
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.varNumAfiliado)
        Me.Panel5.Controls.Add(Me.Label5)
        Me.Panel5.Location = New System.Drawing.Point(239, 105)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(266, 27)
        Me.Panel5.TabIndex = 71
        '
        'varNumAfiliado
        '
        Me.varNumAfiliado.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varNumAfiliado.Location = New System.Drawing.Point(108, 3)
        Me.varNumAfiliado.Name = "varNumAfiliado"
        Me.varNumAfiliado.ReadOnly = True
        Me.varNumAfiliado.Size = New System.Drawing.Size(155, 20)
        Me.varNumAfiliado.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(3, 6)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(99, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Numero de Afiliado:"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.varApellido)
        Me.Panel4.Controls.Add(Me.varNombre)
        Me.Panel4.Controls.Add(Me.Label4)
        Me.Panel4.Location = New System.Drawing.Point(7, 39)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(443, 27)
        Me.Panel4.TabIndex = 70
        '
        'varApellido
        '
        Me.varApellido.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varApellido.Location = New System.Drawing.Point(298, 3)
        Me.varApellido.Name = "varApellido"
        Me.varApellido.ReadOnly = True
        Me.varApellido.Size = New System.Drawing.Size(142, 20)
        Me.varApellido.TabIndex = 5
        '
        'varNombre
        '
        Me.varNombre.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varNombre.Location = New System.Drawing.Point(159, 3)
        Me.varNombre.Name = "varNombre"
        Me.varNombre.ReadOnly = True
        Me.varNombre.Size = New System.Drawing.Size(133, 20)
        Me.varNombre.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(3, 6)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(150, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Nombre/Apellido del paciente:"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.varFecha)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Location = New System.Drawing.Point(574, 6)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(148, 27)
        Me.Panel3.TabIndex = 68
        '
        'varFecha
        '
        Me.varFecha.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.varFecha.Location = New System.Drawing.Point(50, 2)
        Me.varFecha.Name = "varFecha"
        Me.varFecha.Size = New System.Drawing.Size(95, 20)
        Me.varFecha.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 6)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Fecha:"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.varIdConsultorio)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Location = New System.Drawing.Point(296, 6)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(272, 27)
        Me.Panel2.TabIndex = 65
        '
        'varIdConsultorio
        '
        Me.varIdConsultorio.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varIdConsultorio.Location = New System.Drawing.Point(118, 3)
        Me.varIdConsultorio.Name = "varIdConsultorio"
        Me.varIdConsultorio.Size = New System.Drawing.Size(151, 20)
        Me.varIdConsultorio.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 6)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(109, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Lugar del Consultorio:"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.varIdOdontologo)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(7, 6)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(283, 27)
        Me.Panel1.TabIndex = 63
        '
        'varIdOdontologo
        '
        Me.varIdOdontologo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varIdOdontologo.FormattingEnabled = True
        Me.varIdOdontologo.Location = New System.Drawing.Point(74, 2)
        Me.varIdOdontologo.Name = "varIdOdontologo"
        Me.varIdOdontologo.Size = New System.Drawing.Size(206, 21)
        Me.varIdOdontologo.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Odontologo:"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Panel33)
        Me.TabPage2.Controls.Add(Me.Panel19)
        Me.TabPage2.Controls.Add(Me.Panel29)
        Me.TabPage2.Controls.Add(Me.Panel28)
        Me.TabPage2.Controls.Add(Me.Panel27)
        Me.TabPage2.Controls.Add(Me.Panel25)
        Me.TabPage2.Controls.Add(Me.Panel24)
        Me.TabPage2.Controls.Add(Me.Panel23)
        Me.TabPage2.Controls.Add(Me.Panel22)
        Me.TabPage2.Controls.Add(Me.Panel21)
        Me.TabPage2.Controls.Add(Me.Panel20)
        Me.TabPage2.Controls.Add(Me.Panel32)
        Me.TabPage2.Controls.Add(Label19)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(728, 424)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "TabPage2"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.Panel46)
        Me.TabPage3.Controls.Add(Me.Panel30)
        Me.TabPage3.Controls.Add(Me.Panel38)
        Me.TabPage3.Controls.Add(Me.Panel45)
        Me.TabPage3.Controls.Add(Me.Panel37)
        Me.TabPage3.Controls.Add(Me.Panel39)
        Me.TabPage3.Controls.Add(Me.Panel44)
        Me.TabPage3.Controls.Add(Me.Panel36)
        Me.TabPage3.Controls.Add(Me.Panel31)
        Me.TabPage3.Controls.Add(Me.Panel40)
        Me.TabPage3.Controls.Add(Me.Panel43)
        Me.TabPage3.Controls.Add(Me.Panel35)
        Me.TabPage3.Controls.Add(Me.Panel34)
        Me.TabPage3.Controls.Add(Me.Panel41)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(728, 424)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "TabPage3"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(647, 360)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 87
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Panel33
        '
        Me.Panel33.Controls.Add(Me.varOtraEnfermedadInfectocontagiosa)
        Me.Panel33.Controls.Add(Me.varTransfusiones)
        Me.Panel33.Location = New System.Drawing.Point(8, 237)
        Me.Panel33.Name = "Panel33"
        Me.Panel33.Size = New System.Drawing.Size(650, 27)
        Me.Panel33.TabIndex = 70
        '
        'varOtraEnfermedadInfectocontagiosa
        '
        Me.varOtraEnfermedadInfectocontagiosa.AutoSize = True
        Me.varOtraEnfermedadInfectocontagiosa.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varOtraEnfermedadInfectocontagiosa.Location = New System.Drawing.Point(6, 6)
        Me.varOtraEnfermedadInfectocontagiosa.Name = "varOtraEnfermedadInfectocontagiosa"
        Me.varOtraEnfermedadInfectocontagiosa.Size = New System.Drawing.Size(204, 17)
        Me.varOtraEnfermedadInfectocontagiosa.TabIndex = 50
        Me.varOtraEnfermedadInfectocontagiosa.Text = "Otra enfermedad infecto-contagiosa?:"
        Me.varOtraEnfermedadInfectocontagiosa.UseVisualStyleBackColor = True
        '
        'varTransfusiones
        '
        Me.varTransfusiones.AutoSize = True
        Me.varTransfusiones.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varTransfusiones.Location = New System.Drawing.Point(216, 6)
        Me.varTransfusiones.Name = "varTransfusiones"
        Me.varTransfusiones.Size = New System.Drawing.Size(125, 17)
        Me.varTransfusiones.TabIndex = 51
        Me.varTransfusiones.Text = "Tuvo transfusiones?:"
        Me.varTransfusiones.UseVisualStyleBackColor = True
        '
        'Panel19
        '
        Me.Panel19.Controls.Add(Me.varEnfermedadPasadaActualMadre)
        Me.Panel19.Controls.Add(Me.Label10)
        Me.Panel19.Controls.Add(Me.varMadreConVida)
        Me.Panel19.Location = New System.Drawing.Point(8, 74)
        Me.Panel19.Name = "Panel19"
        Me.Panel19.Size = New System.Drawing.Size(650, 27)
        Me.Panel19.TabIndex = 64
        '
        'varEnfermedadPasadaActualMadre
        '
        Me.varEnfermedadPasadaActualMadre.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varEnfermedadPasadaActualMadre.Location = New System.Drawing.Point(294, 3)
        Me.varEnfermedadPasadaActualMadre.Name = "varEnfermedadPasadaActualMadre"
        Me.varEnfermedadPasadaActualMadre.Size = New System.Drawing.Size(353, 20)
        Me.varEnfermedadPasadaActualMadre.TabIndex = 2
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(109, 6)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(177, 13)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Enfermedad que padece o padecio:"
        '
        'varMadreConVida
        '
        Me.varMadreConVida.AutoSize = True
        Me.varMadreConVida.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varMadreConVida.Location = New System.Drawing.Point(6, 5)
        Me.varMadreConVida.Name = "varMadreConVida"
        Me.varMadreConVida.Size = New System.Drawing.Size(106, 17)
        Me.varMadreConVida.TabIndex = 38
        Me.varMadreConVida.Text = "Madre con vida?"
        Me.varMadreConVida.UseVisualStyleBackColor = True
        '
        'Panel29
        '
        Me.Panel29.Controls.Add(Me.varCualTratamientoAlternativo)
        Me.Panel29.Controls.Add(Me.varTratamientoAlternativo)
        Me.Panel29.Location = New System.Drawing.Point(8, 391)
        Me.Panel29.Name = "Panel29"
        Me.Panel29.Size = New System.Drawing.Size(650, 26)
        Me.Panel29.TabIndex = 73
        '
        'varCualTratamientoAlternativo
        '
        Me.varCualTratamientoAlternativo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varCualTratamientoAlternativo.Location = New System.Drawing.Point(352, 3)
        Me.varCualTratamientoAlternativo.Name = "varCualTratamientoAlternativo"
        Me.varCualTratamientoAlternativo.Size = New System.Drawing.Size(295, 20)
        Me.varCualTratamientoAlternativo.TabIndex = 2
        '
        'varTratamientoAlternativo
        '
        Me.varTratamientoAlternativo.AutoSize = True
        Me.varTratamientoAlternativo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varTratamientoAlternativo.Location = New System.Drawing.Point(6, 5)
        Me.varTratamientoAlternativo.Name = "varTratamientoAlternativo"
        Me.varTratamientoAlternativo.Size = New System.Drawing.Size(342, 17)
        Me.varTratamientoAlternativo.TabIndex = 61
        Me.varTratamientoAlternativo.Text = "Realiza algun tipo de tratamiento homeopatico, acupuntura, otros?:"
        Me.varTratamientoAlternativo.UseVisualStyleBackColor = True
        '
        'Panel28
        '
        Me.Panel28.Controls.Add(Me.varCualEnfermedadRecomendacion)
        Me.Panel28.Controls.Add(Me.Label30)
        Me.Panel28.Controls.Add(Me.varOtraEnfermedadRecomendacion)
        Me.Panel28.Location = New System.Drawing.Point(8, 362)
        Me.Panel28.Name = "Panel28"
        Me.Panel28.Size = New System.Drawing.Size(650, 26)
        Me.Panel28.TabIndex = 72
        '
        'varCualEnfermedadRecomendacion
        '
        Me.varCualEnfermedadRecomendacion.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varCualEnfermedadRecomendacion.Location = New System.Drawing.Point(509, 3)
        Me.varCualEnfermedadRecomendacion.Name = "varCualEnfermedadRecomendacion"
        Me.varCualEnfermedadRecomendacion.Size = New System.Drawing.Size(138, 20)
        Me.varCualEnfermedadRecomendacion.TabIndex = 2
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(466, 6)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(37, 13)
        Me.Label30.TabIndex = 0
        Me.Label30.Text = "Cual?:"
        '
        'varOtraEnfermedadRecomendacion
        '
        Me.varOtraEnfermedadRecomendacion.AutoSize = True
        Me.varOtraEnfermedadRecomendacion.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varOtraEnfermedadRecomendacion.Location = New System.Drawing.Point(6, 5)
        Me.varOtraEnfermedadRecomendacion.Name = "varOtraEnfermedadRecomendacion"
        Me.varOtraEnfermedadRecomendacion.Size = New System.Drawing.Size(454, 17)
        Me.varOtraEnfermedadRecomendacion.TabIndex = 59
        Me.varOtraEnfermedadRecomendacion.Text = "Hay alguna otra enfermedad o recomendacion de su medico que quiera dejar constanc" &
    "ia?:"
        Me.varOtraEnfermedadRecomendacion.UseVisualStyleBackColor = True
        '
        'Panel27
        '
        Me.Panel27.Controls.Add(Me.varCualProblemasRespiratorios)
        Me.Panel27.Controls.Add(Me.Label29)
        Me.Panel27.Controls.Add(Me.varProblemasRespiratorios)
        Me.Panel27.Controls.Add(Me.varFuma)
        Me.Panel27.Location = New System.Drawing.Point(8, 301)
        Me.Panel27.Name = "Panel27"
        Me.Panel27.Size = New System.Drawing.Size(650, 26)
        Me.Panel27.TabIndex = 68
        '
        'varCualProblemasRespiratorios
        '
        Me.varCualProblemasRespiratorios.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varCualProblemasRespiratorios.Location = New System.Drawing.Point(246, 3)
        Me.varCualProblemasRespiratorios.Name = "varCualProblemasRespiratorios"
        Me.varCualProblemasRespiratorios.Size = New System.Drawing.Size(337, 20)
        Me.varCualProblemasRespiratorios.TabIndex = 2
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(203, 6)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(37, 13)
        Me.Label29.TabIndex = 0
        Me.Label29.Text = "Cual?:"
        '
        'varProblemasRespiratorios
        '
        Me.varProblemasRespiratorios.AutoSize = True
        Me.varProblemasRespiratorios.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varProblemasRespiratorios.Location = New System.Drawing.Point(6, 5)
        Me.varProblemasRespiratorios.Name = "varProblemasRespiratorios"
        Me.varProblemasRespiratorios.Size = New System.Drawing.Size(191, 17)
        Me.varProblemasRespiratorios.TabIndex = 56
        Me.varProblemasRespiratorios.Text = "Tiene algun problema respiratorio?:"
        Me.varProblemasRespiratorios.UseVisualStyleBackColor = True
        '
        'varFuma
        '
        Me.varFuma.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varFuma.AutoSize = True
        Me.varFuma.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varFuma.Location = New System.Drawing.Point(589, 5)
        Me.varFuma.Name = "varFuma"
        Me.varFuma.Size = New System.Drawing.Size(58, 17)
        Me.varFuma.TabIndex = 57
        Me.varFuma.Text = "Fuma?"
        Me.varFuma.UseVisualStyleBackColor = True
        '
        'Panel25
        '
        Me.Panel25.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel25.Controls.Add(Me.varCuandoOperacion)
        Me.Panel25.Controls.Add(Me.Label28)
        Me.Panel25.Controls.Add(Me.varDeQueOperacion)
        Me.Panel25.Controls.Add(Me.Label27)
        Me.Panel25.Controls.Add(Me.varFueOperadoAntes)
        Me.Panel25.Location = New System.Drawing.Point(8, 270)
        Me.Panel25.Name = "Panel25"
        Me.Panel25.Size = New System.Drawing.Size(650, 26)
        Me.Panel25.TabIndex = 67
        '
        'varCuandoOperacion
        '
        Me.varCuandoOperacion.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varCuandoOperacion.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.varCuandoOperacion.Location = New System.Drawing.Point(552, 3)
        Me.varCuandoOperacion.Name = "varCuandoOperacion"
        Me.varCuandoOperacion.Size = New System.Drawing.Size(95, 20)
        Me.varCuandoOperacion.TabIndex = 56
        '
        'Label28
        '
        Me.Label28.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(493, 6)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(53, 13)
        Me.Label28.TabIndex = 55
        Me.Label28.Text = "Cuando?:"
        '
        'varDeQueOperacion
        '
        Me.varDeQueOperacion.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varDeQueOperacion.Location = New System.Drawing.Point(214, 3)
        Me.varDeQueOperacion.Name = "varDeQueOperacion"
        Me.varDeQueOperacion.Size = New System.Drawing.Size(273, 20)
        Me.varDeQueOperacion.TabIndex = 2
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(157, 6)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(51, 13)
        Me.Label27.TabIndex = 0
        Me.Label27.Text = "De que?:"
        '
        'varFueOperadoAntes
        '
        Me.varFueOperadoAntes.AutoSize = True
        Me.varFueOperadoAntes.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varFueOperadoAntes.Location = New System.Drawing.Point(6, 5)
        Me.varFueOperadoAntes.Name = "varFueOperadoAntes"
        Me.varFueOperadoAntes.Size = New System.Drawing.Size(150, 17)
        Me.varFueOperadoAntes.TabIndex = 54
        Me.varFueOperadoAntes.Text = "Fue operado alguna vez?:"
        Me.varFueOperadoAntes.UseVisualStyleBackColor = True
        '
        'Panel24
        '
        Me.Panel24.Controls.Add(Me.varMesesEmbarazo)
        Me.Panel24.Controls.Add(Me.Label26)
        Me.Panel24.Controls.Add(Me.varEmbarazada)
        Me.Panel24.Location = New System.Drawing.Point(8, 333)
        Me.Panel24.Name = "Panel24"
        Me.Panel24.Size = New System.Drawing.Size(295, 27)
        Me.Panel24.TabIndex = 71
        '
        'varMesesEmbarazo
        '
        Me.varMesesEmbarazo.Location = New System.Drawing.Point(239, 3)
        Me.varMesesEmbarazo.Name = "varMesesEmbarazo"
        Me.varMesesEmbarazo.Size = New System.Drawing.Size(53, 20)
        Me.varMesesEmbarazo.TabIndex = 81
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(129, 6)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(104, 13)
        Me.Label26.TabIndex = 0
        Me.Label26.Text = "De cuantos meses?:"
        '
        'varEmbarazada
        '
        Me.varEmbarazada.AutoSize = True
        Me.varEmbarazada.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varEmbarazada.Location = New System.Drawing.Point(6, 5)
        Me.varEmbarazada.Name = "varEmbarazada"
        Me.varEmbarazada.Size = New System.Drawing.Size(117, 17)
        Me.varEmbarazada.TabIndex = 52
        Me.varEmbarazada.Text = "Esta embarasada?:"
        Me.varEmbarazada.UseVisualStyleBackColor = True
        '
        'Panel23
        '
        Me.Panel23.Controls.Add(Me.varMedicamentosUltimos5Anos)
        Me.Panel23.Controls.Add(Me.Label25)
        Me.Panel23.Location = New System.Drawing.Point(8, 204)
        Me.Panel23.Name = "Panel23"
        Me.Panel23.Size = New System.Drawing.Size(650, 27)
        Me.Panel23.TabIndex = 69
        '
        'varMedicamentosUltimos5Anos
        '
        Me.varMedicamentosUltimos5Anos.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varMedicamentosUltimos5Anos.Location = New System.Drawing.Point(286, 4)
        Me.varMedicamentosUltimos5Anos.Name = "varMedicamentosUltimos5Anos"
        Me.varMedicamentosUltimos5Anos.Size = New System.Drawing.Size(361, 20)
        Me.varMedicamentosUltimos5Anos.TabIndex = 2
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(3, 6)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(277, 13)
        Me.Label25.TabIndex = 0
        Me.Label25.Text = "Que medicamentos ha consumido en los ultimos 5 años?:"
        '
        'Panel22
        '
        Me.Panel22.Controls.Add(Me.varMedicamentosActuales)
        Me.Panel22.Controls.Add(Me.Label24)
        Me.Panel22.Location = New System.Drawing.Point(8, 171)
        Me.Panel22.Name = "Panel22"
        Me.Panel22.Size = New System.Drawing.Size(650, 27)
        Me.Panel22.TabIndex = 65
        '
        'varMedicamentosActuales
        '
        Me.varMedicamentosActuales.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varMedicamentosActuales.Location = New System.Drawing.Point(162, 3)
        Me.varMedicamentosActuales.Name = "varMedicamentosActuales"
        Me.varMedicamentosActuales.Size = New System.Drawing.Size(485, 20)
        Me.varMedicamentosActuales.TabIndex = 2
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(3, 6)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(153, 13)
        Me.Label24.TabIndex = 0
        Me.Label24.Text = "Que medicamentos consume?:"
        '
        'Panel21
        '
        Me.Panel21.Controls.Add(Me.varCualTratamientoMedico)
        Me.Panel21.Controls.Add(Me.Label23)
        Me.Panel21.Controls.Add(Me.varHaceTratamientoMedico)
        Me.Panel21.Location = New System.Drawing.Point(8, 139)
        Me.Panel21.Name = "Panel21"
        Me.Panel21.Size = New System.Drawing.Size(650, 26)
        Me.Panel21.TabIndex = 66
        '
        'varCualTratamientoMedico
        '
        Me.varCualTratamientoMedico.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varCualTratamientoMedico.Location = New System.Drawing.Point(237, 3)
        Me.varCualTratamientoMedico.Name = "varCualTratamientoMedico"
        Me.varCualTratamientoMedico.Size = New System.Drawing.Size(410, 20)
        Me.varCualTratamientoMedico.TabIndex = 2
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(194, 6)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(37, 13)
        Me.Label23.TabIndex = 0
        Me.Label23.Text = "Cual?:"
        '
        'varHaceTratamientoMedico
        '
        Me.varHaceTratamientoMedico.AutoSize = True
        Me.varHaceTratamientoMedico.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varHaceTratamientoMedico.Location = New System.Drawing.Point(6, 5)
        Me.varHaceTratamientoMedico.Name = "varHaceTratamientoMedico"
        Me.varHaceTratamientoMedico.Size = New System.Drawing.Size(182, 17)
        Me.varHaceTratamientoMedico.TabIndex = 48
        Me.varHaceTratamientoMedico.Text = "Hace algun tratamiento medico?:"
        Me.varHaceTratamientoMedico.UseVisualStyleBackColor = True
        '
        'Panel20
        '
        Me.Panel20.Controls.Add(Me.varEnfermedadHermano)
        Me.Panel20.Controls.Add(Me.Label22)
        Me.Panel20.Controls.Add(Me.varEnfermoHermano)
        Me.Panel20.Controls.Add(Me.varSanos)
        Me.Panel20.Controls.Add(Me.varHermanos)
        Me.Panel20.Location = New System.Drawing.Point(8, 107)
        Me.Panel20.Name = "Panel20"
        Me.Panel20.Size = New System.Drawing.Size(650, 26)
        Me.Panel20.TabIndex = 63
        '
        'varEnfermedadHermano
        '
        Me.varEnfermedadHermano.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varEnfermedadHermano.Location = New System.Drawing.Point(375, 3)
        Me.varEnfermedadHermano.Name = "varEnfermedadHermano"
        Me.varEnfermedadHermano.Size = New System.Drawing.Size(272, 20)
        Me.varEnfermedadHermano.TabIndex = 2
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(332, 6)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(37, 13)
        Me.Label22.TabIndex = 0
        Me.Label22.Text = "Cual?:"
        '
        'varEnfermoHermano
        '
        Me.varEnfermoHermano.AutoSize = True
        Me.varEnfermoHermano.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varEnfermoHermano.Location = New System.Drawing.Point(160, 5)
        Me.varEnfermoHermano.Name = "varEnfermoHermano"
        Me.varEnfermoHermano.Size = New System.Drawing.Size(166, 17)
        Me.varEnfermoHermano.TabIndex = 47
        Me.varEnfermoHermano.Text = "Sufre de alguna enfermedad?"
        Me.varEnfermoHermano.UseVisualStyleBackColor = True
        '
        'varSanos
        '
        Me.varSanos.AutoSize = True
        Me.varSanos.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varSanos.Location = New System.Drawing.Point(92, 5)
        Me.varSanos.Name = "varSanos"
        Me.varSanos.Size = New System.Drawing.Size(62, 17)
        Me.varSanos.TabIndex = 46
        Me.varSanos.Text = "Sanos?"
        Me.varSanos.UseVisualStyleBackColor = True
        '
        'varHermanos
        '
        Me.varHermanos.AutoSize = True
        Me.varHermanos.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varHermanos.Location = New System.Drawing.Point(6, 5)
        Me.varHermanos.Name = "varHermanos"
        Me.varHermanos.Size = New System.Drawing.Size(80, 17)
        Me.varHermanos.TabIndex = 45
        Me.varHermanos.Text = "Hermanos?"
        Me.varHermanos.UseVisualStyleBackColor = True
        '
        'Panel32
        '
        Me.Panel32.Controls.Add(Me.varEnfermedadPasadaActualPadre)
        Me.Panel32.Controls.Add(Me.Label20)
        Me.Panel32.Controls.Add(Me.varPadreConVida)
        Me.Panel32.Location = New System.Drawing.Point(8, 41)
        Me.Panel32.Name = "Panel32"
        Me.Panel32.Size = New System.Drawing.Size(650, 27)
        Me.Panel32.TabIndex = 62
        '
        'varEnfermedadPasadaActualPadre
        '
        Me.varEnfermedadPasadaActualPadre.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varEnfermedadPasadaActualPadre.Location = New System.Drawing.Point(294, 3)
        Me.varEnfermedadPasadaActualPadre.Name = "varEnfermedadPasadaActualPadre"
        Me.varEnfermedadPasadaActualPadre.Size = New System.Drawing.Size(353, 20)
        Me.varEnfermedadPasadaActualPadre.TabIndex = 2
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(109, 6)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(177, 13)
        Me.Label20.TabIndex = 0
        Me.Label20.Text = "Enfermedad que padece o padecio:"
        '
        'varPadreConVida
        '
        Me.varPadreConVida.AutoSize = True
        Me.varPadreConVida.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varPadreConVida.Location = New System.Drawing.Point(6, 5)
        Me.varPadreConVida.Name = "varPadreConVida"
        Me.varPadreConVida.Size = New System.Drawing.Size(104, 17)
        Me.varPadreConVida.TabIndex = 38
        Me.varPadreConVida.Text = "Padre con vida?"
        Me.varPadreConVida.UseVisualStyleBackColor = True
        '
        'Label19
        '
        Label19.AutoSize = True
        Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label19.Location = New System.Drawing.Point(14, 15)
        Label19.Name = "Label19"
        Label19.Size = New System.Drawing.Size(246, 13)
        Label19.TabIndex = 61
        Label19.Text = "Este cuestionario tiene tenor de declaracion jurada"
        '
        'Panel46
        '
        Me.Panel46.Controls.Add(Me.varSifilisGonorrea)
        Me.Panel46.Location = New System.Drawing.Point(6, 362)
        Me.Panel46.Name = "Panel46"
        Me.Panel46.Size = New System.Drawing.Size(708, 27)
        Me.Panel46.TabIndex = 79
        '
        'varSifilisGonorrea
        '
        Me.varSifilisGonorrea.AutoSize = True
        Me.varSifilisGonorrea.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varSifilisGonorrea.Location = New System.Drawing.Point(6, 6)
        Me.varSifilisGonorrea.Name = "varSifilisGonorrea"
        Me.varSifilisGonorrea.Size = New System.Drawing.Size(163, 17)
        Me.varSifilisGonorrea.TabIndex = 50
        Me.varSifilisGonorrea.Text = "Ha tenido Sifilis o Gonorrea?:"
        Me.varSifilisGonorrea.UseVisualStyleBackColor = True
        '
        'Panel30
        '
        Me.Panel30.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel30.Controls.Add(Me.varMedicoClinico)
        Me.Panel30.Controls.Add(Me.Label31)
        Me.Panel30.Location = New System.Drawing.Point(6, 6)
        Me.Panel30.Name = "Panel30"
        Me.Panel30.Size = New System.Drawing.Size(276, 27)
        Me.Panel30.TabIndex = 66
        '
        'varMedicoClinico
        '
        Me.varMedicoClinico.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varMedicoClinico.Location = New System.Drawing.Point(82, 3)
        Me.varMedicoClinico.Name = "varMedicoClinico"
        Me.varMedicoClinico.Size = New System.Drawing.Size(191, 20)
        Me.varMedicoClinico.TabIndex = 2
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(3, 6)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(79, 13)
        Me.Label31.TabIndex = 0
        Me.Label31.Text = "Medico Clinico:"
        '
        'Panel38
        '
        Me.Panel38.Controls.Add(Me.varMedicacionDiabetico)
        Me.Panel38.Controls.Add(Me.Label35)
        Me.Panel38.Controls.Add(Me.varDiabetico)
        Me.Panel38.Controls.Add(Me.varDiabeticoControlado)
        Me.Panel38.Location = New System.Drawing.Point(6, 153)
        Me.Panel38.Name = "Panel38"
        Me.Panel38.Size = New System.Drawing.Size(708, 27)
        Me.Panel38.TabIndex = 70
        '
        'varMedicacionDiabetico
        '
        Me.varMedicacionDiabetico.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varMedicacionDiabetico.Location = New System.Drawing.Point(282, 4)
        Me.varMedicacionDiabetico.Name = "varMedicacionDiabetico"
        Me.varMedicacionDiabetico.Size = New System.Drawing.Size(423, 20)
        Me.varMedicacionDiabetico.TabIndex = 61
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(220, 7)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(56, 13)
        Me.Label35.TabIndex = 60
        Me.Label35.Text = "Con que?:"
        '
        'varDiabetico
        '
        Me.varDiabetico.AutoSize = True
        Me.varDiabetico.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varDiabetico.Location = New System.Drawing.Point(6, 6)
        Me.varDiabetico.Name = "varDiabetico"
        Me.varDiabetico.Size = New System.Drawing.Size(93, 17)
        Me.varDiabetico.TabIndex = 50
        Me.varDiabetico.Text = "Es diabetico?:"
        Me.varDiabetico.UseVisualStyleBackColor = True
        '
        'varDiabeticoControlado
        '
        Me.varDiabeticoControlado.AutoSize = True
        Me.varDiabeticoControlado.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varDiabeticoControlado.Location = New System.Drawing.Point(105, 6)
        Me.varDiabeticoControlado.Name = "varDiabeticoControlado"
        Me.varDiabeticoControlado.Size = New System.Drawing.Size(109, 17)
        Me.varDiabeticoControlado.TabIndex = 51
        Me.varDiabeticoControlado.Text = "Esta controlado?:"
        Me.varDiabeticoControlado.UseVisualStyleBackColor = True
        '
        'Panel45
        '
        Me.Panel45.Controls.Add(Me.varMedicacionEpilepsia)
        Me.Panel45.Controls.Add(Me.Label41)
        Me.Panel45.Controls.Add(Me.varConvulsiones)
        Me.Panel45.Controls.Add(Me.varEpilepsia)
        Me.Panel45.Location = New System.Drawing.Point(6, 332)
        Me.Panel45.Name = "Panel45"
        Me.Panel45.Size = New System.Drawing.Size(708, 27)
        Me.Panel45.TabIndex = 73
        '
        'varMedicacionEpilepsia
        '
        Me.varMedicacionEpilepsia.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varMedicacionEpilepsia.Location = New System.Drawing.Point(350, 4)
        Me.varMedicacionEpilepsia.Name = "varMedicacionEpilepsia"
        Me.varMedicacionEpilepsia.Size = New System.Drawing.Size(355, 20)
        Me.varMedicacionEpilepsia.TabIndex = 61
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(232, 7)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(112, 13)
        Me.Label41.TabIndex = 60
        Me.Label41.Text = "Medicacion que toma:"
        '
        'varConvulsiones
        '
        Me.varConvulsiones.AutoSize = True
        Me.varConvulsiones.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varConvulsiones.Location = New System.Drawing.Point(6, 6)
        Me.varConvulsiones.Name = "varConvulsiones"
        Me.varConvulsiones.Size = New System.Drawing.Size(125, 17)
        Me.varConvulsiones.TabIndex = 50
        Me.varConvulsiones.Text = "Tuvo convulsiones?:"
        Me.varConvulsiones.UseVisualStyleBackColor = True
        '
        'varEpilepsia
        '
        Me.varEpilepsia.AutoSize = True
        Me.varEpilepsia.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varEpilepsia.Location = New System.Drawing.Point(131, 6)
        Me.varEpilepsia.Name = "varEpilepsia"
        Me.varEpilepsia.Size = New System.Drawing.Size(95, 17)
        Me.varEpilepsia.TabIndex = 51
        Me.varEpilepsia.Text = "Es epileptico?:"
        Me.varEpilepsia.UseVisualStyleBackColor = True
        '
        'Panel37
        '
        Me.Panel37.Controls.Add(Me.varMedicacionFiebreReumatica)
        Me.Panel37.Controls.Add(Me.Label34)
        Me.Panel37.Controls.Add(Me.varHiperlaxitud)
        Me.Panel37.Controls.Add(Me.varAntecedentesFiebreReumatica)
        Me.Panel37.Location = New System.Drawing.Point(6, 123)
        Me.Panel37.Name = "Panel37"
        Me.Panel37.Size = New System.Drawing.Size(708, 27)
        Me.Panel37.TabIndex = 68
        '
        'varMedicacionFiebreReumatica
        '
        Me.varMedicacionFiebreReumatica.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varMedicacionFiebreReumatica.Location = New System.Drawing.Point(633, 4)
        Me.varMedicacionFiebreReumatica.Name = "varMedicacionFiebreReumatica"
        Me.varMedicacionFiebreReumatica.Size = New System.Drawing.Size(72, 20)
        Me.varMedicacionFiebreReumatica.TabIndex = 61
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(446, 7)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(181, 13)
        Me.Label34.TabIndex = 60
        Me.Label34.Text = "Se protege con alguna medicación?:"
        '
        'varHiperlaxitud
        '
        Me.varHiperlaxitud.AutoSize = True
        Me.varHiperlaxitud.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varHiperlaxitud.Location = New System.Drawing.Point(6, 6)
        Me.varHiperlaxitud.Name = "varHiperlaxitud"
        Me.varHiperlaxitud.Size = New System.Drawing.Size(237, 17)
        Me.varHiperlaxitud.TabIndex = 50
        Me.varHiperlaxitud.Text = "Tiene problemas de colageno (hiperlaxitud)?:"
        Me.varHiperlaxitud.UseVisualStyleBackColor = True
        '
        'varAntecedentesFiebreReumatica
        '
        Me.varAntecedentesFiebreReumatica.AutoSize = True
        Me.varAntecedentesFiebreReumatica.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varAntecedentesFiebreReumatica.Location = New System.Drawing.Point(249, 6)
        Me.varAntecedentesFiebreReumatica.Name = "varAntecedentesFiebreReumatica"
        Me.varAntecedentesFiebreReumatica.Size = New System.Drawing.Size(194, 17)
        Me.varAntecedentesFiebreReumatica.TabIndex = 51
        Me.varAntecedentesFiebreReumatica.Text = "Antecedentes de fiebre reumatica?:"
        Me.varAntecedentesFiebreReumatica.UseVisualStyleBackColor = True
        '
        'Panel39
        '
        Me.Panel39.Controls.Add(Me.varCualProblemaCardiaco)
        Me.Panel39.Controls.Add(Me.Label36)
        Me.Panel39.Controls.Add(Me.varProblemaCardiaco)
        Me.Panel39.Location = New System.Drawing.Point(6, 183)
        Me.Panel39.Name = "Panel39"
        Me.Panel39.Size = New System.Drawing.Size(708, 27)
        Me.Panel39.TabIndex = 74
        '
        'varCualProblemaCardiaco
        '
        Me.varCualProblemaCardiaco.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varCualProblemaCardiaco.Location = New System.Drawing.Point(236, 4)
        Me.varCualProblemaCardiaco.Name = "varCualProblemaCardiaco"
        Me.varCualProblemaCardiaco.Size = New System.Drawing.Size(469, 20)
        Me.varCualProblemaCardiaco.TabIndex = 61
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(193, 7)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(37, 13)
        Me.Label36.TabIndex = 60
        Me.Label36.Text = "Cual?:"
        '
        'varProblemaCardiaco
        '
        Me.varProblemaCardiaco.AutoSize = True
        Me.varProblemaCardiaco.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varProblemaCardiaco.Location = New System.Drawing.Point(6, 6)
        Me.varProblemaCardiaco.Name = "varProblemaCardiaco"
        Me.varProblemaCardiaco.Size = New System.Drawing.Size(181, 17)
        Me.varProblemaCardiaco.TabIndex = 50
        Me.varProblemaCardiaco.Text = "Tiene algun problema cardiaco?:"
        Me.varProblemaCardiaco.UseVisualStyleBackColor = True
        '
        'Panel44
        '
        Me.Panel44.Controls.Add(Me.varCualProblemaHepatico)
        Me.Panel44.Controls.Add(Me.Label40)
        Me.Panel44.Controls.Add(Me.varProblemaHepatico)
        Me.Panel44.Location = New System.Drawing.Point(6, 302)
        Me.Panel44.Name = "Panel44"
        Me.Panel44.Size = New System.Drawing.Size(708, 27)
        Me.Panel44.TabIndex = 77
        '
        'varCualProblemaHepatico
        '
        Me.varCualProblemaHepatico.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varCualProblemaHepatico.Location = New System.Drawing.Point(236, 4)
        Me.varCualProblemaHepatico.Name = "varCualProblemaHepatico"
        Me.varCualProblemaHepatico.Size = New System.Drawing.Size(469, 20)
        Me.varCualProblemaHepatico.TabIndex = 61
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(193, 7)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(37, 13)
        Me.Label40.TabIndex = 60
        Me.Label40.Text = "Cual?:"
        '
        'varProblemaHepatico
        '
        Me.varProblemaHepatico.AutoSize = True
        Me.varProblemaHepatico.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varProblemaHepatico.Location = New System.Drawing.Point(6, 6)
        Me.varProblemaHepatico.Name = "varProblemaHepatico"
        Me.varProblemaHepatico.Size = New System.Drawing.Size(181, 17)
        Me.varProblemaHepatico.TabIndex = 50
        Me.varProblemaHepatico.Text = "Tiene algun problema hepatico?:"
        Me.varProblemaHepatico.UseVisualStyleBackColor = True
        '
        'Panel36
        '
        Me.Panel36.Controls.Add(Me.varComoCicatrizaSangra)
        Me.Panel36.Controls.Add(Me.Label33)
        Me.Panel36.Location = New System.Drawing.Point(6, 94)
        Me.Panel36.Name = "Panel36"
        Me.Panel36.Size = New System.Drawing.Size(708, 27)
        Me.Panel36.TabIndex = 75
        '
        'varComoCicatrizaSangra
        '
        Me.varComoCicatrizaSangra.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varComoCicatrizaSangra.Location = New System.Drawing.Point(361, 3)
        Me.varComoCicatrizaSangra.Name = "varComoCicatrizaSangra"
        Me.varComoCicatrizaSangra.Size = New System.Drawing.Size(344, 20)
        Me.varComoCicatrizaSangra.TabIndex = 2
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(3, 6)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(353, 13)
        Me.Label33.TabIndex = 0
        Me.Label33.Text = "Cuando le sacan una muela o se lastima, cicatriza bien?, sangra mucho?:"
        '
        'Panel31
        '
        Me.Panel31.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel31.Controls.Add(Me.varClinicaHospital)
        Me.Panel31.Controls.Add(Me.Label32)
        Me.Panel31.Location = New System.Drawing.Point(288, 6)
        Me.Panel31.Name = "Panel31"
        Me.Panel31.Size = New System.Drawing.Size(426, 27)
        Me.Panel31.TabIndex = 71
        '
        'varClinicaHospital
        '
        Me.varClinicaHospital.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varClinicaHospital.Location = New System.Drawing.Point(189, 4)
        Me.varClinicaHospital.Name = "varClinicaHospital"
        Me.varClinicaHospital.Size = New System.Drawing.Size(234, 20)
        Me.varClinicaHospital.TabIndex = 2
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(3, 6)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(180, 13)
        Me.Label32.TabIndex = 0
        Me.Label32.Text = "Clinica/Hospital(caso de derivacion):"
        '
        'Panel40
        '
        Me.Panel40.Controls.Add(Me.varFrecuenciaAnticoagulante)
        Me.Panel40.Controls.Add(Me.Label37)
        Me.Panel40.Controls.Add(Me.varTomaAnticoagulante)
        Me.Panel40.Location = New System.Drawing.Point(6, 213)
        Me.Panel40.Name = "Panel40"
        Me.Panel40.Size = New System.Drawing.Size(708, 27)
        Me.Panel40.TabIndex = 78
        '
        'varFrecuenciaAnticoagulante
        '
        Me.varFrecuenciaAnticoagulante.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varFrecuenciaAnticoagulante.Location = New System.Drawing.Point(360, 4)
        Me.varFrecuenciaAnticoagulante.Name = "varFrecuenciaAnticoagulante"
        Me.varFrecuenciaAnticoagulante.Size = New System.Drawing.Size(345, 20)
        Me.varFrecuenciaAnticoagulante.TabIndex = 61
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(245, 7)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(109, 13)
        Me.Label37.TabIndex = 60
        Me.Label37.Text = "Con que frecuencia?:"
        '
        'varTomaAnticoagulante
        '
        Me.varTomaAnticoagulante.AutoSize = True
        Me.varTomaAnticoagulante.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varTomaAnticoagulante.Location = New System.Drawing.Point(6, 6)
        Me.varTomaAnticoagulante.Name = "varTomaAnticoagulante"
        Me.varTomaAnticoagulante.Size = New System.Drawing.Size(233, 17)
        Me.varTomaAnticoagulante.TabIndex = 50
        Me.varTomaAnticoagulante.Text = "Toma seguido aspirina y/o anticoagulante?:"
        Me.varTomaAnticoagulante.UseVisualStyleBackColor = True
        '
        'Panel43
        '
        Me.Panel43.Controls.Add(Me.varHepatitisC)
        Me.Panel43.Controls.Add(Me.varHepatitisB)
        Me.Panel43.Controls.Add(Me.varHepatitisA)
        Me.Panel43.Controls.Add(Me.varTuvoHepatitis)
        Me.Panel43.Controls.Add(Me.varProblemasRenales)
        Me.Panel43.Controls.Add(Me.varUlceraGastrica)
        Me.Panel43.Location = New System.Drawing.Point(6, 273)
        Me.Panel43.Name = "Panel43"
        Me.Panel43.Size = New System.Drawing.Size(708, 27)
        Me.Panel43.TabIndex = 76
        '
        'varHepatitisC
        '
        Me.varHepatitisC.AutoSize = True
        Me.varHepatitisC.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varHepatitisC.Location = New System.Drawing.Point(514, 6)
        Me.varHepatitisC.Name = "varHepatitisC"
        Me.varHepatitisC.Size = New System.Drawing.Size(60, 17)
        Me.varHepatitisC.TabIndex = 55
        Me.varHepatitisC.Text = "Tipo C:"
        Me.varHepatitisC.UseVisualStyleBackColor = True
        '
        'varHepatitisB
        '
        Me.varHepatitisB.AutoSize = True
        Me.varHepatitisB.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varHepatitisB.Location = New System.Drawing.Point(448, 6)
        Me.varHepatitisB.Name = "varHepatitisB"
        Me.varHepatitisB.Size = New System.Drawing.Size(60, 17)
        Me.varHepatitisB.TabIndex = 54
        Me.varHepatitisB.Text = "Tipo B:"
        Me.varHepatitisB.UseVisualStyleBackColor = True
        '
        'varHepatitisA
        '
        Me.varHepatitisA.AutoSize = True
        Me.varHepatitisA.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varHepatitisA.Location = New System.Drawing.Point(382, 6)
        Me.varHepatitisA.Name = "varHepatitisA"
        Me.varHepatitisA.Size = New System.Drawing.Size(60, 17)
        Me.varHepatitisA.TabIndex = 53
        Me.varHepatitisA.Text = "Tipo A:"
        Me.varHepatitisA.UseVisualStyleBackColor = True
        '
        'varTuvoHepatitis
        '
        Me.varTuvoHepatitis.AutoSize = True
        Me.varTuvoHepatitis.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varTuvoHepatitis.Location = New System.Drawing.Point(274, 6)
        Me.varTuvoHepatitis.Name = "varTuvoHepatitis"
        Me.varTuvoHepatitis.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.varTuvoHepatitis.Size = New System.Drawing.Size(102, 17)
        Me.varTuvoHepatitis.TabIndex = 52
        Me.varTuvoHepatitis.Text = "Tuvo hepatitis?:"
        Me.varTuvoHepatitis.UseVisualStyleBackColor = True
        '
        'varProblemasRenales
        '
        Me.varProblemasRenales.AutoSize = True
        Me.varProblemasRenales.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varProblemasRenales.Location = New System.Drawing.Point(6, 6)
        Me.varProblemasRenales.Name = "varProblemasRenales"
        Me.varProblemasRenales.Size = New System.Drawing.Size(150, 17)
        Me.varProblemasRenales.TabIndex = 50
        Me.varProblemasRenales.Text = "Tiene problemas renales?:"
        Me.varProblemasRenales.UseVisualStyleBackColor = True
        '
        'varUlceraGastrica
        '
        Me.varUlceraGastrica.AutoSize = True
        Me.varUlceraGastrica.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varUlceraGastrica.Location = New System.Drawing.Point(162, 6)
        Me.varUlceraGastrica.Name = "varUlceraGastrica"
        Me.varUlceraGastrica.Size = New System.Drawing.Size(106, 17)
        Me.varUlceraGastrica.TabIndex = 51
        Me.varUlceraGastrica.Text = "Ulcera gastrica?:"
        Me.varUlceraGastrica.UseVisualStyleBackColor = True
        '
        'Panel35
        '
        Me.Panel35.Controls.Add(Me.Label21)
        Me.Panel35.Controls.Add(Me.varAlergicoOtros)
        Me.Panel35.Controls.Add(Me.varAlergicoPenisilina)
        Me.Panel35.Controls.Add(Me.varAlergicoDrogas)
        Me.Panel35.Controls.Add(Me.varAlergicoAnestesia)
        Me.Panel35.Location = New System.Drawing.Point(6, 65)
        Me.Panel35.Name = "Panel35"
        Me.Panel35.Size = New System.Drawing.Size(708, 27)
        Me.Panel35.TabIndex = 69
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(361, 7)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(35, 13)
        Me.Label21.TabIndex = 3
        Me.Label21.Text = "Otros:"
        '
        'varAlergicoOtros
        '
        Me.varAlergicoOtros.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varAlergicoOtros.Location = New System.Drawing.Point(402, 3)
        Me.varAlergicoOtros.Name = "varAlergicoOtros"
        Me.varAlergicoOtros.Size = New System.Drawing.Size(303, 20)
        Me.varAlergicoOtros.TabIndex = 3
        '
        'varAlergicoPenisilina
        '
        Me.varAlergicoPenisilina.AutoSize = True
        Me.varAlergicoPenisilina.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varAlergicoPenisilina.Location = New System.Drawing.Point(282, 6)
        Me.varAlergicoPenisilina.Name = "varAlergicoPenisilina"
        Me.varAlergicoPenisilina.Size = New System.Drawing.Size(73, 17)
        Me.varAlergicoPenisilina.TabIndex = 52
        Me.varAlergicoPenisilina.Text = "Penisilina:"
        Me.varAlergicoPenisilina.UseVisualStyleBackColor = True
        '
        'varAlergicoDrogas
        '
        Me.varAlergicoDrogas.AutoSize = True
        Me.varAlergicoDrogas.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varAlergicoDrogas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.varAlergicoDrogas.Location = New System.Drawing.Point(6, 6)
        Me.varAlergicoDrogas.Name = "varAlergicoDrogas"
        Me.varAlergicoDrogas.Size = New System.Drawing.Size(189, 17)
        Me.varAlergicoDrogas.TabIndex = 50
        Me.varAlergicoDrogas.Text = "Es alergico a alguna droga?:"
        Me.varAlergicoDrogas.UseVisualStyleBackColor = True
        '
        'varAlergicoAnestesia
        '
        Me.varAlergicoAnestesia.AutoSize = True
        Me.varAlergicoAnestesia.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varAlergicoAnestesia.Location = New System.Drawing.Point(201, 6)
        Me.varAlergicoAnestesia.Name = "varAlergicoAnestesia"
        Me.varAlergicoAnestesia.Size = New System.Drawing.Size(75, 17)
        Me.varAlergicoAnestesia.TabIndex = 51
        Me.varAlergicoAnestesia.Text = "Anestesia:"
        Me.varAlergicoAnestesia.UseVisualStyleBackColor = True
        '
        'Panel34
        '
        Me.Panel34.Controls.Add(Me.varHaceDeporte)
        Me.Panel34.Controls.Add(Me.varMalestarAlRealizarlo)
        Me.Panel34.Location = New System.Drawing.Point(6, 36)
        Me.Panel34.Name = "Panel34"
        Me.Panel34.Size = New System.Drawing.Size(708, 27)
        Me.Panel34.TabIndex = 67
        '
        'varHaceDeporte
        '
        Me.varHaceDeporte.AutoSize = True
        Me.varHaceDeporte.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varHaceDeporte.Location = New System.Drawing.Point(6, 6)
        Me.varHaceDeporte.Name = "varHaceDeporte"
        Me.varHaceDeporte.Size = New System.Drawing.Size(138, 17)
        Me.varHaceDeporte.TabIndex = 50
        Me.varHaceDeporte.Text = "Realiza algun deporte?:"
        Me.varHaceDeporte.UseVisualStyleBackColor = True
        '
        'varMalestarAlRealizarlo
        '
        Me.varMalestarAlRealizarlo.AutoSize = True
        Me.varMalestarAlRealizarlo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varMalestarAlRealizarlo.Location = New System.Drawing.Point(150, 6)
        Me.varMalestarAlRealizarlo.Name = "varMalestarAlRealizarlo"
        Me.varMalestarAlRealizarlo.Size = New System.Drawing.Size(184, 17)
        Me.varMalestarAlRealizarlo.TabIndex = 51
        Me.varMalestarAlRealizarlo.Text = "Nota algún malestar al realizarlo?:"
        Me.varMalestarAlRealizarlo.UseVisualStyleBackColor = True
        '
        'Panel41
        '
        Me.Panel41.Controls.Add(Me.varTratamientoPresionChagas)
        Me.Panel41.Controls.Add(Me.Label38)
        Me.Panel41.Controls.Add(Me.varPresionAlta)
        Me.Panel41.Controls.Add(Me.varChagas)
        Me.Panel41.Location = New System.Drawing.Point(6, 243)
        Me.Panel41.Name = "Panel41"
        Me.Panel41.Size = New System.Drawing.Size(708, 27)
        Me.Panel41.TabIndex = 72
        '
        'varTratamientoPresionChagas
        '
        Me.varTratamientoPresionChagas.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varTratamientoPresionChagas.Location = New System.Drawing.Point(321, 4)
        Me.varTratamientoPresionChagas.Name = "varTratamientoPresionChagas"
        Me.varTratamientoPresionChagas.Size = New System.Drawing.Size(384, 20)
        Me.varTratamientoPresionChagas.TabIndex = 61
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(208, 7)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(107, 13)
        Me.Label38.TabIndex = 60
        Me.Label38.Text = "Esta en tratamiento?:"
        '
        'varPresionAlta
        '
        Me.varPresionAlta.AutoSize = True
        Me.varPresionAlta.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varPresionAlta.Location = New System.Drawing.Point(6, 6)
        Me.varPresionAlta.Name = "varPresionAlta"
        Me.varPresionAlta.Size = New System.Drawing.Size(119, 17)
        Me.varPresionAlta.TabIndex = 50
        Me.varPresionAlta.Text = "Tiene presión alta?:"
        Me.varPresionAlta.UseVisualStyleBackColor = True
        '
        'varChagas
        '
        Me.varChagas.AutoSize = True
        Me.varChagas.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varChagas.Location = New System.Drawing.Point(131, 6)
        Me.varChagas.Name = "varChagas"
        Me.varChagas.Size = New System.Drawing.Size(71, 17)
        Me.varChagas.TabIndex = 51
        Me.varChagas.Text = "Chagas?:"
        Me.varChagas.UseVisualStyleBackColor = True
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.Panel52)
        Me.TabPage4.Controls.Add(Me.Panel61)
        Me.TabPage4.Controls.Add(Me.Panel60)
        Me.TabPage4.Controls.Add(Me.Panel59)
        Me.TabPage4.Controls.Add(Me.Panel58)
        Me.TabPage4.Controls.Add(Me.Panel57)
        Me.TabPage4.Controls.Add(Me.Panel56)
        Me.TabPage4.Controls.Add(Me.Panel55)
        Me.TabPage4.Controls.Add(Me.Panel54)
        Me.TabPage4.Controls.Add(Me.Panel53)
        Me.TabPage4.Controls.Add(Me.Panel51)
        Me.TabPage4.Controls.Add(Me.Panel49)
        Me.TabPage4.Controls.Add(Me.Panel48)
        Me.TabPage4.Controls.Add(Me.Panel47)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(728, 424)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "TabPage4"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'Panel52
        '
        Me.Panel52.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel52.Controls.Add(Me.IdDuracionDolor)
        Me.Panel52.Controls.Add(Me.IdIntensidadDolor)
        Me.Panel52.Controls.Add(Me.varHaTenidoDolor)
        Me.Panel52.Location = New System.Drawing.Point(6, 126)
        Me.Panel52.Name = "Panel52"
        Me.Panel52.Size = New System.Drawing.Size(600, 28)
        Me.Panel52.TabIndex = 125
        '
        'IdDuracionDolor
        '
        Me.IdDuracionDolor.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.IdDuracionDolor.BackColor = System.Drawing.Color.Yellow
        Me.IdDuracionDolor.Controls.Add(Me.varIdDuracionDolor3)
        Me.IdDuracionDolor.Controls.Add(Me.varIdDuracionDolor2)
        Me.IdDuracionDolor.Controls.Add(Me.varIdDuracionDolor1)
        Me.IdDuracionDolor.Location = New System.Drawing.Point(352, 0)
        Me.IdDuracionDolor.Name = "IdDuracionDolor"
        Me.IdDuracionDolor.Size = New System.Drawing.Size(245, 27)
        Me.IdDuracionDolor.TabIndex = 100
        Me.IdDuracionDolor.TabStop = False
        '
        'varIdDuracionDolor3
        '
        Me.varIdDuracionDolor3.AutoSize = True
        Me.varIdDuracionDolor3.Location = New System.Drawing.Point(177, 5)
        Me.varIdDuracionDolor3.Name = "varIdDuracionDolor3"
        Me.varIdDuracionDolor3.Size = New System.Drawing.Size(67, 17)
        Me.varIdDuracionDolor3.TabIndex = 2
        Me.varIdDuracionDolor3.TabStop = True
        Me.varIdDuracionDolor3.Text = "Continuo"
        Me.varIdDuracionDolor3.UseVisualStyleBackColor = True
        '
        'varIdDuracionDolor2
        '
        Me.varIdDuracionDolor2.AutoSize = True
        Me.varIdDuracionDolor2.Location = New System.Drawing.Point(90, 5)
        Me.varIdDuracionDolor2.Name = "varIdDuracionDolor2"
        Me.varIdDuracionDolor2.Size = New System.Drawing.Size(80, 17)
        Me.varIdDuracionDolor2.TabIndex = 1
        Me.varIdDuracionDolor2.TabStop = True
        Me.varIdDuracionDolor2.Text = "Intermitente"
        Me.varIdDuracionDolor2.UseVisualStyleBackColor = True
        '
        'varIdDuracionDolor1
        '
        Me.varIdDuracionDolor1.AutoSize = True
        Me.varIdDuracionDolor1.Location = New System.Drawing.Point(6, 5)
        Me.varIdDuracionDolor1.Name = "varIdDuracionDolor1"
        Me.varIdDuracionDolor1.Size = New System.Drawing.Size(78, 17)
        Me.varIdDuracionDolor1.TabIndex = 0
        Me.varIdDuracionDolor1.TabStop = True
        Me.varIdDuracionDolor1.Text = "Temporario"
        Me.varIdDuracionDolor1.UseVisualStyleBackColor = True
        '
        'IdIntensidadDolor
        '
        Me.IdIntensidadDolor.BackColor = System.Drawing.Color.Yellow
        Me.IdIntensidadDolor.Controls.Add(Me.varIdIntensidadDolor3)
        Me.IdIntensidadDolor.Controls.Add(Me.varIdIntensidadDolor2)
        Me.IdIntensidadDolor.Controls.Add(Me.varIdIntensidadDolor1)
        Me.IdIntensidadDolor.Location = New System.Drawing.Point(119, 0)
        Me.IdIntensidadDolor.Name = "IdIntensidadDolor"
        Me.IdIntensidadDolor.Size = New System.Drawing.Size(212, 27)
        Me.IdIntensidadDolor.TabIndex = 98
        Me.IdIntensidadDolor.TabStop = False
        '
        'varIdIntensidadDolor3
        '
        Me.varIdIntensidadDolor3.AutoSize = True
        Me.varIdIntensidadDolor3.Location = New System.Drawing.Point(147, 5)
        Me.varIdIntensidadDolor3.Name = "varIdIntensidadDolor3"
        Me.varIdIntensidadDolor3.Size = New System.Drawing.Size(60, 17)
        Me.varIdIntensidadDolor3.TabIndex = 2
        Me.varIdIntensidadDolor3.TabStop = True
        Me.varIdIntensidadDolor3.Text = "Intenso"
        Me.varIdIntensidadDolor3.UseVisualStyleBackColor = True
        '
        'varIdIntensidadDolor2
        '
        Me.varIdIntensidadDolor2.AutoSize = True
        Me.varIdIntensidadDolor2.Location = New System.Drawing.Point(68, 5)
        Me.varIdIntensidadDolor2.Name = "varIdIntensidadDolor2"
        Me.varIdIntensidadDolor2.Size = New System.Drawing.Size(73, 17)
        Me.varIdIntensidadDolor2.TabIndex = 1
        Me.varIdIntensidadDolor2.TabStop = True
        Me.varIdIntensidadDolor2.Text = "Moderado"
        Me.varIdIntensidadDolor2.UseVisualStyleBackColor = True
        '
        'varIdIntensidadDolor1
        '
        Me.varIdIntensidadDolor1.AutoSize = True
        Me.varIdIntensidadDolor1.Location = New System.Drawing.Point(6, 5)
        Me.varIdIntensidadDolor1.Name = "varIdIntensidadDolor1"
        Me.varIdIntensidadDolor1.Size = New System.Drawing.Size(56, 17)
        Me.varIdIntensidadDolor1.TabIndex = 0
        Me.varIdIntensidadDolor1.TabStop = True
        Me.varIdIntensidadDolor1.Text = "Suave"
        Me.varIdIntensidadDolor1.UseVisualStyleBackColor = True
        '
        'varHaTenidoDolor
        '
        Me.varHaTenidoDolor.AutoSize = True
        Me.varHaTenidoDolor.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varHaTenidoDolor.Location = New System.Drawing.Point(6, 6)
        Me.varHaTenidoDolor.Name = "varHaTenidoDolor"
        Me.varHaTenidoDolor.Size = New System.Drawing.Size(107, 17)
        Me.varHaTenidoDolor.TabIndex = 50
        Me.varHaTenidoDolor.Text = "Ha tenido dolor?:"
        Me.varHaTenidoDolor.UseVisualStyleBackColor = True
        '
        'Panel61
        '
        Me.Panel61.Controls.Add(Me.varTragar)
        Me.Panel61.Controls.Add(Me.Label57)
        Me.Panel61.Location = New System.Drawing.Point(6, 392)
        Me.Panel61.Name = "Panel61"
        Me.Panel61.Size = New System.Drawing.Size(716, 27)
        Me.Panel61.TabIndex = 134
        '
        'varTragar
        '
        Me.varTragar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varTragar.Location = New System.Drawing.Point(214, 4)
        Me.varTragar.Name = "varTragar"
        Me.varTragar.Size = New System.Drawing.Size(499, 20)
        Me.varTragar.TabIndex = 63
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(3, 7)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(205, 13)
        Me.Label57.TabIndex = 62
        Me.Label57.Text = "Tiene dificultad para tragar los alimentos?:"
        '
        'Panel60
        '
        Me.Panel60.Controls.Add(Me.varAbrirBoca)
        Me.Panel60.Controls.Add(Me.Label56)
        Me.Panel60.Location = New System.Drawing.Point(6, 362)
        Me.Panel60.Name = "Panel60"
        Me.Panel60.Size = New System.Drawing.Size(716, 27)
        Me.Panel60.TabIndex = 132
        '
        'varAbrirBoca
        '
        Me.varAbrirBoca.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varAbrirBoca.Location = New System.Drawing.Point(182, 4)
        Me.varAbrirBoca.Name = "varAbrirBoca"
        Me.varAbrirBoca.Size = New System.Drawing.Size(531, 20)
        Me.varAbrirBoca.TabIndex = 63
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(3, 7)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(173, 13)
        Me.Label56.TabIndex = 62
        Me.Label56.Text = "Tiene dificultad para abrir la boca?:"
        '
        'Panel59
        '
        Me.Panel59.Controls.Add(Me.varMasticar)
        Me.Panel59.Controls.Add(Me.Label55)
        Me.Panel59.Location = New System.Drawing.Point(6, 332)
        Me.Panel59.Name = "Panel59"
        Me.Panel59.Size = New System.Drawing.Size(716, 27)
        Me.Panel59.TabIndex = 131
        '
        'varMasticar
        '
        Me.varMasticar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varMasticar.Location = New System.Drawing.Point(161, 4)
        Me.varMasticar.Name = "varMasticar"
        Me.varMasticar.Size = New System.Drawing.Size(552, 20)
        Me.varMasticar.TabIndex = 63
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(3, 7)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(152, 13)
        Me.Label55.TabIndex = 62
        Me.Label55.Text = "Tiene dificultad para marticar?:"
        '
        'Panel58
        '
        Me.Panel58.Controls.Add(Me.varHablar)
        Me.Panel58.Controls.Add(Me.Label54)
        Me.Panel58.Location = New System.Drawing.Point(6, 302)
        Me.Panel58.Name = "Panel58"
        Me.Panel58.Size = New System.Drawing.Size(716, 27)
        Me.Panel58.TabIndex = 130
        '
        'varHablar
        '
        Me.varHablar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varHablar.Location = New System.Drawing.Point(153, 4)
        Me.varHablar.Name = "varHablar"
        Me.varHablar.Size = New System.Drawing.Size(560, 20)
        Me.varHablar.TabIndex = 63
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(3, 7)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(144, 13)
        Me.Label54.TabIndex = 62
        Me.Label54.Text = "Tiene dificultad para hablar?:"
        '
        'Panel57
        '
        Me.Panel57.Controls.Add(Me.varRecibioTratamientoHCO)
        Me.Panel57.Controls.Add(Me.Label53)
        Me.Panel57.Location = New System.Drawing.Point(6, 272)
        Me.Panel57.Name = "Panel57"
        Me.Panel57.Size = New System.Drawing.Size(716, 27)
        Me.Panel57.TabIndex = 128
        '
        'varRecibioTratamientoHCO
        '
        Me.varRecibioTratamientoHCO.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varRecibioTratamientoHCO.Location = New System.Drawing.Point(145, 4)
        Me.varRecibioTratamientoHCO.Name = "varRecibioTratamientoHCO"
        Me.varRecibioTratamientoHCO.Size = New System.Drawing.Size(568, 20)
        Me.varRecibioTratamientoHCO.TabIndex = 63
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(3, 7)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(136, 13)
        Me.Label53.TabIndex = 62
        Me.Label53.Text = "Recibio algun tratamiento?:"
        '
        'Panel56
        '
        Me.Panel56.Controls.Add(Me.varCualFracturaDeDiente)
        Me.Panel56.Controls.Add(Me.Label52)
        Me.Panel56.Controls.Add(Me.varFracturaDeDiente)
        Me.Panel56.Location = New System.Drawing.Point(6, 242)
        Me.Panel56.Name = "Panel56"
        Me.Panel56.Size = New System.Drawing.Size(716, 27)
        Me.Panel56.TabIndex = 127
        '
        'varCualFracturaDeDiente
        '
        Me.varCualFracturaDeDiente.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varCualFracturaDeDiente.Location = New System.Drawing.Point(214, 4)
        Me.varCualFracturaDeDiente.Name = "varCualFracturaDeDiente"
        Me.varCualFracturaDeDiente.Size = New System.Drawing.Size(499, 20)
        Me.varCualFracturaDeDiente.TabIndex = 63
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(171, 7)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(37, 13)
        Me.Label52.TabIndex = 62
        Me.Label52.Text = "Cual?:"
        '
        'varFracturaDeDiente
        '
        Me.varFracturaDeDiente.AutoSize = True
        Me.varFracturaDeDiente.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varFracturaDeDiente.Location = New System.Drawing.Point(6, 6)
        Me.varFracturaDeDiente.Name = "varFracturaDeDiente"
        Me.varFracturaDeDiente.Size = New System.Drawing.Size(159, 17)
        Me.varFracturaDeDiente.TabIndex = 50
        Me.varFracturaDeDiente.Text = "Se le fracturo algun diente?:"
        Me.varFracturaDeDiente.UseVisualStyleBackColor = True
        '
        'Panel55
        '
        Me.Panel55.Controls.Add(Me.varCuandoGolpeEnDientes)
        Me.Panel55.Controls.Add(Me.varComoGolpeEnDientes)
        Me.Panel55.Controls.Add(Me.Label50)
        Me.Panel55.Controls.Add(Me.Label51)
        Me.Panel55.Controls.Add(Me.varGolpeEnDientes)
        Me.Panel55.Location = New System.Drawing.Point(6, 212)
        Me.Panel55.Name = "Panel55"
        Me.Panel55.Size = New System.Drawing.Size(716, 27)
        Me.Panel55.TabIndex = 129
        '
        'varCuandoGolpeEnDientes
        '
        Me.varCuandoGolpeEnDientes.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.varCuandoGolpeEnDientes.Location = New System.Drawing.Point(280, 4)
        Me.varCuandoGolpeEnDientes.Name = "varCuandoGolpeEnDientes"
        Me.varCuandoGolpeEnDientes.Size = New System.Drawing.Size(100, 20)
        Me.varCuandoGolpeEnDientes.TabIndex = 63
        '
        'varComoGolpeEnDientes
        '
        Me.varComoGolpeEnDientes.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varComoGolpeEnDientes.Location = New System.Drawing.Point(487, 4)
        Me.varComoGolpeEnDientes.Name = "varComoGolpeEnDientes"
        Me.varComoGolpeEnDientes.Size = New System.Drawing.Size(226, 20)
        Me.varComoGolpeEnDientes.TabIndex = 65
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(386, 7)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(95, 13)
        Me.Label50.TabIndex = 64
        Me.Label50.Text = "Cómo se produjo?:"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(221, 7)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(53, 13)
        Me.Label51.TabIndex = 62
        Me.Label51.Text = "Cuando?:"
        '
        'varGolpeEnDientes
        '
        Me.varGolpeEnDientes.AutoSize = True
        Me.varGolpeEnDientes.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varGolpeEnDientes.Location = New System.Drawing.Point(6, 6)
        Me.varGolpeEnDientes.Name = "varGolpeEnDientes"
        Me.varGolpeEnDientes.Size = New System.Drawing.Size(209, 17)
        Me.varGolpeEnDientes.TabIndex = 50
        Me.varGolpeEnDientes.Text = "Ha sufrido algun golpe en los dientes?:"
        Me.varGolpeEnDientes.UseVisualStyleBackColor = True
        '
        'Panel54
        '
        Me.Panel54.Controls.Add(Me.varPudoCalmarIrradiacion)
        Me.Panel54.Controls.Add(Me.Label49)
        Me.Panel54.Controls.Add(Me.varHaciaDondeIrradiado)
        Me.Panel54.Controls.Add(Me.Label48)
        Me.Panel54.Controls.Add(Me.varIrradiado)
        Me.Panel54.Location = New System.Drawing.Point(6, 183)
        Me.Panel54.Name = "Panel54"
        Me.Panel54.Size = New System.Drawing.Size(821, 27)
        Me.Panel54.TabIndex = 126
        '
        'varPudoCalmarIrradiacion
        '
        Me.varPudoCalmarIrradiacion.Location = New System.Drawing.Point(556, 4)
        Me.varPudoCalmarIrradiacion.Name = "varPudoCalmarIrradiacion"
        Me.varPudoCalmarIrradiacion.Size = New System.Drawing.Size(262, 20)
        Me.varPudoCalmarIrradiacion.TabIndex = 65
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(417, 7)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(133, 13)
        Me.Label49.TabIndex = 64
        Me.Label49.Text = "Puede calmarlo con algo?:"
        '
        'varHaciaDondeIrradiado
        '
        Me.varHaciaDondeIrradiado.Location = New System.Drawing.Point(162, 4)
        Me.varHaciaDondeIrradiado.Name = "varHaciaDondeIrradiado"
        Me.varHaciaDondeIrradiado.Size = New System.Drawing.Size(249, 20)
        Me.varHaciaDondeIrradiado.TabIndex = 63
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(79, 7)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(77, 13)
        Me.Label48.TabIndex = 62
        Me.Label48.Text = "Hacia donde?:"
        '
        'varIrradiado
        '
        Me.varIrradiado.AutoSize = True
        Me.varIrradiado.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varIrradiado.Location = New System.Drawing.Point(6, 6)
        Me.varIrradiado.Name = "varIrradiado"
        Me.varIrradiado.Size = New System.Drawing.Size(67, 17)
        Me.varIrradiado.TabIndex = 50
        Me.varIrradiado.Text = "Irradiado"
        Me.varIrradiado.UseVisualStyleBackColor = True
        '
        'Panel53
        '
        Me.Panel53.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel53.Controls.Add(Me.varDondeDolor)
        Me.Panel53.Controls.Add(Me.Label47)
        Me.Panel53.Controls.Add(Me.varDolorProvocado)
        Me.Panel53.Controls.Add(Me.varDolorLocalizado)
        Me.Panel53.Controls.Add(Me.varDolorAlFrio)
        Me.Panel53.Controls.Add(Me.varDolorAlCalor)
        Me.Panel53.Controls.Add(Me.varDolorEspontaneo)
        Me.Panel53.Location = New System.Drawing.Point(6, 153)
        Me.Panel53.Name = "Panel53"
        Me.Panel53.Size = New System.Drawing.Size(716, 27)
        Me.Panel53.TabIndex = 123
        '
        'varDondeDolor
        '
        Me.varDondeDolor.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varDondeDolor.Location = New System.Drawing.Point(441, 4)
        Me.varDondeDolor.Name = "varDondeDolor"
        Me.varDondeDolor.Size = New System.Drawing.Size(272, 20)
        Me.varDondeDolor.TabIndex = 63
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(387, 7)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(48, 13)
        Me.Label47.TabIndex = 62
        Me.Label47.Text = "Donde?:"
        '
        'varDolorProvocado
        '
        Me.varDolorProvocado.AutoSize = True
        Me.varDolorProvocado.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varDolorProvocado.Location = New System.Drawing.Point(95, 6)
        Me.varDolorProvocado.Name = "varDolorProvocado"
        Me.varDolorProvocado.Size = New System.Drawing.Size(78, 17)
        Me.varDolorProvocado.TabIndex = 59
        Me.varDolorProvocado.Text = "Provocado"
        Me.varDolorProvocado.UseVisualStyleBackColor = True
        '
        'varDolorLocalizado
        '
        Me.varDolorLocalizado.AutoSize = True
        Me.varDolorLocalizado.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varDolorLocalizado.Location = New System.Drawing.Point(304, 6)
        Me.varDolorLocalizado.Name = "varDolorLocalizado"
        Me.varDolorLocalizado.Size = New System.Drawing.Size(77, 17)
        Me.varDolorLocalizado.TabIndex = 51
        Me.varDolorLocalizado.Text = "Localizado"
        Me.varDolorLocalizado.UseVisualStyleBackColor = True
        '
        'varDolorAlFrio
        '
        Me.varDolorAlFrio.AutoSize = True
        Me.varDolorAlFrio.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varDolorAlFrio.Location = New System.Drawing.Point(179, 6)
        Me.varDolorAlFrio.Name = "varDolorAlFrio"
        Me.varDolorAlFrio.Size = New System.Drawing.Size(52, 17)
        Me.varDolorAlFrio.TabIndex = 58
        Me.varDolorAlFrio.Text = "Al frio"
        Me.varDolorAlFrio.UseVisualStyleBackColor = True
        '
        'varDolorAlCalor
        '
        Me.varDolorAlCalor.AutoSize = True
        Me.varDolorAlCalor.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varDolorAlCalor.Location = New System.Drawing.Point(237, 6)
        Me.varDolorAlCalor.Name = "varDolorAlCalor"
        Me.varDolorAlCalor.Size = New System.Drawing.Size(61, 17)
        Me.varDolorAlCalor.TabIndex = 50
        Me.varDolorAlCalor.Text = "Al calor"
        Me.varDolorAlCalor.UseVisualStyleBackColor = True
        '
        'varDolorEspontaneo
        '
        Me.varDolorEspontaneo.AutoSize = True
        Me.varDolorEspontaneo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varDolorEspontaneo.Location = New System.Drawing.Point(6, 6)
        Me.varDolorEspontaneo.Name = "varDolorEspontaneo"
        Me.varDolorEspontaneo.Size = New System.Drawing.Size(83, 17)
        Me.varDolorEspontaneo.TabIndex = 57
        Me.varDolorEspontaneo.Text = "Espontaneo"
        Me.varDolorEspontaneo.UseVisualStyleBackColor = True
        '
        'Panel51
        '
        Me.Panel51.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel51.Controls.Add(Me.varMedicacionHCO)
        Me.Panel51.Controls.Add(Me.varObtuvoResultadoHCO)
        Me.Panel51.Controls.Add(Me.Label44)
        Me.Panel51.Controls.Add(Me.varDesdeCuandoMedicacionHCO)
        Me.Panel51.Controls.Add(Me.Label46)
        Me.Panel51.Location = New System.Drawing.Point(6, 98)
        Me.Panel51.Name = "Panel51"
        Me.Panel51.Size = New System.Drawing.Size(716, 27)
        Me.Panel51.TabIndex = 133
        '
        'varObtuvoResultadoHCO
        '
        Me.varObtuvoResultadoHCO.AutoSize = True
        Me.varObtuvoResultadoHCO.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varObtuvoResultadoHCO.Location = New System.Drawing.Point(592, 5)
        Me.varObtuvoResultadoHCO.Name = "varObtuvoResultadoHCO"
        Me.varObtuvoResultadoHCO.Size = New System.Drawing.Size(121, 17)
        Me.varObtuvoResultadoHCO.TabIndex = 62
        Me.varObtuvoResultadoHCO.Text = "Obtuvo resultados?:"
        Me.varObtuvoResultadoHCO.UseVisualStyleBackColor = True
        '
        'varDesdeCuandoMedicacionHCO
        '
        Me.varDesdeCuandoMedicacionHCO.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.varDesdeCuandoMedicacionHCO.Location = New System.Drawing.Point(486, 3)
        Me.varDesdeCuandoMedicacionHCO.Name = "varDesdeCuandoMedicacionHCO"
        Me.varDesdeCuandoMedicacionHCO.Size = New System.Drawing.Size(100, 20)
        Me.varDesdeCuandoMedicacionHCO.TabIndex = 1
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(394, 6)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(86, 13)
        Me.Label46.TabIndex = 0
        Me.Label46.Text = "Desde cuando?:"
        '
        'Panel49
        '
        Me.Panel49.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel49.Controls.Add(Me.varTomoAlgoHCO)
        Me.Panel49.Controls.Add(Me.varConsultoAntes)
        Me.Panel49.Location = New System.Drawing.Point(6, 69)
        Me.Panel49.Name = "Panel49"
        Me.Panel49.Size = New System.Drawing.Size(716, 27)
        Me.Panel49.TabIndex = 122
        '
        'varMedicacionHCO
        '
        Me.varMedicacionHCO.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varMedicacionHCO.Location = New System.Drawing.Point(122, 3)
        Me.varMedicacionHCO.Name = "varMedicacionHCO"
        Me.varMedicacionHCO.Size = New System.Drawing.Size(266, 20)
        Me.varMedicacionHCO.TabIndex = 61
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(4, 6)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(112, 13)
        Me.Label44.TabIndex = 60
        Me.Label44.Text = "Medicacion que toma:"
        '
        'varTomoAlgoHCO
        '
        Me.varTomoAlgoHCO.AutoSize = True
        Me.varTomoAlgoHCO.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varTomoAlgoHCO.Location = New System.Drawing.Point(242, 6)
        Me.varTomoAlgoHCO.Name = "varTomoAlgoHCO"
        Me.varTomoAlgoHCO.Size = New System.Drawing.Size(157, 17)
        Me.varTomoAlgoHCO.TabIndex = 51
        Me.varTomoAlgoHCO.Text = "Tomo algun medicamento?:"
        Me.varTomoAlgoHCO.UseVisualStyleBackColor = True
        '
        'varConsultoAntes
        '
        Me.varConsultoAntes.AutoSize = True
        Me.varConsultoAntes.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varConsultoAntes.Location = New System.Drawing.Point(6, 6)
        Me.varConsultoAntes.Name = "varConsultoAntes"
        Me.varConsultoAntes.Size = New System.Drawing.Size(230, 17)
        Me.varConsultoAntes.TabIndex = 50
        Me.varConsultoAntes.Text = "Consulto antes con algun otro profesional?:"
        Me.varConsultoAntes.UseVisualStyleBackColor = True
        '
        'Panel48
        '
        Me.Panel48.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel48.Controls.Add(Me.varMotivoConsulta)
        Me.Panel48.Controls.Add(Me.Label43)
        Me.Panel48.Location = New System.Drawing.Point(6, 40)
        Me.Panel48.Name = "Panel48"
        Me.Panel48.Size = New System.Drawing.Size(716, 27)
        Me.Panel48.TabIndex = 135
        '
        'varMotivoConsulta
        '
        Me.varMotivoConsulta.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varMotivoConsulta.Location = New System.Drawing.Point(123, 3)
        Me.varMotivoConsulta.Name = "varMotivoConsulta"
        Me.varMotivoConsulta.Size = New System.Drawing.Size(590, 20)
        Me.varMotivoConsulta.TabIndex = 2
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(3, 6)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(114, 13)
        Me.Label43.TabIndex = 0
        Me.Label43.Text = "Motivo de su consulta:"
        '
        'Panel47
        '
        Me.Panel47.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel47.Controls.Add(Me.Label42)
        Me.Panel47.Location = New System.Drawing.Point(6, 6)
        Me.Panel47.Name = "Panel47"
        Me.Panel47.Size = New System.Drawing.Size(716, 30)
        Me.Panel47.TabIndex = 124
        '
        'Label42
        '
        Me.Label42.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.Location = New System.Drawing.Point(3, 5)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(240, 20)
        Me.Label42.TabIndex = 81
        Me.Label42.Text = "Historia Clinica Odontologica"
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.Panel79)
        Me.TabPage5.Controls.Add(Me.Panel78)
        Me.TabPage5.Controls.Add(Me.Panel77)
        Me.TabPage5.Controls.Add(Me.Panel76)
        Me.TabPage5.Controls.Add(Me.Panel75)
        Me.TabPage5.Controls.Add(Me.Panel74)
        Me.TabPage5.Controls.Add(Me.Panel73)
        Me.TabPage5.Controls.Add(Me.Panel71)
        Me.TabPage5.Controls.Add(Me.Panel69)
        Me.TabPage5.Controls.Add(Me.Panel72)
        Me.TabPage5.Controls.Add(Me.Panel67)
        Me.TabPage5.Controls.Add(Me.Panel65)
        Me.TabPage5.Controls.Add(Me.Panel63)
        Me.TabPage5.Controls.Add(Me.Panel62)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(728, 424)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "TabPage5"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'Panel79
        '
        Me.Panel79.Controls.Add(Me.Label75)
        Me.Panel79.Controls.Add(Me.EstadoHigieneBucalMalo)
        Me.Panel79.Controls.Add(Me.EstadoHigieneBucalDeficiente)
        Me.Panel79.Controls.Add(Me.EstadoHigieneBucalBueno)
        Me.Panel79.Controls.Add(Me.EstadoHigieneBucalMuyBueno)
        Me.Panel79.Location = New System.Drawing.Point(4, 393)
        Me.Panel79.Name = "Panel79"
        Me.Panel79.Size = New System.Drawing.Size(720, 27)
        Me.Panel79.TabIndex = 139
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Location = New System.Drawing.Point(3, 7)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(135, 13)
        Me.Label75.TabIndex = 64
        Me.Label75.Text = "Estado de la higiene bucal:"
        '
        'EstadoHigieneBucalMalo
        '
        Me.EstadoHigieneBucalMalo.AutoSize = True
        Me.EstadoHigieneBucalMalo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.EstadoHigieneBucalMalo.Location = New System.Drawing.Point(394, 6)
        Me.EstadoHigieneBucalMalo.Name = "EstadoHigieneBucalMalo"
        Me.EstadoHigieneBucalMalo.Size = New System.Drawing.Size(52, 17)
        Me.EstadoHigieneBucalMalo.TabIndex = 66
        Me.EstadoHigieneBucalMalo.Text = "Malo:"
        Me.EstadoHigieneBucalMalo.UseVisualStyleBackColor = True
        '
        'EstadoHigieneBucalDeficiente
        '
        Me.EstadoHigieneBucalDeficiente.AutoSize = True
        Me.EstadoHigieneBucalDeficiente.Cursor = System.Windows.Forms.Cursors.Hand
        Me.EstadoHigieneBucalDeficiente.Location = New System.Drawing.Point(311, 6)
        Me.EstadoHigieneBucalDeficiente.Name = "EstadoHigieneBucalDeficiente"
        Me.EstadoHigieneBucalDeficiente.Size = New System.Drawing.Size(77, 17)
        Me.EstadoHigieneBucalDeficiente.TabIndex = 65
        Me.EstadoHigieneBucalDeficiente.Text = "Deficiente:"
        Me.EstadoHigieneBucalDeficiente.UseVisualStyleBackColor = True
        '
        'EstadoHigieneBucalBueno
        '
        Me.EstadoHigieneBucalBueno.AutoSize = True
        Me.EstadoHigieneBucalBueno.Cursor = System.Windows.Forms.Cursors.Hand
        Me.EstadoHigieneBucalBueno.Location = New System.Drawing.Point(245, 6)
        Me.EstadoHigieneBucalBueno.Name = "EstadoHigieneBucalBueno"
        Me.EstadoHigieneBucalBueno.Size = New System.Drawing.Size(60, 17)
        Me.EstadoHigieneBucalBueno.TabIndex = 64
        Me.EstadoHigieneBucalBueno.Text = "Bueno:"
        Me.EstadoHigieneBucalBueno.UseVisualStyleBackColor = True
        '
        'EstadoHigieneBucalMuyBueno
        '
        Me.EstadoHigieneBucalMuyBueno.AutoSize = True
        Me.EstadoHigieneBucalMuyBueno.Cursor = System.Windows.Forms.Cursors.Hand
        Me.EstadoHigieneBucalMuyBueno.Location = New System.Drawing.Point(160, 6)
        Me.EstadoHigieneBucalMuyBueno.Name = "EstadoHigieneBucalMuyBueno"
        Me.EstadoHigieneBucalMuyBueno.Size = New System.Drawing.Size(82, 17)
        Me.EstadoHigieneBucalMuyBueno.TabIndex = 63
        Me.EstadoHigieneBucalMuyBueno.Text = "Muy bueno:"
        Me.EstadoHigieneBucalMuyBueno.UseVisualStyleBackColor = True
        '
        'Panel78
        '
        Me.Panel78.Controls.Add(Me.IndiceDePlaca)
        Me.Panel78.Controls.Add(Me.MomentosDeAzucarDiario)
        Me.Panel78.Controls.Add(Me.Label74)
        Me.Panel78.Controls.Add(Me.Label73)
        Me.Panel78.Location = New System.Drawing.Point(4, 363)
        Me.Panel78.Name = "Panel78"
        Me.Panel78.Size = New System.Drawing.Size(720, 27)
        Me.Panel78.TabIndex = 135
        '
        'IndiceDePlaca
        '
        Me.IndiceDePlaca.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.IndiceDePlaca.Location = New System.Drawing.Point(477, 4)
        Me.IndiceDePlaca.Name = "IndiceDePlaca"
        Me.IndiceDePlaca.Size = New System.Drawing.Size(240, 20)
        Me.IndiceDePlaca.TabIndex = 63
        '
        'MomentosDeAzucarDiario
        '
        Me.MomentosDeAzucarDiario.Location = New System.Drawing.Point(146, 4)
        Me.MomentosDeAzucarDiario.Name = "MomentosDeAzucarDiario"
        Me.MomentosDeAzucarDiario.Size = New System.Drawing.Size(236, 20)
        Me.MomentosDeAzucarDiario.TabIndex = 63
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.Location = New System.Drawing.Point(388, 7)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(83, 13)
        Me.Label74.TabIndex = 62
        Me.Label74.Text = "Indice de placa:"
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Location = New System.Drawing.Point(3, 7)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(137, 13)
        Me.Label73.TabIndex = 62
        Me.Label73.Text = "Momentos de azucar diario:"
        '
        'Panel77
        '
        Me.Panel77.Controls.Add(Me.varSePusoOtros)
        Me.Panel77.Controls.Add(Me.varSePusoCalor)
        Me.Panel77.Controls.Add(Me.Label72)
        Me.Panel77.Controls.Add(Me.varSePusoHielo)
        Me.Panel77.Controls.Add(Me.varCaraHinchada)
        Me.Panel77.Location = New System.Drawing.Point(4, 333)
        Me.Panel77.Name = "Panel77"
        Me.Panel77.Size = New System.Drawing.Size(720, 27)
        Me.Panel77.TabIndex = 138
        '
        'varSePusoOtros
        '
        Me.varSePusoOtros.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varSePusoOtros.Location = New System.Drawing.Point(403, 4)
        Me.varSePusoOtros.Name = "varSePusoOtros"
        Me.varSePusoOtros.Size = New System.Drawing.Size(314, 20)
        Me.varSePusoOtros.TabIndex = 65
        '
        'varSePusoCalor
        '
        Me.varSePusoCalor.AutoSize = True
        Me.varSePusoCalor.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varSePusoCalor.Location = New System.Drawing.Point(297, 6)
        Me.varSePusoCalor.Name = "varSePusoCalor"
        Me.varSePusoCalor.Size = New System.Drawing.Size(59, 17)
        Me.varSePusoCalor.TabIndex = 65
        Me.varSePusoCalor.Text = "Calor?:"
        Me.varSePusoCalor.UseVisualStyleBackColor = True
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.Location = New System.Drawing.Point(362, 7)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(35, 13)
        Me.Label72.TabIndex = 64
        Me.Label72.Text = "Otros:"
        '
        'varSePusoHielo
        '
        Me.varSePusoHielo.AutoSize = True
        Me.varSePusoHielo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varSePusoHielo.Location = New System.Drawing.Point(192, 6)
        Me.varSePusoHielo.Name = "varSePusoHielo"
        Me.varSePusoHielo.Size = New System.Drawing.Size(99, 17)
        Me.varSePusoHielo.TabIndex = 64
        Me.varSePusoHielo.Text = "Se puso hielo?:"
        Me.varSePusoHielo.UseVisualStyleBackColor = True
        '
        'varCaraHinchada
        '
        Me.varCaraHinchada.AutoSize = True
        Me.varCaraHinchada.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varCaraHinchada.Location = New System.Drawing.Point(6, 6)
        Me.varCaraHinchada.Name = "varCaraHinchada"
        Me.varCaraHinchada.Size = New System.Drawing.Size(163, 17)
        Me.varCaraHinchada.TabIndex = 63
        Me.varCaraHinchada.Text = "Ha tenido la cara hinchada?:"
        Me.varCaraHinchada.UseVisualStyleBackColor = True
        '
        'Panel76
        '
        Me.Panel76.Controls.Add(Me.varSienteAltosDientesAlMorder)
        Me.Panel76.Controls.Add(Me.Label71)
        Me.Panel76.Controls.Add(Me.varMovilidadEnDientes)
        Me.Panel76.Location = New System.Drawing.Point(4, 304)
        Me.Panel76.Name = "Panel76"
        Me.Panel76.Size = New System.Drawing.Size(720, 27)
        Me.Panel76.TabIndex = 128
        '
        'varSienteAltosDientesAlMorder
        '
        Me.varSienteAltosDientesAlMorder.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varSienteAltosDientesAlMorder.Location = New System.Drawing.Point(367, 4)
        Me.varSienteAltosDientesAlMorder.Name = "varSienteAltosDientesAlMorder"
        Me.varSienteAltosDientesAlMorder.Size = New System.Drawing.Size(350, 20)
        Me.varSienteAltosDientesAlMorder.TabIndex = 63
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.Location = New System.Drawing.Point(192, 7)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(169, 13)
        Me.Label71.TabIndex = 62
        Me.Label71.Text = "Al morder siente altos los dientes?:"
        '
        'varMovilidadEnDientes
        '
        Me.varMovilidadEnDientes.AutoSize = True
        Me.varMovilidadEnDientes.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varMovilidadEnDientes.Location = New System.Drawing.Point(6, 6)
        Me.varMovilidadEnDientes.Name = "varMovilidadEnDientes"
        Me.varMovilidadEnDientes.Size = New System.Drawing.Size(180, 17)
        Me.varMovilidadEnDientes.TabIndex = 50
        Me.varMovilidadEnDientes.Text = "Tiene movilidad en sus dientes?:"
        Me.varMovilidadEnDientes.UseVisualStyleBackColor = True
        '
        'Panel75
        '
        Me.Panel75.Controls.Add(Me.varDondeSalePusEnBoca)
        Me.Panel75.Controls.Add(Me.Label70)
        Me.Panel75.Controls.Add(Me.varSalePusEnBoca)
        Me.Panel75.Location = New System.Drawing.Point(4, 274)
        Me.Panel75.Name = "Panel75"
        Me.Panel75.Size = New System.Drawing.Size(720, 27)
        Me.Panel75.TabIndex = 127
        '
        'varDondeSalePusEnBoca
        '
        Me.varDondeSalePusEnBoca.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varDondeSalePusEnBoca.Location = New System.Drawing.Point(283, 4)
        Me.varDondeSalePusEnBoca.Name = "varDondeSalePusEnBoca"
        Me.varDondeSalePusEnBoca.Size = New System.Drawing.Size(434, 20)
        Me.varDondeSalePusEnBoca.TabIndex = 63
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Location = New System.Drawing.Point(214, 7)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(63, 13)
        Me.Label70.TabIndex = 62
        Me.Label70.Text = "De donde?:"
        '
        'varSalePusEnBoca
        '
        Me.varSalePusEnBoca.AutoSize = True
        Me.varSalePusEnBoca.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varSalePusEnBoca.Location = New System.Drawing.Point(6, 6)
        Me.varSalePusEnBoca.Name = "varSalePusEnBoca"
        Me.varSalePusEnBoca.Size = New System.Drawing.Size(202, 17)
        Me.varSalePusEnBoca.TabIndex = 50
        Me.varSalePusEnBoca.Text = "Sale pus de algun lugar de su boca?:"
        Me.varSalePusEnBoca.UseVisualStyleBackColor = True
        '
        'Panel74
        '
        Me.Panel74.Controls.Add(Me.varCuandoSangranEncias)
        Me.Panel74.Controls.Add(Me.Label67)
        Me.Panel74.Controls.Add(Me.varSangranEncias)
        Me.Panel74.Location = New System.Drawing.Point(4, 244)
        Me.Panel74.Name = "Panel74"
        Me.Panel74.Size = New System.Drawing.Size(720, 27)
        Me.Panel74.TabIndex = 126
        '
        'varCuandoSangranEncias
        '
        Me.varCuandoSangranEncias.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varCuandoSangranEncias.Location = New System.Drawing.Point(209, 4)
        Me.varCuandoSangranEncias.Name = "varCuandoSangranEncias"
        Me.varCuandoSangranEncias.Size = New System.Drawing.Size(508, 20)
        Me.varCuandoSangranEncias.TabIndex = 63
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Location = New System.Drawing.Point(150, 7)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(53, 13)
        Me.Label67.TabIndex = 62
        Me.Label67.Text = "Cuando?:"
        '
        'varSangranEncias
        '
        Me.varSangranEncias.AutoSize = True
        Me.varSangranEncias.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varSangranEncias.Location = New System.Drawing.Point(6, 6)
        Me.varSangranEncias.Name = "varSangranEncias"
        Me.varSangranEncias.Size = New System.Drawing.Size(138, 17)
        Me.varSangranEncias.TabIndex = 50
        Me.varSangranEncias.Text = "Le sangran las encias?:"
        Me.varSangranEncias.UseVisualStyleBackColor = True
        '
        'Panel73
        '
        Me.Panel73.Controls.Add(Me.varOtros)
        Me.Panel73.Controls.Add(Me.Label69)
        Me.Panel73.Location = New System.Drawing.Point(4, 214)
        Me.Panel73.Name = "Panel73"
        Me.Panel73.Size = New System.Drawing.Size(720, 27)
        Me.Panel73.TabIndex = 134
        '
        'varOtros
        '
        Me.varOtros.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varOtros.Location = New System.Drawing.Point(50, 4)
        Me.varOtros.Name = "varOtros"
        Me.varOtros.Size = New System.Drawing.Size(667, 20)
        Me.varOtros.TabIndex = 63
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Location = New System.Drawing.Point(3, 7)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(41, 13)
        Me.Label69.TabIndex = 62
        Me.Label69.Text = "Otros?:"
        '
        'Panel71
        '
        Me.Panel71.Controls.Add(Me.varAmpollas)
        Me.Panel71.Controls.Add(Me.varUlceraciones)
        Me.Panel71.Controls.Add(Me.varAbultamientoTejidos)
        Me.Panel71.Controls.Add(Me.varManchas)
        Me.Panel71.Location = New System.Drawing.Point(4, 185)
        Me.Panel71.Name = "Panel71"
        Me.Panel71.Size = New System.Drawing.Size(720, 27)
        Me.Panel71.TabIndex = 137
        '
        'varAmpollas
        '
        Me.varAmpollas.AutoSize = True
        Me.varAmpollas.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varAmpollas.Location = New System.Drawing.Point(368, 6)
        Me.varAmpollas.Name = "varAmpollas"
        Me.varAmpollas.Size = New System.Drawing.Size(77, 17)
        Me.varAmpollas.TabIndex = 66
        Me.varAmpollas.Text = "Ampollas?:"
        Me.varAmpollas.UseVisualStyleBackColor = True
        '
        'varUlceraciones
        '
        Me.varUlceraciones.AutoSize = True
        Me.varUlceraciones.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varUlceraciones.Location = New System.Drawing.Point(265, 6)
        Me.varUlceraciones.Name = "varUlceraciones"
        Me.varUlceraciones.Size = New System.Drawing.Size(97, 17)
        Me.varUlceraciones.TabIndex = 65
        Me.varUlceraciones.Text = "Ulceraciones?:"
        Me.varUlceraciones.UseVisualStyleBackColor = True
        '
        'varAbultamientoTejidos
        '
        Me.varAbultamientoTejidos.AutoSize = True
        Me.varAbultamientoTejidos.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varAbultamientoTejidos.Location = New System.Drawing.Point(99, 6)
        Me.varAbultamientoTejidos.Name = "varAbultamientoTejidos"
        Me.varAbultamientoTejidos.Size = New System.Drawing.Size(160, 17)
        Me.varAbultamientoTejidos.TabIndex = 64
        Me.varAbultamientoTejidos.Text = "Abultamiento de los tejidos?:"
        Me.varAbultamientoTejidos.UseVisualStyleBackColor = True
        '
        'varManchas
        '
        Me.varManchas.AutoSize = True
        Me.varManchas.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varManchas.Location = New System.Drawing.Point(6, 6)
        Me.varManchas.Name = "varManchas"
        Me.varManchas.Size = New System.Drawing.Size(79, 17)
        Me.varManchas.TabIndex = 63
        Me.varManchas.Text = "Manchas?:"
        Me.varManchas.UseVisualStyleBackColor = True
        '
        'Panel69
        '
        Me.Panel69.Controls.Add(Me.varRetromolar)
        Me.Panel69.Controls.Add(Me.varTrigono)
        Me.Panel69.Controls.Add(Me.Label66)
        Me.Panel69.Controls.Add(Me.Label65)
        Me.Panel69.Location = New System.Drawing.Point(4, 125)
        Me.Panel69.Name = "Panel69"
        Me.Panel69.Size = New System.Drawing.Size(720, 27)
        Me.Panel69.TabIndex = 132
        '
        'varRetromolar
        '
        Me.varRetromolar.Location = New System.Drawing.Point(433, 4)
        Me.varRetromolar.Name = "varRetromolar"
        Me.varRetromolar.Size = New System.Drawing.Size(281, 20)
        Me.varRetromolar.TabIndex = 63
        '
        'varTrigono
        '
        Me.varTrigono.Location = New System.Drawing.Point(61, 4)
        Me.varTrigono.Name = "varTrigono"
        Me.varTrigono.Size = New System.Drawing.Size(293, 20)
        Me.varTrigono.TabIndex = 63
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Location = New System.Drawing.Point(360, 7)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(67, 13)
        Me.Label66.TabIndex = 62
        Me.Label66.Text = "Retromolar?:"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Location = New System.Drawing.Point(3, 7)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(52, 13)
        Me.Label65.TabIndex = 62
        Me.Label65.Text = "Trigono?:"
        '
        'Panel72
        '
        Me.Panel72.Controls.Add(Me.Label68)
        Me.Panel72.Location = New System.Drawing.Point(4, 155)
        Me.Panel72.Name = "Panel72"
        Me.Panel72.Size = New System.Drawing.Size(720, 27)
        Me.Panel72.TabIndex = 136
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Location = New System.Drawing.Point(3, 7)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(150, 13)
        Me.Label68.TabIndex = 62
        Me.Label68.Text = "Que tipo de lesiones presenta:"
        '
        'Panel67
        '
        Me.Panel67.Controls.Add(Me.varRebordes)
        Me.Panel67.Controls.Add(Me.varCarrillos)
        Me.Panel67.Controls.Add(Me.Label64)
        Me.Panel67.Controls.Add(Me.Label63)
        Me.Panel67.Location = New System.Drawing.Point(4, 95)
        Me.Panel67.Name = "Panel67"
        Me.Panel67.Size = New System.Drawing.Size(720, 27)
        Me.Panel67.TabIndex = 133
        '
        'varRebordes
        '
        Me.varRebordes.Location = New System.Drawing.Point(428, 4)
        Me.varRebordes.Name = "varRebordes"
        Me.varRebordes.Size = New System.Drawing.Size(286, 20)
        Me.varRebordes.TabIndex = 63
        '
        'varCarrillos
        '
        Me.varCarrillos.Location = New System.Drawing.Point(61, 4)
        Me.varCarrillos.Name = "varCarrillos"
        Me.varCarrillos.Size = New System.Drawing.Size(293, 20)
        Me.varCarrillos.TabIndex = 63
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Location = New System.Drawing.Point(360, 7)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(62, 13)
        Me.Label64.TabIndex = 62
        Me.Label64.Text = "Rebordes?:"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Location = New System.Drawing.Point(3, 7)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(52, 13)
        Me.Label63.TabIndex = 62
        Me.Label63.Text = "Carrillos?:"
        '
        'Panel65
        '
        Me.Panel65.Controls.Add(Me.varPisoDeBoca)
        Me.Panel65.Controls.Add(Me.varPaladar)
        Me.Panel65.Controls.Add(Me.Label62)
        Me.Panel65.Controls.Add(Me.Label61)
        Me.Panel65.Location = New System.Drawing.Point(4, 65)
        Me.Panel65.Name = "Panel65"
        Me.Panel65.Size = New System.Drawing.Size(720, 27)
        Me.Panel65.TabIndex = 131
        '
        'varPisoDeBoca
        '
        Me.varPisoDeBoca.Location = New System.Drawing.Point(439, 4)
        Me.varPisoDeBoca.Name = "varPisoDeBoca"
        Me.varPisoDeBoca.Size = New System.Drawing.Size(275, 20)
        Me.varPisoDeBoca.TabIndex = 63
        '
        'varPaladar
        '
        Me.varPaladar.Location = New System.Drawing.Point(61, 4)
        Me.varPaladar.Name = "varPaladar"
        Me.varPaladar.Size = New System.Drawing.Size(293, 20)
        Me.varPaladar.TabIndex = 63
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(360, 7)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(78, 13)
        Me.Label62.TabIndex = 62
        Me.Label62.Text = "Piso de boca?:"
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Location = New System.Drawing.Point(3, 7)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(52, 13)
        Me.Label61.TabIndex = 62
        Me.Label61.Text = "Paladar?:"
        '
        'Panel63
        '
        Me.Panel63.Controls.Add(Me.varLengua)
        Me.Panel63.Controls.Add(Me.varLabios)
        Me.Panel63.Controls.Add(Me.Label60)
        Me.Panel63.Controls.Add(Me.Label59)
        Me.Panel63.Location = New System.Drawing.Point(4, 35)
        Me.Panel63.Name = "Panel63"
        Me.Panel63.Size = New System.Drawing.Size(720, 27)
        Me.Panel63.TabIndex = 130
        '
        'varLengua
        '
        Me.varLengua.Location = New System.Drawing.Point(418, 4)
        Me.varLengua.Name = "varLengua"
        Me.varLengua.Size = New System.Drawing.Size(296, 20)
        Me.varLengua.TabIndex = 63
        '
        'varLabios
        '
        Me.varLabios.Location = New System.Drawing.Point(56, 4)
        Me.varLabios.Name = "varLabios"
        Me.varLabios.Size = New System.Drawing.Size(298, 20)
        Me.varLabios.TabIndex = 63
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(360, 7)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(52, 13)
        Me.Label60.TabIndex = 62
        Me.Label60.Text = "Lengua?:"
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Location = New System.Drawing.Point(3, 7)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(47, 13)
        Me.Label59.TabIndex = 62
        Me.Label59.Text = "Labios?:"
        '
        'Panel62
        '
        Me.Panel62.Controls.Add(Me.Label58)
        Me.Panel62.Location = New System.Drawing.Point(4, 5)
        Me.Panel62.Name = "Panel62"
        Me.Panel62.Size = New System.Drawing.Size(720, 27)
        Me.Panel62.TabIndex = 129
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(3, 7)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(155, 13)
        Me.Label58.TabIndex = 62
        Me.Label58.Text = "Ha observado algo anormal en:"
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.Panel81)
        Me.TabPage6.Controls.Add(Me.Label76)
        Me.TabPage6.Controls.Add(Me.Panel80)
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(728, 424)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "TabPage6"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'Panel80
        '
        Me.Panel80.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel80.Controls.Add(Me.Panel26)
        Me.Panel80.Controls.Add(Me.Cuadraditos_82)
        Me.Panel80.Controls.Add(Me.Cuadraditos_86)
        Me.Panel80.Controls.Add(Me.Cuadraditos_81)
        Me.Panel80.Controls.Add(Me.Cuadraditos_48)
        Me.Panel80.Controls.Add(Me.Cuadraditos_62)
        Me.Panel80.Controls.Add(Me.Cuadraditos_85)
        Me.Panel80.Controls.Add(Me.Cuadraditos_61)
        Me.Panel80.Controls.Add(Me.Cuadraditos_46)
        Me.Panel80.Controls.Add(Me.Cuadraditos_66)
        Me.Panel80.Controls.Add(Me.Cuadraditos_47)
        Me.Panel80.Controls.Add(Me.Cuadraditos_65)
        Me.Panel80.Controls.Add(Me.Cuadraditos_84)
        Me.Panel80.Controls.Add(Me.Cuadraditos_28)
        Me.Panel80.Controls.Add(Me.Cuadraditos_45)
        Me.Panel80.Controls.Add(Me.Cuadraditos_27)
        Me.Panel80.Controls.Add(Me.Cuadraditos_44)
        Me.Panel80.Controls.Add(Me.Cuadraditos_26)
        Me.Panel80.Controls.Add(Me.Cuadraditos_83)
        Me.Panel80.Controls.Add(Me.Cuadraditos_25)
        Me.Panel80.Controls.Add(Me.Cuadraditos_42)
        Me.Panel80.Controls.Add(Me.Cuadraditos_64)
        Me.Panel80.Controls.Add(Me.Cuadraditos_43)
        Me.Panel80.Controls.Add(Me.Cuadraditos_63)
        Me.Panel80.Controls.Add(Me.Cuadraditos_41)
        Me.Panel80.Controls.Add(Me.Cuadraditos_24)
        Me.Panel80.Controls.Add(Me.Cuadraditos_22)
        Me.Panel80.Controls.Add(Me.Cuadraditos_23)
        Me.Panel80.Controls.Add(Me.Cuadraditos_21)
        Me.Panel80.Controls.Add(Me.Cuadraditos_75)
        Me.Panel80.Controls.Add(Me.Cuadraditos_73)
        Me.Panel80.Controls.Add(Me.Cuadraditos_76)
        Me.Panel80.Controls.Add(Me.Cuadraditos_71)
        Me.Panel80.Controls.Add(Me.Cuadraditos_55)
        Me.Panel80.Controls.Add(Me.Cuadraditos_74)
        Me.Panel80.Controls.Add(Me.Cuadraditos_56)
        Me.Panel80.Controls.Add(Me.Cuadraditos_31)
        Me.Panel80.Controls.Add(Me.Cuadraditos_53)
        Me.Panel80.Controls.Add(Me.Cuadraditos_72)
        Me.Panel80.Controls.Add(Me.Cuadraditos_54)
        Me.Panel80.Controls.Add(Me.Cuadraditos_33)
        Me.Panel80.Controls.Add(Me.Cuadraditos_51)
        Me.Panel80.Controls.Add(Me.Cuadraditos_32)
        Me.Panel80.Controls.Add(Me.Cuadraditos_52)
        Me.Panel80.Controls.Add(Me.Cuadraditos_35)
        Me.Panel80.Controls.Add(Me.Cuadraditos_11)
        Me.Panel80.Controls.Add(Me.Cuadraditos_34)
        Me.Panel80.Controls.Add(Me.Cuadraditos_12)
        Me.Panel80.Controls.Add(Me.Cuadraditos_37)
        Me.Panel80.Controls.Add(Me.Cuadraditos_13)
        Me.Panel80.Controls.Add(Me.Cuadraditos_36)
        Me.Panel80.Controls.Add(Me.Cuadraditos_14)
        Me.Panel80.Controls.Add(Me.Cuadraditos_38)
        Me.Panel80.Controls.Add(Me.Cuadraditos_15)
        Me.Panel80.Controls.Add(Me.Cuadraditos_17)
        Me.Panel80.Controls.Add(Me.Cuadraditos_16)
        Me.Panel80.Controls.Add(Me.Cuadraditos_18)
        Me.Panel80.Location = New System.Drawing.Point(4, 45)
        Me.Panel80.Name = "Panel80"
        Me.Panel80.Size = New System.Drawing.Size(720, 195)
        Me.Panel80.TabIndex = 129
        '
        'Panel26
        '
        Me.Panel26.BackColor = System.Drawing.Color.Black
        Me.Panel26.Location = New System.Drawing.Point(358, 10)
        Me.Panel26.Name = "Panel26"
        Me.Panel26.Size = New System.Drawing.Size(3, 178)
        Me.Panel26.TabIndex = 134
        '
        'Cuadraditos_82
        '
        Me.Cuadraditos_82.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_82.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_82.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_82.Location = New System.Drawing.Point(410, 148)
        Me.Cuadraditos_82.Name = "Cuadraditos_82"
        Me.Cuadraditos_82.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_82.TabIndex = 95
        '
        'Cuadraditos_86
        '
        Me.Cuadraditos_86.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_86.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_86.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_86.Location = New System.Drawing.Point(586, 148)
        Me.Cuadraditos_86.Name = "Cuadraditos_86"
        Me.Cuadraditos_86.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_86.TabIndex = 95
        '
        'Cuadraditos_81
        '
        Me.Cuadraditos_81.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_81.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_81.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_81.Location = New System.Drawing.Point(366, 148)
        Me.Cuadraditos_81.Name = "Cuadraditos_81"
        Me.Cuadraditos_81.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_81.TabIndex = 96
        '
        'Cuadraditos_48
        '
        Me.Cuadraditos_48.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_48.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_48.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_48.Location = New System.Drawing.Point(674, 56)
        Me.Cuadraditos_48.Name = "Cuadraditos_48"
        Me.Cuadraditos_48.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_48.TabIndex = 95
        '
        'Cuadraditos_62
        '
        Me.Cuadraditos_62.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_62.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_62.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_62.Location = New System.Drawing.Point(410, 102)
        Me.Cuadraditos_62.Name = "Cuadraditos_62"
        Me.Cuadraditos_62.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_62.TabIndex = 97
        '
        'Cuadraditos_85
        '
        Me.Cuadraditos_85.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_85.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_85.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_85.Location = New System.Drawing.Point(542, 148)
        Me.Cuadraditos_85.Name = "Cuadraditos_85"
        Me.Cuadraditos_85.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_85.TabIndex = 96
        '
        'Cuadraditos_61
        '
        Me.Cuadraditos_61.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_61.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_61.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_61.Location = New System.Drawing.Point(366, 102)
        Me.Cuadraditos_61.Name = "Cuadraditos_61"
        Me.Cuadraditos_61.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_61.TabIndex = 98
        '
        'Cuadraditos_46
        '
        Me.Cuadraditos_46.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_46.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_46.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_46.Location = New System.Drawing.Point(586, 56)
        Me.Cuadraditos_46.Name = "Cuadraditos_46"
        Me.Cuadraditos_46.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_46.TabIndex = 95
        '
        'Cuadraditos_66
        '
        Me.Cuadraditos_66.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_66.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_66.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_66.Location = New System.Drawing.Point(586, 102)
        Me.Cuadraditos_66.Name = "Cuadraditos_66"
        Me.Cuadraditos_66.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_66.TabIndex = 97
        '
        'Cuadraditos_47
        '
        Me.Cuadraditos_47.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_47.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_47.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_47.Location = New System.Drawing.Point(630, 56)
        Me.Cuadraditos_47.Name = "Cuadraditos_47"
        Me.Cuadraditos_47.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_47.TabIndex = 96
        '
        'Cuadraditos_65
        '
        Me.Cuadraditos_65.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_65.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_65.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_65.Location = New System.Drawing.Point(542, 102)
        Me.Cuadraditos_65.Name = "Cuadraditos_65"
        Me.Cuadraditos_65.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_65.TabIndex = 98
        '
        'Cuadraditos_84
        '
        Me.Cuadraditos_84.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_84.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_84.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_84.Location = New System.Drawing.Point(498, 148)
        Me.Cuadraditos_84.Name = "Cuadraditos_84"
        Me.Cuadraditos_84.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_84.TabIndex = 95
        '
        'Cuadraditos_28
        '
        Me.Cuadraditos_28.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_28.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_28.Location = New System.Drawing.Point(674, 10)
        Me.Cuadraditos_28.Name = "Cuadraditos_28"
        Me.Cuadraditos_28.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_28.TabIndex = 97
        '
        'Cuadraditos_45
        '
        Me.Cuadraditos_45.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_45.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_45.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_45.Location = New System.Drawing.Point(542, 56)
        Me.Cuadraditos_45.Name = "Cuadraditos_45"
        Me.Cuadraditos_45.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_45.TabIndex = 96
        '
        'Cuadraditos_27
        '
        Me.Cuadraditos_27.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_27.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_27.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_27.Location = New System.Drawing.Point(630, 10)
        Me.Cuadraditos_27.Name = "Cuadraditos_27"
        Me.Cuadraditos_27.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_27.TabIndex = 98
        '
        'Cuadraditos_44
        '
        Me.Cuadraditos_44.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_44.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_44.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_44.Location = New System.Drawing.Point(498, 56)
        Me.Cuadraditos_44.Name = "Cuadraditos_44"
        Me.Cuadraditos_44.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_44.TabIndex = 95
        '
        'Cuadraditos_26
        '
        Me.Cuadraditos_26.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_26.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_26.Location = New System.Drawing.Point(586, 10)
        Me.Cuadraditos_26.Name = "Cuadraditos_26"
        Me.Cuadraditos_26.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_26.TabIndex = 97
        '
        'Cuadraditos_83
        '
        Me.Cuadraditos_83.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_83.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_83.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_83.Location = New System.Drawing.Point(454, 148)
        Me.Cuadraditos_83.Name = "Cuadraditos_83"
        Me.Cuadraditos_83.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_83.TabIndex = 96
        '
        'Cuadraditos_25
        '
        Me.Cuadraditos_25.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_25.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_25.Location = New System.Drawing.Point(542, 10)
        Me.Cuadraditos_25.Name = "Cuadraditos_25"
        Me.Cuadraditos_25.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_25.TabIndex = 98
        '
        'Cuadraditos_42
        '
        Me.Cuadraditos_42.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_42.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_42.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_42.Location = New System.Drawing.Point(410, 56)
        Me.Cuadraditos_42.Name = "Cuadraditos_42"
        Me.Cuadraditos_42.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_42.TabIndex = 107
        '
        'Cuadraditos_64
        '
        Me.Cuadraditos_64.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_64.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_64.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_64.Location = New System.Drawing.Point(498, 102)
        Me.Cuadraditos_64.Name = "Cuadraditos_64"
        Me.Cuadraditos_64.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_64.TabIndex = 97
        '
        'Cuadraditos_43
        '
        Me.Cuadraditos_43.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_43.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_43.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_43.Location = New System.Drawing.Point(454, 56)
        Me.Cuadraditos_43.Name = "Cuadraditos_43"
        Me.Cuadraditos_43.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_43.TabIndex = 96
        '
        'Cuadraditos_63
        '
        Me.Cuadraditos_63.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_63.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_63.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_63.Location = New System.Drawing.Point(454, 102)
        Me.Cuadraditos_63.Name = "Cuadraditos_63"
        Me.Cuadraditos_63.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_63.TabIndex = 98
        '
        'Cuadraditos_41
        '
        Me.Cuadraditos_41.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_41.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_41.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_41.Location = New System.Drawing.Point(366, 56)
        Me.Cuadraditos_41.Name = "Cuadraditos_41"
        Me.Cuadraditos_41.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_41.TabIndex = 108
        '
        'Cuadraditos_24
        '
        Me.Cuadraditos_24.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_24.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_24.Location = New System.Drawing.Point(498, 10)
        Me.Cuadraditos_24.Name = "Cuadraditos_24"
        Me.Cuadraditos_24.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_24.TabIndex = 97
        '
        'Cuadraditos_22
        '
        Me.Cuadraditos_22.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_22.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_22.Location = New System.Drawing.Point(410, 10)
        Me.Cuadraditos_22.Name = "Cuadraditos_22"
        Me.Cuadraditos_22.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_22.TabIndex = 109
        '
        'Cuadraditos_23
        '
        Me.Cuadraditos_23.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_23.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_23.Location = New System.Drawing.Point(454, 10)
        Me.Cuadraditos_23.Name = "Cuadraditos_23"
        Me.Cuadraditos_23.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_23.TabIndex = 98
        '
        'Cuadraditos_21
        '
        Me.Cuadraditos_21.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Cuadraditos_21.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_21.Location = New System.Drawing.Point(366, 10)
        Me.Cuadraditos_21.Name = "Cuadraditos_21"
        Me.Cuadraditos_21.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_21.TabIndex = 110
        '
        'Cuadraditos_75
        '
        Me.Cuadraditos_75.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_75.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_75.Location = New System.Drawing.Point(137, 148)
        Me.Cuadraditos_75.Name = "Cuadraditos_75"
        Me.Cuadraditos_75.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_75.TabIndex = 95
        '
        'Cuadraditos_73
        '
        Me.Cuadraditos_73.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_73.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_73.Location = New System.Drawing.Point(225, 148)
        Me.Cuadraditos_73.Name = "Cuadraditos_73"
        Me.Cuadraditos_73.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_73.TabIndex = 95
        '
        'Cuadraditos_76
        '
        Me.Cuadraditos_76.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_76.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_76.Location = New System.Drawing.Point(93, 148)
        Me.Cuadraditos_76.Name = "Cuadraditos_76"
        Me.Cuadraditos_76.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_76.TabIndex = 96
        '
        'Cuadraditos_71
        '
        Me.Cuadraditos_71.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_71.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_71.Location = New System.Drawing.Point(313, 148)
        Me.Cuadraditos_71.Name = "Cuadraditos_71"
        Me.Cuadraditos_71.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_71.TabIndex = 95
        '
        'Cuadraditos_55
        '
        Me.Cuadraditos_55.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_55.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_55.Location = New System.Drawing.Point(137, 102)
        Me.Cuadraditos_55.Name = "Cuadraditos_55"
        Me.Cuadraditos_55.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_55.TabIndex = 97
        '
        'Cuadraditos_74
        '
        Me.Cuadraditos_74.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_74.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_74.Location = New System.Drawing.Point(181, 148)
        Me.Cuadraditos_74.Name = "Cuadraditos_74"
        Me.Cuadraditos_74.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_74.TabIndex = 96
        '
        'Cuadraditos_56
        '
        Me.Cuadraditos_56.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_56.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_56.Location = New System.Drawing.Point(93, 102)
        Me.Cuadraditos_56.Name = "Cuadraditos_56"
        Me.Cuadraditos_56.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_56.TabIndex = 98
        '
        'Cuadraditos_31
        '
        Me.Cuadraditos_31.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_31.Location = New System.Drawing.Point(313, 56)
        Me.Cuadraditos_31.Name = "Cuadraditos_31"
        Me.Cuadraditos_31.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_31.TabIndex = 95
        '
        'Cuadraditos_53
        '
        Me.Cuadraditos_53.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_53.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_53.Location = New System.Drawing.Point(225, 102)
        Me.Cuadraditos_53.Name = "Cuadraditos_53"
        Me.Cuadraditos_53.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_53.TabIndex = 97
        '
        'Cuadraditos_72
        '
        Me.Cuadraditos_72.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_72.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_72.Location = New System.Drawing.Point(269, 148)
        Me.Cuadraditos_72.Name = "Cuadraditos_72"
        Me.Cuadraditos_72.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_72.TabIndex = 96
        '
        'Cuadraditos_54
        '
        Me.Cuadraditos_54.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_54.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_54.Location = New System.Drawing.Point(181, 102)
        Me.Cuadraditos_54.Name = "Cuadraditos_54"
        Me.Cuadraditos_54.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_54.TabIndex = 98
        '
        'Cuadraditos_33
        '
        Me.Cuadraditos_33.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_33.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_33.Location = New System.Drawing.Point(225, 56)
        Me.Cuadraditos_33.Name = "Cuadraditos_33"
        Me.Cuadraditos_33.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_33.TabIndex = 95
        '
        'Cuadraditos_51
        '
        Me.Cuadraditos_51.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_51.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_51.Location = New System.Drawing.Point(313, 102)
        Me.Cuadraditos_51.Name = "Cuadraditos_51"
        Me.Cuadraditos_51.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_51.TabIndex = 97
        '
        'Cuadraditos_32
        '
        Me.Cuadraditos_32.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_32.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_32.Location = New System.Drawing.Point(269, 56)
        Me.Cuadraditos_32.Name = "Cuadraditos_32"
        Me.Cuadraditos_32.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_32.TabIndex = 96
        '
        'Cuadraditos_52
        '
        Me.Cuadraditos_52.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_52.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_52.Location = New System.Drawing.Point(269, 102)
        Me.Cuadraditos_52.Name = "Cuadraditos_52"
        Me.Cuadraditos_52.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_52.TabIndex = 98
        '
        'Cuadraditos_35
        '
        Me.Cuadraditos_35.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_35.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_35.Location = New System.Drawing.Point(137, 56)
        Me.Cuadraditos_35.Name = "Cuadraditos_35"
        Me.Cuadraditos_35.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_35.TabIndex = 95
        '
        'Cuadraditos_11
        '
        Me.Cuadraditos_11.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_11.Location = New System.Drawing.Point(313, 10)
        Me.Cuadraditos_11.Name = "Cuadraditos_11"
        Me.Cuadraditos_11.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_11.TabIndex = 97
        '
        'Cuadraditos_34
        '
        Me.Cuadraditos_34.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_34.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_34.Location = New System.Drawing.Point(181, 56)
        Me.Cuadraditos_34.Name = "Cuadraditos_34"
        Me.Cuadraditos_34.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_34.TabIndex = 96
        '
        'Cuadraditos_12
        '
        Me.Cuadraditos_12.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_12.Location = New System.Drawing.Point(269, 10)
        Me.Cuadraditos_12.Name = "Cuadraditos_12"
        Me.Cuadraditos_12.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_12.TabIndex = 98
        '
        'Cuadraditos_37
        '
        Me.Cuadraditos_37.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_37.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_37.Location = New System.Drawing.Point(49, 56)
        Me.Cuadraditos_37.Name = "Cuadraditos_37"
        Me.Cuadraditos_37.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_37.TabIndex = 103
        '
        'Cuadraditos_13
        '
        Me.Cuadraditos_13.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_13.Location = New System.Drawing.Point(225, 10)
        Me.Cuadraditos_13.Name = "Cuadraditos_13"
        Me.Cuadraditos_13.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_13.TabIndex = 97
        '
        'Cuadraditos_36
        '
        Me.Cuadraditos_36.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_36.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_36.Location = New System.Drawing.Point(93, 56)
        Me.Cuadraditos_36.Name = "Cuadraditos_36"
        Me.Cuadraditos_36.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_36.TabIndex = 96
        '
        'Cuadraditos_14
        '
        Me.Cuadraditos_14.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_14.Location = New System.Drawing.Point(181, 10)
        Me.Cuadraditos_14.Name = "Cuadraditos_14"
        Me.Cuadraditos_14.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_14.TabIndex = 98
        '
        'Cuadraditos_38
        '
        Me.Cuadraditos_38.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_38.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_38.Location = New System.Drawing.Point(5, 56)
        Me.Cuadraditos_38.Name = "Cuadraditos_38"
        Me.Cuadraditos_38.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_38.TabIndex = 104
        '
        'Cuadraditos_15
        '
        Me.Cuadraditos_15.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_15.Location = New System.Drawing.Point(137, 10)
        Me.Cuadraditos_15.Name = "Cuadraditos_15"
        Me.Cuadraditos_15.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_15.TabIndex = 97
        '
        'Cuadraditos_17
        '
        Me.Cuadraditos_17.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_17.Location = New System.Drawing.Point(49, 10)
        Me.Cuadraditos_17.Name = "Cuadraditos_17"
        Me.Cuadraditos_17.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_17.TabIndex = 105
        '
        'Cuadraditos_16
        '
        Me.Cuadraditos_16.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_16.Location = New System.Drawing.Point(93, 10)
        Me.Cuadraditos_16.Name = "Cuadraditos_16"
        Me.Cuadraditos_16.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_16.TabIndex = 98
        '
        'Cuadraditos_18
        '
        Me.Cuadraditos_18.BackColor = System.Drawing.Color.Black
        Me.Cuadraditos_18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cuadraditos_18.Location = New System.Drawing.Point(5, 10)
        Me.Cuadraditos_18.Name = "Cuadraditos_18"
        Me.Cuadraditos_18.Size = New System.Drawing.Size(40, 40)
        Me.Cuadraditos_18.TabIndex = 106
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label76.Location = New System.Drawing.Point(305, 12)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(118, 20)
        Me.Label76.TabIndex = 130
        Me.Label76.Text = "Odontograma"
        Me.Label76.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TabPage7
        '
        Me.TabPage7.Controls.Add(Me.Label89)
        Me.TabPage7.Controls.Add(Me.Panel85)
        Me.TabPage7.Controls.Add(Me.Panel84)
        Me.TabPage7.Controls.Add(Me.Panel83)
        Me.TabPage7.Controls.Add(Me.Panel82)
        Me.TabPage7.Controls.Add(Me.Label85)
        Me.TabPage7.Location = New System.Drawing.Point(4, 22)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage7.Size = New System.Drawing.Size(728, 424)
        Me.TabPage7.TabIndex = 6
        Me.TabPage7.Text = "TabPage7"
        Me.TabPage7.UseVisualStyleBackColor = True
        '
        'Panel85
        '
        Me.Panel85.Controls.Add(Me.Label88)
        Me.Panel85.Controls.Add(Me.varObservaciones)
        Me.Panel85.Location = New System.Drawing.Point(8, 259)
        Me.Panel85.Name = "Panel85"
        Me.Panel85.Size = New System.Drawing.Size(710, 100)
        Me.Panel85.TabIndex = 137
        '
        'Label88
        '
        Me.Label88.AutoSize = True
        Me.Label88.Location = New System.Drawing.Point(9, 9)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(81, 13)
        Me.Label88.TabIndex = 1
        Me.Label88.Text = "Observaciones:"
        '
        'varObservaciones
        '
        Me.varObservaciones.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varObservaciones.Location = New System.Drawing.Point(9, 25)
        Me.varObservaciones.Multiline = True
        Me.varObservaciones.Name = "varObservaciones"
        Me.varObservaciones.Size = New System.Drawing.Size(690, 65)
        Me.varObservaciones.TabIndex = 0
        '
        'Panel84
        '
        Me.Panel84.Controls.Add(Me.varCuandoPlanTratamiento)
        Me.Panel84.Controls.Add(Me.Label87)
        Me.Panel84.Controls.Add(Me.varPlanTratamiento)
        Me.Panel84.Location = New System.Drawing.Point(8, 153)
        Me.Panel84.Name = "Panel84"
        Me.Panel84.Size = New System.Drawing.Size(710, 100)
        Me.Panel84.TabIndex = 136
        '
        'varCuandoPlanTratamiento
        '
        Me.varCuandoPlanTratamiento.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varCuandoPlanTratamiento.Location = New System.Drawing.Point(499, 3)
        Me.varCuandoPlanTratamiento.Name = "varCuandoPlanTratamiento"
        Me.varCuandoPlanTratamiento.Size = New System.Drawing.Size(200, 20)
        Me.varCuandoPlanTratamiento.TabIndex = 2
        '
        'Label87
        '
        Me.Label87.AutoSize = True
        Me.Label87.Location = New System.Drawing.Point(9, 9)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(101, 13)
        Me.Label87.TabIndex = 1
        Me.Label87.Text = "Plan de tratamiento:"
        '
        'varPlanTratamiento
        '
        Me.varPlanTratamiento.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varPlanTratamiento.Location = New System.Drawing.Point(9, 25)
        Me.varPlanTratamiento.Multiline = True
        Me.varPlanTratamiento.Name = "varPlanTratamiento"
        Me.varPlanTratamiento.Size = New System.Drawing.Size(690, 65)
        Me.varPlanTratamiento.TabIndex = 0
        '
        'Panel83
        '
        Me.Panel83.Controls.Add(Me.Label86)
        Me.Panel83.Controls.Add(Me.varDiagnosticoPresuntivo)
        Me.Panel83.Location = New System.Drawing.Point(8, 47)
        Me.Panel83.Name = "Panel83"
        Me.Panel83.Size = New System.Drawing.Size(710, 100)
        Me.Panel83.TabIndex = 135
        '
        'Label86
        '
        Me.Label86.AutoSize = True
        Me.Label86.Location = New System.Drawing.Point(9, 9)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(118, 13)
        Me.Label86.TabIndex = 1
        Me.Label86.Text = "Diagnostico presuntivo:"
        '
        'varDiagnosticoPresuntivo
        '
        Me.varDiagnosticoPresuntivo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.varDiagnosticoPresuntivo.Location = New System.Drawing.Point(9, 25)
        Me.varDiagnosticoPresuntivo.Multiline = True
        Me.varDiagnosticoPresuntivo.Name = "varDiagnosticoPresuntivo"
        Me.varDiagnosticoPresuntivo.Size = New System.Drawing.Size(690, 65)
        Me.varDiagnosticoPresuntivo.TabIndex = 0
        '
        'Panel82
        '
        Me.Panel82.Controls.Add(Me.varEnfermedadPeriodontal)
        Me.Panel82.Controls.Add(Me.varSarro)
        Me.Panel82.Location = New System.Drawing.Point(385, 12)
        Me.Panel82.Name = "Panel82"
        Me.Panel82.Size = New System.Drawing.Size(292, 27)
        Me.Panel82.TabIndex = 134
        '
        'varEnfermedadPeriodontal
        '
        Me.varEnfermedadPeriodontal.AutoSize = True
        Me.varEnfermedadPeriodontal.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varEnfermedadPeriodontal.Location = New System.Drawing.Point(135, 6)
        Me.varEnfermedadPeriodontal.Name = "varEnfermedadPeriodontal"
        Me.varEnfermedadPeriodontal.Size = New System.Drawing.Size(147, 17)
        Me.varEnfermedadPeriodontal.TabIndex = 65
        Me.varEnfermedadPeriodontal.Text = "Enfermedad periodontal?:"
        Me.varEnfermedadPeriodontal.UseVisualStyleBackColor = True
        '
        'varSarro
        '
        Me.varSarro.AutoSize = True
        Me.varSarro.Cursor = System.Windows.Forms.Cursors.Hand
        Me.varSarro.Location = New System.Drawing.Point(6, 6)
        Me.varSarro.Name = "varSarro"
        Me.varSarro.Size = New System.Drawing.Size(123, 17)
        Me.varSarro.TabIndex = 63
        Me.varSarro.Text = "Presencia de sarro?:"
        Me.varSarro.UseVisualStyleBackColor = True
        '
        'Label85
        '
        Me.Label85.AutoSize = True
        Me.Label85.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label85.Location = New System.Drawing.Point(39, 12)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(184, 20)
        Me.Label85.TabIndex = 133
        Me.Label85.Text = "Estado bucal general:"
        '
        'Label89
        '
        Me.Label89.Location = New System.Drawing.Point(17, 370)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(538, 43)
        Me.Label89.TabIndex = 138
        Me.Label89.Text = resources.GetString("Label89.Text")
        Me.Label89.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel81
        '
        Me.Panel81.Controls.Add(Me.varCantidadDientes)
        Me.Panel81.Controls.Add(Me.Label84)
        Me.Panel81.Controls.Add(Me.Label83)
        Me.Panel81.Controls.Add(Me.Label82)
        Me.Panel81.Controls.Add(Me.Label81)
        Me.Panel81.Controls.Add(Me.Label80)
        Me.Panel81.Controls.Add(Me.Label79)
        Me.Panel81.Controls.Add(Me.Label78)
        Me.Panel81.Controls.Add(Me.Label77)
        Me.Panel81.Location = New System.Drawing.Point(9, 250)
        Me.Panel81.Name = "Panel81"
        Me.Panel81.Size = New System.Drawing.Size(715, 158)
        Me.Panel81.TabIndex = 131
        '
        'varCantidadDientes
        '
        Me.varCantidadDientes.Location = New System.Drawing.Point(647, 121)
        Me.varCantidadDientes.Name = "varCantidadDientes"
        Me.varCantidadDientes.Size = New System.Drawing.Size(53, 20)
        Me.varCantidadDientes.TabIndex = 82
        '
        'Label84
        '
        Me.Label84.AutoSize = True
        Me.Label84.Location = New System.Drawing.Point(488, 124)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(154, 13)
        Me.Label84.TabIndex = 7
        Me.Label84.Text = "Cantidad de dientes existentes:"
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.Location = New System.Drawing.Point(245, 92)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(60, 13)
        Me.Label83.TabIndex = 6
        Me.Label83.Text = "CORONAS"
        '
        'Label82
        '
        Me.Label82.AutoSize = True
        Me.Label82.Location = New System.Drawing.Point(245, 64)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(126, 13)
        Me.Label82.TabIndex = 5
        Me.Label82.Text = "PROTESIS REMOVIBLE"
        '
        'Label81
        '
        Me.Label81.AutoSize = True
        Me.Label81.Location = New System.Drawing.Point(245, 40)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(88, 13)
        Me.Label81.TabIndex = 4
        Me.Label81.Text = "PROTESIS FIJA."
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.Location = New System.Drawing.Point(245, 14)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(179, 13)
        Me.Label80.TabIndex = 3
        Me.Label80.Text = "DIENTE AUSENTE O A EXTRAER."
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.Location = New System.Drawing.Point(20, 73)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(197, 13)
        Me.Label79.TabIndex = 2
        Me.Label79.Text = "COLOR AZUL: Prestaciones requeridas."
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Location = New System.Drawing.Point(20, 49)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(196, 13)
        Me.Label78.TabIndex = 1
        Me.Label78.Text = "COLOR ROJO: Prestaciones existentes."
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label77.Location = New System.Drawing.Point(20, 14)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(111, 20)
        Me.Label77.TabIndex = 0
        Me.Label77.Text = "Referencias:"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.ClientSize = New System.Drawing.Size(734, 477)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        TabPage1.ResumeLayout(False)
        Me.Panel18.ResumeLayout(False)
        Me.Panel14.ResumeLayout(False)
        Me.Panel14.PerformLayout()
        Me.Panel90.ResumeLayout(False)
        Me.Panel90.PerformLayout()
        Me.Panel91.ResumeLayout(False)
        Me.Panel91.PerformLayout()
        Me.Panel50.ResumeLayout(False)
        Me.Panel50.PerformLayout()
        Me.Panel86.ResumeLayout(False)
        Me.Panel86.PerformLayout()
        Me.Panel87.ResumeLayout(False)
        Me.Panel87.PerformLayout()
        Me.Panel88.ResumeLayout(False)
        Me.Panel88.PerformLayout()
        Me.Panel89.ResumeLayout(False)
        Me.Panel89.PerformLayout()
        Me.Panel17.ResumeLayout(False)
        Me.Panel17.PerformLayout()
        Me.Panel16.ResumeLayout(False)
        Me.Panel16.PerformLayout()
        Me.Panel15.ResumeLayout(False)
        Me.Panel15.PerformLayout()
        Me.Panel13.ResumeLayout(False)
        Me.Panel13.PerformLayout()
        Me.Panel12.ResumeLayout(False)
        Me.Panel12.PerformLayout()
        Me.Panel11.ResumeLayout(False)
        Me.Panel11.PerformLayout()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.Panel33.ResumeLayout(False)
        Me.Panel33.PerformLayout()
        Me.Panel19.ResumeLayout(False)
        Me.Panel19.PerformLayout()
        Me.Panel29.ResumeLayout(False)
        Me.Panel29.PerformLayout()
        Me.Panel28.ResumeLayout(False)
        Me.Panel28.PerformLayout()
        Me.Panel27.ResumeLayout(False)
        Me.Panel27.PerformLayout()
        Me.Panel25.ResumeLayout(False)
        Me.Panel25.PerformLayout()
        Me.Panel24.ResumeLayout(False)
        Me.Panel24.PerformLayout()
        Me.Panel23.ResumeLayout(False)
        Me.Panel23.PerformLayout()
        Me.Panel22.ResumeLayout(False)
        Me.Panel22.PerformLayout()
        Me.Panel21.ResumeLayout(False)
        Me.Panel21.PerformLayout()
        Me.Panel20.ResumeLayout(False)
        Me.Panel20.PerformLayout()
        Me.Panel32.ResumeLayout(False)
        Me.Panel32.PerformLayout()
        Me.Panel46.ResumeLayout(False)
        Me.Panel46.PerformLayout()
        Me.Panel30.ResumeLayout(False)
        Me.Panel30.PerformLayout()
        Me.Panel38.ResumeLayout(False)
        Me.Panel38.PerformLayout()
        Me.Panel45.ResumeLayout(False)
        Me.Panel45.PerformLayout()
        Me.Panel37.ResumeLayout(False)
        Me.Panel37.PerformLayout()
        Me.Panel39.ResumeLayout(False)
        Me.Panel39.PerformLayout()
        Me.Panel44.ResumeLayout(False)
        Me.Panel44.PerformLayout()
        Me.Panel36.ResumeLayout(False)
        Me.Panel36.PerformLayout()
        Me.Panel31.ResumeLayout(False)
        Me.Panel31.PerformLayout()
        Me.Panel40.ResumeLayout(False)
        Me.Panel40.PerformLayout()
        Me.Panel43.ResumeLayout(False)
        Me.Panel43.PerformLayout()
        Me.Panel35.ResumeLayout(False)
        Me.Panel35.PerformLayout()
        Me.Panel34.ResumeLayout(False)
        Me.Panel34.PerformLayout()
        Me.Panel41.ResumeLayout(False)
        Me.Panel41.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.Panel52.ResumeLayout(False)
        Me.Panel52.PerformLayout()
        Me.IdDuracionDolor.ResumeLayout(False)
        Me.IdDuracionDolor.PerformLayout()
        Me.IdIntensidadDolor.ResumeLayout(False)
        Me.IdIntensidadDolor.PerformLayout()
        Me.Panel61.ResumeLayout(False)
        Me.Panel61.PerformLayout()
        Me.Panel60.ResumeLayout(False)
        Me.Panel60.PerformLayout()
        Me.Panel59.ResumeLayout(False)
        Me.Panel59.PerformLayout()
        Me.Panel58.ResumeLayout(False)
        Me.Panel58.PerformLayout()
        Me.Panel57.ResumeLayout(False)
        Me.Panel57.PerformLayout()
        Me.Panel56.ResumeLayout(False)
        Me.Panel56.PerformLayout()
        Me.Panel55.ResumeLayout(False)
        Me.Panel55.PerformLayout()
        Me.Panel54.ResumeLayout(False)
        Me.Panel54.PerformLayout()
        Me.Panel53.ResumeLayout(False)
        Me.Panel53.PerformLayout()
        Me.Panel51.ResumeLayout(False)
        Me.Panel51.PerformLayout()
        Me.Panel49.ResumeLayout(False)
        Me.Panel49.PerformLayout()
        Me.Panel48.ResumeLayout(False)
        Me.Panel48.PerformLayout()
        Me.Panel47.ResumeLayout(False)
        Me.Panel47.PerformLayout()
        Me.TabPage5.ResumeLayout(False)
        Me.Panel79.ResumeLayout(False)
        Me.Panel79.PerformLayout()
        Me.Panel78.ResumeLayout(False)
        Me.Panel78.PerformLayout()
        Me.Panel77.ResumeLayout(False)
        Me.Panel77.PerformLayout()
        Me.Panel76.ResumeLayout(False)
        Me.Panel76.PerformLayout()
        Me.Panel75.ResumeLayout(False)
        Me.Panel75.PerformLayout()
        Me.Panel74.ResumeLayout(False)
        Me.Panel74.PerformLayout()
        Me.Panel73.ResumeLayout(False)
        Me.Panel73.PerformLayout()
        Me.Panel71.ResumeLayout(False)
        Me.Panel71.PerformLayout()
        Me.Panel69.ResumeLayout(False)
        Me.Panel69.PerformLayout()
        Me.Panel72.ResumeLayout(False)
        Me.Panel72.PerformLayout()
        Me.Panel67.ResumeLayout(False)
        Me.Panel67.PerformLayout()
        Me.Panel65.ResumeLayout(False)
        Me.Panel65.PerformLayout()
        Me.Panel63.ResumeLayout(False)
        Me.Panel63.PerformLayout()
        Me.Panel62.ResumeLayout(False)
        Me.Panel62.PerformLayout()
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage6.PerformLayout()
        Me.Panel80.ResumeLayout(False)
        Me.TabPage7.ResumeLayout(False)
        Me.TabPage7.PerformLayout()
        Me.Panel85.ResumeLayout(False)
        Me.Panel85.PerformLayout()
        Me.Panel84.ResumeLayout(False)
        Me.Panel84.PerformLayout()
        Me.Panel83.ResumeLayout(False)
        Me.Panel83.PerformLayout()
        Me.Panel82.ResumeLayout(False)
        Me.Panel82.PerformLayout()
        Me.Panel81.ResumeLayout(False)
        Me.Panel81.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents GuardarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents Panel18 As Panel
    Friend WithEvents Panel14 As Panel
    Friend WithEvents varCodigoPostal As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents Panel90 As Panel
    Friend WithEvents varLocalidad As TextBox
    Friend WithEvents Label93 As Label
    Friend WithEvents Panel91 As Panel
    Friend WithEvents varProvincia As TextBox
    Friend WithEvents Label94 As Label
    Friend WithEvents Panel50 As Panel
    Friend WithEvents varCalle As TextBox
    Friend WithEvents Label39 As Label
    Friend WithEvents Panel86 As Panel
    Friend WithEvents varNumero As TextBox
    Friend WithEvents Label45 As Label
    Friend WithEvents Panel87 As Panel
    Friend WithEvents varPiso As TextBox
    Friend WithEvents Label90 As Label
    Friend WithEvents Panel88 As Panel
    Friend WithEvents varDepartamento As TextBox
    Friend WithEvents Label91 As Label
    Friend WithEvents Panel89 As Panel
    Friend WithEvents varBarrio As TextBox
    Friend WithEvents Label92 As Label
    Friend WithEvents Panel17 As Panel
    Friend WithEvents varLugarTrabajo As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents Panel16 As Panel
    Friend WithEvents varTitular As TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents Panel15 As Panel
    Friend WithEvents varProfesionActividad As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents Panel13 As Panel
    Friend WithEvents varCelular As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents Panel12 As Panel
    Friend WithEvents varNumDocumento As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Panel11 As Panel
    Friend WithEvents varNacionalidad As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Panel10 As Panel
    Friend WithEvents varIdEstadoCivil As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents Panel9 As Panel
    Friend WithEvents varEdad As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Panel8 As Panel
    Friend WithEvents varTelefono As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Panel7 As Panel
    Friend WithEvents varFechaNacimiento As DateTimePicker
    Friend WithEvents Label7 As Label
    Friend WithEvents Panel6 As Panel
    Friend WithEvents varIdObraSocial As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Panel5 As Panel
    Friend WithEvents varNumAfiliado As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Panel4 As Panel
    Friend WithEvents varApellido As TextBox
    Friend WithEvents varNombre As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Panel3 As Panel
    Friend WithEvents varFecha As DateTimePicker
    Friend WithEvents Label3 As Label
    Friend WithEvents Panel2 As Panel
    Friend WithEvents varIdConsultorio As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents varIdOdontologo As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents Button1 As Button
    Friend WithEvents Panel33 As Panel
    Friend WithEvents varOtraEnfermedadInfectocontagiosa As CheckBox
    Friend WithEvents varTransfusiones As CheckBox
    Friend WithEvents Panel19 As Panel
    Friend WithEvents varEnfermedadPasadaActualMadre As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents varMadreConVida As CheckBox
    Friend WithEvents Panel29 As Panel
    Friend WithEvents varCualTratamientoAlternativo As TextBox
    Friend WithEvents varTratamientoAlternativo As CheckBox
    Friend WithEvents Panel28 As Panel
    Friend WithEvents varCualEnfermedadRecomendacion As TextBox
    Friend WithEvents Label30 As Label
    Friend WithEvents varOtraEnfermedadRecomendacion As CheckBox
    Friend WithEvents Panel27 As Panel
    Friend WithEvents varCualProblemasRespiratorios As TextBox
    Friend WithEvents Label29 As Label
    Friend WithEvents varProblemasRespiratorios As CheckBox
    Friend WithEvents varFuma As CheckBox
    Friend WithEvents Panel25 As Panel
    Friend WithEvents varCuandoOperacion As DateTimePicker
    Friend WithEvents Label28 As Label
    Friend WithEvents varDeQueOperacion As TextBox
    Friend WithEvents Label27 As Label
    Friend WithEvents varFueOperadoAntes As CheckBox
    Friend WithEvents Panel24 As Panel
    Friend WithEvents varMesesEmbarazo As TextBox
    Friend WithEvents Label26 As Label
    Friend WithEvents varEmbarazada As CheckBox
    Friend WithEvents Panel23 As Panel
    Friend WithEvents varMedicamentosUltimos5Anos As TextBox
    Friend WithEvents Label25 As Label
    Friend WithEvents Panel22 As Panel
    Friend WithEvents varMedicamentosActuales As TextBox
    Friend WithEvents Label24 As Label
    Friend WithEvents Panel21 As Panel
    Friend WithEvents varCualTratamientoMedico As TextBox
    Friend WithEvents Label23 As Label
    Friend WithEvents varHaceTratamientoMedico As CheckBox
    Friend WithEvents Panel20 As Panel
    Friend WithEvents varEnfermedadHermano As TextBox
    Friend WithEvents Label22 As Label
    Friend WithEvents varEnfermoHermano As CheckBox
    Friend WithEvents varSanos As CheckBox
    Friend WithEvents varHermanos As CheckBox
    Friend WithEvents Panel32 As Panel
    Friend WithEvents varEnfermedadPasadaActualPadre As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents varPadreConVida As CheckBox
    Friend WithEvents Panel46 As Panel
    Friend WithEvents varSifilisGonorrea As CheckBox
    Friend WithEvents Panel30 As Panel
    Friend WithEvents varMedicoClinico As TextBox
    Friend WithEvents Label31 As Label
    Friend WithEvents Panel38 As Panel
    Friend WithEvents varMedicacionDiabetico As TextBox
    Friend WithEvents Label35 As Label
    Friend WithEvents varDiabetico As CheckBox
    Friend WithEvents varDiabeticoControlado As CheckBox
    Friend WithEvents Panel45 As Panel
    Friend WithEvents varMedicacionEpilepsia As TextBox
    Friend WithEvents Label41 As Label
    Friend WithEvents varConvulsiones As CheckBox
    Friend WithEvents varEpilepsia As CheckBox
    Friend WithEvents Panel37 As Panel
    Friend WithEvents varMedicacionFiebreReumatica As TextBox
    Friend WithEvents Label34 As Label
    Friend WithEvents varHiperlaxitud As CheckBox
    Friend WithEvents varAntecedentesFiebreReumatica As CheckBox
    Friend WithEvents Panel39 As Panel
    Friend WithEvents varCualProblemaCardiaco As TextBox
    Friend WithEvents Label36 As Label
    Friend WithEvents varProblemaCardiaco As CheckBox
    Friend WithEvents Panel44 As Panel
    Friend WithEvents varCualProblemaHepatico As TextBox
    Friend WithEvents Label40 As Label
    Friend WithEvents varProblemaHepatico As CheckBox
    Friend WithEvents Panel36 As Panel
    Friend WithEvents varComoCicatrizaSangra As TextBox
    Friend WithEvents Label33 As Label
    Friend WithEvents Panel31 As Panel
    Friend WithEvents varClinicaHospital As TextBox
    Friend WithEvents Label32 As Label
    Friend WithEvents Panel40 As Panel
    Friend WithEvents varFrecuenciaAnticoagulante As TextBox
    Friend WithEvents Label37 As Label
    Friend WithEvents varTomaAnticoagulante As CheckBox
    Friend WithEvents Panel43 As Panel
    Friend WithEvents varHepatitisC As CheckBox
    Friend WithEvents varHepatitisB As CheckBox
    Friend WithEvents varHepatitisA As CheckBox
    Friend WithEvents varTuvoHepatitis As CheckBox
    Friend WithEvents varProblemasRenales As CheckBox
    Friend WithEvents varUlceraGastrica As CheckBox
    Friend WithEvents Panel35 As Panel
    Friend WithEvents Label21 As Label
    Friend WithEvents varAlergicoOtros As TextBox
    Friend WithEvents varAlergicoPenisilina As CheckBox
    Friend WithEvents varAlergicoDrogas As CheckBox
    Friend WithEvents varAlergicoAnestesia As CheckBox
    Friend WithEvents Panel34 As Panel
    Friend WithEvents varHaceDeporte As CheckBox
    Friend WithEvents varMalestarAlRealizarlo As CheckBox
    Friend WithEvents Panel41 As Panel
    Friend WithEvents varTratamientoPresionChagas As TextBox
    Friend WithEvents Label38 As Label
    Friend WithEvents varPresionAlta As CheckBox
    Friend WithEvents varChagas As CheckBox
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents Panel52 As Panel
    Friend WithEvents IdDuracionDolor As GroupBox
    Friend WithEvents varIdDuracionDolor3 As RadioButton
    Friend WithEvents varIdDuracionDolor2 As RadioButton
    Friend WithEvents varIdDuracionDolor1 As RadioButton
    Friend WithEvents IdIntensidadDolor As GroupBox
    Friend WithEvents varIdIntensidadDolor3 As RadioButton
    Friend WithEvents varIdIntensidadDolor2 As RadioButton
    Friend WithEvents varIdIntensidadDolor1 As RadioButton
    Friend WithEvents varHaTenidoDolor As CheckBox
    Friend WithEvents Panel61 As Panel
    Friend WithEvents varTragar As TextBox
    Friend WithEvents Label57 As Label
    Friend WithEvents Panel60 As Panel
    Friend WithEvents varAbrirBoca As TextBox
    Friend WithEvents Label56 As Label
    Friend WithEvents Panel59 As Panel
    Friend WithEvents varMasticar As TextBox
    Friend WithEvents Label55 As Label
    Friend WithEvents Panel58 As Panel
    Friend WithEvents varHablar As TextBox
    Friend WithEvents Label54 As Label
    Friend WithEvents Panel57 As Panel
    Friend WithEvents varRecibioTratamientoHCO As TextBox
    Friend WithEvents Label53 As Label
    Friend WithEvents Panel56 As Panel
    Friend WithEvents varCualFracturaDeDiente As TextBox
    Friend WithEvents Label52 As Label
    Friend WithEvents varFracturaDeDiente As CheckBox
    Friend WithEvents Panel55 As Panel
    Friend WithEvents varCuandoGolpeEnDientes As DateTimePicker
    Friend WithEvents varComoGolpeEnDientes As TextBox
    Friend WithEvents Label50 As Label
    Friend WithEvents Label51 As Label
    Friend WithEvents varGolpeEnDientes As CheckBox
    Friend WithEvents Panel54 As Panel
    Friend WithEvents varPudoCalmarIrradiacion As TextBox
    Friend WithEvents Label49 As Label
    Friend WithEvents varHaciaDondeIrradiado As TextBox
    Friend WithEvents Label48 As Label
    Friend WithEvents varIrradiado As CheckBox
    Friend WithEvents Panel53 As Panel
    Friend WithEvents varDondeDolor As TextBox
    Friend WithEvents Label47 As Label
    Friend WithEvents varDolorProvocado As CheckBox
    Friend WithEvents varDolorLocalizado As CheckBox
    Friend WithEvents varDolorAlFrio As CheckBox
    Friend WithEvents varDolorAlCalor As CheckBox
    Friend WithEvents varDolorEspontaneo As CheckBox
    Friend WithEvents Panel51 As Panel
    Friend WithEvents varMedicacionHCO As TextBox
    Friend WithEvents varObtuvoResultadoHCO As CheckBox
    Friend WithEvents Label44 As Label
    Friend WithEvents varDesdeCuandoMedicacionHCO As DateTimePicker
    Friend WithEvents Label46 As Label
    Friend WithEvents Panel49 As Panel
    Friend WithEvents varTomoAlgoHCO As CheckBox
    Friend WithEvents varConsultoAntes As CheckBox
    Friend WithEvents Panel48 As Panel
    Friend WithEvents varMotivoConsulta As TextBox
    Friend WithEvents Label43 As Label
    Friend WithEvents Panel47 As Panel
    Friend WithEvents Label42 As Label
    Friend WithEvents TabPage5 As TabPage
    Friend WithEvents Panel79 As Panel
    Friend WithEvents Label75 As Label
    Friend WithEvents EstadoHigieneBucalMalo As CheckBox
    Friend WithEvents EstadoHigieneBucalDeficiente As CheckBox
    Friend WithEvents EstadoHigieneBucalBueno As CheckBox
    Friend WithEvents EstadoHigieneBucalMuyBueno As CheckBox
    Friend WithEvents Panel78 As Panel
    Friend WithEvents IndiceDePlaca As TextBox
    Friend WithEvents MomentosDeAzucarDiario As TextBox
    Friend WithEvents Label74 As Label
    Friend WithEvents Label73 As Label
    Friend WithEvents Panel77 As Panel
    Friend WithEvents varSePusoOtros As TextBox
    Friend WithEvents varSePusoCalor As CheckBox
    Friend WithEvents Label72 As Label
    Friend WithEvents varSePusoHielo As CheckBox
    Friend WithEvents varCaraHinchada As CheckBox
    Friend WithEvents Panel76 As Panel
    Friend WithEvents varSienteAltosDientesAlMorder As TextBox
    Friend WithEvents Label71 As Label
    Friend WithEvents varMovilidadEnDientes As CheckBox
    Friend WithEvents Panel75 As Panel
    Friend WithEvents varDondeSalePusEnBoca As TextBox
    Friend WithEvents Label70 As Label
    Friend WithEvents varSalePusEnBoca As CheckBox
    Friend WithEvents Panel74 As Panel
    Friend WithEvents varCuandoSangranEncias As TextBox
    Friend WithEvents Label67 As Label
    Friend WithEvents varSangranEncias As CheckBox
    Friend WithEvents Panel73 As Panel
    Friend WithEvents varOtros As TextBox
    Friend WithEvents Label69 As Label
    Friend WithEvents Panel71 As Panel
    Friend WithEvents varAmpollas As CheckBox
    Friend WithEvents varUlceraciones As CheckBox
    Friend WithEvents varAbultamientoTejidos As CheckBox
    Friend WithEvents varManchas As CheckBox
    Friend WithEvents Panel69 As Panel
    Friend WithEvents varRetromolar As TextBox
    Friend WithEvents varTrigono As TextBox
    Friend WithEvents Label66 As Label
    Friend WithEvents Label65 As Label
    Friend WithEvents Panel72 As Panel
    Friend WithEvents Label68 As Label
    Friend WithEvents Panel67 As Panel
    Friend WithEvents varRebordes As TextBox
    Friend WithEvents varCarrillos As TextBox
    Friend WithEvents Label64 As Label
    Friend WithEvents Label63 As Label
    Friend WithEvents Panel65 As Panel
    Friend WithEvents varPisoDeBoca As TextBox
    Friend WithEvents varPaladar As TextBox
    Friend WithEvents Label62 As Label
    Friend WithEvents Label61 As Label
    Friend WithEvents Panel63 As Panel
    Friend WithEvents varLengua As TextBox
    Friend WithEvents varLabios As TextBox
    Friend WithEvents Label60 As Label
    Friend WithEvents Label59 As Label
    Friend WithEvents Panel62 As Panel
    Friend WithEvents Label58 As Label
    Friend WithEvents TabPage6 As TabPage
    Friend WithEvents Panel80 As Panel
    Friend WithEvents Panel26 As Panel
    Friend WithEvents Cuadraditos_82 As Panel
    Friend WithEvents Cuadraditos_86 As Panel
    Friend WithEvents Cuadraditos_81 As Panel
    Friend WithEvents Cuadraditos_48 As Panel
    Friend WithEvents Cuadraditos_62 As Panel
    Friend WithEvents Cuadraditos_85 As Panel
    Friend WithEvents Cuadraditos_61 As Panel
    Friend WithEvents Cuadraditos_46 As Panel
    Friend WithEvents Cuadraditos_66 As Panel
    Friend WithEvents Cuadraditos_47 As Panel
    Friend WithEvents Cuadraditos_65 As Panel
    Friend WithEvents Cuadraditos_84 As Panel
    Friend WithEvents Cuadraditos_28 As Panel
    Friend WithEvents Cuadraditos_45 As Panel
    Friend WithEvents Cuadraditos_27 As Panel
    Friend WithEvents Cuadraditos_44 As Panel
    Friend WithEvents Cuadraditos_26 As Panel
    Friend WithEvents Cuadraditos_83 As Panel
    Friend WithEvents Cuadraditos_25 As Panel
    Friend WithEvents Cuadraditos_42 As Panel
    Friend WithEvents Cuadraditos_64 As Panel
    Friend WithEvents Cuadraditos_43 As Panel
    Friend WithEvents Cuadraditos_63 As Panel
    Friend WithEvents Cuadraditos_41 As Panel
    Friend WithEvents Cuadraditos_24 As Panel
    Friend WithEvents Cuadraditos_22 As Panel
    Friend WithEvents Cuadraditos_23 As Panel
    Friend WithEvents Cuadraditos_21 As Panel
    Friend WithEvents Cuadraditos_75 As Panel
    Friend WithEvents Cuadraditos_73 As Panel
    Friend WithEvents Cuadraditos_76 As Panel
    Friend WithEvents Cuadraditos_71 As Panel
    Friend WithEvents Cuadraditos_55 As Panel
    Friend WithEvents Cuadraditos_74 As Panel
    Friend WithEvents Cuadraditos_56 As Panel
    Friend WithEvents Cuadraditos_31 As Panel
    Friend WithEvents Cuadraditos_53 As Panel
    Friend WithEvents Cuadraditos_72 As Panel
    Friend WithEvents Cuadraditos_54 As Panel
    Friend WithEvents Cuadraditos_33 As Panel
    Friend WithEvents Cuadraditos_51 As Panel
    Friend WithEvents Cuadraditos_32 As Panel
    Friend WithEvents Cuadraditos_52 As Panel
    Friend WithEvents Cuadraditos_35 As Panel
    Friend WithEvents Cuadraditos_11 As Panel
    Friend WithEvents Cuadraditos_34 As Panel
    Friend WithEvents Cuadraditos_12 As Panel
    Friend WithEvents Cuadraditos_37 As Panel
    Friend WithEvents Cuadraditos_13 As Panel
    Friend WithEvents Cuadraditos_36 As Panel
    Friend WithEvents Cuadraditos_14 As Panel
    Friend WithEvents Cuadraditos_38 As Panel
    Friend WithEvents Cuadraditos_15 As Panel
    Friend WithEvents Cuadraditos_17 As Panel
    Friend WithEvents Cuadraditos_16 As Panel
    Friend WithEvents Cuadraditos_18 As Panel
    Friend WithEvents Label76 As Label
    Friend WithEvents TabPage7 As TabPage
    Friend WithEvents Panel85 As Panel
    Friend WithEvents Label88 As Label
    Friend WithEvents varObservaciones As TextBox
    Friend WithEvents Panel84 As Panel
    Friend WithEvents varCuandoPlanTratamiento As DateTimePicker
    Friend WithEvents Label87 As Label
    Friend WithEvents varPlanTratamiento As TextBox
    Friend WithEvents Panel83 As Panel
    Friend WithEvents Label86 As Label
    Friend WithEvents varDiagnosticoPresuntivo As TextBox
    Friend WithEvents Panel82 As Panel
    Friend WithEvents varEnfermedadPeriodontal As CheckBox
    Friend WithEvents varSarro As CheckBox
    Friend WithEvents Label85 As Label
    Friend WithEvents Label89 As Label
    Friend WithEvents Panel81 As Panel
    Friend WithEvents varCantidadDientes As TextBox
    Friend WithEvents Label84 As Label
    Friend WithEvents Label83 As Label
    Friend WithEvents Label82 As Label
    Friend WithEvents Label81 As Label
    Friend WithEvents Label80 As Label
    Friend WithEvents Label79 As Label
    Friend WithEvents Label78 As Label
    Friend WithEvents Label77 As Label
End Class
