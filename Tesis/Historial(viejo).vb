﻿Option Explicit On
Option Infer On
Imports System.IO.File
Imports System.Drawing
Public Class Historial
    Dim var As New Paciente()
    Dim cuadraditos(56) As String
    Dim textoparacuadraditos(56) As String
    Public Shared cubito As String
    Dim reseteocuadrados As Boolean = False
    Dim varIdIntensidadDolor As Integer
    Dim varIdDuracionDolor As Integer
    Dim varIdConsulta As Integer
    Dim varIdDomicilio As Integer
    Dim varIdCuestionario As Integer
    Dim varIdHCO As Integer
    Dim varIdEstimuloDolor As Integer
    Dim varIdDificultad As Integer
    Dim varIdAnormalidad As Integer
    Dim varIdLesiones As Integer
    Dim varIdCuadraditos As Integer
    Dim varIdEstadoBucal As Integer

    Private Sub BuscarPacienteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BuscarPacienteToolStripMenuItem.Click
        Dim frm2 = New BusquedaPersona()
        frm2.MdiParent = Me.MdiParent
        frm2.ShowDialog()
    End Sub
    Public Sub New()
        InitializeComponent()
        Preparar_Interfaz()
        For index = 1 To 56
            cuadraditos(index) = "000000"
        Next
        llenarmapa()
        var.DesdeCuandoMedicacionHCO = Date.Now
        var.CuandoGolpeEnDientes = Date.Now
        var.CuandoPlanTratamiento = Date.Now
        var.CuandoOperacion = Date.Now
        var.FechaNacimiento = Date.Now
        var.Fecha = Date.Now
        Clase_A_Interfaz()
    End Sub
    Sub Preparar_Interfaz()
        varIdOdontologo.DataSource = Paciente.Obtener_Odontologos()
        varIdOdontologo.ValueMember = "Id"
        varIdOdontologo.DisplayMember = "Nombre,',',Apellido"
    End Sub
    Private Sub GuardarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GuardarToolStripMenuItem.Click
        ' Try

        Guardar()

        ' Catch ex As Exception

        'MessageBox.Show(ex.Message)

        ' End Try
    End Sub

    ''' <summary>
    '''
    ''' </summary>
    Sub Clase_A_Interfaz()

        varFecha.Value = var.Fecha
        varNombre.Text = var.Nombre
        varApellido.Text = var.Apellido
        varIdObraSocial.Text = var.NumAfiliado
        varFechaNacimiento.Value = var.FechaNacimiento
        varTelefono.Text = var.Telefono
        varCelular.Text = var.Celular
        varEdad.Text = var.Edad
        varNacionalidad.Text = var.Nacionalidad
        varNumDocumento.Text = var.NumDocumento
        varProfesionActividad.Text = var.ProfesionActividad
        varTitular.Text = var.Titular
        varLugarTrabajo.Text = var.LugarTrabajo
        varPadreConVida.Checked = var.PadreConVida
        varEnfermedadPasadaActualPadre.Text = var.EnfermedadPasadaActualPadre
        varMadreConVida.Checked = var.MadreConVida
        varEnfermedadPasadaActualMadre.Text = var.EnfermedadPasadaActualMadre
        varHermanos.Checked = var.Hermanos
        varSanos.Checked = var.Sanos
        varEnfermoHermano.Checked = var.EnfermoHermano
        varEnfermedadHermano.Text = var.EnfermedadHermano
        varHaceTratamientoMedico.Checked = var.HaceTratamientoMedico
        varCualTratamientoMedico.Text = var.CualTratamientoMedico
        varMedicamentosActuales.Text = var.MedicamentosActuales
        varMedicamentosUltimos5Anos.Text = var.MedicamentosUltimos5anos
        varOtraEnfermedadInfectocontagiosa.Text = var.OtraEnfermedadInfectocontagiosa
        varTransfusiones.Checked = var.Transfusiones
        varFueOperadoAntes.Checked = var.FueOperadoAntes
        varDeQueOperacion.Text = var.DeQueOperacion
        varCuandoOperacion.Value = var.CuandoOperacion
        varProblemasRespiratorios.Checked = var.ProblemasRespiratorios
        varCualProblemasRespiratorios.Text = var.CualProblemasRespiratorios
        varFuma.Checked = var.Fuma
        varEmbarazada.Checked = var.Embarazada
        varMesesEmbarazo.Text = var.MesesEmbarazo
        varOtraEnfermedadRecomendacion.Checked = var.OtraEnfermedadRecomendacion
        varCualEnfermedadRecomendacion.Text = var.CualEnfermedadRecomendacion
        varTratamientoAlternativo.Checked = var.TratamientoAlternativo
        varCualTratamientoAlternativo.Text = var.CualTratamientoAlternativo
        varMedicoClinico.Text = var.MedicoClinico
        varClinicaHospital.Text = var.ClinicaHospital
        varHaceDeporte.Checked = var.HaceDeporte
        varMalestarAlRealizarlo.Checked = var.MalestarAlRealizarlo
        varAlergicoDrogas.Checked = var.AlergicoDrogas
        varAlergicoAnestesia.Checked = var.AlergicoAnestesia
        varAlergicoPenisilina.Checked = var.AlergicoPenisilina
        varAlergicoOtros.Text = var.AlergicoOtros
        varComoCicatrizaSangra.Text = var.ComoCicatrizaSangra
        varHiperlaxitud.Checked = var.Hiperlaxitud
        varAntecedentesFiebreReumatica.Checked = var.AntecedentesFiebreReumatica
        varMedicacionFiebreReumatica.Text = var.MedicacionFiebreReumatica
        varDiabetico.Checked = var.Diabetico
        varDiabeticoControlado.Checked = var.DiabeticoControlado
        varMedicacionDiabetico.Text = var.MedicacionDiabetico
        varProblemaCardiaco.Checked = var.ProblemaCardiaco
        varCualProblemaCardiaco.Text = var.CualProblemaCardiaco
        varTomaAnticoagulante.Checked = var.TomaAnticoagulante
        varFrecuenciaAnticoagulante.Text = var.FrecuenciaAnticoagulante
        varPresionAlta.Checked = var.PresionAlta
        varChagas.Checked = var.Chagas
        varTratamientoPresionChagas.Text = var.TratamientoPresionchagas
        varProblemasRenales.Checked = var.ProblemasRenales
        varUlceraGastrica.Checked = var.UlceraGastrica
        varTuvoHepatitis.Checked = var.TuvoHepatitis
        varHepatitisA.Checked = var.HepatitisA
        varHepatitisB.Checked = var.HepatitisB
        varHepatitisC.Checked = var.HepatitisC
        varProblemaHepatico.Checked = var.ProblemaHepatico
        varCualProblemaHepatico.Text = var.CualProblemaHepatico
        varConvulsiones.Checked = var.Convulsiones
        varEpilepsia.Checked = var.Epilepsia
        varMedicacionEpilepsia.Text = var.MedicacionEpilepsia
        varSifilisGonorrea.Checked = var.SifilisGonorrea
        varMotivoConsulta.Text = var.MotivoConsulta
        varConsultoAntes.Checked = var.ConsultoAntes
        varTomoAlgoHCO.Checked = var.TomoAlgoHCO
        varMedicacionHCO.Text = var.MedicacionHCO
        varDesdeCuandoMedicacionHCO.Value = var.DesdeCuandoMedicacionHCO
        varObtuvoResultadoHCO.Checked = var.ObtuvoResultadoHCO
        varHaTenidoDolor.Checked = var.HaTenidoDolor
        varDolorLocalizado.Checked = var.DolorLocalizado
        varDondeDolor.Text = var.DondeDolor
        varIrradiado.Checked = var.Irradiado
        varHaciaDondeIrradiado.Text = var.HaciaDondeIrradiado
        varPudoCalmarIrradiacion.Text = var.PudoCalmarIrradiacion
        varGolpeEnDientes.Checked = var.GolpeEnDientes
        varCuandoGolpeEnDientes.Value = var.CuandoGolpeEnDientes
        varComoGolpeEnDientes.Text = var.ComoGolpeEnDientes
        varFracturaDeDiente.Checked = var.FracturaDeDiente
        varCualFracturaDeDiente.Text = var.CualFracturaDeDiente
        varRecibioTratamientoHCO.Text = var.RecibioTratamientoHCO
        varCalle.Text = var.Calle
        varNumero.Text = var.Numero
        varPiso.Text = var.Piso
        varDepartamento.Text = var.Departamento
        varBarrio.Text = var.Barrio
        varLocalidad.Text = var.Localidad
        varProvincia.Text = var.Provincia
        varCodigoPostal.Text = var.CodigoPostal
        varDolorEspontaneo.Checked = var.DolorEspontaneo
        varDolorProvocado.Checked = var.DolorProvocado
        varDolorAlFrio.Checked = var.DolorAlFrio
        varDolorAlCalor.Checked = var.DolorAlCalor
        varHablar.Text = var.Hablar
        varMasticar.Text = var.Masticar
        varAbrirBoca.Text = var.AbrirBoca
        varTragar.Text = var.Tragar
        varLabios.Text = var.Labios
        varLengua.Text = var.Lengua
        varPaladar.Text = var.Paladar
        varPisoDeBoca.Text = var.PisoDeBoca
        varCarrillos.Text = var.Carrillos
        varRebordes.Text = var.Rebordes
        varTrigono.Text = var.Trigono
        varRetromolar.Text = var.Retromolar
        varManchas.Checked = var.Manchas
        varAbultamientoTejidos.Checked = var.AbultamientoTejidos
        varUlceraciones.Checked = var.Ulceraciones
        varAmpollas.Checked = var.Ampollas
        varOtros.Text = var.Otros
        varSangranEncias.Checked = var.SangranEncias
        varCuandoSangranEncias.Text = var.CuandoSangranEncias
        varSalePusEnBoca.Checked = var.SalePusEnBoca
        varDondeSalePusEnBoca.Text = var.DondeSalePusEnBoca
        varMovilidadEnDientes.Checked = var.MovilidadEnDientes
        varSienteAltosDientesAlMorder.Text = var.SienteAltosDientesAlMorder
        varCaraHinchada.Checked = var.CaraHinchada
        varSePusoHielo.Checked = var.SePusoHielo
        varSePusoCalor.Checked = var.SePusoCalor
        varSePusoOtros.Text = var.SePusoOtros
        varSarro.Checked = var.Sarro
        varEnfermedadPeriodontal.Checked = var.EnfermedadPeriodontal
        varDiagnosticoPresuntivo.Text = var.DiagnosticoPresuntivo
        varPlanTratamiento.Text = var.PlanTratamiento
        varCuandoPlanTratamiento.Value = var.CuandoPlanTratamiento
        varCantidadDientes.Text = var.CantidadDientes
        varObservaciones.Text = var.Observaciones
        Cuadraditos_11.Text = var.i11
        Cuadraditos_12.Text = var.i12
        Cuadraditos_13.Text = var.i13
        Cuadraditos_14.Text = var.i14
        Cuadraditos_15.Text = var.i15
        Cuadraditos_16.Text = var.i16
        Cuadraditos_17.Text = var.i17
        Cuadraditos_18.Text = var.i18
        Cuadraditos_21.Text = var.i21
        Cuadraditos_22.Text = var.i22
        Cuadraditos_23.Text = var.i23
        Cuadraditos_24.Text = var.i24
        Cuadraditos_25.Text = var.i25
        Cuadraditos_26.Text = var.i26
        Cuadraditos_27.Text = var.i27
        Cuadraditos_28.Text = var.i28
        Cuadraditos_31.Text = var.i31
        Cuadraditos_32.Text = var.i32
        Cuadraditos_33.Text = var.i33
        Cuadraditos_34.Text = var.i34
        Cuadraditos_35.Text = var.i35
        Cuadraditos_36.Text = var.i36
        Cuadraditos_37.Text = var.i37
        Cuadraditos_38.Text = var.i38
        Cuadraditos_41.Text = var.i41
        Cuadraditos_42.Text = var.i42
        Cuadraditos_43.Text = var.i43
        Cuadraditos_44.Text = var.i44
        Cuadraditos_45.Text = var.i45
        Cuadraditos_46.Text = var.i46
        Cuadraditos_47.Text = var.i47
        Cuadraditos_48.Text = var.i48
        Cuadraditos_51.Text = var.i51
        Cuadraditos_52.Text = var.i52
        Cuadraditos_53.Text = var.i53
        Cuadraditos_54.Text = var.i54
        Cuadraditos_55.Text = var.i55
        Cuadraditos_56.Text = var.i56
        Cuadraditos_61.Text = var.i61
        Cuadraditos_62.Text = var.i62
        Cuadraditos_63.Text = var.i63
        Cuadraditos_64.Text = var.i64
        Cuadraditos_65.Text = var.i65
        Cuadraditos_66.Text = var.i66
        Cuadraditos_71.Text = var.i71
        Cuadraditos_72.Text = var.i72
        Cuadraditos_73.Text = var.i73
        Cuadraditos_74.Text = var.i74
        Cuadraditos_75.Text = var.i75
        Cuadraditos_76.Text = var.i76
        Cuadraditos_81.Text = var.i81
        Cuadraditos_82.Text = var.i82
        Cuadraditos_83.Text = var.i83
        Cuadraditos_84.Text = var.i84
        Cuadraditos_85.Text = var.i85
        Cuadraditos_86.Text = var.i86
        varIdConsulta = var.IdConsulta
        varNumAfiliado.Text = var.IdPersona
        varIdObraSocial.Text = var.IdObraSocial
        varIdEstadoCivil.Text = var.IdEstadoCivil
        varIdDomicilio = var.IdDomicilio
        varIdCuestionario = var.IdCuestionario
        varIdHCO = var.IdHCO
        varIdIntensidadDolor = var.IdIntensidadDolor
        varIdDuracionDolor = var.IdDuracionDolor
        varIdEstimuloDolor = var.IdEstimuloDolor
        varIdDificultad = var.IdDificultad
        varIdAnormalidad = var.IdAnormalidad
        varIdLesiones = var.IdLesiones
        varIdCuadraditos = var.IdCuadraditos
        varIdOdontologo.Text = var.IdOdontologo
        varIdConsultorio.Text = var.IdConsultorio
        varIdEstadoBucal = var.IdEstadoBucal

        preparacion_cuadraditos()



    End Sub
    Sub Interfaz_A_Clase()

        If (var Is Nothing) Then
            var = New Paciente()
        End If
        var.Fecha = varFecha.Value
        var.Nombre = varNombre.ToString
        var.Apellido = varApellido.ToString
        var.NumAfiliado = Convert.ToInt32(varIdObraSocial.Text)
        var.FechaNacimiento = varFechaNacimiento.Value
        var.Telefono = varTelefono.ToString
        var.Celular = varCelular.ToString
        var.Edad = Convert.ToInt32(varEdad.Text)
        var.Nacionalidad = varNacionalidad.ToString
        var.NumDocumento = Convert.ToInt32(varNumDocumento.Text)
        var.ProfesionActividad = varProfesionActividad.ToString
        var.Titular = varTitular.ToString
        var.LugarTrabajo = varLugarTrabajo.ToString
        var.PadreConVida = varPadreConVida.Checked
        var.EnfermedadPasadaActualPadre = varEnfermedadPasadaActualPadre.ToString
        var.MadreConVida = varMadreConVida.Checked
        var.EnfermedadPasadaActualMadre = varEnfermedadPasadaActualMadre.ToString
        var.Hermanos = varHermanos.Checked
        var.Sanos = varSanos.Checked
        var.EnfermoHermano = varEnfermoHermano.Checked
        var.EnfermedadHermano = varEnfermedadHermano.ToString
        var.HaceTratamientoMedico = varHaceTratamientoMedico.Checked
        var.CualTratamientoMedico = varCualTratamientoMedico.ToString
        var.MedicamentosActuales = varMedicamentosActuales.ToString
        var.MedicamentosUltimos5anos = varMedicamentosUltimos5Anos.ToString
        var.OtraEnfermedadInfectocontagiosa = varOtraEnfermedadInfectocontagiosa.ToString
        var.Transfusiones = varTransfusiones.Checked
        var.FueOperadoAntes = varFueOperadoAntes.Checked
        var.DeQueOperacion = varDeQueOperacion.ToString
        var.CuandoOperacion = varCuandoOperacion.Value
        var.ProblemasRespiratorios = varProblemasRespiratorios.Checked
        var.CualProblemasRespiratorios = varCualProblemasRespiratorios.ToString
        var.Fuma = varFuma.Checked
        var.Embarazada = varEmbarazada.Checked
        var.MesesEmbarazo = varMesesEmbarazo.ToString
        var.OtraEnfermedadRecomendacion = varOtraEnfermedadRecomendacion.Checked
        var.CualEnfermedadRecomendacion = varCualEnfermedadRecomendacion.ToString
        var.TratamientoAlternativo = varTratamientoAlternativo.Checked
        var.CualTratamientoAlternativo = varCualTratamientoAlternativo.ToString
        var.MedicoClinico = varMedicoClinico.ToString
        var.ClinicaHospital = varClinicaHospital.ToString
        var.HaceDeporte = varHaceDeporte.Checked
        var.MalestarAlRealizarlo = varMalestarAlRealizarlo.Checked
        var.AlergicoDrogas = varAlergicoDrogas.Checked
        var.AlergicoAnestesia = varAlergicoAnestesia.Checked
        var.AlergicoPenisilina = varAlergicoPenisilina.Checked
        var.AlergicoOtros = varAlergicoOtros.ToString
        var.ComoCicatrizaSangra = varComoCicatrizaSangra.ToString
        var.Hiperlaxitud = varHiperlaxitud.Checked
        var.AntecedentesFiebreReumatica = varAntecedentesFiebreReumatica.Checked
        var.MedicacionFiebreReumatica = varMedicacionFiebreReumatica.ToString
        var.Diabetico = varDiabetico.Checked
        var.DiabeticoControlado = varDiabeticoControlado.Checked
        var.MedicacionDiabetico = varMedicacionDiabetico.ToString
        var.ProblemaCardiaco = varProblemaCardiaco.Checked
        var.CualProblemaCardiaco = varCualProblemaCardiaco.ToString
        var.TomaAnticoagulante = varTomaAnticoagulante.Checked
        var.FrecuenciaAnticoagulante = varFrecuenciaAnticoagulante.ToString
        var.PresionAlta = varPresionAlta.Checked
        var.Chagas = varChagas.Checked
        var.TratamientoPresionchagas = varTratamientoPresionChagas.ToString
        var.ProblemasRenales = varProblemasRenales.Checked
        var.UlceraGastrica = varUlceraGastrica.Checked
        var.TuvoHepatitis = varTuvoHepatitis.Checked
        var.HepatitisA = varHepatitisA.Checked
        var.HepatitisB = varHepatitisB.Checked
        var.HepatitisC = varHepatitisC.Checked
        var.ProblemaHepatico = varProblemaHepatico.Checked
        var.CualProblemaHepatico = varCualProblemaHepatico.ToString
        var.Convulsiones = varConvulsiones.Checked
        var.Epilepsia = varEpilepsia.Checked
        var.MedicacionEpilepsia = varMedicacionEpilepsia.ToString
        var.SifilisGonorrea = varSifilisGonorrea.Checked
        var.MotivoConsulta = varMotivoConsulta.ToString
        var.ConsultoAntes = varConsultoAntes.Checked
        var.TomoAlgoHCO = varTomoAlgoHCO.Checked
        var.MedicacionHCO = varMedicacionHCO.ToString
        var.DesdeCuandoMedicacionHCO = varDesdeCuandoMedicacionHCO.Value
        var.ObtuvoResultadoHCO = varObtuvoResultadoHCO.Checked
        var.HaTenidoDolor = varHaTenidoDolor.Checked
        var.DolorLocalizado = varDolorLocalizado.Checked
        var.DondeDolor = varDondeDolor.ToString
        var.Irradiado = varIrradiado.Checked
        var.HaciaDondeIrradiado = varHaciaDondeIrradiado.ToString
        var.PudoCalmarIrradiacion = varPudoCalmarIrradiacion.ToString
        var.GolpeEnDientes = varGolpeEnDientes.Checked
        var.CuandoGolpeEnDientes = varCuandoGolpeEnDientes.Value
        var.ComoGolpeEnDientes = varComoGolpeEnDientes.ToString
        var.FracturaDeDiente = varFracturaDeDiente.Checked
        var.CualFracturaDeDiente = varCualFracturaDeDiente.ToString
        var.RecibioTratamientoHCO = varRecibioTratamientoHCO.ToString
        var.Calle = varCalle.ToString
        var.Numero = Convert.ToInt32(varNumero.Text)
        var.Piso = Convert.ToInt32(varPiso.Text)
        var.Departamento = varDepartamento.ToString
        var.Barrio = varBarrio.ToString
        var.Localidad = varLocalidad.ToString
        var.Provincia = varProvincia.ToString
        var.CodigoPostal = varCodigoPostal.ToString
        var.DolorEspontaneo = varDolorEspontaneo.Checked
        var.DolorProvocado = varDolorProvocado.Checked
        var.DolorAlFrio = varDolorAlFrio.Checked
        var.DolorAlCalor = varDolorAlCalor.Checked
        var.Hablar = varHablar.ToString
        var.Masticar = varMasticar.ToString
        var.AbrirBoca = varAbrirBoca.ToString
        var.Tragar = varTragar.ToString
        var.Labios = varLabios.ToString
        var.Lengua = varLengua.ToString
        var.Paladar = varPaladar.ToString
        var.PisoDeBoca = varPisoDeBoca.ToString
        var.Carrillos = varCarrillos.ToString
        var.Rebordes = varRebordes.ToString
        var.Trigono = varTrigono.ToString
        var.Retromolar = varRetromolar.ToString
        var.Manchas = varManchas.Checked
        var.AbultamientoTejidos = varAbultamientoTejidos.Checked
        var.Ulceraciones = varUlceraciones.Checked
        var.Ampollas = varAmpollas.Checked
        var.Otros = varOtros.ToString
        var.SangranEncias = varSangranEncias.Checked
        var.CuandoSangranEncias = varCuandoSangranEncias.ToString
        var.SalePusEnBoca = varSalePusEnBoca.Checked
        var.DondeSalePusEnBoca = varDondeSalePusEnBoca.ToString
        var.MovilidadEnDientes = varMovilidadEnDientes.Checked
        var.SienteAltosDientesAlMorder = varSienteAltosDientesAlMorder.ToString
        var.CaraHinchada = varCaraHinchada.Checked
        var.SePusoHielo = varSePusoHielo.Checked
        var.SePusoCalor = varSePusoCalor.Checked
        var.SePusoOtros = varSePusoOtros.ToString
        var.Sarro = varSarro.Checked
        var.EnfermedadPeriodontal = varEnfermedadPeriodontal.Checked
        var.DiagnosticoPresuntivo = varDiagnosticoPresuntivo.ToString
        var.PlanTratamiento = varPlanTratamiento.ToString
        var.CuandoPlanTratamiento = varCuandoPlanTratamiento.Value
        var.CantidadDientes = Convert.ToInt32(varCantidadDientes.Text)
        var.Observaciones = varObservaciones.ToString
        var.i11 = Convert.ToInt32(Cuadraditos_11.Text)
        var.i12 = Convert.ToInt32(Cuadraditos_12.Text)
        var.i13 = Convert.ToInt32(Cuadraditos_13.Text)
        var.i14 = Convert.ToInt32(Cuadraditos_14.Text)
        var.i15 = Convert.ToInt32(Cuadraditos_15.Text)
        var.i16 = Convert.ToInt32(Cuadraditos_16.Text)
        var.i17 = Convert.ToInt32(Cuadraditos_17.Text)
        var.i18 = Convert.ToInt32(Cuadraditos_18.Text)
        var.i21 = Convert.ToInt32(Cuadraditos_21.Text)
        var.i22 = Convert.ToInt32(Cuadraditos_22.Text)
        var.i23 = Convert.ToInt32(Cuadraditos_23.Text)
        var.i24 = Convert.ToInt32(Cuadraditos_24.Text)
        var.i25 = Convert.ToInt32(Cuadraditos_25.Text)
        var.i26 = Convert.ToInt32(Cuadraditos_26.Text)
        var.i27 = Convert.ToInt32(Cuadraditos_27.Text)
        var.i28 = Convert.ToInt32(Cuadraditos_28.Text)
        var.i31 = Convert.ToInt32(Cuadraditos_31.Text)
        var.i32 = Convert.ToInt32(Cuadraditos_32.Text)
        var.i33 = Convert.ToInt32(Cuadraditos_33.Text)
        var.i34 = Convert.ToInt32(Cuadraditos_34.Text)
        var.i35 = Convert.ToInt32(Cuadraditos_35.Text)
        var.i36 = Convert.ToInt32(Cuadraditos_36.Text)
        var.i37 = Convert.ToInt32(Cuadraditos_37.Text)
        var.i38 = Convert.ToInt32(Cuadraditos_38.Text)
        var.i41 = Convert.ToInt32(Cuadraditos_41.Text)
        var.i42 = Convert.ToInt32(Cuadraditos_42.Text)
        var.i43 = Convert.ToInt32(Cuadraditos_43.Text)
        var.i44 = Convert.ToInt32(Cuadraditos_44.Text)
        var.i45 = Convert.ToInt32(Cuadraditos_45.Text)
        var.i46 = Convert.ToInt32(Cuadraditos_46.Text)
        var.i47 = Convert.ToInt32(Cuadraditos_47.Text)
        var.i48 = Convert.ToInt32(Cuadraditos_48.Text)
        var.i51 = Convert.ToInt32(Cuadraditos_51.Text)
        var.i52 = Convert.ToInt32(Cuadraditos_52.Text)
        var.i53 = Convert.ToInt32(Cuadraditos_53.Text)
        var.i54 = Convert.ToInt32(Cuadraditos_54.Text)
        var.i55 = Convert.ToInt32(Cuadraditos_55.Text)
        var.i56 = Convert.ToInt32(Cuadraditos_56.Text)
        var.i61 = Convert.ToInt32(Cuadraditos_61.Text)
        var.i62 = Convert.ToInt32(Cuadraditos_62.Text)
        var.i63 = Convert.ToInt32(Cuadraditos_63.Text)
        var.i64 = Convert.ToInt32(Cuadraditos_64.Text)
        var.i65 = Convert.ToInt32(Cuadraditos_65.Text)
        var.i66 = Convert.ToInt32(Cuadraditos_66.Text)
        var.i71 = Convert.ToInt32(Cuadraditos_71.Text)
        var.i72 = Convert.ToInt32(Cuadraditos_72.Text)
        var.i73 = Convert.ToInt32(Cuadraditos_73.Text)
        var.i74 = Convert.ToInt32(Cuadraditos_74.Text)
        var.i75 = Convert.ToInt32(Cuadraditos_75.Text)
        var.i76 = Convert.ToInt32(Cuadraditos_76.Text)
        var.i81 = Convert.ToInt32(Cuadraditos_81.Text)
        var.i82 = Convert.ToInt32(Cuadraditos_82.Text)
        var.i83 = Convert.ToInt32(Cuadraditos_83.Text)
        var.i84 = Convert.ToInt32(Cuadraditos_84.Text)
        var.i85 = Convert.ToInt32(Cuadraditos_85.Text)
        var.i86 = Convert.ToInt32(Cuadraditos_86.Text)
        var.IdConsulta = varIdConsulta 'eq
        var.IdPersona = Convert.ToInt32(varNumAfiliado.Text)
        var.IdObraSocial = Convert.ToInt32(varIdObraSocial.Text)
        var.IdEstadoCivil = Convert.ToInt32(varIdEstadoCivil.Text)
        var.IdDomicilio = varIdDomicilio 'eq
        var.IdCuestionario = varIdCuestionario 'eq
        var.IdHCO = varIdHCO 'eq
        var.IdIntensidadDolor = varIdIntensidadDolor
        var.IdDuracionDolor = varIdDuracionDolor
        var.IdEstimuloDolor = varIdEstimuloDolor
        var.IdDificultad = varIdDificultad
        var.IdAnormalidad = varIdAnormalidad
        var.IdLesiones = varIdLesiones
        var.IdCuadraditos = varIdCuadraditos
        var.IdOdontologo = Convert.ToInt32(varIdOdontologo.Text)
        var.IdConsultorio = Convert.ToInt32(varIdConsultorio.Text)
        var.IdEstadoBucal = varIdEstadoBucal

        var.i11 = cuadraditos(1)
        var.i12 = cuadraditos(2)
        var.i13 = cuadraditos(3)
        var.i14 = cuadraditos(4)
        var.i15 = cuadraditos(5)
        var.i16 = cuadraditos(6)
        var.i17 = cuadraditos(7)
        var.i18 = cuadraditos(8)
        var.i21 = cuadraditos(9)
        var.i22 = cuadraditos(10)
        var.i23 = cuadraditos(11)
        var.i24 = cuadraditos(12)
        var.i25 = cuadraditos(13)
        var.i26 = cuadraditos(14)
        var.i27 = cuadraditos(15)
        var.i28 = cuadraditos(16)
        var.i31 = cuadraditos(17)
        var.i32 = cuadraditos(18)
        var.i33 = cuadraditos(19)
        var.i34 = cuadraditos(20)
        var.i35 = cuadraditos(21)
        var.i36 = cuadraditos(22)
        var.i37 = cuadraditos(23)
        var.i38 = cuadraditos(24)
        var.i41 = cuadraditos(25)
        var.i42 = cuadraditos(26)
        var.i43 = cuadraditos(27)
        var.i44 = cuadraditos(28)
        var.i45 = cuadraditos(29)
        var.i46 = cuadraditos(30)
        var.i47 = cuadraditos(31)
        var.i48 = cuadraditos(32)
        var.i51 = cuadraditos(33)
        var.i52 = cuadraditos(34)
        var.i53 = cuadraditos(35)
        var.i54 = cuadraditos(36)
        var.i55 = cuadraditos(37)
        var.i56 = cuadraditos(38)
        var.i61 = cuadraditos(39)
        var.i62 = cuadraditos(40)
        var.i63 = cuadraditos(41)
        var.i64 = cuadraditos(42)
        var.i65 = cuadraditos(43)
        var.i66 = cuadraditos(44)
        var.i71 = cuadraditos(45)
        var.i72 = cuadraditos(46)
        var.i73 = cuadraditos(47)
        var.i74 = cuadraditos(48)
        var.i75 = cuadraditos(49)
        var.i76 = cuadraditos(50)
        var.i81 = cuadraditos(51)
        var.i82 = cuadraditos(52)
        var.i83 = cuadraditos(53)
        var.i84 = cuadraditos(54)
        var.i85 = cuadraditos(55)
        var.i86 = cuadraditos(56)
    End Sub
    Sub Guardar()
        Interfaz_A_Clase()
        var.Insertar()
        Clase_A_Interfaz()
    End Sub
    Sub Eliminar()
        Paciente.Eliminar()
        MessageBox.Show("El producto se ha eliminado")
        Me.Close()
    End Sub
    Public Sub preparacion_cuadraditos()
        For index = 1 To 56
            textoparacuadraditos((index)) = "C:\Program Files (x86)\Tesis\Tesis\Cuadraditos\" + (cuadraditos(index).ToString) + ".png"
        Next
        Cuadraditos_18.BackgroundImage = Image.FromFile(textoparacuadraditos(1), True)
        Cuadraditos_17.BackgroundImage = Image.FromFile(textoparacuadraditos(2), True)
        Cuadraditos_16.BackgroundImage = Image.FromFile(textoparacuadraditos(3), True)
        Cuadraditos_15.BackgroundImage = Image.FromFile(textoparacuadraditos(4), True)
        Cuadraditos_14.BackgroundImage = Image.FromFile(textoparacuadraditos(5), True)
        Cuadraditos_13.BackgroundImage = Image.FromFile(textoparacuadraditos(6), True)
        Cuadraditos_12.BackgroundImage = Image.FromFile(textoparacuadraditos(7), True)
        Cuadraditos_11.BackgroundImage = Image.FromFile(textoparacuadraditos(8), True)
        Cuadraditos_21.BackgroundImage = Image.FromFile(textoparacuadraditos(9), True)
        Cuadraditos_22.BackgroundImage = Image.FromFile(textoparacuadraditos(10), True)
        Cuadraditos_23.BackgroundImage = Image.FromFile(textoparacuadraditos(11), True)
        Cuadraditos_24.BackgroundImage = Image.FromFile(textoparacuadraditos(12), True)
        Cuadraditos_25.BackgroundImage = Image.FromFile(textoparacuadraditos(13), True)
        Cuadraditos_26.BackgroundImage = Image.FromFile(textoparacuadraditos(14), True)
        Cuadraditos_27.BackgroundImage = Image.FromFile(textoparacuadraditos(15), True)
        Cuadraditos_28.BackgroundImage = Image.FromFile(textoparacuadraditos(16), True)
        Cuadraditos_38.BackgroundImage = Image.FromFile(textoparacuadraditos(17), True)
        Cuadraditos_37.BackgroundImage = Image.FromFile(textoparacuadraditos(18), True)
        Cuadraditos_36.BackgroundImage = Image.FromFile(textoparacuadraditos(19), True)
        Cuadraditos_35.BackgroundImage = Image.FromFile(textoparacuadraditos(20), True)
        Cuadraditos_34.BackgroundImage = Image.FromFile(textoparacuadraditos(21), True)
        Cuadraditos_33.BackgroundImage = Image.FromFile(textoparacuadraditos(22), True)
        Cuadraditos_32.BackgroundImage = Image.FromFile(textoparacuadraditos(23), True)
        Cuadraditos_31.BackgroundImage = Image.FromFile(textoparacuadraditos(24), True)
        Cuadraditos_41.BackgroundImage = Image.FromFile(textoparacuadraditos(25), True)
        Cuadraditos_42.BackgroundImage = Image.FromFile(textoparacuadraditos(26), True)
        Cuadraditos_43.BackgroundImage = Image.FromFile(textoparacuadraditos(27), True)
        Cuadraditos_44.BackgroundImage = Image.FromFile(textoparacuadraditos(28), True)
        Cuadraditos_45.BackgroundImage = Image.FromFile(textoparacuadraditos(29), True)
        Cuadraditos_46.BackgroundImage = Image.FromFile(textoparacuadraditos(30), True)
        Cuadraditos_47.BackgroundImage = Image.FromFile(textoparacuadraditos(31), True)
        Cuadraditos_48.BackgroundImage = Image.FromFile(textoparacuadraditos(32), True)
        Cuadraditos_56.BackgroundImage = Image.FromFile(textoparacuadraditos(33), True)
        Cuadraditos_55.BackgroundImage = Image.FromFile(textoparacuadraditos(34), True)
        Cuadraditos_54.BackgroundImage = Image.FromFile(textoparacuadraditos(35), True)
        Cuadraditos_53.BackgroundImage = Image.FromFile(textoparacuadraditos(36), True)
        Cuadraditos_52.BackgroundImage = Image.FromFile(textoparacuadraditos(37), True)
        Cuadraditos_51.BackgroundImage = Image.FromFile(textoparacuadraditos(38), True)
        Cuadraditos_61.BackgroundImage = Image.FromFile(textoparacuadraditos(39), True)
        Cuadraditos_62.BackgroundImage = Image.FromFile(textoparacuadraditos(40), True)
        Cuadraditos_63.BackgroundImage = Image.FromFile(textoparacuadraditos(41), True)
        Cuadraditos_64.BackgroundImage = Image.FromFile(textoparacuadraditos(42), True)
        Cuadraditos_65.BackgroundImage = Image.FromFile(textoparacuadraditos(43), True)
        Cuadraditos_66.BackgroundImage = Image.FromFile(textoparacuadraditos(44), True)
        Cuadraditos_76.BackgroundImage = Image.FromFile(textoparacuadraditos(45), True)
        Cuadraditos_75.BackgroundImage = Image.FromFile(textoparacuadraditos(46), True)
        Cuadraditos_74.BackgroundImage = Image.FromFile(textoparacuadraditos(47), True)
        Cuadraditos_73.BackgroundImage = Image.FromFile(textoparacuadraditos(48), True)
        Cuadraditos_72.BackgroundImage = Image.FromFile(textoparacuadraditos(49), True)
        Cuadraditos_71.BackgroundImage = Image.FromFile(textoparacuadraditos(50), True)
        Cuadraditos_81.BackgroundImage = Image.FromFile(textoparacuadraditos(51), True)
        Cuadraditos_82.BackgroundImage = Image.FromFile(textoparacuadraditos(52), True)
        Cuadraditos_83.BackgroundImage = Image.FromFile(textoparacuadraditos(53), True)
        Cuadraditos_84.BackgroundImage = Image.FromFile(textoparacuadraditos(54), True)
        Cuadraditos_85.BackgroundImage = Image.FromFile(textoparacuadraditos(55), True)
        Cuadraditos_86.BackgroundImage = Image.FromFile(textoparacuadraditos(56), True)

        textoparacuadraditos(0) = ""
    End Sub

    Dim mapa As New Dictionary(Of String, String)
    Public Sub llenarmapa()

        mapa("18") = 1
        mapa("17") = 2
        mapa("16") = 3
        mapa("15") = 4
        mapa("14") = 5
        mapa("13") = 6
        mapa("12") = 7
        mapa("11") = 8
        mapa("21") = 9
        mapa("22") = 10
        mapa("23") = 11
        mapa("24") = 12
        mapa("25") = 13
        mapa("26") = 14
        mapa("27") = 15
        mapa("28") = 16
        mapa("38") = 17
        mapa("37") = 18
        mapa("36") = 19
        mapa("35") = 20
        mapa("34") = 21
        mapa("33") = 22
        mapa("32") = 23
        mapa("31") = 24
        mapa("41") = 25
        mapa("42") = 26
        mapa("43") = 27
        mapa("44") = 28
        mapa("45") = 29
        mapa("46") = 30
        mapa("47") = 31
        mapa("48") = 32
        mapa("56") = 33
        mapa("55") = 34
        mapa("54") = 35
        mapa("53") = 36
        mapa("52") = 37
        mapa("51") = 38
        mapa("61") = 39
        mapa("62") = 40
        mapa("63") = 41
        mapa("64") = 42
        mapa("65") = 43
        mapa("66") = 44
        mapa("76") = 45
        mapa("75") = 46
        mapa("74") = 47
        mapa("73") = 48
        mapa("72") = 49
        mapa("71") = 50
        mapa("81") = 51
        mapa("82") = 52
        mapa("83") = 53
        mapa("84") = 54
        mapa("85") = 55
        mapa("86") = 56

    End Sub
    Public Sub actualizacioncuadraditos()

        textoparacuadraditos(cuadraditos(0)) = "C:\Program Files (x86)\Tesis\Tesis\Cuadraditos\" + (cubito).ToString + ".png"
        cuadraditos(cuadraditos(0)) = cubito

        preparacion_cuadraditos()
    End Sub

    Private Sub metodo_de_los_cuadrados(sender As Object, e As MouseEventArgs) Handles Cuadraditos_18.MouseClick, Cuadraditos_17.MouseClick, Cuadraditos_16.MouseClick, Cuadraditos_15.MouseClick, Cuadraditos_14.MouseClick, Cuadraditos_13.MouseClick, Cuadraditos_12.MouseClick, Cuadraditos_11.MouseClick, Cuadraditos_21.MouseClick, Cuadraditos_22.MouseClick, Cuadraditos_23.MouseClick, Cuadraditos_24.MouseClick, Cuadraditos_25.MouseClick, Cuadraditos_26.MouseClick, Cuadraditos_27.MouseClick, Cuadraditos_28.MouseClick, Cuadraditos_38.MouseClick, Cuadraditos_37.MouseClick, Cuadraditos_36.MouseClick, Cuadraditos_35.MouseClick, Cuadraditos_34.MouseClick, Cuadraditos_33.MouseClick, Cuadraditos_32.MouseClick, Cuadraditos_31.MouseClick, Cuadraditos_41.MouseClick, Cuadraditos_42.MouseClick, Cuadraditos_43.MouseClick, Cuadraditos_44.MouseClick, Cuadraditos_45.MouseClick, Cuadraditos_46.MouseClick, Cuadraditos_47.MouseClick, Cuadraditos_48.MouseClick, Cuadraditos_56.MouseClick, Cuadraditos_55.MouseClick, Cuadraditos_54.MouseClick, Cuadraditos_53.MouseClick, Cuadraditos_52.MouseClick, Cuadraditos_51.MouseClick, Cuadraditos_61.MouseClick, Cuadraditos_62.MouseClick, Cuadraditos_63.MouseClick, Cuadraditos_64.MouseClick, Cuadraditos_65.MouseClick, Cuadraditos_66.MouseClick, Cuadraditos_76.MouseClick, Cuadraditos_75.MouseClick, Cuadraditos_74.MouseClick, Cuadraditos_73.MouseClick, Cuadraditos_72.MouseClick, Cuadraditos_71.MouseClick, Cuadraditos_81.MouseClick, Cuadraditos_82.MouseClick, Cuadraditos_83.MouseClick, Cuadraditos_84.MouseClick, Cuadraditos_85.MouseClick, Cuadraditos_86.MouseClick
        Dim elemento As Panel = sender
        Dim nombre = elemento.Name.Split("_")(1)
        nombre = mapa(nombre)
        ' aislar el nombre del cuadradito del resto del texto
        ' elemento.BackgroundImage.Dispose()
        cuadraditos(0) = nombre
        cubito = cuadraditos(nombre)

        Dim cosa As New SeleccionCuadradito(cubito.ToString, nombre, sender)
        cosa.Show()

    End Sub

    Private Sub varIdIntensidadDolor_CheckedChanged(sender As Object, e As EventArgs) Handles varIdIntensidadDolor1.CheckedChanged, varIdIntensidadDolor2.CheckedChanged, varIdIntensidadDolor3.CheckedChanged
        If varIdIntensidadDolor1.Checked = True Then
            varIdIntensidadDolor = 1
        ElseIf varIdIntensidadDolor2.Checked = True Then
            varIdIntensidadDolor = 2
        ElseIf varIdIntensidadDolor3.Checked = True Then
            varIdIntensidadDolor = 3
        End If
    End Sub

    Private Sub varIdDuracionDolor_CheckedChanged(sender As Object, e As EventArgs) Handles varIdDuracionDolor1.CheckedChanged, varIdDuracionDolor2.CheckedChanged, varIdDuracionDolor3.CheckedChanged
        If varIdDuracionDolor1.Checked = True Then

            varIdDuracionDolor = 1

        ElseIf varIdDuracionDolor2.Checked = True Then

            varIdDuracionDolor = 2

        ElseIf varIdDuracionDolor3.Checked = True Then

            varIdDuracionDolor = 3

        End If
    End Sub

    Private Sub vvarIdEstimuloDolor_CheckedChanged(sender As Object, e As EventArgs) Handles varDolorEspontaneo.CheckedChanged
        Try
        Finally
            Dim strin As String
            strin = ""
            If varDolorEspontaneo.Checked Then
                strin += "1"
            Else
                strin += "0"
            End If
            If varDolorProvocado.Checked Then
                strin += "1"
            Else
                strin += "0"
            End If
            If varDolorAlFrio.Checked Then
                strin += "1"
            Else
                strin += "0"
            End If
            If varDolorAlFrio.Checked Then
                strin += "1"
            Else
                strin += "0"
            End If
            varIdEstimuloDolor = strin

        End Try
    End Sub

    '=============================================================
End Class