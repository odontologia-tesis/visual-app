﻿
Imports System.IO.File
Imports System.Drawing
Public Class SeleccionCuadradito
    'Dim cubito As New String
    Dim cubito, cubito2 As String
    Public varArriba, varIzquierda, varAbajo, varDerecha, varEspecial, varCentro As UShort

    '===============Metodos iniciales y actualizaciones basicas============
    Private Sub agregarOpciones()
        ListBoxAbajo.Items.Add("Blanco")
        ListBoxAbajo.Items.Add("Rojo")
        ListBoxAbajo.Items.Add("Azul")

        ListBoxArriba.Items.Add("Blanco")
        ListBoxArriba.Items.Add("Rojo")
        ListBoxArriba.Items.Add("Azul")

        ListBoxCentro.Items.Add("Blanco")
        ListBoxCentro.Items.Add("Rojo")
        ListBoxCentro.Items.Add("Azul")
        ListBoxCentro.Items.Add("Cruz Roja")
        ListBoxCentro.Items.Add("Corona")
        ListBoxCentro.Items.Add("Cruz Negra")

        ListBoxDerecha.Items.Add("Blanco")
        ListBoxDerecha.Items.Add("Rojo")
        ListBoxDerecha.Items.Add("Azul")

        ListBoxIzquierda.Items.Add("Blanco")
        ListBoxIzquierda.Items.Add("Rojo")
        ListBoxIzquierda.Items.Add("Azul")

        ListBoxEspecial.Items.Add("Nada")
        ListBoxEspecial.Items.Add("Arco Rojo a la Izaquierda")
        ListBoxEspecial.Items.Add("Arco Rojo a la Derecha")
        ListBoxEspecial.Items.Add("Arco Azul a la Izquierda")
        ListBoxEspecial.Items.Add("Arco Azul a la Derecha")


    End Sub
    Private cubito3 As String
    Public Property cuadradoimportado() As String
        Get
            Return cubito3
        End Get
        Set(ByVal value As String)
            cubito3 = value
        End Set
    End Property
    Dim mapa As New Dictionary(Of String, String)
    Public Sub llenarmapa()

        mapa("1") = 18
        mapa("2") = 17
        mapa("3") = 16
        mapa("4") = 15
        mapa("5") = 14
        mapa("6") = 13
        mapa("7") = 12
        mapa("8") = 11
        mapa("9") = 21
        mapa("10") = 22
        mapa("11") = 23
        mapa("12") = 24
        mapa("13") = 25
        mapa("14") = 26
        mapa("15") = 27
        mapa("16") = 28
        mapa("17") = 38
        mapa("18") = 37
        mapa("19") = 36
        mapa("20") = 35
        mapa("21") = 34
        mapa("22") = 33
        mapa("23") = 32
        mapa("24") = 31
        mapa("25") = 41
        mapa("26") = 42
        mapa("27") = 43
        mapa("28") = 44
        mapa("29") = 45
        mapa("30") = 46
        mapa("31") = 47
        mapa("32") = 48
        mapa("33") = 56
        mapa("34") = 55
        mapa("35") = 54
        mapa("36") = 53
        mapa("37") = 52
        mapa("38") = 51
        mapa("39") = 61
        mapa("40") = 62
        mapa("41") = 63
        mapa("42") = 64
        mapa("43") = 65
        mapa("44") = 66
        mapa("45") = 76
        mapa("46") = 75
        mapa("47") = 74
        mapa("48") = 73
        mapa("49") = 72
        mapa("50") = 71
        mapa("51") = 81
        mapa("52") = 82
        mapa("53") = 83
        mapa("54") = 84
        mapa("55") = 85
        mapa("56") = 86

    End Sub
    Dim piezaactual As Panel
    Public Sub New(cosita As String, numero As String, pieza As Panel)
        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        piezaactual = pieza
        ' llenarmapa()
        Me.lbl_elemento.Text = numero 'antes mapa(numero)
        Me.cubito = cosita

        agregarOpciones()
        cubito2 = Me.cubito
        ListBoxIzquierda.SelectedIndex = Integer.Parse(cubito.Chars(cubito.Length - 6))
        ListBoxArriba.SelectedIndex = Integer.Parse(cubito.Chars(cubito.Length - 5))
        ListBoxDerecha.SelectedIndex = Integer.Parse(cubito.Chars(cubito.Length - 4))
        ListBoxAbajo.SelectedIndex = Integer.Parse(cubito.Chars(cubito.Length - 3))
        ListBoxCentro.SelectedIndex = Integer.Parse(cubito.Chars(cubito.Length - 2))
        ListBoxEspecial.SelectedIndex = Integer.Parse(cubito.Chars(cubito.Length - 1))

        ImagenAntes.BackgroundImage = Image.FromFile(("C:\Program Files (x86)\Tesis\Tesis\Cuadraditos\" + cubito3 + ".png"), True)
        actualizacionCuadradito(Me.cubito)


        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
    End Sub
    Private Function actualizacionCuadradito(lacosa)
        actualizacioncubito(varIzquierda, varArriba, varDerecha, varAbajo, varCentro, varEspecial)
        lacosa = cubito3
        ImagenAhora.BackgroundImage = Image.FromFile(("C:\Program Files (x86)\Tesis\Tesis\Cuadraditos\" + cubito3 + ".png"), True)

    End Function
    Private Function actualizacioncubito(aIzquierda As String, aarriba As String, aderecha As String, aabajo As String, acentro As String, aespecial As String)
        cubito3 = (aIzquierda + aarriba + aderecha + aabajo + acentro + aespecial)
    End Function
    '=================fin metodos iniciales y actualizaciones basicas==============

    '================acciones de botones===========
    Private Sub BotonGuardar_Click(sender As Object, e As EventArgs) Handles BotonGuardar.Click
        piezaactual.BackgroundImage = ImagenAhora.BackgroundImage
        Historial.cubito = cubito3
        reseteocuadrados = True
        Historial.actualizacioncuadraditos()
        Close()
    End Sub

    Private Sub ImagenAntes_Paint(sender As Object, e As PaintEventArgs) Handles ImagenAntes.Paint

    End Sub


    '===================fin acciones de botones==============================
    '=================================Actualizaciones de listbox====================
    Private Sub ListBoxIzquierda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxIzquierda.SelectedIndexChanged
        varIzquierda = ListBoxIzquierda.SelectedIndex
        actualizacionCuadradito(cubito)
    End Sub
    Private Sub ListBoxArriba_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxArriba.SelectedIndexChanged
        varArriba = ListBoxArriba.SelectedIndex
        actualizacionCuadradito(cubito)
    End Sub
    Private Sub ListBoxDerecha_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxDerecha.SelectedIndexChanged
        varDerecha = ListBoxDerecha.SelectedIndex
        actualizacionCuadradito(cubito)
    End Sub
    Private Sub ListBoxAbajo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxAbajo.SelectedIndexChanged
        varAbajo = ListBoxAbajo.SelectedIndex
        actualizacionCuadradito(cubito)
    End Sub
    Private Sub ListBoxCentro_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxCentro.SelectedIndexChanged
        'verificaciondeindexado()
        If ListBoxCentro.SelectedItem = "Blanco" Or ListBoxCentro.SelectedItem = "Rojo" Or ListBoxCentro.SelectedItem = "Azul" Then
            varCentro = ListBoxCentro.SelectedIndex
            ListBoxEspecial.Enabled = False
            ListBoxArriba.Enabled = True
            ListBoxAbajo.Enabled = True
            ListBoxDerecha.Enabled = True
            ListBoxIzquierda.Enabled = True
            PanelEspecial.BackColor = red
            PanelIzquierda.BackColor = green
            PanelArriba.BackColor = green
            PanelDerecha.BackColor = green
            PanelAbajo.BackColor = green

            varEspecial = 0
            varArriba = ListBoxArriba.SelectedIndex
            varAbajo = ListBoxAbajo.SelectedIndex
            varDerecha = ListBoxDerecha.SelectedIndex
            varIzquierda = ListBoxIzquierda.SelectedIndex
        End If
        If ListBoxCentro.SelectedItem = "Cruz Roja" Or ListBoxCentro.SelectedItem = "Corona" Or ListBoxCentro.SelectedItem = "Cruz Negra" Then
            varCentro = ListBoxCentro.SelectedIndex
            ListBoxEspecial.Enabled = True
            ListBoxArriba.Enabled = False
            ListBoxAbajo.Enabled = False
            ListBoxDerecha.Enabled = False
            ListBoxIzquierda.Enabled = False
            PanelEspecial.BackColor = green
            PanelIzquierda.BackColor = red
            PanelArriba.BackColor = red
            PanelDerecha.BackColor = red
            PanelAbajo.BackColor = red

            varAbajo = 0
            varArriba = 0
            varIzquierda = 0
            varDerecha = 0
            ' varEspecial = ListBoxEspecial.SelectedIndex
        End If
        actualizacionCuadradito(cubito)
    End Sub
    Private Sub ListBoxEspecial_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxEspecial.SelectedIndexChanged
        varEspecial = ListBoxEspecial.SelectedIndex
        actualizacionCuadradito(cubito)
    End Sub
    '=========================Fin de actualizaciones de listbox=============================

End Class